<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	'bootstrap_slogan' => 'BootStrap 4 pour SPIP',
	'bootstrap_description' => 'Adaptateur BootStrap 4 pour SPIP.
Inclue hashgrid.',
);

?>