<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/paquet-comments?lang_cible=it
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// C
	'comments_description' => 'ATTENZIONE, VERSIONE IN SVILUPPO PER SPIP 3!!!<br /> Mostrare messaggi in lista, in modo da commentare i blog, con una forma semplificata. Commenti microformattati, nomenclatura omogenea.',
	'comments_slogan' => 'I commenti, semplicemente'
);
