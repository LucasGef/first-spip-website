<?php

if (defined('_ECRIRE_INC_VERSION')) {
define('_PLUGINS_HASH','a5baf784143adf0c9e8f24257f574b44');
include_once_check(_ROOT_PLUGINS_DIST.'aide/aide_options.php');
include_once_check(_ROOT_PLUGINS_DIST.'petitions/petitions_options.php');
include_once_check(_ROOT_PLUGINS_DIST.'squelettes_par_rubrique/squelettes_par_rubrique_options.php');
include_once_check(_ROOT_PLUGINS.'auto/spip_bonux/v3.5.4/spip_bonux_options.php');
include_once_check(_ROOT_PLUGINS.'auto/saisies/v3.41.0/saisies_options.php');
include_once_check(_ROOT_PLUGINS.'auto/skeleditor/v3.1.0/skeleditor_options.php');
include_once_check(_ROOT_PLUGINS.'auto/comments/v3.5.0/comments_options.php');
include_once_check(_ROOT_PLUGINS.'auto/zcore/v2.8.7/zcore_options.php');
include_once_check(_ROOT_PLUGINS_DIST.'compresseur/compresseur_options.php');
include_once_check(_ROOT_PLUGINS.'auto/scssphp/v2.4.4/scssphp_options.php');
include_once_check(_ROOT_PLUGINS.'auto/bootstrap4/v4.4.1.7/bootstrap4_options.php');
include_once_check(_ROOT_PLUGINS.'auto/spipr_dist/v2.2.6/spipr_dist_options.php');
}
?>