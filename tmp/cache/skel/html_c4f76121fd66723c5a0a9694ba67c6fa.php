<?php

/*
 * Squelette : plugins/auto/z/v1.7.31/inclure/article-resume.html
 * Date :      Tue, 07 Nov 2017 17:27:36 GMT
 * Compile :   Tue, 16 Jun 2020 14:20:21 GMT
 * Boucles :   _nb_commentaires, _articles
 */ 

function BOUCLE_nb_commentaireshtml_c4f76121fd66723c5a0a9694ba67c6fa(&$Cache, &$Pile, &$doublons, &$Numrows, $SP) {

	static $command = array();
	static $connect;
	$command['connect'] = $connect = '';
	if (!isset($command['table'])) {
		$command['table'] = 'forum';
		$command['id'] = '_nb_commentaires';
		$command['from'] = array('forum' => 'spip_forum');
		$command['type'] = array();
		$command['groupby'] = array();
		$command['select'] = array("count(*)");
		$command['orderby'] = array();
		$command['join'] = array();
		$command['limit'] = '';
		$command['having'] = 
			array();
	}
	$command['where'] = 
			array(
quete_condition_statut('forum.statut','publie,prop','publie',''), 
			array('=', 'forum.id_objet', sql_quote($Pile[$SP]['id_article'], '','bigint(21) NOT NULL DEFAULT \'0\'')), 
			array('=', 'forum.objet', sql_quote('article')));
	if (defined("_BOUCLE_PROFILER")) $timer = time()+(float)microtime();
	$t0 = "";
	// REQUETE
	$iter = IterFactory::create(
		"SQL",
		$command,
		array('plugins/auto/z/v1.7.31/inclure/article-resume.html','html_c4f76121fd66723c5a0a9694ba67c6fa','_nb_commentaires',15,$GLOBALS['spip_lang'])
	);
	if (!$iter->err()) {
	$Numrows['_nb_commentaires']['total'] = @intval($iter->count());
	$SP++;
	// RESULTATS
	
	$iter->free();
	}
	if (defined("_BOUCLE_PROFILER")
	AND 1000*($timer = (time()+(float)microtime())-$timer) > _BOUCLE_PROFILER)
		spip_log(intval(1000*$timer)."ms BOUCLE_nb_commentaires @ plugins/auto/z/v1.7.31/inclure/article-resume.html","profiler"._LOG_AVERTISSEMENT);
	return $t0;
}


function BOUCLE_articleshtml_c4f76121fd66723c5a0a9694ba67c6fa(&$Cache, &$Pile, &$doublons, &$Numrows, $SP) {

	static $command = array();
	static $connect;
	$command['connect'] = $connect = '';
	$in = array();
	if (!(is_array($a = (@$Pile[0]['statut']))))
		$in[]= $a;
	else $in = array_merge($in, $a);
	if (!isset($command['table'])) {
		$command['table'] = 'articles';
		$command['id'] = '_articles';
		$command['from'] = array('articles' => 'spip_articles');
		$command['type'] = array();
		$command['groupby'] = array();
		$command['select'] = array("articles.id_article",
		"articles.id_rubrique",
		"articles.titre",
		"articles.date",
		"articles.texte",
		"articles.descriptif",
		"articles.chapo",
		"articles.lang");
		$command['orderby'] = array();
		$command['join'] = array();
		$command['limit'] = '';
		$command['having'] = 
			array();
	}
	$command['where'] = 
			array(
			array('=', 'articles.id_article', sql_quote(@$Pile[0]['id_article'], '','bigint(21) NOT NULL AUTO_INCREMENT')), (!(is_array(@$Pile[0]['statut'])?count(@$Pile[0]['statut']):strlen(@$Pile[0]['statut'])) ? '' : ((is_array(@$Pile[0]['statut'])) ? sql_in('articles.statut',sql_quote($in)) : 
			array('=', 'articles.statut', sql_quote(@$Pile[0]['statut'], '','varchar(10) NOT NULL DEFAULT \'0\'')))));
	if (defined("_BOUCLE_PROFILER")) $timer = time()+(float)microtime();
	$t0 = "";
	// REQUETE
	$iter = IterFactory::create(
		"SQL",
		$command,
		array('plugins/auto/z/v1.7.31/inclure/article-resume.html','html_c4f76121fd66723c5a0a9694ba67c6fa','_articles',8,$GLOBALS['spip_lang'])
	);
	if (!$iter->err()) {
	lang_select($GLOBALS['spip_lang']);
	$SP++;
	// RESULTATS
	while ($Pile[$SP]=$iter->fetch()) {

		lang_select_public($Pile[$SP]['lang'], '', $Pile[$SP]['titre']);
		$t0 .= (
'
<li class="item hentry">
	<h3 class="h3 entry-title"><a href="' .
vider_url(urlencode_1738(generer_url_entite($Pile[$SP]['id_article'], 'article', '', '', true))) .
'" rel="bookmark">' .
filtrer('image_graver',filtrer('image_reduire',
((!is_array($l = quete_logo('id_article', 'ON', $Pile[$SP]['id_article'],$Pile[$SP]['id_rubrique'], 0))) ? '':
 ("<img class=\"spip_logo spip_logos\" alt=\"\" src=\"$l[0]\"" . $l[2] .  ($l[1] ? " onmouseover=\"this.src='$l[1]'\" onmouseout=\"this.src='$l[0]'\"" : "") . ' />')),'150','100')) .
interdire_scripts(supprimer_numero(typo($Pile[$SP]['titre']), "TYPO", $connect, $Pile[0])) .
'</a></h3>
	<div class="info-publi">' .
(($t1 = strval(interdire_scripts(affdate_jourcourt(normaliser_date($Pile[$SP]['date'])))))!=='' ?
		((	'<abbr class="published" title="' .
	interdire_scripts(date_iso(normaliser_date($Pile[$SP]['date']))) .
	'">') . $t1 . '</abbr>') :
		'') .
(($t1 = strval(recuperer_fond('modeles/lesauteurs', array('objet'=>'article','id_objet' => $Pile[$SP]['id_article'],'id_article' => $Pile[$SP]['id_article']), array('trim'=>true, 'compil'=>array('plugins/auto/z/v1.7.31/inclure/article-resume.html','html_c4f76121fd66723c5a0a9694ba67c6fa','_articles',8,$GLOBALS['spip_lang'])), '')))!=='' ?
		((	'<span class="sep">, </span><span class="auteurs">' .
	_T('public|spip|ecrire:par_auteur') .
	' ') . $t1 . '</span>') :
		'') .
'</div>
	' .
(($t1 = strval(interdire_scripts(filtre_introduction($Pile[$SP]['descriptif'], (strlen($Pile[$SP]['descriptif']))
		? ''
		: $Pile[$SP]['chapo'] . "\n\n" . $Pile[$SP]['texte'], 500, $connect, null))))!=='' ?
		((	'<div class="introduction entry-content">') . $t1 . '</div>') :
		'') .
'
	<div class="meta-publi">
	<a class="lire-la-suite" href="' .
vider_url(urlencode_1738(generer_url_entite($Pile[$SP]['id_article'], 'article', '', '', true))) .
'">' .
_T('zpip:lire_la_suite') .
'<span class="lire-la-suite-titre">' .
_T('zpip:lire_la_suite_de') .
' <em>' .
interdire_scripts(supprimer_numero(typo($Pile[$SP]['titre']), "TYPO", $connect, $Pile[0])) .
'</em></span></a>
	' .
BOUCLE_nb_commentaireshtml_c4f76121fd66723c5a0a9694ba67c6fa($Cache, $Pile, $doublons, $Numrows, $SP)
. (	'	' .
	(($Numrows['_nb_commentaires']['total'])  ?
			(' ' . (	'
		<span class="sep">|</span>
		<a' .
		(($t3 = strval(ancre_url(vider_url(urlencode_1738(generer_url_entite($Pile[$SP]['id_article'], 'article', '', '', true))),'forum')))!=='' ?
				(' href="' . $t3 . '"') :
				'') .
		' class="nb_commentaires">' .
		$Numrows['_nb_commentaires']['total'] .
		'&nbsp;' .
		(($Numrows['_nb_commentaires']['total'] == '1') ? _T('zpip:commentaire'):_T('zpip:commentaires')) .
		'</a>
		')) :
			'') .
	'
	') .
'
	</div>
</li>
');
		lang_select();
	}
	lang_select();
	$iter->free();
	}
	if (defined("_BOUCLE_PROFILER")
	AND 1000*($timer = (time()+(float)microtime())-$timer) > _BOUCLE_PROFILER)
		spip_log(intval(1000*$timer)."ms BOUCLE_articles @ plugins/auto/z/v1.7.31/inclure/article-resume.html","profiler"._LOG_AVERTISSEMENT);
	return $t0;
}

//
// Fonction principale du squelette plugins/auto/z/v1.7.31/inclure/article-resume.html
// Temps de compilation total: 3.200 ms
//

function html_c4f76121fd66723c5a0a9694ba67c6fa($Cache, $Pile, $doublons = array(), $Numrows = array(), $SP = 0) {

	if (isset($Pile[0]["doublons"]) AND is_array($Pile[0]["doublons"]))
		$doublons = nettoyer_env_doublons($Pile[0]["doublons"]);

	$connect = '';
	$page = (
'
' .
BOUCLE_articleshtml_c4f76121fd66723c5a0a9694ba67c6fa($Cache, $Pile, $doublons, $Numrows, $SP));

	return analyse_resultat_skel('html_c4f76121fd66723c5a0a9694ba67c6fa', $Cache, $page, 'plugins/auto/z/v1.7.31/inclure/article-resume.html');
}
?>