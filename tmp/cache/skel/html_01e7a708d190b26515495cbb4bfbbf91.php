<?php

/*
 * Squelette : plugins/auto/spipr_dist/v2.2.6/content/rubrique.html
 * Date :      Mon, 30 Mar 2020 09:25:24 GMT
 * Compile :   Wed, 17 Jun 2020 09:07:11 GMT
 * Boucles :   _m2, _miniplan, _sous_rubriques, _articles, _breves, _syndic, _sites, _content
 */ 

function BOUCLE_m2html_01e7a708d190b26515495cbb4bfbbf91(&$Cache, &$Pile, &$doublons, &$Numrows, $SP) {

	static $command = array();
	static $connect;
	$command['connect'] = $connect = '';
	$save_numrows = (isset($Numrows['_miniplan']) ? $Numrows['_miniplan'] : array());
	$t0 = (($t1 = BOUCLE_miniplanhtml_01e7a708d190b26515495cbb4bfbbf91($Cache, $Pile, $doublons, $Numrows, $SP))!=='' ?
		('
					<ul class="menu-items menu-liste">
						' . $t1 . '
					</ul>
					') :
		'');
	$Numrows['_miniplan'] = ($save_numrows);
	return $t0;
}


function BOUCLE_miniplanhtml_01e7a708d190b26515495cbb4bfbbf91(&$Cache, &$Pile, &$doublons, &$Numrows, $SP) {

	static $command = array();
	static $connect;
	$command['connect'] = $connect = '';
	if (!isset($command['table'])) {
		$command['table'] = 'rubriques';
		$command['id'] = '_miniplan';
		$command['from'] = array('rubriques' => 'spip_rubriques');
		$command['type'] = array();
		$command['groupby'] = array();
		$command['select'] = array("rubriques.id_rubrique",
		"0+rubriques.titre AS num",
		"rubriques.titre",
		"rubriques.lang");
		$command['orderby'] = array('num', 'rubriques.titre');
		$command['join'] = array();
		$command['limit'] = '';
		$command['having'] = 
			array();
	}
	$command['where'] = 
			array(
quete_condition_statut('rubriques.statut','!','publie',''), 
			array('=', 'rubriques.id_parent', sql_quote($Pile[$SP]['id_rubrique'], '','bigint(21) NOT NULL DEFAULT \'0\'')));
	if (defined("_BOUCLE_PROFILER")) $timer = time()+(float)microtime();
	$t0 = "";
	// REQUETE
	$iter = IterFactory::create(
		"SQL",
		$command,
		array('plugins/auto/spipr_dist/v2.2.6/content/rubrique.html','html_01e7a708d190b26515495cbb4bfbbf91','_miniplan',24,$GLOBALS['spip_lang'])
	);
	if (!$iter->err()) {
	lang_select($GLOBALS['spip_lang']);
	$SP++;
	// RESULTATS
	while ($Pile[$SP]=$iter->fetch()) {

		lang_select_public($Pile[$SP]['lang'], '', $Pile[$SP]['titre']);
		$t0 .= (
'
						<li class="item menu-entree">
							<a href="' .
vider_url(urlencode_1738(generer_url_entite($Pile[$SP]['id_rubrique'], 'rubrique', '', '', true))) .
'">' .
interdire_scripts(supprimer_numero(typo($Pile[$SP]['titre']), "TYPO", $connect, $Pile[0])) .
'</a>
							' .
BOUCLE_m2html_01e7a708d190b26515495cbb4bfbbf91($Cache, $Pile, $doublons, $Numrows, $SP) .
'
						</li>
						');
		lang_select();
	}
	lang_select();
	$iter->free();
	}
	if (defined("_BOUCLE_PROFILER")
	AND 1000*($timer = (time()+(float)microtime())-$timer) > _BOUCLE_PROFILER)
		spip_log(intval(1000*$timer)."ms BOUCLE_miniplan @ plugins/auto/spipr_dist/v2.2.6/content/rubrique.html","profiler"._LOG_AVERTISSEMENT);
	return $t0;
}


function BOUCLE_sous_rubriqueshtml_01e7a708d190b26515495cbb4bfbbf91(&$Cache, &$Pile, &$doublons, &$Numrows, $SP) {

	static $command = array();
	static $connect;
	$command['connect'] = $connect = '';
	if (!isset($command['table'])) {
		$command['table'] = 'rubriques';
		$command['id'] = '_sous_rubriques';
		$command['from'] = array('rubriques' => 'spip_rubriques');
		$command['type'] = array();
		$command['groupby'] = array();
		$command['select'] = array("rubriques.id_rubrique",
		"0+rubriques.titre AS num",
		"rubriques.titre",
		"rubriques.lang");
		$command['orderby'] = array('num', 'rubriques.titre');
		$command['join'] = array();
		$command['limit'] = '';
		$command['having'] = 
			array();
	}
	$command['where'] = 
			array(
quete_condition_statut('rubriques.statut','!','publie',''), 
			array('=', 'rubriques.id_parent', sql_quote($Pile[$SP]['id_rubrique'], '','bigint(21) NOT NULL DEFAULT \'0\'')));
	if (defined("_BOUCLE_PROFILER")) $timer = time()+(float)microtime();
	$t0 = "";
	// REQUETE
	$iter = IterFactory::create(
		"SQL",
		$command,
		array('plugins/auto/spipr_dist/v2.2.6/content/rubrique.html','html_01e7a708d190b26515495cbb4bfbbf91','_sous_rubriques',15,$GLOBALS['spip_lang'])
	);
	if (!$iter->err()) {
	lang_select($GLOBALS['spip_lang']);
	$SP++;
	// RESULTATS
	while ($Pile[$SP]=$iter->fetch()) {

		lang_select_public($Pile[$SP]['lang'], '', $Pile[$SP]['titre']);
		$t0 .= (
'
				<li class="item menu-entree">
					<a href="' .
vider_url(urlencode_1738(generer_url_entite($Pile[$SP]['id_rubrique'], 'rubrique', '', '', true))) .
'">' .
interdire_scripts(supprimer_numero(typo($Pile[$SP]['titre']), "TYPO", $connect, $Pile[0])) .
'</a>

					
					' .
(($t1 = BOUCLE_miniplanhtml_01e7a708d190b26515495cbb4bfbbf91($Cache, $Pile, $doublons, $Numrows, $SP))!=='' ?
		('
					<ul class="menu-items menu-liste">
						' . $t1 . '
					</ul>
					') :
		'') .
'

				</li>
				');
		lang_select();
	}
	lang_select();
	$iter->free();
	}
	if (defined("_BOUCLE_PROFILER")
	AND 1000*($timer = (time()+(float)microtime())-$timer) > _BOUCLE_PROFILER)
		spip_log(intval(1000*$timer)."ms BOUCLE_sous_rubriques @ plugins/auto/spipr_dist/v2.2.6/content/rubrique.html","profiler"._LOG_AVERTISSEMENT);
	return $t0;
}


function BOUCLE_articleshtml_01e7a708d190b26515495cbb4bfbbf91(&$Cache, &$Pile, &$doublons, &$Numrows, $SP) {

	static $command = array();
	static $connect;
	$command['connect'] = $connect = '';
	if (!isset($command['table'])) {
		$command['table'] = 'articles';
		$command['id'] = '_articles';
		$command['from'] = array('articles' => 'spip_articles');
		$command['type'] = array();
		$command['groupby'] = array();
		$command['select'] = array("articles.lang",
		"articles.titre");
		$command['orderby'] = array();
		$command['join'] = array();
		$command['limit'] = '0,1';
		$command['having'] = 
			array();
	}
	$command['where'] = 
			array(
quete_condition_statut('articles.statut','publie,prop,prepa/auteur','publie',''), 
quete_condition_postdates('articles.date',''), 
			array('=', 'articles.id_rubrique', sql_quote($Pile[$SP]['id_rubrique'], '','bigint(21) NOT NULL DEFAULT \'0\'')));
	if (defined("_BOUCLE_PROFILER")) $timer = time()+(float)microtime();
	$t0 = "";
	// REQUETE
	$iter = IterFactory::create(
		"SQL",
		$command,
		array('plugins/auto/spipr_dist/v2.2.6/content/rubrique.html','html_01e7a708d190b26515495cbb4bfbbf91','_articles',8,$GLOBALS['spip_lang'])
	);
	if (!$iter->err()) {
	lang_select($GLOBALS['spip_lang']);
	$SP++;
	// RESULTATS
	while ($Pile[$SP]=$iter->fetch()) {

		lang_select_public($Pile[$SP]['lang'], '', $Pile[$SP]['titre']);
		$t0 .= (
'
			<div class="liste articles">
			' .

'<'.'?php echo recuperer_fond( ' . argumenter_squelette('liste/articles-resume') . ', array_merge('.var_export($Pile[0],1).',array(\'titre\' => ' . argumenter_squelette(_T('public|spip|ecrire:articles_rubrique')) . ',
	\'lang\' => ' . argumenter_squelette($GLOBALS["spip_lang"]) . ')), array("compil"=>array(\'plugins/auto/spipr_dist/v2.2.6/content/rubrique.html\',\'html_01e7a708d190b26515495cbb4bfbbf91\',\'\',10,$GLOBALS[\'spip_lang\']),\'ajax\' => ($v=( ' . argumenter_squelette(@$Pile[0]['ajax']) . '))?$v:true), _request("connect"));
?'.'>
			</div>
		');
		lang_select();
	}
	lang_select();
	$iter->free();
	}
	if (defined("_BOUCLE_PROFILER")
	AND 1000*($timer = (time()+(float)microtime())-$timer) > _BOUCLE_PROFILER)
		spip_log(intval(1000*$timer)."ms BOUCLE_articles @ plugins/auto/spipr_dist/v2.2.6/content/rubrique.html","profiler"._LOG_AVERTISSEMENT);
	return $t0;
}


function BOUCLE_breveshtml_01e7a708d190b26515495cbb4bfbbf91(&$Cache, &$Pile, &$doublons, &$Numrows, $SP) {

	static $command = array();
	static $connect;
	$command['connect'] = $connect = '';
	$command['pagination'] = array((isset($Pile[0]['debut_breves']) ? $Pile[0]['debut_breves'] : null), 5);
	if (!isset($command['table'])) {
		$command['table'] = 'breves';
		$command['id'] = '_breves';
		$command['from'] = array('breves' => 'spip_breves');
		$command['type'] = array();
		$command['groupby'] = array();
		$command['select'] = array("breves.date_heure",
		"breves.id_breve",
		"breves.lang",
		"breves.titre");
		$command['orderby'] = array('breves.date_heure DESC');
		$command['join'] = array();
		$command['limit'] = '';
		$command['having'] = 
			array();
	}
	$command['where'] = 
			array(
quete_condition_statut('breves.statut','publie,prop','publie',''), 
			array('=', 'breves.id_rubrique', sql_quote($Pile[$SP]['id_rubrique'], '','bigint(21) NOT NULL DEFAULT \'0\'')));
	if (defined("_BOUCLE_PROFILER")) $timer = time()+(float)microtime();
	$t0 = "";
	// REQUETE
	$iter = IterFactory::create(
		"SQL",
		$command,
		array('plugins/auto/spipr_dist/v2.2.6/content/rubrique.html','html_01e7a708d190b26515495cbb4bfbbf91','_breves',48,$GLOBALS['spip_lang'])
	);
	if (!$iter->err()) {
	
	// COMPTEUR
	$Numrows['_breves']['compteur_boucle'] = 0;
	$Numrows['_breves']['total'] = @intval($iter->count());
	$debut_boucle = isset($Pile[0]['debut_breves']) ? $Pile[0]['debut_breves'] : _request('debut_breves');
	if(substr($debut_boucle,0,1)=='@'){
		$debut_boucle = $Pile[0]['debut_breves'] = quete_debut_pagination('id_breve',$Pile[0]['@id_breve'] = substr($debut_boucle,1),5,$iter);
		$iter->seek(0);
	}
	$debut_boucle = intval($debut_boucle);
	$debut_boucle = (($tout=($debut_boucle == -1))?0:($debut_boucle));
	$debut_boucle = max(0,min($debut_boucle,floor(($Numrows['_breves']['total']-1)/(5))*(5)));
	$debut_boucle = intval($debut_boucle);
	$fin_boucle = min(($tout ? $Numrows['_breves']['total'] : $debut_boucle + 4), $Numrows['_breves']['total'] - 1);
	$Numrows['_breves']['grand_total'] = $Numrows['_breves']['total'];
	$Numrows['_breves']["total"] = max(0,$fin_boucle - $debut_boucle + 1);
	if ($debut_boucle>0 AND $debut_boucle < $Numrows['_breves']['grand_total'] AND $iter->seek($debut_boucle,'continue'))
		$Numrows['_breves']['compteur_boucle'] = $debut_boucle;
	
	lang_select($GLOBALS['spip_lang']);
	$SP++;
	// RESULTATS
	while ($Pile[$SP]=$iter->fetch()) {

		$Numrows['_breves']['compteur_boucle']++;
		if ($Numrows['_breves']['compteur_boucle'] <= $debut_boucle) continue;
		if ($Numrows['_breves']['compteur_boucle']-1 > $fin_boucle) break;
		lang_select_public($Pile[$SP]['lang'], '', $Pile[$SP]['titre']);
		$t0 .= (
'
							<li class="item short">' .
recuperer_fond( 'inclure/resume/breve' , array('id_breve' => $Pile[$SP]['id_breve'] ), array('compil'=>array('plugins/auto/spipr_dist/v2.2.6/content/rubrique.html','html_01e7a708d190b26515495cbb4bfbbf91','_breves',54,$GLOBALS['spip_lang'])), _request('connect')) .
'</li>
							');
		lang_select();
	}
	lang_select();
	$iter->free();
	}
	if (defined("_BOUCLE_PROFILER")
	AND 1000*($timer = (time()+(float)microtime())-$timer) > _BOUCLE_PROFILER)
		spip_log(intval(1000*$timer)."ms BOUCLE_breves @ plugins/auto/spipr_dist/v2.2.6/content/rubrique.html","profiler"._LOG_AVERTISSEMENT);
	return $t0;
}


function BOUCLE_syndichtml_01e7a708d190b26515495cbb4bfbbf91(&$Cache, &$Pile, &$doublons, &$Numrows, $SP) {

	static $command = array();
	static $connect;
	$command['connect'] = $connect = '';
	if (!isset($command['table'])) {
		$command['table'] = 'syndic_articles';
		$command['id'] = '_syndic';
		$command['from'] = array('syndic_articles' => 'spip_syndic_articles','L1' => 'spip_syndic');
		$command['type'] = array();
		$command['groupby'] = array();
		$command['select'] = array("syndic_articles.date",
		"syndic_articles.id_syndic_article");
		$command['orderby'] = array('syndic_articles.date DESC');
		$command['join'] = array('L1' => array('syndic_articles','id_syndic'));
		$command['limit'] = '0,3';
		$command['having'] = 
			array();
	}
	$command['where'] = 
			array(
quete_condition_statut('L1.statut','publie,prop','publie',''), 
quete_condition_statut('syndic_articles.statut','publie,prop','publie',''), 
			array('=', 'syndic_articles.id_syndic', sql_quote($Pile[$SP]['id_syndic'], '','bigint(21) NOT NULL DEFAULT \'0\'')), 
			array('<', 'TIMESTAMPDIFF(HOUR,syndic_articles.date,NOW())/24', "180"));
	if (defined("_BOUCLE_PROFILER")) $timer = time()+(float)microtime();
	$t0 = "";
	// REQUETE
	$iter = IterFactory::create(
		"SQL",
		$command,
		array('plugins/auto/spipr_dist/v2.2.6/content/rubrique.html','html_01e7a708d190b26515495cbb4bfbbf91','_syndic',68,$GLOBALS['spip_lang'])
	);
	if (!$iter->err()) {
	$SP++;
	// RESULTATS
	while ($Pile[$SP]=$iter->fetch()) {

		$t0 .= (
'
											<li class="short with-publication-inline">' .
recuperer_fond( 'inclure/resume/syndic_article' , array('id_syndic_article' => $Pile[$SP]['id_syndic_article'] ), array('compil'=>array('plugins/auto/spipr_dist/v2.2.6/content/rubrique.html','html_01e7a708d190b26515495cbb4bfbbf91','_syndic',71,$GLOBALS['spip_lang'])), _request('connect')) .
'</li>
											');
	}
	$iter->free();
	}
	if (defined("_BOUCLE_PROFILER")
	AND 1000*($timer = (time()+(float)microtime())-$timer) > _BOUCLE_PROFILER)
		spip_log(intval(1000*$timer)."ms BOUCLE_syndic @ plugins/auto/spipr_dist/v2.2.6/content/rubrique.html","profiler"._LOG_AVERTISSEMENT);
	return $t0;
}


function BOUCLE_siteshtml_01e7a708d190b26515495cbb4bfbbf91(&$Cache, &$Pile, &$doublons, &$Numrows, $SP) {

	static $command = array();
	static $connect;
	$command['connect'] = $connect = '';
	if (!isset($command['table'])) {
		$command['table'] = 'syndic';
		$command['id'] = '_sites';
		$command['from'] = array('syndic' => 'spip_syndic');
		$command['type'] = array();
		$command['groupby'] = array();
		$command['select'] = array("syndic.id_syndic",
		"syndic.date",
		"syndic.nom_site");
		$command['orderby'] = array('syndic.nom_site');
		$command['join'] = array();
		$command['limit'] = '';
		$command['having'] = 
			array();
	}
	$command['where'] = 
			array(
quete_condition_statut('syndic.statut','publie,prop','publie',''), 
			array('=', 'syndic.id_rubrique', sql_quote($Pile[$SP]['id_rubrique'], '','bigint(21) NOT NULL DEFAULT \'0\'')));
	if (defined("_BOUCLE_PROFILER")) $timer = time()+(float)microtime();
	$t0 = "";
	// REQUETE
	$iter = IterFactory::create(
		"SQL",
		$command,
		array('plugins/auto/spipr_dist/v2.2.6/content/rubrique.html','html_01e7a708d190b26515495cbb4bfbbf91','_sites',62,$GLOBALS['spip_lang'])
	);
	if (!$iter->err()) {
	$SP++;
	// RESULTATS
	while ($Pile[$SP]=$iter->fetch()) {

		$t0 .= (
'
							<li class="item short">' .
recuperer_fond( 'inclure/resume/site' , array('id_syndic' => $Pile[$SP]['id_syndic'] ), array('compil'=>array('plugins/auto/spipr_dist/v2.2.6/content/rubrique.html','html_01e7a708d190b26515495cbb4bfbbf91','_sites',67,$GLOBALS['spip_lang'])), _request('connect')) .
(($t1 = BOUCLE_syndichtml_01e7a708d190b26515495cbb4bfbbf91($Cache, $Pile, $doublons, $Numrows, $SP))!=='' ?
		('
									<ul class="spip">
											' . $t1 . '
									</ul>
									') :
		'') .
'
							</li>
							');
	}
	$iter->free();
	}
	if (defined("_BOUCLE_PROFILER")
	AND 1000*($timer = (time()+(float)microtime())-$timer) > _BOUCLE_PROFILER)
		spip_log(intval(1000*$timer)."ms BOUCLE_sites @ plugins/auto/spipr_dist/v2.2.6/content/rubrique.html","profiler"._LOG_AVERTISSEMENT);
	return $t0;
}


function BOUCLE_contenthtml_01e7a708d190b26515495cbb4bfbbf91(&$Cache, &$Pile, &$doublons, &$Numrows, $SP) {

	static $command = array();
	static $connect;
	$command['connect'] = $connect = '';
	if (!isset($command['table'])) {
		$command['table'] = 'rubriques';
		$command['id'] = '_content';
		$command['from'] = array('rubriques' => 'spip_rubriques');
		$command['type'] = array();
		$command['groupby'] = array();
		$command['select'] = array("rubriques.id_rubrique",
		"rubriques.texte",
		"rubriques.lang",
		"rubriques.titre");
		$command['orderby'] = array();
		$command['join'] = array();
		$command['limit'] = '';
		$command['having'] = 
			array();
	}
	$command['where'] = 
			array(
quete_condition_statut('rubriques.statut','!','publie',''), 
			array('=', 'rubriques.id_rubrique', sql_quote(@$Pile[0]['id_rubrique'], '','bigint(21) NOT NULL AUTO_INCREMENT')));
	if (defined("_BOUCLE_PROFILER")) $timer = time()+(float)microtime();
	$t0 = "";
	// REQUETE
	$iter = IterFactory::create(
		"SQL",
		$command,
		array('plugins/auto/spipr_dist/v2.2.6/content/rubrique.html','html_01e7a708d190b26515495cbb4bfbbf91','_content',1,$GLOBALS['spip_lang'])
	);
	if (!$iter->err()) {
	lang_select($GLOBALS['spip_lang']);
	$SP++;
	// RESULTATS
	while ($Pile[$SP]=$iter->fetch()) {

		lang_select_public($Pile[$SP]['lang'], '', $Pile[$SP]['titre']);
		$t0 .= (
'
<section>
	' .
(($t1 = strval(interdire_scripts(((entites_html(sinon(table_valeur(@$Pile[0], (string)'cartouche', null), '1'),true)) ?' ' :''))))!=='' ?
		($t1 . 
'<'.'?php echo recuperer_fond( ' . argumenter_squelette((	'cartouche/' .
		interdire_scripts(entites_html(table_valeur(@$Pile[0], (string)'type-page', null),true)))) . ', array_merge('.var_export($Pile[0],1).',array(\'lang\' => ' . argumenter_squelette($GLOBALS["spip_lang"]) . ')), array("compil"=>array(\'plugins/auto/spipr_dist/v2.2.6/content/rubrique.html\',\'html_01e7a708d190b26515495cbb4bfbbf91\',\'\',3,$GLOBALS[\'spip_lang\'])), _request("connect"));
?'.'>') :
		'') .
'

	<div class="main">
		' .
(($t1 = strval(interdire_scripts(adaptive_images(propre($Pile[$SP]['texte'], $connect, $Pile[0])))))!=='' ?
		((	'<div class="chapo">') . $t1 . '</div>') :
		'') .
'

		' .
(($t1 = BOUCLE_articleshtml_01e7a708d190b26515495cbb4bfbbf91($Cache, $Pile, $doublons, $Numrows, $SP))!=='' ?
		$t1 :
		((	'

		
		' .
	(($t2 = BOUCLE_sous_rubriqueshtml_01e7a708d190b26515495cbb4bfbbf91($Cache, $Pile, $doublons, $Numrows, $SP))!=='' ?
			((	'
		<div class="menu rubriques">
			<h2 class="h2">' .
			_T('public|spip|ecrire:sous_rubriques') .
			'</h2>
			<ul class="menu-items menu-liste">
				') . $t2 . '
			</ul>
			</div>
		') :
			'') .
	'
	'))) .
'
	</div>

	<aside>
		
		' .
recuperer_fond( 'inclure/documents' , array('id_rubrique' => $Pile[$SP]['id_rubrique'] ), array('compil'=>array('plugins/auto/spipr_dist/v2.2.6/content/rubrique.html','html_01e7a708d190b26515495cbb4bfbbf91','_content',45,$GLOBALS['spip_lang'])), _request('connect')) .
'

		
			' .
(($t1 = BOUCLE_breveshtml_01e7a708d190b26515495cbb4bfbbf91($Cache, $Pile, $doublons, $Numrows, $SP))!=='' ?
		((	'
			<div class="liste breves">
					' .
		filtre_pagination_dist($Numrows["_breves"]["grand_total"],
 		'_breves',
		isset($Pile[0]['debut_breves'])?$Pile[0]['debut_breves']:intval(_request('debut_breves')),
		5, false, '', '', array()) .
		'
					<h2 class="h2">' .
		_T('breves:breves') .
		'</h2>
					<ul class="liste-items">
							') . $t1 . (	'
					</ul>
					' .
		(($t3 = strval(filtre_pagination_dist($Numrows["_breves"]["grand_total"],
 		'_breves',
		isset($Pile[0]['debut_breves'])?$Pile[0]['debut_breves']:intval(_request('debut_breves')),
		5, true, '', '', array())))!=='' ?
				('<div class="pagination">' . $t3 . '</div>') :
				'') .
		'
			</div>
			')) :
		'') .
'

			
			' .
(($t1 = BOUCLE_siteshtml_01e7a708d190b26515495cbb4bfbbf91($Cache, $Pile, $doublons, $Numrows, $SP))!=='' ?
		((	'
			<div class="liste sites">
					<h2 class="h2">' .
		_T('public|spip|ecrire:sur_web') .
		'</h2>
					<ul class="liste-items">
							') . $t1 . '
					</ul>
			</div>
			') :
		'') .
'

		
		' .
executer_balise_dynamique('FORMULAIRE_SITE',
	array($Pile[$SP]['id_rubrique']),
	array('plugins/auto/spipr_dist/v2.2.6/content/rubrique.html','html_01e7a708d190b26515495cbb4bfbbf91','_content',82,$GLOBALS['spip_lang'])) .
'
	</aside>

	<footer>
		' .
(($t1 = strval(interdire_scripts(calculer_notes())))!=='' ?
		((	'<div class="notes"><h2>' .
	_T('public|spip|ecrire:info_notes') .
	'</h2>') . $t1 . '</div>') :
		'') .
'
	</footer>

</section>
');
		lang_select();
	}
	lang_select();
	$iter->free();
	}
	if (defined("_BOUCLE_PROFILER")
	AND 1000*($timer = (time()+(float)microtime())-$timer) > _BOUCLE_PROFILER)
		spip_log(intval(1000*$timer)."ms BOUCLE_content @ plugins/auto/spipr_dist/v2.2.6/content/rubrique.html","profiler"._LOG_AVERTISSEMENT);
	return $t0;
}

//
// Fonction principale du squelette plugins/auto/spipr_dist/v2.2.6/content/rubrique.html
// Temps de compilation total: 4.681 ms
//

function html_01e7a708d190b26515495cbb4bfbbf91($Cache, $Pile, $doublons = array(), $Numrows = array(), $SP = 0) {

	if (isset($Pile[0]["doublons"]) AND is_array($Pile[0]["doublons"]))
		$doublons = nettoyer_env_doublons($Pile[0]["doublons"]);

	$connect = '';
	$page = BOUCLE_contenthtml_01e7a708d190b26515495cbb4bfbbf91($Cache, $Pile, $doublons, $Numrows, $SP);

	return analyse_resultat_skel('html_01e7a708d190b26515495cbb4bfbbf91', $Cache, $page, 'plugins/auto/spipr_dist/v2.2.6/content/rubrique.html');
}
?>