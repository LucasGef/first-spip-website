<?php

/*
 * Squelette : ../plugins/auto/menus/v1.7.26/prive/squelettes/contenu/menu_edit.html
 * Date :      Mon, 08 Jun 2020 15:55:26 GMT
 * Compile :   Tue, 16 Jun 2020 14:50:01 GMT
 * Boucles :   _menu
 */ 

function BOUCLE_menuhtml_2cb41ee8aafe95391abdd65f8243aa17(&$Cache, &$Pile, &$doublons, &$Numrows, $SP) {

	static $command = array();
	static $connect;
	$command['connect'] = $connect = '';
	if (!isset($command['table'])) {
		$command['table'] = 'menus';
		$command['id'] = '_menu';
		$command['from'] = array('menus' => 'spip_menus');
		$command['type'] = array();
		$command['groupby'] = array();
		$command['select'] = array("menus.titre");
		$command['orderby'] = array();
		$command['where'] = 
			array(
			array('=', 'menus.id_menu', sql_quote(_request('id_menu'), '', 'bigint(21) NOT NULL AUTO_INCREMENT')));
		$command['join'] = array();
		$command['limit'] = '';
		$command['having'] = 
			array();
	}
	if (defined("_BOUCLE_PROFILER")) $timer = time()+(float)microtime();
	$t0 = "";
	// REQUETE
	$iter = IterFactory::create(
		"SQL",
		$command,
		array('../plugins/auto/menus/v1.7.26/prive/squelettes/contenu/menu_edit.html','html_2cb41ee8aafe95391abdd65f8243aa17','_menu',7,$GLOBALS['spip_lang'])
	);
	if (!$iter->err()) {
	$SP++;
	// RESULTATS
	while ($Pile[$SP]=$iter->fetch()) {

		$t0 .= (
'
			' .
interdire_scripts(supprimer_numero(typo($Pile[$SP]['titre']), "TYPO", $connect, $Pile[0])) .
'
			');
	}
	$iter->free();
	}
	if (defined("_BOUCLE_PROFILER")
	AND 1000*($timer = (time()+(float)microtime())-$timer) > _BOUCLE_PROFILER)
		spip_log(intval(1000*$timer)."ms BOUCLE_menu @ ../plugins/auto/menus/v1.7.26/prive/squelettes/contenu/menu_edit.html","profiler"._LOG_AVERTISSEMENT);
	return $t0;
}

//
// Fonction principale du squelette ../plugins/auto/menus/v1.7.26/prive/squelettes/contenu/menu_edit.html
// Temps de compilation total: 2.338 ms
//

function html_2cb41ee8aafe95391abdd65f8243aa17($Cache, $Pile, $doublons = array(), $Numrows = array(), $SP = 0) {

	if (isset($Pile[0]["doublons"]) AND is_array($Pile[0]["doublons"]))
		$doublons = nettoyer_env_doublons($Pile[0]["doublons"]);

	$connect = '';
	$page = (
'
<div class="cadre-formulaire-editer">
	<div class="entete-formulaire">
		' .
interdire_scripts(filtre_icone_verticale_dist(entites_html(sinon(table_valeur(@$Pile[0], (string)'redirect', null), generer_url_ecrire('menus')),true),_T('public|spip|ecrire:retour'),'menus-24','','left')) .
'
		' .
_T('menus:formulaire_modifier_menu') .
'
		<h1>
			' .
(($t1 = BOUCLE_menuhtml_2cb41ee8aafe95391abdd65f8243aa17($Cache, $Pile, $doublons, $Numrows, $SP))!=='' ?
		$t1 :
		((	'
			' .
	interdire_scripts(((entites_html(table_valeur(@$Pile[0], (string)'id_menu', null),true) == 'new') ? _T('menus:formulaire_nouveau'):_request('id_menu'))) .
	'
			'))) .
'
		</h1>
	</div>
	' .
executer_balise_dynamique('FORMULAIRE_EDITER_MENU',
	array(interdire_scripts(entites_html(sinon(table_valeur(@$Pile[0], (string)'id_menu', null), ''),true)),interdire_scripts(entites_html(table_valeur(@$Pile[0], (string)'redirect', null),true)),interdire_scripts(entites_html(table_valeur(@$Pile[0], (string)'associer_objet', null),true))),
	array('../plugins/auto/menus/v1.7.26/prive/squelettes/contenu/menu_edit.html','html_2cb41ee8aafe95391abdd65f8243aa17','',14,$GLOBALS['spip_lang'])) .
(($t1 = strval(interdire_scripts(((intval(entites_html(sinon(table_valeur(@$Pile[0], (string)'id_menu', null), '0'),true))) ?' ' :''))))!=='' ?
		($t1 . (	'
		<div class="ajax">' .
	executer_balise_dynamique('FORMULAIRE_EDITER_MENUS_ENTREE',
	array(interdire_scripts(entites_html(table_valeur(@$Pile[0], (string)'id_menu', null),true))),
	array('../plugins/auto/menus/v1.7.26/prive/squelettes/contenu/menu_edit.html','html_2cb41ee8aafe95391abdd65f8243aa17','',16,$GLOBALS['spip_lang'])) .
	'</div>
	')) :
		'') .
'

</div>');

	return analyse_resultat_skel('html_2cb41ee8aafe95391abdd65f8243aa17', $Cache, $page, '../plugins/auto/menus/v1.7.26/prive/squelettes/contenu/menu_edit.html');
}
?>