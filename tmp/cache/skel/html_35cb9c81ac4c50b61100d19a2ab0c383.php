<?php

/*
 * Squelette : plugins/auto/spipr_dist/v2.2.6/body.html
 * Date :      Mon, 30 Mar 2020 09:25:24 GMT
 * Compile :   Wed, 17 Jun 2020 09:07:11 GMT
 * Boucles :   
 */ 
//
// Fonction principale du squelette plugins/auto/spipr_dist/v2.2.6/body.html
// Temps de compilation total: 0.274 ms
//

function html_35cb9c81ac4c50b61100d19a2ab0c383($Cache, $Pile, $doublons = array(), $Numrows = array(), $SP = 0) {

	if (isset($Pile[0]["doublons"]) AND is_array($Pile[0]["doublons"]))
		$doublons = nettoyer_env_doublons($Pile[0]["doublons"]);

	$connect = '';
	$page = (
'<body>
	<div class="page">
		<div class="container">
			<div class="header" id="header">
				' .

'<'.'?php echo recuperer_fond( ' . argumenter_squelette((	'header/' .
	interdire_scripts(entites_html(table_valeur(@$Pile[0], (string)'type-page', null),true)))) . ', array_merge('.var_export($Pile[0],1).',array(\'lang\' => ' . argumenter_squelette($GLOBALS["spip_lang"]) . ')), array("compil"=>array(\'plugins/auto/spipr_dist/v2.2.6/body.html\',\'html_35cb9c81ac4c50b61100d19a2ab0c383\',\'\',5,$GLOBALS[\'spip_lang\'])), _request("connect"));
?'.'>
			</div>
			' .
(($t1 = strval(navbar_responsive(recuperer_fond( 'inclure/nav' , array_merge($Pile[0],array()), array('compil'=>array('plugins/auto/spipr_dist/v2.2.6/body.html','html_35cb9c81ac4c50b61100d19a2ab0c383','',7,$GLOBALS['spip_lang'])), _request('connect')))))!=='' ?
		('<div class="navbar navbar-expand-md navbar-dark bg-dark" id="nav">
				' . $t1 . '
			</div>') :
		'') .
'

			<div class="row justify-content-between">
				<div class="content col-md-9 col-lg-8" id="content">
					<nav class="nav-breadcrumb" aria-label="Breadcrumb">
						' .

'<'.'?php echo recuperer_fond( ' . argumenter_squelette((	'breadcrumb/' .
	interdire_scripts(entites_html(table_valeur(@$Pile[0], (string)'type-page', null),true)))) . ', array_merge('.var_export($Pile[0],1).',array(\'lang\' => ' . argumenter_squelette($GLOBALS["spip_lang"]) . ')), array("compil"=>array(\'plugins/auto/spipr_dist/v2.2.6/body.html\',\'html_35cb9c81ac4c50b61100d19a2ab0c383\',\'\',12,$GLOBALS[\'spip_lang\'])), _request("connect"));
?'.'>
					</nav>
					' .

'<'.'?php echo recuperer_fond( ' . argumenter_squelette((	'content/' .
	interdire_scripts(entites_html(table_valeur(@$Pile[0], (string)'type-page', null),true)))) . ', array_merge('.var_export($Pile[0],1).',array(\'lang\' => ' . argumenter_squelette($GLOBALS["spip_lang"]) . ')), array("compil"=>array(\'plugins/auto/spipr_dist/v2.2.6/body.html\',\'html_35cb9c81ac4c50b61100d19a2ab0c383\',\'\',14,$GLOBALS[\'spip_lang\'])), _request("connect"));
?'.'>
				</div>
				<div class="aside secondary col-md-3" id="aside">
					' .

'<'.'?php echo recuperer_fond( ' . argumenter_squelette((	'aside/' .
	interdire_scripts(entites_html(table_valeur(@$Pile[0], (string)'type-page', null),true)))) . ', array_merge('.var_export($Pile[0],1).',array(\'lang\' => ' . argumenter_squelette($GLOBALS["spip_lang"]) . ')), array("compil"=>array(\'plugins/auto/spipr_dist/v2.2.6/body.html\',\'html_35cb9c81ac4c50b61100d19a2ab0c383\',\'\',17,$GLOBALS[\'spip_lang\'])), _request("connect"));
?'.'>
					' .

'<'.'?php echo recuperer_fond( ' . argumenter_squelette((	'extra/' .
	interdire_scripts(entites_html(table_valeur(@$Pile[0], (string)'type-page', null),true)))) . ', array_merge('.var_export($Pile[0],1).',array(\'lang\' => ' . argumenter_squelette($GLOBALS["spip_lang"]) . ')), array("compil"=>array(\'plugins/auto/spipr_dist/v2.2.6/body.html\',\'html_35cb9c81ac4c50b61100d19a2ab0c383\',\'\',18,$GLOBALS[\'spip_lang\'])), _request("connect"));
?'.'>
				</div>
			</div>

			<div class="footer" id="footer">
				' .

'<'.'?php echo recuperer_fond( ' . argumenter_squelette((	'footer/' .
	interdire_scripts(entites_html(table_valeur(@$Pile[0], (string)'type-page', null),true)))) . ', array_merge('.var_export($Pile[0],1).',array(\'lang\' => ' . argumenter_squelette($GLOBALS["spip_lang"]) . ')), array("compil"=>array(\'plugins/auto/spipr_dist/v2.2.6/body.html\',\'html_35cb9c81ac4c50b61100d19a2ab0c383\',\'\',23,$GLOBALS[\'spip_lang\'])), _request("connect"));
?'.'>
			</div>
		</div>
	</div>
</body>');

	return analyse_resultat_skel('html_35cb9c81ac4c50b61100d19a2ab0c383', $Cache, $page, 'plugins/auto/spipr_dist/v2.2.6/body.html');
}
?>