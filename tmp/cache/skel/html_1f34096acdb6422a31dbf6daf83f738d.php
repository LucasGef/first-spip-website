<?php

/*
 * Squelette : plugins/auto/zcore/v2.8.7/page.html
 * Date :      Mon, 30 Mar 2020 16:36:24 GMT
 * Compile :   Wed, 17 Jun 2020 07:36:19 GMT
 * Boucles :   
 */ 
//
// Fonction principale du squelette plugins/auto/zcore/v2.8.7/page.html
// Temps de compilation total: 0.122 ms
//

function html_1f34096acdb6422a31dbf6daf83f738d($Cache, $Pile, $doublons = array(), $Numrows = array(), $SP = 0) {

	if (isset($Pile[0]["doublons"]) AND is_array($Pile[0]["doublons"]))
		$doublons = nettoyer_env_doublons($Pile[0]["doublons"]);

	$connect = '';
	$page = 
'<'.'?php echo recuperer_fond( ' . argumenter_squelette('structure') . ', array_merge('.var_export($Pile[0],1).',array(\'type-page\' => ' . argumenter_squelette(interdire_scripts(entites_html(sinon(table_valeur(@$Pile[0], (string)'page', null), interdire_scripts(entites_html(sinon(table_valeur(@$Pile[0], (string)'type-page', null), 'sommaire'),true))),true))) . ',
	\'composition\' => ' . argumenter_squelette(interdire_scripts(entites_html(sinon(table_valeur(@$Pile[0], (string)'composition', null), ''),true))) . ',
	\'lang\' => ' . argumenter_squelette($GLOBALS["spip_lang"]) . ')), array("compil"=>array(\'plugins/auto/zcore/v2.8.7/page.html\',\'html_1f34096acdb6422a31dbf6daf83f738d\',\'\',1,$GLOBALS[\'spip_lang\'])), _request("connect"));
?'.'>';

	return analyse_resultat_skel('html_1f34096acdb6422a31dbf6daf83f738d', $Cache, $page, 'plugins/auto/zcore/v2.8.7/page.html');
}
?>