<?php

/*
 * Squelette : plugins/auto/menus/v1.7.26/menus/objet.html
 * Date :      Mon, 08 Jun 2020 15:55:26 GMT
 * Compile :   Wed, 17 Jun 2020 09:07:11 GMT
 * Boucles :   _art_lang, _art_orig, _si_art, _rub_lang, _rub_orig, _si_rub, _test_tradrub, _parametres, _public
 */ 

function BOUCLE_art_langhtml_974d960102a653ed87bd04817418765f(&$Cache, &$Pile, &$doublons, &$Numrows, $SP) {

	static $command = array();
	static $connect;
	$command['connect'] = $connect = '';
	if (!isset($command['table'])) {
		$command['table'] = 'articles';
		$command['id'] = '_art_lang';
		$command['from'] = array('articles' => 'spip_articles');
		$command['type'] = array();
		$command['groupby'] = array();
		$command['select'] = array("articles.id_article",
		"articles.lang",
		"articles.titre");
		$command['orderby'] = array();
		$command['join'] = array();
		$command['limit'] = '';
		$command['having'] = 
			array();
	}
	$command['where'] = 
			array(
quete_condition_statut('articles.statut','publie,prop,prepa/auteur','publie',''), 
quete_condition_postdates('articles.date',''), 
			array('OR', 
			array('AND', 
			array('=', 'articles.id_trad', 0), 
			array('=', 'articles.id_article', sql_quote($Pile[$SP]['id_article'], '', ''))), 
			array('AND', 
			array('>', 'articles.id_trad', 0), 
			array('=', 'articles.id_trad', sql_quote($Pile[$SP]['id_trad'], '', '')))), 
			array('=', 'articles.lang', sql_quote(interdire_scripts(entites_html(table_valeur(@$Pile[0], (string)'lang', null),true)), '', 'varchar(10) NOT NULL DEFAULT \'\'')));
	if (defined("_BOUCLE_PROFILER")) $timer = time()+(float)microtime();
	$t0 = "";
	// REQUETE
	$iter = IterFactory::create(
		"SQL",
		$command,
		array('plugins/auto/menus/v1.7.26/menus/objet.html','html_974d960102a653ed87bd04817418765f','_art_lang',4,$GLOBALS['spip_lang'])
	);
	if (!$iter->err()) {
	lang_select($GLOBALS['spip_lang']);
	$SP++;
	// RESULTATS
	while ($Pile[$SP]=$iter->fetch()) {

		lang_select_public($Pile[$SP]['lang'], '', $Pile[$SP]['titre']);
		$t0 .= (
'
' .
vide($Pile['vars'][$_zzz=(string)'id_objet'] = $Pile[$SP]['id_article']));
		lang_select();
	}
	lang_select();
	$iter->free();
	}
	if (defined("_BOUCLE_PROFILER")
	AND 1000*($timer = (time()+(float)microtime())-$timer) > _BOUCLE_PROFILER)
		spip_log(intval(1000*$timer)."ms BOUCLE_art_lang @ plugins/auto/menus/v1.7.26/menus/objet.html","profiler"._LOG_AVERTISSEMENT);
	return $t0;
}


function BOUCLE_art_orightml_974d960102a653ed87bd04817418765f(&$Cache, &$Pile, &$doublons, &$Numrows, $SP) {

	static $command = array();
	static $connect;
	$command['connect'] = $connect = '';
	if (!isset($command['table'])) {
		$command['table'] = 'articles';
		$command['id'] = '_art_orig';
		$command['from'] = array('articles' => 'spip_articles');
		$command['type'] = array();
		$command['groupby'] = array();
		$command['select'] = array("articles.id_trad",
		"articles.id_article",
		"articles.lang",
		"articles.titre");
		$command['orderby'] = array();
		$command['join'] = array();
		$command['limit'] = '';
		$command['having'] = 
			array();
	}
	$command['where'] = 
			array(
quete_condition_statut('articles.statut','publie,prop,prepa/auteur','publie',''), 
quete_condition_postdates('articles.date',''), 
			array('=', 'articles.id_article', sql_quote(interdire_scripts(generer_info_entite(interdire_scripts(entites_html(table_valeur(@$Pile[0], (string)'id_objet', null),true)), 'article', 'id_trad')), '', 'bigint(21) NOT NULL AUTO_INCREMENT')), 
			array('OR', 
			array('=', 'articles.id_trad', 'articles.id_article'), 
			array('=', 'articles.id_trad', '0')));
	if (defined("_BOUCLE_PROFILER")) $timer = time()+(float)microtime();
	$t0 = "";
	// REQUETE
	$iter = IterFactory::create(
		"SQL",
		$command,
		array('plugins/auto/menus/v1.7.26/menus/objet.html','html_974d960102a653ed87bd04817418765f','_art_orig',3,$GLOBALS['spip_lang'])
	);
	if (!$iter->err()) {
	lang_select($GLOBALS['spip_lang']);
	$SP++;
	// RESULTATS
	while ($Pile[$SP]=$iter->fetch()) {

		lang_select_public($Pile[$SP]['lang'], '', $Pile[$SP]['titre']);
		$t0 .= (
'
' .
(($t1 = BOUCLE_art_langhtml_974d960102a653ed87bd04817418765f($Cache, $Pile, $doublons, $Numrows, $SP))!=='' ?
		$t1 :
		((	'
' .
	vide($Pile['vars'][$_zzz=(string)'id_objet'] = $Pile[$SP]['id_article'])))) .
'
');
		lang_select();
	}
	lang_select();
	$iter->free();
	}
	if (defined("_BOUCLE_PROFILER")
	AND 1000*($timer = (time()+(float)microtime())-$timer) > _BOUCLE_PROFILER)
		spip_log(intval(1000*$timer)."ms BOUCLE_art_orig @ plugins/auto/menus/v1.7.26/menus/objet.html","profiler"._LOG_AVERTISSEMENT);
	return $t0;
}


function BOUCLE_si_arthtml_974d960102a653ed87bd04817418765f(&$Cache, &$Pile, &$doublons, &$Numrows, $SP) {

	static $command = array();
	static $connect;
	$command['connect'] = $connect = '';
	if (!isset($si_init)) { $command['si'] = array(); $si_init = true; }
	$command['si'][] = interdire_scripts(((((entites_html(table_valeur(@$Pile[0], (string)'type_objet', null),true) == 'article')) AND (interdire_scripts((entites_html(table_valeur(@$Pile[0], (string)'trad', null),true) == 'trad')))) ?' ' :''));

	if (!isset($command['table'])) {
		$command['table'] = '';
		$command['id'] = '_si_art';
		$command['from'] = array();
		$command['type'] = array();
		$command['groupby'] = array();
		$command['select'] = array("1");
		$command['orderby'] = array();
		$command['where'] = 
			array();
		$command['join'] = array();
		$command['limit'] = '';
		$command['having'] = 
			array();
	}
	if (defined("_BOUCLE_PROFILER")) $timer = time()+(float)microtime();
	$t0 = "";
	// REQUETE
	$iter = IterFactory::create(
		"CONDITION",
		$command,
		array('plugins/auto/menus/v1.7.26/menus/objet.html','html_974d960102a653ed87bd04817418765f','_si_art',2,$GLOBALS['spip_lang'])
	);
	if (!$iter->err()) {
	$SP++;
	// RESULTATS
	while ($Pile[$SP]=$iter->fetch()) {

		$t0 .= (
'
' .
BOUCLE_art_orightml_974d960102a653ed87bd04817418765f($Cache, $Pile, $doublons, $Numrows, $SP) .
'
');
	}
	$iter->free();
	}
	if (defined("_BOUCLE_PROFILER")
	AND 1000*($timer = (time()+(float)microtime())-$timer) > _BOUCLE_PROFILER)
		spip_log(intval(1000*$timer)."ms BOUCLE_si_art @ plugins/auto/menus/v1.7.26/menus/objet.html","profiler"._LOG_AVERTISSEMENT);
	return $t0;
}


function BOUCLE_rub_langhtml_974d960102a653ed87bd04817418765f(&$Cache, &$Pile, &$doublons, &$Numrows, $SP) {

	static $command = array();
	static $connect;
	$command['connect'] = $connect = '';
	if (!isset($command['table'])) {
		$command['table'] = 'rubriques';
		$command['id'] = '_rub_lang';
		$command['from'] = array('rubriques' => 'spip_rubriques');
		$command['type'] = array();
		$command['groupby'] = array();
		$command['select'] = array("rubriques.id_rubrique",
		"rubriques.lang",
		"rubriques.titre");
		$command['orderby'] = array();
		$command['join'] = array();
		$command['limit'] = '';
		$command['having'] = 
			array();
	}
	$command['where'] = 
			array(
quete_condition_statut('rubriques.statut','!','publie',''), 
			array('OR', 
			array('AND', 
			array('=', 'rubriques.id_trad', 0), 
			array('=', 'rubriques.id_rubrique', sql_quote($Pile[$SP]['id_rubrique'], '', ''))), 
			array('AND', 
			array('>', 'rubriques.id_trad', 0), 
			array('=', 'rubriques.id_trad', sql_quote(@$Pile[0]['id_trad'], '', '')))), 
			array('=', 'rubriques.lang', sql_quote(interdire_scripts(entites_html(table_valeur(@$Pile[0], (string)'lang', null),true)), '', 'varchar(10) NOT NULL DEFAULT \'\'')));
	if (defined("_BOUCLE_PROFILER")) $timer = time()+(float)microtime();
	$t0 = "";
	// REQUETE
	$iter = IterFactory::create(
		"SQL",
		$command,
		array('plugins/auto/menus/v1.7.26/menus/objet.html','html_974d960102a653ed87bd04817418765f','_rub_lang',15,$GLOBALS['spip_lang'])
	);
	if (!$iter->err()) {
	lang_select($GLOBALS['spip_lang']);
	$SP++;
	// RESULTATS
	while ($Pile[$SP]=$iter->fetch()) {

		lang_select_public($Pile[$SP]['lang'], '', $Pile[$SP]['titre']);
		$t0 .= (
'
' .
vide($Pile['vars'][$_zzz=(string)'id_objet'] = $Pile[$SP]['id_rubrique']));
		lang_select();
	}
	lang_select();
	$iter->free();
	}
	if (defined("_BOUCLE_PROFILER")
	AND 1000*($timer = (time()+(float)microtime())-$timer) > _BOUCLE_PROFILER)
		spip_log(intval(1000*$timer)."ms BOUCLE_rub_lang @ plugins/auto/menus/v1.7.26/menus/objet.html","profiler"._LOG_AVERTISSEMENT);
	return $t0;
}


function BOUCLE_rub_orightml_974d960102a653ed87bd04817418765f(&$Cache, &$Pile, &$doublons, &$Numrows, $SP) {

	static $command = array();
	static $connect;
	$command['connect'] = $connect = '';
	if (!isset($command['table'])) {
		$command['table'] = 'rubriques';
		$command['id'] = '_rub_orig';
		$command['from'] = array('rubriques' => 'spip_rubriques');
		$command['type'] = array();
		$command['groupby'] = array();
		$command['select'] = array("rubriques.id_rubrique",
		"rubriques.lang",
		"rubriques.titre");
		$command['orderby'] = array();
		$command['join'] = array();
		$command['limit'] = '';
		$command['having'] = 
			array();
	}
	$command['where'] = 
			array(
quete_condition_statut('rubriques.statut','!','publie',''), 
			array('=', 'rubriques.id_rubrique', sql_quote(interdire_scripts(entites_html(table_valeur(@$Pile[0], (string)'id_objet', null),true)), '', 'bigint(21) NOT NULL AUTO_INCREMENT')), 
			array('OR', 
			array('=', 'rubriques.id_trad', 'rubriques.id_rubrique'), 
			array('=', 'rubriques.id_trad', '0')));
	if (defined("_BOUCLE_PROFILER")) $timer = time()+(float)microtime();
	$t0 = "";
	// REQUETE
	$iter = IterFactory::create(
		"SQL",
		$command,
		array('plugins/auto/menus/v1.7.26/menus/objet.html','html_974d960102a653ed87bd04817418765f','_rub_orig',14,$GLOBALS['spip_lang'])
	);
	if (!$iter->err()) {
	lang_select($GLOBALS['spip_lang']);
	$SP++;
	// RESULTATS
	while ($Pile[$SP]=$iter->fetch()) {

		lang_select_public($Pile[$SP]['lang'], '', $Pile[$SP]['titre']);
		$t0 .= (
'
' .
(($t1 = BOUCLE_rub_langhtml_974d960102a653ed87bd04817418765f($Cache, $Pile, $doublons, $Numrows, $SP))!=='' ?
		$t1 :
		((	'
' .
	vide($Pile['vars'][$_zzz=(string)'id_objet'] = $Pile[$SP]['id_rubrique'])))) .
'
');
		lang_select();
	}
	lang_select();
	$iter->free();
	}
	if (defined("_BOUCLE_PROFILER")
	AND 1000*($timer = (time()+(float)microtime())-$timer) > _BOUCLE_PROFILER)
		spip_log(intval(1000*$timer)."ms BOUCLE_rub_orig @ plugins/auto/menus/v1.7.26/menus/objet.html","profiler"._LOG_AVERTISSEMENT);
	return $t0;
}


function BOUCLE_si_rubhtml_974d960102a653ed87bd04817418765f(&$Cache, &$Pile, &$doublons, &$Numrows, $SP) {

	static $command = array();
	static $connect;
	$command['connect'] = $connect = '';
	if (!isset($si_init)) { $command['si'] = array(); $si_init = true; }
	$command['si'][] = interdire_scripts(((((entites_html(table_valeur(@$Pile[0], (string)'type_objet', null),true) == 'rubrique')) AND (interdire_scripts((entites_html(table_valeur(@$Pile[0], (string)'trad', null),true) == 'trad')))) ?' ' :''));

	if (!isset($command['table'])) {
		$command['table'] = '';
		$command['id'] = '_si_rub';
		$command['from'] = array();
		$command['type'] = array();
		$command['groupby'] = array();
		$command['select'] = array("1");
		$command['orderby'] = array();
		$command['where'] = 
			array();
		$command['join'] = array();
		$command['limit'] = '';
		$command['having'] = 
			array();
	}
	if (defined("_BOUCLE_PROFILER")) $timer = time()+(float)microtime();
	$t0 = "";
	// REQUETE
	$iter = IterFactory::create(
		"CONDITION",
		$command,
		array('plugins/auto/menus/v1.7.26/menus/objet.html','html_974d960102a653ed87bd04817418765f','_si_rub',13,$GLOBALS['spip_lang'])
	);
	if (!$iter->err()) {
	$SP++;
	// RESULTATS
	while ($Pile[$SP]=$iter->fetch()) {

		$t0 .= (
'
' .
BOUCLE_rub_orightml_974d960102a653ed87bd04817418765f($Cache, $Pile, $doublons, $Numrows, $SP) .
'
');
	}
	$iter->free();
	}
	if (defined("_BOUCLE_PROFILER")
	AND 1000*($timer = (time()+(float)microtime())-$timer) > _BOUCLE_PROFILER)
		spip_log(intval(1000*$timer)."ms BOUCLE_si_rub @ plugins/auto/menus/v1.7.26/menus/objet.html","profiler"._LOG_AVERTISSEMENT);
	return $t0;
}


function BOUCLE_test_tradrubhtml_974d960102a653ed87bd04817418765f(&$Cache, &$Pile, &$doublons, &$Numrows, $SP) {

	static $command = array();
	static $connect;
	$command['connect'] = $connect = '';
	if (!isset($si_init)) { $command['si'] = array(); $si_init = true; }
	$command['si'][] = interdire_scripts(filtre_info_plugin_dist("tradrub", "est_actif"));

	if (!isset($command['table'])) {
		$command['table'] = '';
		$command['id'] = '_test_tradrub';
		$command['from'] = array();
		$command['type'] = array();
		$command['groupby'] = array();
		$command['select'] = array("1");
		$command['orderby'] = array();
		$command['where'] = 
			array();
		$command['join'] = array();
		$command['limit'] = '';
		$command['having'] = 
			array();
	}
	if (defined("_BOUCLE_PROFILER")) $timer = time()+(float)microtime();
	$t0 = "";
	// REQUETE
	$iter = IterFactory::create(
		"CONDITION",
		$command,
		array('plugins/auto/menus/v1.7.26/menus/objet.html','html_974d960102a653ed87bd04817418765f','_test_tradrub',12,$GLOBALS['spip_lang'])
	);
	if (!$iter->err()) {
	$SP++;
	// RESULTATS
	while ($Pile[$SP]=$iter->fetch()) {

		$t0 .= (
'
' .
BOUCLE_si_rubhtml_974d960102a653ed87bd04817418765f($Cache, $Pile, $doublons, $Numrows, $SP) .
'
');
	}
	$iter->free();
	}
	if (defined("_BOUCLE_PROFILER")
	AND 1000*($timer = (time()+(float)microtime())-$timer) > _BOUCLE_PROFILER)
		spip_log(intval(1000*$timer)."ms BOUCLE_test_tradrub @ plugins/auto/menus/v1.7.26/menus/objet.html","profiler"._LOG_AVERTISSEMENT);
	return $t0;
}


function BOUCLE_parametreshtml_974d960102a653ed87bd04817418765f(&$Cache, &$Pile, &$doublons, &$Numrows, $SP) {

	static $command = array();
	static $connect;
	$command['connect'] = $connect = '';
	$command['sourcemode'] = 'table';

	$command['source'] = array(interdire_scripts(filtre_explode_dist(table_valeur(@$Pile[0], (string)'parametres', null),'&')));

	if (!isset($command['table'])) {
		$command['table'] = '';
		$command['id'] = '_parametres';
		$command['from'] = array();
		$command['type'] = array();
		$command['groupby'] = array();
		$command['select'] = array(".valeur");
		$command['orderby'] = array();
		$command['where'] = 
			array();
		$command['join'] = array();
		$command['limit'] = '';
		$command['having'] = 
			array();
	}
	if (defined("_BOUCLE_PROFILER")) $timer = time()+(float)microtime();
	$t0 = "";
	// REQUETE
	$iter = IterFactory::create(
		"DATA",
		$command,
		array('plugins/auto/menus/v1.7.26/menus/objet.html','html_974d960102a653ed87bd04817418765f','_parametres',52,$GLOBALS['spip_lang'])
	);
	if (!$iter->err()) {
	$SP++;
	// RESULTATS
	while ($Pile[$SP]=$iter->fetch()) {

		$t0 .= (
'
	' .
vide($Pile['vars'][$_zzz=(string)'param'] = interdire_scripts(filtre_explode_dist(safehtml($Pile[$SP]['valeur']),'='))) .
vide($Pile['vars'][$_zzz=(string)'url'] = parametre_url(table_valeur($Pile["vars"], (string)'url', null),table_valeur($Pile["vars"], (string)'param/0', null),table_valeur($Pile["vars"], (string)'param/1', null))));
	}
	$iter->free();
	}
	if (defined("_BOUCLE_PROFILER")
	AND 1000*($timer = (time()+(float)microtime())-$timer) > _BOUCLE_PROFILER)
		spip_log(intval(1000*$timer)."ms BOUCLE_parametres @ plugins/auto/menus/v1.7.26/menus/objet.html","profiler"._LOG_AVERTISSEMENT);
	return $t0;
}


function BOUCLE_publichtml_974d960102a653ed87bd04817418765f(&$Cache, &$Pile, &$doublons, &$Numrows, $SP) {

	static $command = array();
	static $connect;
	$command['connect'] = $connect = '';
	if (!isset($si_init)) { $command['si'] = array(); $si_init = true; }
	$command['si'][] = interdire_scripts((((entites_html(table_valeur(@$Pile[0], (string)'appel_menu', null),true)) AND ((table_valeur($Pile["vars"], (string)'visible', null) == 'oui'))) ?' ' :''));

	if (!isset($command['table'])) {
		$command['table'] = '';
		$command['id'] = '_public';
		$command['from'] = array();
		$command['type'] = array();
		$command['groupby'] = array();
		$command['select'] = array("1");
		$command['orderby'] = array();
		$command['where'] = 
			array();
		$command['join'] = array();
		$command['limit'] = '';
		$command['having'] = 
			array();
	}
	if (defined("_BOUCLE_PROFILER")) $timer = time()+(float)microtime();
	$t0 = "";
	// REQUETE
	$iter = IterFactory::create(
		"CONDITION",
		$command,
		array('plugins/auto/menus/v1.7.26/menus/objet.html','html_974d960102a653ed87bd04817418765f','_public',50,$GLOBALS['spip_lang'])
	);
	if (!$iter->err()) {
	$SP++;
	// RESULTATS
	while ($Pile[$SP]=$iter->fetch()) {

		$t0 .= (
'
	' .
vide($Pile['vars'][$_zzz=(string)'url'] = interdire_scripts(generer_info_entite(table_valeur($Pile["vars"], (string)'id_objet', null), interdire_scripts(entites_html(table_valeur(@$Pile[0], (string)'type_objet', null),true)), 'url'))) .
BOUCLE_parametreshtml_974d960102a653ed87bd04817418765f($Cache, $Pile, $doublons, $Numrows, $SP) .
'
	' .
vide($Pile['vars'][$_zzz=(string)'url'] = ancre_url(table_valeur($Pile["vars"], (string)'url', null),interdire_scripts(entites_html(table_valeur(@$Pile[0], (string)'ancre', null),true)))) .
'<li class="menu-entree item menu-items__item menu-items__item_objet menu-items__item_' .
interdire_scripts(entites_html(table_valeur(@$Pile[0], (string)'type_objet', null),true)) .
(($t1 = strval(interdire_scripts(entites_html(table_valeur(@$Pile[0], (string)'css', null),true))))!=='' ?
		(' ' . $t1) :
		'') .
(($t1 = strval(menus_exposer(table_valeur($Pile["vars"], (string)'id_objet', null),interdire_scripts(entites_html(table_valeur(@$Pile[0], (string)'type_objet', null),true)),interdire_scripts(table_valeur(@$Pile[0], (string)'env', null)))))!=='' ?
		(' ' . $t1) :
		'') .
'">
		<a href="' .
table_valeur($Pile["vars"], (string)'url', null) .
'" class="menu-items__lien' .
(($t1 = strval(interdire_scripts(entites_html(table_valeur(@$Pile[0], (string)'css_lien', null),true))))!=='' ?
		(' ' . $t1) :
		'') .
'">' .
table_valeur($Pile["vars"], (string)'titre', null) .
'</a>
');
	}
	$iter->free();
	}
	if (defined("_BOUCLE_PROFILER")
	AND 1000*($timer = (time()+(float)microtime())-$timer) > _BOUCLE_PROFILER)
		spip_log(intval(1000*$timer)."ms BOUCLE_public @ plugins/auto/menus/v1.7.26/menus/objet.html","profiler"._LOG_AVERTISSEMENT);
	return $t0;
}

//
// Fonction principale du squelette plugins/auto/menus/v1.7.26/menus/objet.html
// Temps de compilation total: 5.869 ms
//

function html_974d960102a653ed87bd04817418765f($Cache, $Pile, $doublons = array(), $Numrows = array(), $SP = 0) {

	if (isset($Pile[0]["doublons"]) AND is_array($Pile[0]["doublons"]))
		$doublons = nettoyer_env_doublons($Pile[0]["doublons"]);

	$connect = '';
	$page = (
vide($Pile['vars'][$_zzz=(string)'id_objet'] = interdire_scripts(entites_html(table_valeur(@$Pile[0], (string)'id_objet', null),true))) .
BOUCLE_si_arthtml_974d960102a653ed87bd04817418765f($Cache, $Pile, $doublons, $Numrows, $SP) .
'

' .
BOUCLE_test_tradrubhtml_974d960102a653ed87bd04817418765f($Cache, $Pile, $doublons, $Numrows, $SP) .
'

' .
vide($Pile['vars'][$_zzz=(string)'titre'] = interdire_scripts(((($a = ((($a = typo(table_valeur(@$Pile[0], (string)'titre', null))) OR (is_string($a) AND strlen($a))) ? $a : interdire_scripts(generer_info_entite(table_valeur($Pile["vars"], (string)'id_objet', null), interdire_scripts(entites_html(table_valeur(@$Pile[0], (string)'type_objet', null),true)), 'titre')))) OR (is_string($a) AND strlen($a))) ? $a : _T('public|spip|ecrire:info_sans_titre')))) .
'

' .
(($t1 = strval(interdire_scripts(((((((entites_html(table_valeur(@$Pile[0], (string)'trad', null),true) == 'trad')) AND (interdire_scripts((entites_html(table_valeur(@$Pile[0], (string)'type_objet', null),true) == 'article')))) ?' ' :'')) ?' ' :''))))!=='' ?
		($t1 . vide($Pile['vars'][$_zzz=(string)'info_trad'] = _T('menus:info_traduction_recuperee'))) :
		'') .
'


' .
vide($Pile['vars'][$_zzz=(string)'visible'] = 'oui') .
(($t1 = strval(interdire_scripts((((entites_html(table_valeur(@$Pile[0], (string)'connexion', null),true) == 'session')) ?' ' :''))))!=='' ?
		($t1 . (	'
	' .
	(($t2 = strval(interdire_scripts(invalideur_session($Cache, ((table_valeur($GLOBALS["visiteur_session"], (string)'statut', null)) ?'' :' ')))))!=='' ?
			($t2 . (	'
		' .
		vide($Pile['vars'][$_zzz=(string)'visible'] = 'non'))) :
			'') .
	'
	' .
	vide($Pile['vars'][$_zzz=(string)'info_session'] = _T('menus:info_connexion_obligatoire')))) :
		'') .
'
' .
(($t1 = strval(interdire_scripts((((entites_html(table_valeur(@$Pile[0], (string)'connexion', null),true) == 'nosession')) ?' ' :''))))!=='' ?
		($t1 . (	'
	' .
	(($t2 = strval(interdire_scripts(invalideur_session($Cache, ((table_valeur($GLOBALS["visiteur_session"], (string)'statut', null)) ?' ' :'')))))!=='' ?
			($t2 . (	'
		' .
		vide($Pile['vars'][$_zzz=(string)'visible'] = 'non'))) :
			'') .
	'
	' .
	vide($Pile['vars'][$_zzz=(string)'info_session'] = _T('menus:info_deconnexion_obligatoire')))) :
		'') .
'

' .
(($t1 = strval(interdire_scripts(((objet_test_si_publie(entites_html(table_valeur(@$Pile[0], (string)'type_objet', null),true),table_valeur($Pile["vars"], (string)'id_objet', null))) ?'' :' '))))!=='' ?
		($t1 . (	'
	' .
	vide($Pile['vars'][$_zzz=(string)'visible'] = 'non'))) :
		'') .
'

' .
(($t1 = strval(interdire_scripts(((entites_html(table_valeur(@$Pile[0], (string)'appel_formulaire', null),true)) ?' ' :''))))!=='' ?
		($t1 . (	'
	<div class="titre">' .
	table_valeur($Pile["vars"], (string)'titre', null) .
	'</div>
	<div class="infos">' .
	interdire_scripts(entites_html(table_valeur(@$Pile[0], (string)'type_objet', null),true)) .
	'-' .
	table_valeur($Pile["vars"], (string)'id_objet', null) .
	(($t2 = strval(table_valeur($Pile["vars"], (string)'info_session', null)))!=='' ?
			('-' . $t2) :
			'') .
	(($t2 = strval(table_valeur($Pile["vars"], (string)'info_trad', null)))!=='' ?
			('-' . $t2) :
			'') .
	'</div>
')) :
		'') .
'

' .
BOUCLE_publichtml_974d960102a653ed87bd04817418765f($Cache, $Pile, $doublons, $Numrows, $SP) .
'
');

	return analyse_resultat_skel('html_974d960102a653ed87bd04817418765f', $Cache, $page, 'plugins/auto/menus/v1.7.26/menus/objet.html');
}
?>