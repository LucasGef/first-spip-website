<?php

/*
 * Squelette : plugins/auto/menus/v1.7.26/inclure/nav.html
 * Date :      Mon, 08 Jun 2020 15:55:26 GMT
 * Compile :   Wed, 17 Jun 2020 09:07:11 GMT
 * Boucles :   
 */ 
//
// Fonction principale du squelette plugins/auto/menus/v1.7.26/inclure/nav.html
// Temps de compilation total: 0.062 ms
//

function html_d34df97f3b617ab421873448fed0023f($Cache, $Pile, $doublons = array(), $Numrows = array(), $SP = 0) {

	if (isset($Pile[0]["doublons"]) AND is_array($Pile[0]["doublons"]))
		$doublons = nettoyer_env_doublons($Pile[0]["doublons"]);

	$connect = '';
	$page = (
'<div class="menu-conteneur navbar-inner">
' .
recuperer_fond( 'inclure/menu' , array_merge($Pile[0],array('identifiant' => 'barrenav' )), array('compil'=>array('plugins/auto/menus/v1.7.26/inclure/nav.html','html_d34df97f3b617ab421873448fed0023f','',2,$GLOBALS['spip_lang'])), _request('connect')) .
'</div>
');

	return analyse_resultat_skel('html_d34df97f3b617ab421873448fed0023f', $Cache, $page, 'plugins/auto/menus/v1.7.26/inclure/nav.html');
}
?>