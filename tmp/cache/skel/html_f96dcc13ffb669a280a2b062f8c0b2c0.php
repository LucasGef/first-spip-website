<?php

/*
 * Squelette : plugins/auto/z/v1.7.31/head/page-sommaire.html
 * Date :      Tue, 07 Nov 2017 17:27:36 GMT
 * Compile :   Tue, 16 Jun 2020 14:20:21 GMT
 * Boucles :   
 */ 
//
// Fonction principale du squelette plugins/auto/z/v1.7.31/head/page-sommaire.html
// Temps de compilation total: 0.658 ms
//

function html_f96dcc13ffb669a280a2b062f8c0b2c0($Cache, $Pile, $doublons = array(), $Numrows = array(), $SP = 0) {

	if (isset($Pile[0]["doublons"]) AND is_array($Pile[0]["doublons"]))
		$doublons = nettoyer_env_doublons($Pile[0]["doublons"]);

	$connect = '';
	$page = (
'
<title>' .
interdire_scripts(textebrut(typo(typo($GLOBALS['meta']['nom_site'], "TYPO", $connect, $Pile[0])))) .
(($t1 = strval(interdire_scripts(textebrut(typo(typo($GLOBALS['meta']['slogan_site'], "TYPO", $connect, $Pile[0]))))))!=='' ?
		(' - ' . $t1) :
		'') .
'</title>
' .
(($t1 = strval(interdire_scripts(textebrut(couper(propre($GLOBALS['meta']['descriptif_site'], $connect, $Pile[0]),'150')))))!=='' ?
		('<meta name="description" content="' . $t1 . '" />') :
		'') .
'
' .
(($t1 = strval(find_in_path('favicon.ico')))!=='' ?
		('<link rel="icon" type="image/x-icon" href="' . $t1 . (	'" />
' .
	(($t2 = strval(find_in_path('favicon.ico')))!=='' ?
			('<link rel="shortcut icon" type="image/x-icon" href="' . $t2 . '" />') :
			''))) :
		'') .
'
');

	return analyse_resultat_skel('html_f96dcc13ffb669a280a2b062f8c0b2c0', $Cache, $page, 'plugins/auto/z/v1.7.31/head/page-sommaire.html');
}
?>