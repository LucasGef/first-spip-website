<?php

/*
 * Squelette : ../prive/squelettes/head/dist.html
 * Date :      Tue, 16 Jun 2020 14:01:35 GMT
 * Compile :   Wed, 17 Jun 2020 07:16:15 GMT
 * Boucles :   
 */ 
//
// Fonction principale du squelette ../prive/squelettes/head/dist.html
// Temps de compilation total: 0.326 ms
//

function html_41c6568c0d24670d215458344f8c900e($Cache, $Pile, $doublons = array(), $Numrows = array(), $SP = 0) {

	if (isset($Pile[0]["doublons"]) AND is_array($Pile[0]["doublons"]))
		$doublons = nettoyer_env_doublons($Pile[0]["doublons"]);

	$connect = '';
	$page = (
'

' .
vide($Pile['vars'][$_zzz=(string)'paramcss'] = parametres_css_prive('')) .
pipeline( 'header_prive' , recuperer_fond( 'prive/squelettes/inclure/head' , array('titre' => @$Pile[0]['titre'] ,
	'minipres' => @$Pile[0]['minipres'] ,
	'paramcss' => table_valeur($Pile["vars"], (string)'paramcss', null) ,
	'espace_prive' => @$Pile[0]['espace_prive'] ), array('compil'=>array('../prive/squelettes/head/dist.html','html_41c6568c0d24670d215458344f8c900e','',0,$GLOBALS['spip_lang'])), _request('connect')) ));

	return analyse_resultat_skel('html_41c6568c0d24670d215458344f8c900e', $Cache, $page, '../prive/squelettes/head/dist.html');
}
?>