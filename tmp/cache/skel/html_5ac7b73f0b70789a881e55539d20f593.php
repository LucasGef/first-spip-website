<?php

/*
 * Squelette : ../prive/objets/contenu/objet.html
 * Date :      Tue, 16 Jun 2020 14:01:32 GMT
 * Compile :   Wed, 17 Jun 2020 06:58:47 GMT
 * Boucles :   _champs
 */ 

function BOUCLE_champshtml_5ac7b73f0b70789a881e55539d20f593(&$Cache, &$Pile, &$doublons, &$Numrows, $SP) {

	static $command = array();
	static $connect;
	$command['connect'] = $connect = '';
	$command['sourcemode'] = 'table';

	$command['source'] = array(interdire_scripts(objet_info(entites_html(table_valeur(@$Pile[0], (string)'objet', null),true),'champs_contenu')));

	if (!isset($command['table'])) {
		$command['table'] = '';
		$command['id'] = '_champs';
		$command['from'] = array();
		$command['type'] = array();
		$command['groupby'] = array();
		$command['select'] = array(".valeur");
		$command['orderby'] = array();
		$command['where'] = 
			array();
		$command['join'] = array();
		$command['limit'] = '';
		$command['having'] = 
			array();
	}
	if (defined("_BOUCLE_PROFILER")) $timer = time()+(float)microtime();
	$t0 = "";
	// REQUETE
	$iter = IterFactory::create(
		"DATA",
		$command,
		array('../prive/objets/contenu/objet.html','html_5ac7b73f0b70789a881e55539d20f593','_champs',1,$GLOBALS['spip_lang'])
	);
	if (!$iter->err()) {
	$SP++;
	// RESULTATS
	while ($Pile[$SP]=$iter->fetch()) {

		$t0 .= (
'
' .
vide($Pile['vars'][$_zzz=(string)'value'] = interdire_scripts(generer_info_entite(entites_html(table_valeur(@$Pile[0], (string)'id', null),true),interdire_scripts(entites_html(table_valeur(@$Pile[0], (string)'objet', null),true)),interdire_scripts(safehtml($Pile[$SP]['valeur']))))) .
'<div class="champ contenu_' .
interdire_scripts(safehtml($Pile[$SP]['valeur'])) .
(!strlen(table_valeur($Pile["vars"], (string)'value', null))  ?
		(' ' . 'vide') :
		'') .
'">
<div class=\'label\'>' .
interdire_scripts(_T(concat(entites_html(table_valeur(@$Pile[0], (string)'objet', null),true),':info_',interdire_scripts(safehtml($Pile[$SP]['valeur']))))) .
'</div>
<div class=\'' .
interdire_scripts(safehtml($Pile[$SP]['valeur'])) .
'\'>' .
table_valeur($Pile["vars"], (string)'value', null) .
'</div>
</div>
');
	}
	$iter->free();
	}
	if (defined("_BOUCLE_PROFILER")
	AND 1000*($timer = (time()+(float)microtime())-$timer) > _BOUCLE_PROFILER)
		spip_log(intval(1000*$timer)."ms BOUCLE_champs @ ../prive/objets/contenu/objet.html","profiler"._LOG_AVERTISSEMENT);
	return $t0;
}

//
// Fonction principale du squelette ../prive/objets/contenu/objet.html
// Temps de compilation total: 0.455 ms
//

function html_5ac7b73f0b70789a881e55539d20f593($Cache, $Pile, $doublons = array(), $Numrows = array(), $SP = 0) {

	if (isset($Pile[0]["doublons"]) AND is_array($Pile[0]["doublons"]))
		$doublons = nettoyer_env_doublons($Pile[0]["doublons"]);

	$connect = '';
	$page = (($t1 = BOUCLE_champshtml_5ac7b73f0b70789a881e55539d20f593($Cache, $Pile, $doublons, $Numrows, $SP))!=='' ?
		($t1 . (	'
' .
		(($t3 = strval(interdire_scripts(calculer_notes())))!=='' ?
				((	'<div class="champ contenu_notes">
<div class=\'label\'>' .
			_T('public|spip|ecrire:info_notes') .
			'</div>
<div dir=\'' .
			lang_dir(@$Pile[0]['lang'], 'ltr','rtl') .
			'\' class=\'notes\'>') . $t3 . '</div>
</div>') :
				'') .
		'
')) :
		'');

	return analyse_resultat_skel('html_5ac7b73f0b70789a881e55539d20f593', $Cache, $page, '../prive/objets/contenu/objet.html');
}
?>