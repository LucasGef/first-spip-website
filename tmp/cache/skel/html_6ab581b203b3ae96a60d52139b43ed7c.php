<?php

/*
 * Squelette : plugins/auto/menus/v1.7.26/menu.html
 * Date :      Mon, 08 Jun 2020 15:55:26 GMT
 * Compile :   Wed, 17 Jun 2020 06:58:44 GMT
 * Boucles :   _menu_principal
 */ 

function BOUCLE_menu_principalhtml_6ab581b203b3ae96a60d52139b43ed7c(&$Cache, &$Pile, &$doublons, &$Numrows, $SP) {

	static $command = array();
	static $connect;
	$command['connect'] = $connect = '';
	if (!isset($command['table'])) {
		$command['table'] = 'menus';
		$command['id'] = '_menu_principal';
		$command['from'] = array('menus' => 'spip_menus');
		$command['type'] = array();
		$command['groupby'] = array();
		$command['select'] = array("menus.titre");
		$command['orderby'] = array();
		$command['join'] = array();
		$command['limit'] = '';
		$command['having'] = 
			array();
	}
	$command['where'] = 
			array(
			array('=', 'menus.id_menu', sql_quote(@$Pile[0]['id_menu'], '','bigint(21) NOT NULL AUTO_INCREMENT')));
	if (defined("_BOUCLE_PROFILER")) $timer = time()+(float)microtime();
	$t0 = "";
	// REQUETE
	$iter = IterFactory::create(
		"SQL",
		$command,
		array('plugins/auto/menus/v1.7.26/menu.html','html_6ab581b203b3ae96a60d52139b43ed7c','_menu_principal',1,$GLOBALS['spip_lang'])
	);
	if (!$iter->err()) {
	$SP++;
	// RESULTATS
	while ($Pile[$SP]=$iter->fetch()) {

		$t0 .= (
'
<html>
	<head>
		' .
(($t1 = strval(interdire_scripts(textebrut(supprimer_numero(typo($Pile[$SP]['titre']), "TYPO", $connect, $Pile[0])))))!=='' ?
		('<title>' . $t1 . '</title>') :
		'') .
'
		<meta name="robots" content="none" />
	</head>
	<body>
		<h1>' .
interdire_scripts(supprimer_numero(typo($Pile[$SP]['titre']), "TYPO", $connect, $Pile[0])) .
'</h1>
		' .
recuperer_fond( 'inclure/menu' , array_merge($Pile[0],array()), array('compil'=>array('plugins/auto/menus/v1.7.26/menu.html','html_6ab581b203b3ae96a60d52139b43ed7c','_menu_principal',9,$GLOBALS['spip_lang'])), _request('connect')) .
'</body>
</html>
');
	}
	$iter->free();
	}
	if (defined("_BOUCLE_PROFILER")
	AND 1000*($timer = (time()+(float)microtime())-$timer) > _BOUCLE_PROFILER)
		spip_log(intval(1000*$timer)."ms BOUCLE_menu_principal @ plugins/auto/menus/v1.7.26/menu.html","profiler"._LOG_AVERTISSEMENT);
	return $t0;
}

//
// Fonction principale du squelette plugins/auto/menus/v1.7.26/menu.html
// Temps de compilation total: 5.346 ms
//

function html_6ab581b203b3ae96a60d52139b43ed7c($Cache, $Pile, $doublons = array(), $Numrows = array(), $SP = 0) {

	if (isset($Pile[0]["doublons"]) AND is_array($Pile[0]["doublons"]))
		$doublons = nettoyer_env_doublons($Pile[0]["doublons"]);

	$connect = '';
	$page = (
BOUCLE_menu_principalhtml_6ab581b203b3ae96a60d52139b43ed7c($Cache, $Pile, $doublons, $Numrows, $SP) .
'
');

	return analyse_resultat_skel('html_6ab581b203b3ae96a60d52139b43ed7c', $Cache, $page, 'plugins/auto/menus/v1.7.26/menu.html');
}
?>