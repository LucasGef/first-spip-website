<?php

/*
 * Squelette : plugins/auto/z/v1.7.31/inclure/pied.html
 * Date :      Tue, 07 Nov 2017 17:27:36 GMT
 * Compile :   Wed, 17 Jun 2020 06:56:17 GMT
 * Boucles :   
 */ 
//
// Fonction principale du squelette plugins/auto/z/v1.7.31/inclure/pied.html
// Temps de compilation total: 0.837 ms
//

function html_0273d701b731c6468b1fdc78edc7d389($Cache, $Pile, $doublons = array(), $Numrows = array(), $SP = 0) {

	if (isset($Pile[0]["doublons"]) AND is_array($Pile[0]["doublons"]))
		$doublons = nettoyer_env_doublons($Pile[0]["doublons"]);

	$connect = '';
	$page = (
'
<a href="https://www.spip.net/" title="' .
_T('public|spip|ecrire:site_realise_avec_spip') .
'"><img src="' .
((($a = find_in_path('prive/themes/spip/images/spip.png')) OR (is_string($a) AND strlen($a))) ? $a : 'spip.png') .
'" alt="SPIP" width="48" height="16" /></a>
<?php if (isset($GLOBALS[\'visiteur_session\'][\'id_auteur\']) AND $GLOBALS[\'visiteur_session\'][\'id_auteur\']) { ?>
| <a href="' .
executer_balise_dynamique('URL_LOGOUT',
	array(),
	array('plugins/auto/z/v1.7.31/inclure/pied.html','html_0273d701b731c6468b1fdc78edc7d389','',4,$GLOBALS['spip_lang'])) .
'" rel="nofollow">' .
_T('public|spip|ecrire:icone_deconnecter') .
'</a>
	<?php if (include_spip(\'inc/autoriser\') AND autoriser(\'ecrire\')){ ?>| <a href="' .
interdire_scripts(eval('return '.'_DIR_RESTREINT_ABS'.';')) .
'">' .
_T('public|spip|ecrire:espace_prive') .
'</a><?php } ?>
<?php } else { ?>
| <a href="' .
interdire_scripts(parametre_url(generer_url_public('login', ''),'url',parametre_url(self(),'url',''))) .
'" rel="nofollow" class=\'login_modal\'>' .
_T('public|spip|ecrire:lien_connecter') .
'</a>
<?php } ?>
| <a rel="contents" href="' .
interdire_scripts(generer_url_public('plan', '')) .
'">' .
_T('public|spip|ecrire:plan_site') .
'</a> |
<a href="' .
interdire_scripts(generer_url_public('backend', '')) .
'" rel="alternate" title="' .
_T('public|spip|ecrire:syndiquer_site') .
'"><img src="' .
find_in_path('feed.png') .
'" alt="' .
_T('public|spip|ecrire:icone_suivi_activite') .
'" width="16" height="16" />&nbsp;RSS&nbsp;2.0</a>
');

	return analyse_resultat_skel('html_0273d701b731c6468b1fdc78edc7d389', $Cache, $page, 'plugins/auto/z/v1.7.31/inclure/pied.html');
}
?>