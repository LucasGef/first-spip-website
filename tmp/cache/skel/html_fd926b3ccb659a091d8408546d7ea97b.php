<?php

/*
 * Squelette : plugins/auto/z/v1.7.31/extra/page-sommaire.html
 * Date :      Tue, 07 Nov 2017 17:27:36 GMT
 * Compile :   Tue, 16 Jun 2020 14:20:21 GMT
 * Boucles :   _forums_liens, _syndic
 */ 

function BOUCLE_forums_lienshtml_fd926b3ccb659a091d8408546d7ea97b(&$Cache, &$Pile, &$doublons, &$Numrows, $SP) {

	static $command = array();
	static $connect;
	$command['connect'] = $connect = '';
	if (!isset($command['table'])) {
		$command['table'] = 'forum';
		$command['id'] = '_forums_liens';
		$command['from'] = array('forum' => 'spip_forum');
		$command['type'] = array();
		$command['groupby'] = array();
		$command['select'] = array("forum.date_heure",
		"forum.date_heure AS date",
		"forum.id_forum",
		"forum.titre",
		"forum.texte");
		$command['orderby'] = array('forum.date_heure DESC');
		$command['where'] = 
			array(
quete_condition_statut('forum.statut','publie,prop','publie',''));
		$command['join'] = array();
		$command['limit'] = '0,8';
		$command['having'] = 
			array();
	}
	if (defined("_BOUCLE_PROFILER")) $timer = time()+(float)microtime();
	$t0 = "";
	// REQUETE
	$iter = IterFactory::create(
		"SQL",
		$command,
		array('plugins/auto/z/v1.7.31/extra/page-sommaire.html','html_fd926b3ccb659a091d8408546d7ea97b','_forums_liens',5,$GLOBALS['spip_lang'])
	);
	if (!$iter->err()) {
	$SP++;
	// RESULTATS
	while ($Pile[$SP]=$iter->fetch()) {

		$t0 .= (
'
		<li class="item">' .
(($t1 = strval(interdire_scripts(affdate_jourcourt(normaliser_date($Pile[$SP]['date'])))))!=='' ?
		($t1 . ' &ndash; ') :
		'') .
'<a href="' .
vider_url(urlencode_1738(generer_url_entite($Pile[$SP]['id_forum'], 'forum', '', '', true))) .
'"' .
(($t1 = strval(interdire_scripts(couper(attribut_html(liens_nofollow(safehtml(typo(interdit_html($Pile[$SP]['titre']), "TYPO", $connect, $Pile[0])))),'80'))))!=='' ?
		(' title="' . $t1 . '"') :
		'') .
'>' .
interdire_scripts(couper(liens_nofollow(safehtml(propre(interdit_html($Pile[$SP]['texte']), $connect, $Pile[0]))),'80')) .
'</a></li>
		');
	}
	$iter->free();
	}
	if (defined("_BOUCLE_PROFILER")
	AND 1000*($timer = (time()+(float)microtime())-$timer) > _BOUCLE_PROFILER)
		spip_log(intval(1000*$timer)."ms BOUCLE_forums_liens @ plugins/auto/z/v1.7.31/extra/page-sommaire.html","profiler"._LOG_AVERTISSEMENT);
	return $t0;
}


function BOUCLE_syndichtml_fd926b3ccb659a091d8408546d7ea97b(&$Cache, &$Pile, &$doublons, &$Numrows, $SP) {

	static $command = array();
	static $connect;
	$command['connect'] = $connect = '';
	if (!isset($command['table'])) {
		$command['table'] = 'syndic_articles';
		$command['id'] = '_syndic';
		$command['from'] = array('syndic_articles' => 'spip_syndic_articles','L1' => 'spip_syndic');
		$command['type'] = array();
		$command['groupby'] = array();
		$command['select'] = array("syndic_articles.date",
		"syndic_articles.url",
		"L1.url_site",
		"L1.nom_site",
		"syndic_articles.titre");
		$command['orderby'] = array('syndic_articles.date DESC');
		$command['where'] = 
			array(
quete_condition_statut('L1.statut','publie,prop','publie',''), 
quete_condition_statut('syndic_articles.statut','publie,prop','publie',''), 
			array('<', 'TIMESTAMPDIFF(HOUR,syndic_articles.date,NOW())/24', "180"));
		$command['join'] = array('L1' => array('syndic_articles','id_syndic'));
		$command['limit'] = '0,6';
		$command['having'] = 
			array();
	}
	if (defined("_BOUCLE_PROFILER")) $timer = time()+(float)microtime();
	$t0 = "";
	// REQUETE
	$iter = IterFactory::create(
		"SQL",
		$command,
		array('plugins/auto/z/v1.7.31/extra/page-sommaire.html','html_fd926b3ccb659a091d8408546d7ea97b','_syndic',18,$GLOBALS['spip_lang'])
	);
	if (!$iter->err()) {
	$SP++;
	// RESULTATS
	while ($Pile[$SP]=$iter->fetch()) {

		$t0 .= (
'
		<li class="item">' .
(($t1 = strval(interdire_scripts(affdate_jourcourt(normaliser_date($Pile[$SP]['date'])))))!=='' ?
		($t1 . ' &ndash; ') :
		'') .
'<a href="' .
vider_url($Pile[$SP]['url']) .
'"' .
(($t1 = strval(interdire_scripts(couper(attribut_html(typo(supprimer_numero(calculer_url($Pile[$SP]['url_site'],$Pile[$SP]['nom_site'], 'titre', $connect, false)), "TYPO", $connect, $Pile[0])),'80'))))!=='' ?
		(' title="' . $t1 . '"') :
		'') .
' class="spip_out">' .
interdire_scripts(supprimer_numero(typo($Pile[$SP]['titre']), "TYPO", $connect, $Pile[0])) .
'</a></li>
		');
	}
	$iter->free();
	}
	if (defined("_BOUCLE_PROFILER")
	AND 1000*($timer = (time()+(float)microtime())-$timer) > _BOUCLE_PROFILER)
		spip_log(intval(1000*$timer)."ms BOUCLE_syndic @ plugins/auto/z/v1.7.31/extra/page-sommaire.html","profiler"._LOG_AVERTISSEMENT);
	return $t0;
}

//
// Fonction principale du squelette plugins/auto/z/v1.7.31/extra/page-sommaire.html
// Temps de compilation total: 6.537 ms
//

function html_fd926b3ccb659a091d8408546d7ea97b($Cache, $Pile, $doublons = array(), $Numrows = array(), $SP = 0) {

	if (isset($Pile[0]["doublons"]) AND is_array($Pile[0]["doublons"]))
		$doublons = nettoyer_env_doublons($Pile[0]["doublons"]);

	$connect = '';
	$page = (
'
' .
executer_balise_dynamique('FORMULAIRE_INSCRIPTION',
	array(),
	array('plugins/auto/z/v1.7.31/extra/page-sommaire.html','html_fd926b3ccb659a091d8408546d7ea97b','',2,$GLOBALS['spip_lang'])) .
'


' .
(($t1 = BOUCLE_forums_lienshtml_fd926b3ccb659a091d8408546d7ea97b($Cache, $Pile, $doublons, $Numrows, $SP))!=='' ?
		((	'
<div class="liste forums">
	<h2 class="h2">' .
		_T('public|spip|ecrire:derniers_commentaires') .
		'</h2>
	<ul class="liste-items">
		') . $t1 . '
	</ul>
</div>
') :
		'') .
'



' .
(($t1 = BOUCLE_syndichtml_fd926b3ccb659a091d8408546d7ea97b($Cache, $Pile, $doublons, $Numrows, $SP))!=='' ?
		((	'
<div class="liste syndic_articles">
	<h2 class="h2">' .
		_T('public|spip|ecrire:nouveautes_web') .
		'</h2>
	<ul class="liste-items">
		') . $t1 . '
	</ul>
</div>
') :
		'') .
'
');

	return analyse_resultat_skel('html_fd926b3ccb659a091d8408546d7ea97b', $Cache, $page, 'plugins/auto/z/v1.7.31/extra/page-sommaire.html');
}
?>