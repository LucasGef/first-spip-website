<?php

/*
 * Squelette : plugins/auto/spipr_dist/v2.2.6/breadcrumb/404.html
 * Date :      Mon, 30 Mar 2020 09:25:24 GMT
 * Compile :   Wed, 17 Jun 2020 08:56:36 GMT
 * Boucles :   
 */ 
//
// Fonction principale du squelette plugins/auto/spipr_dist/v2.2.6/breadcrumb/404.html
// Temps de compilation total: 0.060 ms
//

function html_8f7ac7a1c1781411074dfb1830909396($Cache, $Pile, $doublons = array(), $Numrows = array(), $SP = 0) {

	if (isset($Pile[0]["doublons"]) AND is_array($Pile[0]["doublons"]))
		$doublons = nettoyer_env_doublons($Pile[0]["doublons"]);

	$connect = '';
	$page = (
'
<ul class="breadcrumb">
	<li class="breadcrumb-item"><a href="' .
spip_htmlspecialchars(sinon($GLOBALS['meta']['adresse_site'],'.')) .
'/">' .
_T('public|spip|ecrire:accueil_site') .
'</a></li>
	<li class="breadcrumb-item active"><span>' .
_T('public|spip|ecrire:pass_erreur') .
' 404</span></li>
</ul>
');

	return analyse_resultat_skel('html_8f7ac7a1c1781411074dfb1830909396', $Cache, $page, 'plugins/auto/spipr_dist/v2.2.6/breadcrumb/404.html');
}
?>