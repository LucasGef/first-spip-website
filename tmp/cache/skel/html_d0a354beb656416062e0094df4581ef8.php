<?php

/*
 * Squelette : ../prive/objets/editer/liens.html
 * Date :      Tue, 16 Jun 2020 14:01:32 GMT
 * Compile :   Wed, 17 Jun 2020 08:37:26 GMT
 * Boucles :   
 */ 
//
// Fonction principale du squelette ../prive/objets/editer/liens.html
// Temps de compilation total: 0.309 ms
//

function html_d0a354beb656416062e0094df4581ef8($Cache, $Pile, $doublons = array(), $Numrows = array(), $SP = 0) {

	if (isset($Pile[0]["doublons"]) AND is_array($Pile[0]["doublons"]))
		$doublons = nettoyer_env_doublons($Pile[0]["doublons"]);

	$connect = '';
	$page = (
(($t1 = strval(interdire_scripts((((entites_html(table_valeur(@$Pile[0], (string)'lien', null),true) == interdire_scripts(entites_html(table_valeur(@$Pile[0], (string)'objet', null),true)))) ?'' :' '))))!=='' ?
		($t1 . (	'
<div class="ajax">
	' .
	executer_balise_dynamique('FORMULAIRE_EDITER_LIENS',
	array(interdire_scripts(entites_html(table_valeur(@$Pile[0], (string)'table_source', null),true)),interdire_scripts(entites_html(table_valeur(@$Pile[0], (string)'objet', null),true)),interdire_scripts(entites_html(table_valeur(@$Pile[0], (string)'id_objet', null),true)),interdire_scripts(((entites_html(table_valeur(@$Pile[0], (string)'editable', null),true) == 'non') ? '':' '))),
	array('../prive/objets/editer/liens.html','html_d0a354beb656416062e0094df4581ef8','',3,$GLOBALS['spip_lang'])) .
	'</div>
')) :
		'') .
(($t1 = strval(interdire_scripts((((entites_html(table_valeur(@$Pile[0], (string)'lien', null),true) == interdire_scripts(entites_html(table_valeur(@$Pile[0], (string)'objet', null),true)))) ?' ' :''))))!=='' ?
		($t1 . (	'
<div class="ajax">
	' .
	executer_balise_dynamique('FORMULAIRE_EDITER_LIENS',
	array(interdire_scripts(entites_html(table_valeur(@$Pile[0], (string)'objet', null),true)),interdire_scripts(entites_html(table_valeur(@$Pile[0], (string)'id_objet', null),true)),interdire_scripts(entites_html(table_valeur(@$Pile[0], (string)'table_source', null),true)),interdire_scripts(((entites_html(table_valeur(@$Pile[0], (string)'editable', null),true) == 'non') ? '':' '))),
	array('../prive/objets/editer/liens.html','html_d0a354beb656416062e0094df4581ef8','',3,$GLOBALS['spip_lang'])) .
	'</div>
')) :
		''));

	return analyse_resultat_skel('html_d0a354beb656416062e0094df4581ef8', $Cache, $page, '../prive/objets/editer/liens.html');
}
?>