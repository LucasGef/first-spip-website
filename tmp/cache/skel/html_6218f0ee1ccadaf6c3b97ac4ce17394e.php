<?php

/*
 * Squelette : plugins/auto/spipr_dist/v2.2.6/content/login.html
 * Date :      Mon, 30 Mar 2020 09:25:24 GMT
 * Compile :   Wed, 17 Jun 2020 08:04:13 GMT
 * Boucles :   
 */ 
//
// Fonction principale du squelette plugins/auto/spipr_dist/v2.2.6/content/login.html
// Temps de compilation total: 11.893 ms
//

function html_6218f0ee1ccadaf6c3b97ac4ce17394e($Cache, $Pile, $doublons = array(), $Numrows = array(), $SP = 0) {

	if (isset($Pile[0]["doublons"]) AND is_array($Pile[0]["doublons"]))
		$doublons = nettoyer_env_doublons($Pile[0]["doublons"]);

	$connect = '';
	$page = (
'
<section>
	' .
(($t1 = strval(interdire_scripts(((entites_html(sinon(table_valeur(@$Pile[0], (string)'cartouche', null), '1'),true)) ?' ' :''))))!=='' ?
		($t1 . 
'<'.'?php echo recuperer_fond( ' . argumenter_squelette((	'cartouche/' .
		interdire_scripts(entites_html(table_valeur(@$Pile[0], (string)'type-page', null),true)))) . ', array_merge('.var_export($Pile[0],1).',array(\'lang\' => ' . argumenter_squelette($GLOBALS["spip_lang"]) . ')), array("compil"=>array(\'plugins/auto/spipr_dist/v2.2.6/content/login.html\',\'html_6218f0ee1ccadaf6c3b97ac4ce17394e\',\'\',3,$GLOBALS[\'spip_lang\'])), _request("connect"));
?'.'>') :
		'') .
'

	<div class="main">
		' .
executer_balise_dynamique('MENU_LANG_ECRIRE',
	array(spip_htmlentities(@$Pile[0]['lang'] ? @$Pile[0]['lang'] : $GLOBALS['spip_lang'])),
	array('plugins/auto/spipr_dist/v2.2.6/content/login.html','html_6218f0ee1ccadaf6c3b97ac4ce17394e','',6,$GLOBALS['spip_lang'])) .
'

		' .
executer_balise_dynamique('FORMULAIRE_LOGIN',
	array(interdire_scripts(((($a = entites_html(table_valeur(@$Pile[0], (string)'url', null),true)) OR (is_string($a) AND strlen($a))) ? $a : generer_url_ecrire('accueil')))),
	array('plugins/auto/spipr_dist/v2.2.6/content/login.html','html_6218f0ee1ccadaf6c3b97ac4ce17394e','',8,$GLOBALS['spip_lang'])) .
'
	</div>
</section>
');

	return analyse_resultat_skel('html_6218f0ee1ccadaf6c3b97ac4ce17394e', $Cache, $page, 'plugins/auto/spipr_dist/v2.2.6/content/login.html');
}
?>