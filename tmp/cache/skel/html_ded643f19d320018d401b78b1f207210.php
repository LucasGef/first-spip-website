<?php

/*
 * Squelette : plugins/auto/comments/v3.5.0/comments-list.html
 * Date :      Thu, 26 Mar 2020 19:57:32 GMT
 * Compile :   Wed, 17 Jun 2020 09:02:58 GMT
 * Boucles :   _comments_list
 */ 

function BOUCLE_comments_listhtml_ded643f19d320018d401b78b1f207210(&$Cache, &$Pile, &$doublons, &$Numrows, $SP) {

	static $command = array();
	static $connect;
	$command['connect'] = $connect = '';
	$in = array();
	$in[]= 'publie';
	$in1 = array();
	if (!(is_array($a = (@$Pile[0]['id_rubrique']))))
		$in1[]= $a;
	else $in1 = array_merge($in1, $a);
	$in2 = array();
	if (!(is_array($a = (@$Pile[0]['id_article']))))
		$in2[]= $a;
	else $in2 = array_merge($in2, $a);
	$in3 = array();
	if (!(is_array($a = (@$Pile[0]['id_breve']))))
		$in3[]= $a;
	else $in3 = array_merge($in3, $a);
	$in4 = array();
	if (!(is_array($a = (@$Pile[0]['id_syndic']))))
		$in4[]= $a;
	else $in4 = array_merge($in4, $a);
	$in5 = array();
	if (!(is_array($a = (@$Pile[0]['objet']))))
		$in5[]= $a;
	else $in5 = array_merge($in5, $a);
	$in6 = array();
	if (!(is_array($a = (@$Pile[0]['id_objet']))))
		$in6[]= $a;
	else $in6 = array_merge($in6, $a);
	if (!isset($command['table'])) {
		$command['table'] = 'forum';
		$command['id'] = '_comments_list';
		$command['from'] = array('forum' => 'spip_forum');
		$command['type'] = array();
		$command['groupby'] = array();
		$command['select'] = array("forum.date_heure",
		"forum.id_forum",
		"forum.id_objet",
		"forum.objet",
		"forum.id_auteur");
		$command['orderby'] = array('forum.date_heure');
		$command['join'] = array();
		$command['limit'] = '';
		$command['having'] = 
			array();
	}
	$command['where'] = 
			array(sql_in('forum.statut',sql_quote($in)), (!(is_array(@$Pile[0]['id_rubrique'])?count(@$Pile[0]['id_rubrique']):strlen(@$Pile[0]['id_rubrique'])) ? '' : ((is_array(@$Pile[0]['id_rubrique'])) ? sql_in('forum.id_objet',sql_quote($in1)) : 
			array('=', 'forum.id_objet', sql_quote(@$Pile[0]['id_rubrique'], '','bigint(21) NOT NULL DEFAULT \'0\'')))), (!(is_array(@$Pile[0]['id_rubrique'])?count(@$Pile[0]['id_rubrique']):strlen(@$Pile[0]['id_rubrique'])) ? '' : 
			array('=', 'forum.objet', sql_quote('rubrique'))), (!(is_array(@$Pile[0]['id_article'])?count(@$Pile[0]['id_article']):strlen(@$Pile[0]['id_article'])) ? '' : ((is_array(@$Pile[0]['id_article'])) ? sql_in('forum.id_objet',sql_quote($in2)) : 
			array('=', 'forum.id_objet', sql_quote(@$Pile[0]['id_article'], '','bigint(21) NOT NULL DEFAULT \'0\'')))), (!(is_array(@$Pile[0]['id_article'])?count(@$Pile[0]['id_article']):strlen(@$Pile[0]['id_article'])) ? '' : 
			array('=', 'forum.objet', sql_quote('article'))), (!(is_array(@$Pile[0]['id_breve'])?count(@$Pile[0]['id_breve']):strlen(@$Pile[0]['id_breve'])) ? '' : ((is_array(@$Pile[0]['id_breve'])) ? sql_in('forum.id_objet',sql_quote($in3)) : 
			array('=', 'forum.id_objet', sql_quote(@$Pile[0]['id_breve'], '','bigint(21) NOT NULL DEFAULT \'0\'')))), (!(is_array(@$Pile[0]['id_breve'])?count(@$Pile[0]['id_breve']):strlen(@$Pile[0]['id_breve'])) ? '' : 
			array('=', 'forum.objet', sql_quote('breve'))), (!(is_array(@$Pile[0]['id_syndic'])?count(@$Pile[0]['id_syndic']):strlen(@$Pile[0]['id_syndic'])) ? '' : ((is_array(@$Pile[0]['id_syndic'])) ? sql_in('forum.id_objet',sql_quote($in4)) : 
			array('=', 'forum.id_objet', sql_quote(@$Pile[0]['id_syndic'], '','bigint(21) NOT NULL DEFAULT \'0\'')))), (!(is_array(@$Pile[0]['id_syndic'])?count(@$Pile[0]['id_syndic']):strlen(@$Pile[0]['id_syndic'])) ? '' : 
			array('=', 'forum.objet', sql_quote('site'))), (!(is_array(@$Pile[0]['objet'])?count(@$Pile[0]['objet']):strlen(@$Pile[0]['objet'])) ? '' : ((is_array(@$Pile[0]['objet'])) ? sql_in('forum.objet',sql_quote($in5)) : 
			array('=', 'forum.objet', sql_quote(@$Pile[0]['objet'], '','varchar(25) NOT NULL DEFAULT \'\'')))), (!(is_array(@$Pile[0]['id_objet'])?count(@$Pile[0]['id_objet']):strlen(@$Pile[0]['id_objet'])) ? '' : ((is_array(@$Pile[0]['id_objet'])) ? sql_in('forum.id_objet',sql_quote($in6)) : 
			array('=', 'forum.id_objet', sql_quote(@$Pile[0]['id_objet'], '','bigint(21) NOT NULL DEFAULT \'0\'')))));
	if (defined("_BOUCLE_PROFILER")) $timer = time()+(float)microtime();
	$t0 = "";
	// REQUETE
	$iter = IterFactory::create(
		"SQL",
		$command,
		array('plugins/auto/comments/v3.5.0/comments-list.html','html_ded643f19d320018d401b78b1f207210','_comments_list',3,$GLOBALS['spip_lang'])
	);
	if (!$iter->err()) {
	
	// COMPTEUR
	$Numrows['_comments_list']['compteur_boucle'] = 0;
	$SP++;
	// RESULTATS
	while ($Pile[$SP]=$iter->fetch()) {

		$Numrows['_comments_list']['compteur_boucle']++;
		$t0 .= (
'
		<li class="comment-li comment-item' .
(($t1 = strval(alterner($Numrows['_comments_list']['compteur_boucle'],'odd','even')))!=='' ?
		(' ' . $t1) :
		'') .
((($Numrows['_comments_list']['compteur_boucle'] == '1'))  ?
		(' ' . ' ' . 'first') :
		'') .
(calcul_exposer($Pile[$SP]['id_forum'], 'id_forum', $Pile[0], 0, 'id_forum', '')  ?
		(' ' . 'on') :
		'') .
(($t1 = strval(filtre_me_dist($Pile[$SP]['id_objet'],interdire_scripts($Pile[$SP]['objet']),$Pile[$SP]['id_auteur'])))!=='' ?
		($t1 . 'me') :
		'') .
'">
			' .

'<'.'?php echo recuperer_fond( ' . argumenter_squelette('inclure/comment') . ', array(\'id_forum\' => ' . argumenter_squelette($Pile[$SP]['id_forum']) . ',
	\'compteur\' => ' . argumenter_squelette($Numrows['_comments_list']['compteur_boucle']) . ',
	\'lang\' => ' . argumenter_squelette($GLOBALS["spip_lang"]) . '), array("compil"=>array(\'plugins/auto/comments/v3.5.0/comments-list.html\',\'html_ded643f19d320018d401b78b1f207210\',\'\',9,$GLOBALS[\'spip_lang\'])), _request("connect"));
?'.'>
		</li>
		');
	}
	$iter->free();
	}
	if (defined("_BOUCLE_PROFILER")
	AND 1000*($timer = (time()+(float)microtime())-$timer) > _BOUCLE_PROFILER)
		spip_log(intval(1000*$timer)."ms BOUCLE_comments_list @ plugins/auto/comments/v3.5.0/comments-list.html","profiler"._LOG_AVERTISSEMENT);
	return $t0;
}

//
// Fonction principale du squelette plugins/auto/comments/v3.5.0/comments-list.html
// Temps de compilation total: 1.235 ms
//

function html_ded643f19d320018d401b78b1f207210($Cache, $Pile, $doublons = array(), $Numrows = array(), $SP = 0) {

	if (isset($Pile[0]["doublons"]) AND is_array($Pile[0]["doublons"]))
		$doublons = nettoyer_env_doublons($Pile[0]["doublons"]);

	$connect = '';
	$page = (
'
' .
(spip_htmlspecialchars(
		// refus des forums ?
		(quete_accepter_forum(@$Pile[0]['id_article'])=="non" OR
		($GLOBALS["meta"]["forums_publics"] == "non"
		AND quete_accepter_forum(@$Pile[0]['id_article']) == ""))
		? "" : // sinon:
		(calcul_parametres_forum($Pile[0],null,null,null).
	(($lien = (_request("retour") ? _request("retour") : str_replace("&amp;", "&", ''))) ? "&retour=".rawurlencode($lien) : ""))) ? '':'') .
'
' .
(($t1 = BOUCLE_comments_listhtml_ded643f19d320018d401b78b1f207210($Cache, $Pile, $doublons, $Numrows, $SP))!=='' ?
		((	'
<div class="comments-list comments-posts">
	' .
		(($t3 = strval(interdire_scripts((is_null(entites_html(table_valeur(@$Pile[0], (string)'titre_liste_commentaires', null),true)) ? _T('comments:comments_h'):interdire_scripts(trim(table_valeur(@$Pile[0], (string)'titre_liste_commentaires', null)))))))!=='' ?
				('<h2 class="h2">' . $t3 . '</h2>') :
				'') .
		'
	<ul class="comments-ul comments-items">
		') . $t1 . '
	</ul>
</div>
') :
		'') .
'
');

	return analyse_resultat_skel('html_ded643f19d320018d401b78b1f207210', $Cache, $page, 'plugins/auto/comments/v3.5.0/comments-list.html');
}
?>