<?php

/*
 * Squelette : plugins/auto/z/v1.7.31/head/page-plan.html
 * Date :      Tue, 07 Nov 2017 17:27:36 GMT
 * Compile :   Wed, 17 Jun 2020 07:10:32 GMT
 * Boucles :   
 */ 
//
// Fonction principale du squelette plugins/auto/z/v1.7.31/head/page-plan.html
// Temps de compilation total: 0.148 ms
//

function html_7635b87d02a2a362eb9edca877786fcf($Cache, $Pile, $doublons = array(), $Numrows = array(), $SP = 0) {

	if (isset($Pile[0]["doublons"]) AND is_array($Pile[0]["doublons"]))
		$doublons = nettoyer_env_doublons($Pile[0]["doublons"]);

	$connect = '';
	$page = (
'<title>' .
_T('public|spip|ecrire:plan_site') .
' - ' .
interdire_scripts(textebrut(typo($GLOBALS['meta']['nom_site'], "TYPO", $connect, $Pile[0]))) .
'</title>
' .
(($t1 = strval(interdire_scripts(attribut_html(couper(propre($GLOBALS['meta']['descriptif_site'], $connect, $Pile[0]),'150')))))!=='' ?
		('<meta name="description" content="' . $t1 . '" />') :
		'') .
'
<meta name="robots" content="none" />
' .
(($t1 = strval(find_in_path('favicon.ico')))!=='' ?
		('<link rel="icon" type="image/x-icon" href="' . $t1 . (	'" />
' .
	(($t2 = strval(find_in_path('favicon.ico')))!=='' ?
			('<link rel="shortcut icon" type="image/x-icon" href="' . $t2 . '" />') :
			''))) :
		'') .
'
');

	return analyse_resultat_skel('html_7635b87d02a2a362eb9edca877786fcf', $Cache, $page, 'plugins/auto/z/v1.7.31/head/page-plan.html');
}
?>