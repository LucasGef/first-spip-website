<?php

/*
 * Squelette : plugins/auto/theme_keepitsimple/v1.0.3/inc-theme-copyleft.html
 * Date :      Tue, 17 Mar 2020 15:26:48 GMT
 * Compile :   Wed, 17 Jun 2020 06:56:17 GMT
 * Boucles :   
 */ 
//
// Fonction principale du squelette plugins/auto/theme_keepitsimple/v1.0.3/inc-theme-copyleft.html
// Temps de compilation total: 0.044 ms
//

function html_edff2e075e18d517631cc92e3fc7114d($Cache, $Pile, $doublons = array(), $Numrows = array(), $SP = 0) {

	if (isset($Pile[0]["doublons"]) AND is_array($Pile[0]["doublons"]))
		$doublons = nettoyer_env_doublons($Pile[0]["doublons"]);

	$connect = '';
	$page = (
'<div id="copyleft">
	' .
_T('zpip:conception_graphique_par') .
' <a href=\'http://www.styleshout.com/\'>styleshout</a> ' .
_T('zpip:sous_licence') .
' <a href=\'http://creativecommons.org/licenses/by/2.5/\'>Creative Commons Attribution 2.5 License</a>
</div>');

	return analyse_resultat_skel('html_edff2e075e18d517631cc92e3fc7114d', $Cache, $page, 'plugins/auto/theme_keepitsimple/v1.0.3/inc-theme-copyleft.html');
}
?>