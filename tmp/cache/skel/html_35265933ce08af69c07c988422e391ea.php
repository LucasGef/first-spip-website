<?php

/*
 * Squelette : plugins/auto/zcore/v2.8.7/inclure/resume/article.html
 * Date :      Mon, 30 Mar 2020 16:36:24 GMT
 * Compile :   Wed, 17 Jun 2020 09:07:12 GMT
 * Boucles :   _nb_commentaires, _combien, _resume_article
 */ 

function BOUCLE_nb_commentaireshtml_35265933ce08af69c07c988422e391ea(&$Cache, &$Pile, &$doublons, &$Numrows, $SP) {

	static $command = array();
	static $connect;
	$command['connect'] = $connect = '';
	if (!isset($command['table'])) {
		$command['table'] = 'forum';
		$command['id'] = '_nb_commentaires';
		$command['from'] = array('forum' => 'spip_forum');
		$command['type'] = array();
		$command['groupby'] = array();
		$command['select'] = array("count(*)");
		$command['orderby'] = array();
		$command['join'] = array();
		$command['limit'] = '';
		$command['having'] = 
			array();
	}
	$command['where'] = 
			array(
quete_condition_statut('forum.statut','publie,prop','publie',''), 
			array('=', 'forum.id_objet', sql_quote($Pile[$SP]['id_article'], '','bigint(21) NOT NULL DEFAULT \'0\'')), 
			array('=', 'forum.objet', sql_quote('article')));
	if (defined("_BOUCLE_PROFILER")) $timer = time()+(float)microtime();
	$t0 = "";
	// REQUETE
	$iter = IterFactory::create(
		"SQL",
		$command,
		array('plugins/auto/zcore/v2.8.7/inclure/resume/article.html','html_35265933ce08af69c07c988422e391ea','_nb_commentaires',14,$GLOBALS['spip_lang'])
	);
	if (!$iter->err()) {
	$Numrows['_nb_commentaires']['total'] = @intval($iter->count());
	$SP++;
	// RESULTATS
	
	$iter->free();
	}
	if (defined("_BOUCLE_PROFILER")
	AND 1000*($timer = (time()+(float)microtime())-$timer) > _BOUCLE_PROFILER)
		spip_log(intval(1000*$timer)."ms BOUCLE_nb_commentaires @ plugins/auto/zcore/v2.8.7/inclure/resume/article.html","profiler"._LOG_AVERTISSEMENT);
	return $t0;
}


function BOUCLE_combienhtml_35265933ce08af69c07c988422e391ea(&$Cache, &$Pile, &$doublons, &$Numrows, $SP) {

	static $command = array();
	static $connect;
	$command['connect'] = $connect = '';
	if (!isset($command['table'])) {
		$command['table'] = 'signatures';
		$command['id'] = '_combien';
		$command['from'] = array('signatures' => 'spip_signatures','L1' => 'spip_petitions');
		$command['type'] = array();
		$command['groupby'] = array();
		$command['select'] = array("count(*)");
		$command['orderby'] = array();
		$command['join'] = array('L1' => array('signatures','id_petition'));
		$command['limit'] = '';
		$command['having'] = 
			array();
	}
	$command['where'] = 
			array(
quete_condition_statut('signatures.statut','publie','publie',''), 
			array('=', 'L1.id_article', sql_quote($Pile[$SP]['id_article'], '','bigint(21) NOT NULL DEFAULT \'0\'')));
	if (defined("_BOUCLE_PROFILER")) $timer = time()+(float)microtime();
	$t0 = "";
	// REQUETE
	$iter = IterFactory::create(
		"SQL",
		$command,
		array('plugins/auto/zcore/v2.8.7/inclure/resume/article.html','html_35265933ce08af69c07c988422e391ea','_combien',21,$GLOBALS['spip_lang'])
	);
	if (!$iter->err()) {
	$Numrows['_combien']['total'] = @intval($iter->count());
	$SP++;
	// RESULTATS
	
	$iter->free();
	}
	if (defined("_BOUCLE_PROFILER")
	AND 1000*($timer = (time()+(float)microtime())-$timer) > _BOUCLE_PROFILER)
		spip_log(intval(1000*$timer)."ms BOUCLE_combien @ plugins/auto/zcore/v2.8.7/inclure/resume/article.html","profiler"._LOG_AVERTISSEMENT);
	return $t0;
}


function BOUCLE_resume_articlehtml_35265933ce08af69c07c988422e391ea(&$Cache, &$Pile, &$doublons, &$Numrows, $SP) {

	static $command = array();
	static $connect;
	$command['connect'] = $connect = '';
	$in = array();
	if (!(is_array($a = (@$Pile[0]['statut']))))
		$in[]= $a;
	else $in = array_merge($in, $a);
	if (!isset($command['table'])) {
		$command['table'] = 'articles';
		$command['id'] = '_resume_article';
		$command['from'] = array('articles' => 'spip_articles');
		$command['type'] = array();
		$command['groupby'] = array();
		$command['select'] = array("articles.id_article",
		"articles.id_rubrique",
		"articles.titre",
		"articles.date",
		"articles.texte",
		"articles.descriptif",
		"articles.chapo",
		"articles.lang");
		$command['orderby'] = array();
		$command['join'] = array();
		$command['limit'] = '';
		$command['having'] = 
			array();
	}
	$command['where'] = 
			array(
			array('=', 'articles.id_article', sql_quote(@$Pile[0]['id_article'], '','bigint(21) NOT NULL AUTO_INCREMENT')), (!(is_array(@$Pile[0]['statut'])?count(@$Pile[0]['statut']):strlen(@$Pile[0]['statut'])) ? '' : ((is_array(@$Pile[0]['statut'])) ? sql_in('articles.statut',sql_quote($in)) : 
			array('=', 'articles.statut', sql_quote(@$Pile[0]['statut'], '','varchar(10) NOT NULL DEFAULT \'0\'')))));
	if (defined("_BOUCLE_PROFILER")) $timer = time()+(float)microtime();
	$t0 = "";
	// REQUETE
	$iter = IterFactory::create(
		"SQL",
		$command,
		array('plugins/auto/zcore/v2.8.7/inclure/resume/article.html','html_35265933ce08af69c07c988422e391ea','_resume_article',7,$GLOBALS['spip_lang'])
	);
	if (!$iter->err()) {
	lang_select($GLOBALS['spip_lang']);
	$SP++;
	// RESULTATS
	while ($Pile[$SP]=$iter->fetch()) {

		lang_select_public($Pile[$SP]['lang'], '', $Pile[$SP]['titre']);
		$t0 .= (
'
<article class="entry article hentry">
	<strong class="h3-like entry-title"><a href="' .
vider_url(urlencode_1738(generer_url_entite($Pile[$SP]['id_article'], 'article', '', '', true))) .
'" rel="bookmark">' .
interdire_scripts(responsive_logo(filtrer('image_graver', filtrer('image_reduire',(entites_html(sinon(table_valeur(@$Pile[0], (string)'logo_rubrique', null), ' '),true) ? 
((!is_array($l = quete_logo('id_article', 'ON', $Pile[$SP]['id_article'],$Pile[$SP]['id_rubrique'], 0))) ? '':
 ("<img class=\"spip_logo spip_logos\" alt=\"\" src=\"$l[0]\"" . $l[2] .  ($l[1] ? " onmouseover=\"this.src='$l[1]'\" onmouseout=\"this.src='$l[0]'\"" : "") . ' />')):
((!is_array($l = quete_logo('id_article', 'ON', $Pile[$SP]['id_article'],'', 0))) ? '':
 ("<img class=\"spip_logo spip_logos\" alt=\"\" src=\"$l[0]\"" . $l[2] .  ($l[1] ? " onmouseover=\"this.src='$l[1]'\" onmouseout=\"this.src='$l[0]'\"" : "") . ' />'))),interdire_scripts(entites_html(sinon(table_valeur(@$Pile[0], (string)'largeur_logo', null), '-1'),true)),interdire_scripts(entites_html(sinon(table_valeur(@$Pile[0], (string)'hauteur_logo', null), '-1'),true)))))) .
interdire_scripts(supprimer_numero(typo($Pile[$SP]['titre']), "TYPO", $connect, $Pile[0])) .
'<span
		class="read-more hide">' .
afficher_icone_svg('chevron-right', 'icon-heavy', (($t2 = strval(interdire_scripts(supprimer_numero(typo($Pile[$SP]['titre']), "TYPO", $connect, $Pile[0]))))!=='' ?
			((	_T('zcore:lire_la_suite') .
		_T('zcore:lire_la_suite_de') .
		'&laquo;') . $t2 . '&raquo;') :
			'')) .
'</span></a></strong>
	<p class="publication">' .
(($t1 = strval(interdire_scripts(affdate_jourcourt(normaliser_date($Pile[$SP]['date'])))))!=='' ?
		((	'<time pubdate="pubdate" datetime="' .
	interdire_scripts(date_iso(normaliser_date($Pile[$SP]['date']))) .
	'">' .
	afficher_icone_svg('calendar', '', '')) . $t1 . '</time>') :
		'') .
(($t1 = strval(recuperer_fond('modeles/lesauteurs', array('objet'=>'article','id_objet' => $Pile[$SP]['id_article'],'id_article' => $Pile[$SP]['id_article']), array('trim'=>true, 'compil'=>array('plugins/auto/zcore/v2.8.7/inclure/resume/article.html','html_35265933ce08af69c07c988422e391ea','_resume_article',8,$GLOBALS['spip_lang'])), '')))!=='' ?
		((	'<span class="authors"><span class="sep">, </span>' .
	afficher_icone_svg('user', '', '') .
	_T('public|spip|ecrire:par_auteur') .
	' ') . $t1 . '</span>') :
		'') .
'</p>
	' .
(($t1 = strval(interdire_scripts(filtre_introduction($Pile[$SP]['descriptif'], (strlen($Pile[$SP]['descriptif']))
		? ''
		: $Pile[$SP]['chapo'] . "\n\n" . $Pile[$SP]['texte'], is_numeric(interdire_scripts(entites_html(sinon(table_valeur(@$Pile[0], (string)'coupe', null), '300'),true)))?intval(interdire_scripts(entites_html(sinon(table_valeur(@$Pile[0], (string)'coupe', null), '300'),true))):500, $connect, !is_numeric(interdire_scripts(entites_html(sinon(table_valeur(@$Pile[0], (string)'coupe', null), '300'),true)))?interdire_scripts(entites_html(sinon(table_valeur(@$Pile[0], (string)'coupe', null), '300'),true)):null))))!=='' ?
		((	'<div class="introduction entry-content">') . $t1 . '</div>') :
		'') .
'
	<p class="postmeta">
	' .
BOUCLE_nb_commentaireshtml_35265933ce08af69c07c988422e391ea($Cache, $Pile, $doublons, $Numrows, $SP)
. (	(($Numrows['_nb_commentaires']['total'])  ?
			(' ' . (	'
		<span class="comments">
		<span class="sep">|</span>
		<a' .
		(($t3 = strval(ancre_url(vider_url(urlencode_1738(generer_url_entite($Pile[$SP]['id_article'], 'article', '', '', true))),'comments')))!=='' ?
				(' href="' . $t3 . '"') :
				'') .
		' class="nb_commentaires" ' .
		(($t3 = strval(attribut_html(singulier_ou_pluriel($Numrows['_nb_commentaires']['total'],'zcore:info_1_commentaire','zcore:info_nb_commentaires'))))!=='' ?
				('title="' . $t3 . '"') :
				'') .
		'>' .
		(($t3 = strval($Numrows['_nb_commentaires']['total']))!=='' ?
				(afficher_icone_svg('comment', '', '') . $t3) :
				'') .
		'</a>
		</span>
		')) :
			'') .
	'
	') .
'
	' .
BOUCLE_combienhtml_35265933ce08af69c07c988422e391ea($Cache, $Pile, $doublons, $Numrows, $SP)
. (	(($Numrows['_combien']['total'])  ?
			(' ' . (	'
		<span class="signatures">
		<span class="sep">|</span>
		<a' .
		(($t3 = strval(ancre_url(vider_url(urlencode_1738(generer_url_entite($Pile[$SP]['id_article'], 'article', '', '', true))),'petition')))!=='' ?
				(' href="' . $t3 . '"') :
				'') .
		' class="nb_signatures" ' .
		(($t3 = strval(attribut_html(singulier_ou_pluriel($Numrows['_combien']['total'],'zcore:info_1_signature','zcore:info_nb_signatures'))))!=='' ?
				('title="' . $t3 . '"') :
				'') .
		'>' .
		(($t3 = strval($Numrows['_combien']['total']))!=='' ?
				(afficher_icone_svg('ok-circle', '', '') . $t3) :
				'') .
		'</a>
		</span>
		')) :
			'') .
	'
	') .
'
	</p>
</article>
');
		lang_select();
	}
	lang_select();
	$iter->free();
	}
	if (defined("_BOUCLE_PROFILER")
	AND 1000*($timer = (time()+(float)microtime())-$timer) > _BOUCLE_PROFILER)
		spip_log(intval(1000*$timer)."ms BOUCLE_resume_article @ plugins/auto/zcore/v2.8.7/inclure/resume/article.html","profiler"._LOG_AVERTISSEMENT);
	return $t0;
}

//
// Fonction principale du squelette plugins/auto/zcore/v2.8.7/inclure/resume/article.html
// Temps de compilation total: 5.653 ms
//

function html_35265933ce08af69c07c988422e391ea($Cache, $Pile, $doublons = array(), $Numrows = array(), $SP = 0) {

	if (isset($Pile[0]["doublons"]) AND is_array($Pile[0]["doublons"]))
		$doublons = nettoyer_env_doublons($Pile[0]["doublons"]);

	$connect = '';
	$page = (
'
' .
BOUCLE_resume_articlehtml_35265933ce08af69c07c988422e391ea($Cache, $Pile, $doublons, $Numrows, $SP));

	return analyse_resultat_skel('html_35265933ce08af69c07c988422e391ea', $Cache, $page, 'plugins/auto/zcore/v2.8.7/inclure/resume/article.html');
}
?>