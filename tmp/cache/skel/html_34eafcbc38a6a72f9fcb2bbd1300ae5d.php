<?php

/*
 * Squelette : ../plugins-dist/petitions/prive/configurer/petitionner.html
 * Date :      Tue, 16 Jun 2020 14:01:54 GMT
 * Compile :   Wed, 17 Jun 2020 08:37:25 GMT
 * Boucles :   
 */ 
//
// Fonction principale du squelette ../plugins-dist/petitions/prive/configurer/petitionner.html
// Temps de compilation total: 1.375 ms
//

function html_34eafcbc38a6a72f9fcb2bbd1300ae5d($Cache, $Pile, $doublons = array(), $Numrows = array(), $SP = 0) {

	if (isset($Pile[0]["doublons"]) AND is_array($Pile[0]["doublons"]))
		$doublons = nettoyer_env_doublons($Pile[0]["doublons"]);

	$connect = '';
	$page = (
'<div class=\'ajax\'>
' .
executer_balise_dynamique('FORMULAIRE_ACTIVER_PETITION_ARTICLE',
	array(@$Pile[0]['id_article']),
	array('../plugins-dist/petitions/prive/configurer/petitionner.html','html_34eafcbc38a6a72f9fcb2bbd1300ae5d','',2,$GLOBALS['spip_lang'])) .
'</div>');

	return analyse_resultat_skel('html_34eafcbc38a6a72f9fcb2bbd1300ae5d', $Cache, $page, '../plugins-dist/petitions/prive/configurer/petitionner.html');
}
?>