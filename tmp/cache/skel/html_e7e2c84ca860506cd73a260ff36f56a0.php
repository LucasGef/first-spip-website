<?php

/*
 * Squelette : plugins/auto/comments/v3.5.0/comments-feed.html
 * Date :      Thu, 26 Mar 2020 19:57:32 GMT
 * Compile :   Wed, 17 Jun 2020 09:02:58 GMT
 * Boucles :   
 */ 
//
// Fonction principale du squelette plugins/auto/comments/v3.5.0/comments-feed.html
// Temps de compilation total: 0.300 ms
//

function html_e7e2c84ca860506cd73a260ff36f56a0($Cache, $Pile, $doublons = array(), $Numrows = array(), $SP = 0) {

	if (isset($Pile[0]["doublons"]) AND is_array($Pile[0]["doublons"]))
		$doublons = nettoyer_env_doublons($Pile[0]["doublons"]);

	$connect = '';
	$page = (($t1 = strval(((spip_htmlspecialchars(
		// refus des forums ?
		(quete_accepter_forum(@$Pile[0]['id_article'])=="non" OR
		($GLOBALS["meta"]["forums_publics"] == "non"
		AND quete_accepter_forum(@$Pile[0]['id_article']) == ""))
		? "" : // sinon:
		(calcul_parametres_forum($Pile[0],null,null,null).
	(($lien = (_request("retour") ? _request("retour") : str_replace("&amp;", "&", ''))) ? "&retour=".rawurlencode($lien) : "")))) ?' ' :'')))!=='' ?
		($t1 . (	'<p class="comments-feed">' .
	_T('comments:lien_suivre_commentaires') .
	'
<a href="' .
	interdire_scripts(parametre_url(parametre_url(parametre_url(parametre_url(generer_url_public('comments-rss', ''),'id_rubrique',interdire_scripts(entites_html(table_valeur(@$Pile[0], (string)'id_rubrique', null),true))),'objet',interdire_scripts(entites_html(table_valeur(@$Pile[0], (string)'objet', null),true))),'id_objet',interdire_scripts(entites_html(table_valeur(@$Pile[0], (string)'id_objet', null),true))),'id_article',@$Pile[0]['id_article'])) .
	'" rel="nofollow"><img src="' .
	find_in_path('feed/rss.png') .
	'" alt="RSS 2.0" /></a><span class="sep">
|
</span><a href="' .
	interdire_scripts(parametre_url(parametre_url(parametre_url(parametre_url(generer_url_public('comments-atom', ''),'id_rubrique',interdire_scripts(entites_html(table_valeur(@$Pile[0], (string)'id_rubrique', null),true))),'objet',interdire_scripts(entites_html(table_valeur(@$Pile[0], (string)'objet', null),true))),'id_objet',interdire_scripts(entites_html(table_valeur(@$Pile[0], (string)'id_objet', null),true))),'id_article',@$Pile[0]['id_article'])) .
	'" rel="nofollow"><img src="' .
	find_in_path('feed/atom.png') .
	'" alt="Atom" /></a></p>')) :
		'');

	return analyse_resultat_skel('html_e7e2c84ca860506cd73a260ff36f56a0', $Cache, $page, 'plugins/auto/comments/v3.5.0/comments-feed.html');
}
?>