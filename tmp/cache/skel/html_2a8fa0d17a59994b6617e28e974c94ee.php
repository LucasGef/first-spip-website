<?php

/*
 * Squelette : ../plugins-dist/svp/prive/squelettes/contenu/charger_plugin.html
 * Date :      Tue, 16 Jun 2020 14:01:55 GMT
 * Compile :   Wed, 17 Jun 2020 07:19:03 GMT
 * Boucles :   
 */ 
//
// Fonction principale du squelette ../plugins-dist/svp/prive/squelettes/contenu/charger_plugin.html
// Temps de compilation total: 4.216 ms
//

function html_2a8fa0d17a59994b6617e28e974c94ee($Cache, $Pile, $doublons = array(), $Numrows = array(), $SP = 0) {

	if (isset($Pile[0]["doublons"]) AND is_array($Pile[0]["doublons"]))
		$doublons = nettoyer_env_doublons($Pile[0]["doublons"]);

	$connect = '';
	$page = (
invalideur_session($Cache, sinon_interdire_acces(((function_exists("autoriser")||include_spip("inc/autoriser"))&&autoriser('ajouter', '_plugins')?" ":""))) .
'
<h1 class="grostitre">' .
_T('public|spip|ecrire:icone_admin_plugin') .
'</h1>



' .
barre_onglets('plugins','charger_plugin') .
'



<div class="onglets_simple second clearfix">
	<ul>
		<li><strong>' .
_T('svp:titre_plugins') .
'</strong></li>
		<li><a' .
(($t1 = strval(generer_url_ecrire('depots')))!=='' ?
		(' href="' . $t1 . '"') :
		'') .
'>' .
_T('svp:titre_depots') .
'</a></li>
	</ul>
</div>


' .
(!(test_plugins_auto(''))  ?
		(' ' . (	'
<div class=\'notice\'>
	<h3>' .
	_T('svp:erreur_dir_plugins_auto_titre') .
	'</h3>
	' .
	_T('svp:erreur_dir_plugins_auto') .
	'
</div>')) :
		'') .
'


<div class="ajax">
	' .
executer_balise_dynamique('FORMULAIRE_CHARGER_PLUGIN',
	array(),
	array('../plugins-dist/svp/prive/squelettes/contenu/charger_plugin.html','html_2a8fa0d17a59994b6617e28e974c94ee','',22,$GLOBALS['spip_lang'])) .
'
</div>

' .
((test_plugins_auto(''))  ?
		(' ' . (	'
<div class=\'ajax\'>
	' .
	executer_balise_dynamique('FORMULAIRE_CHARGER_PLUGIN_ARCHIVE',
	array(),
	array('../plugins-dist/svp/prive/squelettes/contenu/charger_plugin.html','html_2a8fa0d17a59994b6617e28e974c94ee','',23,$GLOBALS['spip_lang'])) .
	'
</div>')) :
		'') .
'
');

	return analyse_resultat_skel('html_2a8fa0d17a59994b6617e28e974c94ee', $Cache, $page, '../plugins-dist/svp/prive/squelettes/contenu/charger_plugin.html');
}
?>