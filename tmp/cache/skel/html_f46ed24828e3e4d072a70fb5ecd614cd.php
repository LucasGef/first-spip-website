<?php

/*
 * Squelette : plugins/auto/z/v1.7.31/contenu/page-sommaire.html
 * Date :      Tue, 07 Nov 2017 17:27:36 GMT
 * Compile :   Tue, 16 Jun 2020 14:20:21 GMT
 * Boucles :   _articles
 */ 

function BOUCLE_articleshtml_f46ed24828e3e4d072a70fb5ecd614cd(&$Cache, &$Pile, &$doublons, &$Numrows, $SP) {

	static $command = array();
	static $connect;
	$command['connect'] = $connect = '';
	$command['pagination'] = array((isset($Pile[0]['debut_articles']) ? $Pile[0]['debut_articles'] : null), 5);
	if (!isset($command['table'])) {
		$command['table'] = 'articles';
		$command['id'] = '_articles';
		$command['from'] = array('articles' => 'spip_articles');
		$command['type'] = array();
		$command['groupby'] = array();
		$command['select'] = array("articles.date",
		"articles.id_article",
		"articles.lang",
		"articles.titre");
		$command['orderby'] = array('articles.date DESC');
		$command['where'] = 
			array(
quete_condition_statut('articles.statut','publie,prop,prepa/auteur','publie',''), 
quete_condition_postdates('articles.date',''));
		$command['join'] = array();
		$command['limit'] = '';
		$command['having'] = 
			array();
	}
	if (defined("_BOUCLE_PROFILER")) $timer = time()+(float)microtime();
	$t0 = "";
	// REQUETE
	$iter = IterFactory::create(
		"SQL",
		$command,
		array('plugins/auto/z/v1.7.31/contenu/page-sommaire.html','html_f46ed24828e3e4d072a70fb5ecd614cd','_articles',9,$GLOBALS['spip_lang'])
	);
	if (!$iter->err()) {
	
	// COMPTEUR
	$Numrows['_articles']['compteur_boucle'] = 0;
	$Numrows['_articles']['total'] = @intval($iter->count());
	$debut_boucle = isset($Pile[0]['debut_articles']) ? $Pile[0]['debut_articles'] : _request('debut_articles');
	if(substr($debut_boucle,0,1)=='@'){
		$debut_boucle = $Pile[0]['debut_articles'] = quete_debut_pagination('id_article',$Pile[0]['@id_article'] = substr($debut_boucle,1),5,$iter);
		$iter->seek(0);
	}
	$debut_boucle = intval($debut_boucle);
	$debut_boucle = (($tout=($debut_boucle == -1))?0:($debut_boucle));
	$debut_boucle = max(0,min($debut_boucle,floor(($Numrows['_articles']['total']-1)/(5))*(5)));
	$debut_boucle = intval($debut_boucle);
	$fin_boucle = min(($tout ? $Numrows['_articles']['total'] : $debut_boucle + 4), $Numrows['_articles']['total'] - 1);
	$Numrows['_articles']['grand_total'] = $Numrows['_articles']['total'];
	$Numrows['_articles']["total"] = max(0,$fin_boucle - $debut_boucle + 1);
	if ($debut_boucle>0 AND $debut_boucle < $Numrows['_articles']['grand_total'] AND $iter->seek($debut_boucle,'continue'))
		$Numrows['_articles']['compteur_boucle'] = $debut_boucle;
	
	lang_select($GLOBALS['spip_lang']);
	$SP++;
	// RESULTATS
	while ($Pile[$SP]=$iter->fetch()) {

		$Numrows['_articles']['compteur_boucle']++;
		if ($Numrows['_articles']['compteur_boucle'] <= $debut_boucle) continue;
		if ($Numrows['_articles']['compteur_boucle']-1 > $fin_boucle) break;
		lang_select_public($Pile[$SP]['lang'], '', $Pile[$SP]['titre']);
		$t0 .= (
'
		' .
recuperer_fond( 'inclure/article-resume' , array('id_article' => $Pile[$SP]['id_article'] ), array('compil'=>array('plugins/auto/z/v1.7.31/contenu/page-sommaire.html','html_f46ed24828e3e4d072a70fb5ecd614cd','_articles',15,$GLOBALS['spip_lang'])), _request('connect')));
		lang_select();
	}
	lang_select();
	$iter->free();
	}
	if (defined("_BOUCLE_PROFILER")
	AND 1000*($timer = (time()+(float)microtime())-$timer) > _BOUCLE_PROFILER)
		spip_log(intval(1000*$timer)."ms BOUCLE_articles @ plugins/auto/z/v1.7.31/contenu/page-sommaire.html","profiler"._LOG_AVERTISSEMENT);
	return $t0;
}

//
// Fonction principale du squelette plugins/auto/z/v1.7.31/contenu/page-sommaire.html
// Temps de compilation total: 0.299 ms
//

function html_f46ed24828e3e4d072a70fb5ecd614cd($Cache, $Pile, $doublons = array(), $Numrows = array(), $SP = 0) {

	if (isset($Pile[0]["doublons"]) AND is_array($Pile[0]["doublons"]))
		$doublons = nettoyer_env_doublons($Pile[0]["doublons"]);

	$connect = '';
	$page = (
'<div class="contenu-principal">
	<div class="cartouche invisible">
		<h1 class="h1 invisible">' .
interdire_scripts(typo($GLOBALS['meta']['nom_site'], "TYPO", $connect, $Pile[0])) .
'</h1>
	</div>

	' .
(($t1 = strval(interdire_scripts(propre($GLOBALS['meta']['descriptif_site'], $connect, $Pile[0]))))!=='' ?
		('<div id="descriptif_site_spip">' . $t1 . '</div>') :
		'') .
'
</div>

' .
(($t1 = BOUCLE_articleshtml_f46ed24828e3e4d072a70fb5ecd614cd($Cache, $Pile, $doublons, $Numrows, $SP))!=='' ?
		((	'
<div class="liste articles sommaire">
	' .
		filtre_pagination_dist($Numrows["_articles"]["grand_total"],
 		'_articles',
		isset($Pile[0]['debut_articles'])?$Pile[0]['debut_articles']:intval(_request('debut_articles')),
		5, false, '', '', array()) .
		'
	<h2 class="h2">' .
		_T('public|spip|ecrire:derniers_articles') .
		'</h2>
	<ul class="liste-items">
		') . $t1 . (	'
	</ul>
	' .
		(($t3 = strval(filtre_pagination_dist($Numrows["_articles"]["grand_total"],
 		'_articles',
		isset($Pile[0]['debut_articles'])?$Pile[0]['debut_articles']:intval(_request('debut_articles')),
		5, true, '', '', array())))!=='' ?
				('<p class="pagination">' . $t3 . '</p>') :
				'') .
		'
</div>
')) :
		''));

	return analyse_resultat_skel('html_f46ed24828e3e4d072a70fb5ecd614cd', $Cache, $page, 'plugins/auto/z/v1.7.31/contenu/page-sommaire.html');
}
?>