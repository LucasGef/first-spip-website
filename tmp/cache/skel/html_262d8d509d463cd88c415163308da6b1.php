<?php

/*
 * Squelette : plugins/auto/comments/v3.5.0/inclure/forum.html
 * Date :      Thu, 26 Mar 2020 19:57:32 GMT
 * Compile :   Wed, 17 Jun 2020 09:02:58 GMT
 * Boucles :   
 */ 
//
// Fonction principale du squelette plugins/auto/comments/v3.5.0/inclure/forum.html
// Temps de compilation total: 6.189 ms
//

function html_262d8d509d463cd88c415163308da6b1($Cache, $Pile, $doublons = array(), $Numrows = array(), $SP = 0) {

	if (isset($Pile[0]["doublons"]) AND is_array($Pile[0]["doublons"]))
		$doublons = nettoyer_env_doublons($Pile[0]["doublons"]);

	$connect = '';
	$page = (
'<div class="comments">
	<a href="#comments" id="comments"></a> 
	<a href="#forum" id="forum"></a> 
	' .

'<'.'?php echo recuperer_fond( ' . argumenter_squelette((	'comments-' .
	interdire_scripts(replace((include_spip('inc/config')?lire_config('comments/comments_fil','list',false):''),'-','')))) . ', array_merge('.var_export($Pile[0],1).',array(\'lang\' => ' . argumenter_squelette($GLOBALS["spip_lang"]) . ')), array("compil"=>array(\'plugins/auto/comments/v3.5.0/inclure/forum.html\',\'html_262d8d509d463cd88c415163308da6b1\',\'\',4,$GLOBALS[\'spip_lang\']),\'ajax\' => ($v=( ' . argumenter_squelette(@$Pile[0]['ajax']) . '))?$v:true), _request("connect"));
?'.'>' .
(($t1 = strval(interdire_scripts(((entites_html(table_valeur(@$Pile[0], (string)'repondre_url', null),true)) ?' ' :''))))!=='' ?
		($t1 . (	'
	' .
	(($t2 = strval(executer_balise_dynamique('FORMULAIRE_FORUM',
	array(@$Pile[0][''],@$Pile[0]['id_forum'],@$Pile[0]['ajouter_mot'],@$Pile[0]['ajouter_groupe'],@$Pile[0]['forcer_previsu'],@$Pile[0]['id_article'],@$Pile[0]['id_breve'],@$Pile[0]['id_rubrique'],@$Pile[0]['id_syndic'],self(),interdire_scripts(entites_html(table_valeur(@$Pile[0], (string)'objet', null),true)),interdire_scripts(entites_html(table_valeur(@$Pile[0], (string)'id_objet', null),true))),
	array('plugins/auto/comments/v3.5.0/inclure/forum.html','html_262d8d509d463cd88c415163308da6b1','',0,$GLOBALS['spip_lang'], '',4))))!=='' ?
			((	'<div class="comment-form">
		' .
		(($t3 = strval(interdire_scripts(trim(sinon(table_valeur(@$Pile[0], (string)'repondre_titre', null), _T('public|spip|ecrire:repondre_article'))))))!=='' ?
				('<h2 class="h2">' . $t3 . '</h2>') :
				'') .
		'
		') . $t2 . '
	</div>') :
			'') .
	'
	' .
	(($t2 = strval(((spip_htmlspecialchars(
		// refus des forums ?
		(quete_accepter_forum(@$Pile[0]['id_article'])=="non" OR
		($GLOBALS["meta"]["forums_publics"] == "non"
		AND quete_accepter_forum(@$Pile[0]['id_article']) == ""))
		? "" : // sinon:
		(calcul_parametres_forum($Pile[0],null,null,null).
	(($lien = (_request("retour") ? _request("retour") : str_replace("&amp;", "&", ''))) ? "&retour=".rawurlencode($lien) : "")))) ?' ' :'')))!=='' ?
			($t2 . 
'<'.'?php echo recuperer_fond( ' . argumenter_squelette('comments-feed') . ', array_merge('.var_export($Pile[0],1).',array(\'lang\' => ' . argumenter_squelette($GLOBALS["spip_lang"]) . ')), array("compil"=>array(\'plugins/auto/comments/v3.5.0/inclure/forum.html\',\'html_262d8d509d463cd88c415163308da6b1\',\'\',9,$GLOBALS[\'spip_lang\'])), _request("connect"));
?'.'>') :
			'') .
	'
')) :
		'') .
'</div>');

	return analyse_resultat_skel('html_262d8d509d463cd88c415163308da6b1', $Cache, $page, 'plugins/auto/comments/v3.5.0/inclure/forum.html');
}
?>