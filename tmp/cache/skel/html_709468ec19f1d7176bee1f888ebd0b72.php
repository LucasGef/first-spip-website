<?php

/*
 * Squelette : plugins/auto/spipr_dist/v2.2.6/cartouche/plan.html
 * Date :      Mon, 30 Mar 2020 09:25:24 GMT
 * Compile :   Wed, 17 Jun 2020 07:41:25 GMT
 * Boucles :   
 */ 
//
// Fonction principale du squelette plugins/auto/spipr_dist/v2.2.6/cartouche/plan.html
// Temps de compilation total: 0.033 ms
//

function html_709468ec19f1d7176bee1f888ebd0b72($Cache, $Pile, $doublons = array(), $Numrows = array(), $SP = 0) {

	if (isset($Pile[0]["doublons"]) AND is_array($Pile[0]["doublons"]))
		$doublons = nettoyer_env_doublons($Pile[0]["doublons"]);

	$connect = '';
	$page = (
'<header class="cartouche">
	<h1>' .
_T('public|spip|ecrire:plan_site') .
'</h1>
</header>
');

	return analyse_resultat_skel('html_709468ec19f1d7176bee1f888ebd0b72', $Cache, $page, 'plugins/auto/spipr_dist/v2.2.6/cartouche/plan.html');
}
?>