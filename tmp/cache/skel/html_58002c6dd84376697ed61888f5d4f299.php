<?php

/*
 * Squelette : plugins/auto/comments/v3.5.0/formulaires/inc-login_forum.html
 * Date :      Thu, 26 Mar 2020 19:57:32 GMT
 * Compile :   Wed, 17 Jun 2020 09:02:58 GMT
 * Boucles :   
 */ 
//
// Fonction principale du squelette plugins/auto/comments/v3.5.0/formulaires/inc-login_forum.html
// Temps de compilation total: 1.570 ms
//

function html_58002c6dd84376697ed61888f5d4f299($Cache, $Pile, $doublons = array(), $Numrows = array(), $SP = 0) {

	if (isset($Pile[0]["doublons"]) AND is_array($Pile[0]["doublons"]))
		$doublons = nettoyer_env_doublons($Pile[0]["doublons"]);

	$connect = '';
	$page = (
'
<fieldset class="qui' .
(($t1 = strval(interdire_scripts(invalideur_session($Cache, (table_valeur($GLOBALS["visiteur_session"], (string)'auth', null) ? 'session_qui':'saisie_qui')))))!=='' ?
		(' ' . $t1) :
		'') .
'">
	<legend>' .
_T('comments:forum_qui_etes_vous') .
'</legend>
' .
(($t1 = strval(interdire_scripts(invalideur_session($Cache, ((table_valeur($GLOBALS["visiteur_session"], (string)'statut', null)) ?' ' :'')))))!=='' ?
		($t1 . (	'
	' .
	(($t2 = strval(interdire_scripts(invalideur_session($Cache, typo(table_valeur($GLOBALS["visiteur_session"], (string)'nom', null))))))!=='' ?
			((	'<p class="explication"><span class="label">' .
		_T('comments:label_nom') .
		'</span> <strong class="session_nom">') . $t2 . (	'</strong> <span class="details">&#91;<a href="' .
		executer_balise_dynamique('URL_LOGOUT',
	array(),
	array('plugins/auto/comments/v3.5.0/formulaires/inc-login_forum.html','html_58002c6dd84376697ed61888f5d4f299','',4,$GLOBALS['spip_lang'])) .
		'" rel="nofollow">' .
		_T('public|spip|ecrire:icone_deconnecter') .
		'</a>&#93;</span></p>')) :
			'') .
	'
')) :
		'') .
'
' .
(($t1 = strval(interdire_scripts(invalideur_session($Cache, ((table_valeur($GLOBALS["visiteur_session"], (string)'statut', null)) ?'' :' ')))))!=='' ?
		($t1 . (	'
	<ul>
		<li class=\'editer saisie_session_nom' .
	((table_valeur(table_valeur(@$Pile[0], (string)'erreurs', null),'session_nom'))  ?
			(' ' . ' ' . 'erreur') :
			'') .
	'\'>
			<label for="session_nom">' .
	_T('comments:label_nom') .
	'</label>' .
	(($t2 = strval(table_valeur(table_valeur(@$Pile[0], (string)'erreurs', null),'session_nom')))!=='' ?
			('
			<span class=\'erreur_message\'>' . $t2 . '</span>
			') :
			'') .
	'<input type="text" class="text" name="session_nom" id="session_nom" value="' .
	invalideur_session($Cache, entites_html(((($a = table_valeur($GLOBALS["visiteur_session"], (string)'nom', null)) OR (is_string($a) AND strlen($a))) ? $a : invalideur_session($Cache, table_valeur($GLOBALS["visiteur_session"], (string)'session_nom', null))))) .
	'" size="40" />
			' .
	(($t2 = strval(interdire_scripts(((((include_spip('inc/config')?lire_config('accepter_inscriptions',null,false):'') == 'oui')) ?' ' :''))))!=='' ?
			($t2 . (	'
			<span class="details">&#91;<a href="' .
		interdire_scripts(parametre_url(generer_url_public('login', ''),'url',self())) .
		'" rel="nofollow">' .
		_T('public|spip|ecrire:lien_connecter') .
		'</a>&#93;</span>
			')) :
			'') .
	'
		</li>
		<li class=\'editer saisie_session_email' .
	((table_valeur(table_valeur(@$Pile[0], (string)'erreurs', null),'session_email'))  ?
			(' ' . ' ' . 'erreur') :
			'') .
	'\'>
			<label for="session_email">' .
	_T('comments:label_email') .
	'</label>' .
	(($t2 = strval(table_valeur(table_valeur(@$Pile[0], (string)'erreurs', null),'session_email')))!=='' ?
			('
			<span class=\'erreur_message\'>' . $t2 . '</span>
			') :
			'') .
	'<input type="' .
	('' ? 'email':'text') .
	'" class="text email" name="session_email" id="session_email" value="' .
	invalideur_session($Cache, entites_html(((($a = table_valeur($GLOBALS["visiteur_session"], (string)'email', null)) OR (is_string($a) AND strlen($a))) ? $a : invalideur_session($Cache, table_valeur($GLOBALS["visiteur_session"], (string)'session_email', null))))) .
	'" size="40" />
		</li>
		' .
	(($t2 = strval(interdire_scripts((((entites_html(table_valeur(@$Pile[0], (string)'url_site', null),true) != 'http://')) ?' ' :''))))!=='' ?
			($t2 . vide($Pile['vars'][$_zzz=(string)'url_site'] = interdire_scripts(entites_html(table_valeur(@$Pile[0], (string)'url_site', null),true)))) :
			'') .
	'
' .
	interdire_scripts((((include_spip('inc/config')?lire_config('forums_urlref',null,false):'') != 'non') ? (	'<li class=\'editer saisie_url_site' .
		((table_valeur(table_valeur(@$Pile[0], (string)'erreurs', null),'url_site'))  ?
				(' ' . ' ' . 'erreur') :
				'') .
		'\'>
				<label for="url_site">' .
		_T('comments:label_url') .
		'</label>' .
		(($t3 = strval(table_valeur(table_valeur(@$Pile[0], (string)'erreurs', null),'url_site')))!=='' ?
				('
				<span class=\'erreur_message\'>' . $t3 . '</span>
				') :
				'') .
		'<input type="' .
		('' ? 'url':'text') .
		'" class="text url" name="url_site" id="url_site" style="text-align: left;" dir="ltr" size="40" ' .
		(('')  ?
				(' ' . (	' ' .
			(($t4 = strval(interdire_scripts(entites_html(table_valeur(@$Pile[0], (string)'url_site', null),true))))!=='' ?
					('placeholder="' . $t4 . '"') :
					'') .
			(($t4 = strval(table_valeur($Pile["vars"], (string)'url_site', null)))!=='' ?
					(' value="' . $t4 . '"') :
					''))) :
				'') .
		(!('')  ?
				(' ' . (	'value="' .
			interdire_scripts(entites_html(table_valeur(@$Pile[0], (string)'url_site', null),true)) .
			'"')) :
				'') .
		'/>
		</li>
'):'')) .
	'
	</ul>
	' .
	(($t2 = strval(interdire_scripts(((filtre_info_plugin_dist("gravatar", "est_actif")) ?' ' :''))))!=='' ?
			($t2 . (	'<p class="explication gravatar_info_forum">' .
		_T('gravatar:gravatar_info_forum') .
		'</p>')) :
			'') .
	'
')) :
		'') .
'
</fieldset>
');

	return analyse_resultat_skel('html_58002c6dd84376697ed61888f5d4f299', $Cache, $page, 'plugins/auto/comments/v3.5.0/formulaires/inc-login_forum.html');
}
?>