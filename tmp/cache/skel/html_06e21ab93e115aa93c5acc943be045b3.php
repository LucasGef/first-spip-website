<?php

/*
 * Squelette : plugins/auto/spipr_dist/v2.2.6/cartouche/recherche.html
 * Date :      Mon, 30 Mar 2020 09:25:24 GMT
 * Compile :   Wed, 17 Jun 2020 07:36:22 GMT
 * Boucles :   
 */ 
//
// Fonction principale du squelette plugins/auto/spipr_dist/v2.2.6/cartouche/recherche.html
// Temps de compilation total: 0.052 ms
//

function html_06e21ab93e115aa93c5acc943be045b3($Cache, $Pile, $doublons = array(), $Numrows = array(), $SP = 0) {

	if (isset($Pile[0]["doublons"]) AND is_array($Pile[0]["doublons"]))
		$doublons = nettoyer_env_doublons($Pile[0]["doublons"]);

	$connect = '';
	$page = (($t1 = strval(entites_html(_request("recherche"))))!=='' ?
		((	'<header class="cartouche">
	<h1>' .
	_T('public|spip|ecrire:resultats_recherche') .
	'
		<small>&#171;&nbsp;') . $t1 . '&nbsp;&#187;</small>
	</h1>
</header>') :
		'');

	return analyse_resultat_skel('html_06e21ab93e115aa93c5acc943be045b3', $Cache, $page, 'plugins/auto/spipr_dist/v2.2.6/cartouche/recherche.html');
}
?>