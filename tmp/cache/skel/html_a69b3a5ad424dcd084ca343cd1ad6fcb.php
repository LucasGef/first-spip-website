<?php

/*
 * Squelette : plugins/auto/theme_keepitsimple/v1.0.3/body.html
 * Date :      Tue, 17 Mar 2020 15:26:48 GMT
 * Compile :   Wed, 17 Jun 2020 06:56:16 GMT
 * Boucles :   
 */ 
//
// Fonction principale du squelette plugins/auto/theme_keepitsimple/v1.0.3/body.html
// Temps de compilation total: 0.437 ms
//

function html_a69b3a5ad424dcd084ca343cd1ad6fcb($Cache, $Pile, $doublons = array(), $Numrows = array(), $SP = 0) {

	if (isset($Pile[0]["doublons"]) AND is_array($Pile[0]["doublons"]))
		$doublons = nettoyer_env_doublons($Pile[0]["doublons"]);

	$connect = '';
	$page = (
'	<div id="page">
		' .
'
		<div id=\'bloc-haut\'>
			<div id="entete">
				
				' .

'<'.'?php echo recuperer_fond( ' . argumenter_squelette('inclure/entete') . ', array_merge('.var_export($Pile[0],1).',array(\'lang\' => ' . argumenter_squelette($GLOBALS["spip_lang"]) . ')), array("compil"=>array(\'plugins/auto/theme_keepitsimple/v1.0.3/body.html\',\'html_a69b3a5ad424dcd084ca343cd1ad6fcb\',\'\',6,$GLOBALS[\'spip_lang\'])), _request("connect"));
?'.'>
			</div>
			<div id="nav">
				
				' .

'<'.'?php echo recuperer_fond( ' . argumenter_squelette('inclure/barre-nav') . ', array_merge('.var_export($Pile[0],1).',array(\'lang\' => ' . argumenter_squelette($GLOBALS["spip_lang"]) . ')), array("compil"=>array(\'plugins/auto/theme_keepitsimple/v1.0.3/body.html\',\'html_a69b3a5ad424dcd084ca343cd1ad6fcb\',\'\',10,$GLOBALS[\'spip_lang\'])), _request("connect"));
?'.'>' .
'
			</div><!--header ends-->
		</div>
		
		<div id=\'bloc-central\'>
	
			
	    <div class="hfeed" id="conteneur">
	    	<div class="hentry" id="contenu">
					
					' .

'<'.'?php echo recuperer_fond( ' . argumenter_squelette((	'contenu/' .
	interdire_scripts(entites_html(table_valeur(@$Pile[0], (string)'type', null),true)))) . ', array_merge('.var_export($Pile[0],1).',array(\'lang\' => ' . argumenter_squelette($GLOBALS["spip_lang"]) . ')), array("compil"=>array(\'plugins/auto/theme_keepitsimple/v1.0.3/body.html\',\'html_a69b3a5ad424dcd084ca343cd1ad6fcb\',\'\',20,$GLOBALS[\'spip_lang\'])), _request("connect"));
?'.'>
				</div><!--#contenu-->
			</div><!--#conteneur-->
	
	    
	    <div id="navigation">
		    
				' .

'<'.'?php echo recuperer_fond( ' . argumenter_squelette((	'navigation/' .
	interdire_scripts(entites_html(table_valeur(@$Pile[0], (string)'type', null),true)))) . ', array_merge('.var_export($Pile[0],1).',array(\'lang\' => ' . argumenter_squelette($GLOBALS["spip_lang"]) . ')), array("compil"=>array(\'plugins/auto/theme_keepitsimple/v1.0.3/body.html\',\'html_a69b3a5ad424dcd084ca343cd1ad6fcb\',\'\',27,$GLOBALS[\'spip_lang\'])), _request("connect"));
?'.'>
	    </div><!--#navigation-->
	    
	    
	    <div id="extra">
		    
				' .

'<'.'?php echo recuperer_fond( ' . argumenter_squelette((	'extra/' .
	interdire_scripts(entites_html(table_valeur(@$Pile[0], (string)'type', null),true)))) . ', array_merge('.var_export($Pile[0],1).',array(\'lang\' => ' . argumenter_squelette($GLOBALS["spip_lang"]) . ')), array("compil"=>array(\'plugins/auto/theme_keepitsimple/v1.0.3/body.html\',\'html_a69b3a5ad424dcd084ca343cd1ad6fcb\',\'\',33,$GLOBALS[\'spip_lang\'])), _request("connect"));
?'.'>' .
'
	    </div><!--#extra-->
	
			<div class=\'nettoyeur\'></div>
		</div><!-- #bloc-central-->
		
		<div id="bloc-bas">
			<div id="pied">
				
				' .

'<'.'?php echo recuperer_fond( ' . argumenter_squelette('inclure/pied') . ', array_merge('.var_export($Pile[0],1).',array(\'lang\' => ' . argumenter_squelette($GLOBALS["spip_lang"]) . ')), array("compil"=>array(\'plugins/auto/theme_keepitsimple/v1.0.3/body.html\',\'html_a69b3a5ad424dcd084ca343cd1ad6fcb\',\'\',42,$GLOBALS[\'spip_lang\'])), _request("connect"));
?'.'>
				' .

'<'.'?php echo recuperer_fond( ' . argumenter_squelette('inc-theme-copyleft') . ', array(\'lang\' => ' . argumenter_squelette($GLOBALS["spip_lang"]) . '), array("compil"=>array(\'plugins/auto/theme_keepitsimple/v1.0.3/body.html\',\'html_a69b3a5ad424dcd084ca343cd1ad6fcb\',\'\',43,$GLOBALS[\'spip_lang\'])), _request("connect"));
?'.'>
			</div>
		</div>
	</div><!--#page-->
');

	return analyse_resultat_skel('html_a69b3a5ad424dcd084ca343cd1ad6fcb', $Cache, $page, 'plugins/auto/theme_keepitsimple/v1.0.3/body.html');
}
?>