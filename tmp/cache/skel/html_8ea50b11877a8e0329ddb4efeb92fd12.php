<?php

/*
 * Squelette : ../prive/squelettes/contenu/article_edit.html
 * Date :      Tue, 16 Jun 2020 14:01:35 GMT
 * Compile :   Wed, 17 Jun 2020 08:37:26 GMT
 * Boucles :   
 */ 
//
// Fonction principale du squelette ../prive/squelettes/contenu/article_edit.html
// Temps de compilation total: 0.121 ms
//

function html_8ea50b11877a8e0329ddb4efeb92fd12($Cache, $Pile, $doublons = array(), $Numrows = array(), $SP = 0) {

	if (isset($Pile[0]["doublons"]) AND is_array($Pile[0]["doublons"]))
		$doublons = nettoyer_env_doublons($Pile[0]["doublons"]);

	$connect = '';
	$page = 
'<'.'?php echo recuperer_fond( ' . argumenter_squelette('prive/echafaudage/contenu/objet_edit') . ', array_merge('.var_export($Pile[0],1).',array(\'objet\' => ' . argumenter_squelette('article') . ',
	\'id_objet\' => ' . argumenter_squelette(@$Pile[0]['id_article']) . ',
	\'lang\' => ' . argumenter_squelette($GLOBALS["spip_lang"]) . ')), array("compil"=>array(\'../prive/squelettes/contenu/article_edit.html\',\'html_8ea50b11877a8e0329ddb4efeb92fd12\',\'\',1,$GLOBALS[\'spip_lang\'])), _request("connect"));
?'.'>';

	return analyse_resultat_skel('html_8ea50b11877a8e0329ddb4efeb92fd12', $Cache, $page, '../prive/squelettes/contenu/article_edit.html');
}
?>