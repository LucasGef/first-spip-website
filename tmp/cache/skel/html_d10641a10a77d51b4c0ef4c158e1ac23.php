<?php

/*
 * Squelette : plugins/auto/z/v1.7.31/sommaire.html
 * Date :      Tue, 07 Nov 2017 17:27:36 GMT
 * Compile :   Tue, 16 Jun 2020 14:20:21 GMT
 * Boucles :   
 */ 
//
// Fonction principale du squelette plugins/auto/z/v1.7.31/sommaire.html
// Temps de compilation total: 0.091 ms
//

function html_d10641a10a77d51b4c0ef4c158e1ac23($Cache, $Pile, $doublons = array(), $Numrows = array(), $SP = 0) {

	if (isset($Pile[0]["doublons"]) AND is_array($Pile[0]["doublons"]))
		$doublons = nettoyer_env_doublons($Pile[0]["doublons"]);

	$connect = '';
	$page = (
'
' .

'<'.'?php echo recuperer_fond( ' . argumenter_squelette('structure') . ', array_merge('.var_export($Pile[0],1).',array(\'type\' => ' . argumenter_squelette('page') . ',
	\'composition\' => ' . argumenter_squelette('sommaire') . ',
	\'lang\' => ' . argumenter_squelette($GLOBALS["spip_lang"]) . ')), array("compil"=>array(\'plugins/auto/z/v1.7.31/sommaire.html\',\'html_d10641a10a77d51b4c0ef4c158e1ac23\',\'\',2,$GLOBALS[\'spip_lang\'])), _request("connect"));
?'.'>


');

	return analyse_resultat_skel('html_d10641a10a77d51b4c0ef4c158e1ac23', $Cache, $page, 'plugins/auto/z/v1.7.31/sommaire.html');
}
?>