<?php

/*
 * Squelette : plugins/auto/spipr_dist/v2.2.6/footer/dist.html
 * Date :      Wed, 17 Jun 2020 09:02:52 GMT
 * Compile :   Wed, 17 Jun 2020 09:07:12 GMT
 * Boucles :   _annee
 */ 

function BOUCLE_anneehtml_4b692012738da5ec52c1de74bfb0743c(&$Cache, &$Pile, &$doublons, &$Numrows, $SP) {

	static $command = array();
	static $connect;
	$command['connect'] = $connect = '';
	if (!isset($command['table'])) {
		$command['table'] = 'articles';
		$command['id'] = '_annee';
		$command['from'] = array('articles' => 'spip_articles');
		$command['type'] = array();
		$command['groupby'] = array();
		$command['select'] = array("articles.date",
		"articles.lang",
		"articles.titre");
		$command['orderby'] = array('articles.date');
		$command['where'] = 
			array(
quete_condition_statut('articles.statut','publie,prop,prepa/auteur','publie',''), 
quete_condition_postdates('articles.date',''));
		$command['join'] = array();
		$command['limit'] = '0,1';
		$command['having'] = 
			array();
	}
	if (defined("_BOUCLE_PROFILER")) $timer = time()+(float)microtime();
	$t0 = "";
	// REQUETE
	$iter = IterFactory::create(
		"SQL",
		$command,
		array('plugins/auto/spipr_dist/v2.2.6/footer/dist.html','html_4b692012738da5ec52c1de74bfb0743c','_annee',2,$GLOBALS['spip_lang'])
	);
	if (!$iter->err()) {
	lang_select($GLOBALS['spip_lang']);
	$SP++;
	// RESULTATS
	while ($Pile[$SP]=$iter->fetch()) {

		lang_select_public($Pile[$SP]['lang'], '', $Pile[$SP]['titre']);
		$t0 .= (($t1 = strval(interdire_scripts((((annee(normaliser_date($Pile[$SP]['date'])) != date('Y'))) ?' ' :''))))!=='' ?
		($t1 . interdire_scripts(annee(normaliser_date($Pile[$SP]['date'])))) :
		'');
		lang_select();
	}
	lang_select();
	$iter->free();
	}
	if (defined("_BOUCLE_PROFILER")
	AND 1000*($timer = (time()+(float)microtime())-$timer) > _BOUCLE_PROFILER)
		spip_log(intval(1000*$timer)."ms BOUCLE_annee @ plugins/auto/spipr_dist/v2.2.6/footer/dist.html","profiler"._LOG_AVERTISSEMENT);
	return $t0;
}

//
// Fonction principale du squelette plugins/auto/spipr_dist/v2.2.6/footer/dist.html
// Temps de compilation total: 1.016 ms
//

function html_4b692012738da5ec52c1de74bfb0743c($Cache, $Pile, $doublons = array(), $Numrows = array(), $SP = 0) {

	if (isset($Pile[0]["doublons"]) AND is_array($Pile[0]["doublons"]))
		$doublons = nettoyer_env_doublons($Pile[0]["doublons"]);

	$connect = '';
	$page = (
'<p class="colophon">
' .
(($t1 = BOUCLE_anneehtml_4b692012738da5ec52c1de74bfb0743c($Cache, $Pile, $doublons, $Numrows, $SP))!=='' ?
		($t1 . '-') :
		'') .
(($t1 = strval(interdire_scripts(annee(normaliser_date(@$Pile[0]['date'])))))!=='' ?
		($t1 . ' ') :
		'') .
' &mdash; ' .
interdire_scripts(typo($GLOBALS['meta']['nom_site'], "TYPO", $connect, $Pile[0])) .
'<br />
<a rel="contents" href="' .
interdire_scripts(generer_url_public('plan', '')) .
'">' .
_T('public|spip|ecrire:plan_site') .
'</a><?php
if (isset($GLOBALS[\'visiteur_session\'][\'id_auteur\']) AND $GLOBALS[\'visiteur_session\'][\'id_auteur\']) {
?><span class="sep"> | </span><a href="' .
executer_balise_dynamique('URL_LOGOUT',
	array(),
	array('plugins/auto/spipr_dist/v2.2.6/footer/dist.html','html_4b692012738da5ec52c1de74bfb0743c','',5,$GLOBALS['spip_lang'])) .
'" rel="nofollow">' .
_T('public|spip|ecrire:icone_deconnecter') .
'</a><?php
	if (include_spip(\'inc/autoriser\') AND autoriser(\'ecrire\')){
	?><span class="sep"> | </span><a href="' .
interdire_scripts(eval('return '.'_DIR_RESTREINT_ABS'.';')) .
'">' .
_T('public|spip|ecrire:espace_prive') .
'</a><?php
	}
}
else {
?><span class="sep"> | </span><a href="' .
interdire_scripts(parametre_url(generer_url_public('login', ''),'url',parametre_url(self(),'url',''))) .
'" rel="nofollow" class=\'login_modal\'>' .
_T('public|spip|ecrire:lien_connecter') .
'</a><?php
}
?><span class="sep"> |
</span><a href="' .
interdire_scripts(generer_url_public('backend', '')) .
'" rel="alternate" title="' .
_T('public|spip|ecrire:syndiquer_site') .
'">' .
filtre_balise_svg_dist(find_in_path('img/feed.svg'),_T('public|spip|ecrire:icone_suivi_activite')) .
'&nbsp;RSS&nbsp;2.0</a>
</p>');

	return analyse_resultat_skel('html_4b692012738da5ec52c1de74bfb0743c', $Cache, $page, 'plugins/auto/spipr_dist/v2.2.6/footer/dist.html');
}
?>