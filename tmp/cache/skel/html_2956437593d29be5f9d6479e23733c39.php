<?php

/*
 * Squelette : ../plugins/auto/menus/v1.7.26/formulaires/inc-menus_entrees.html
 * Date :      Mon, 08 Jun 2020 15:55:26 GMT
 * Compile :   Tue, 16 Jun 2020 14:50:01 GMT
 * Boucles :   _sous_menu_test, _test_modifier, _sous_menu, _entrees, _pas_demande_entree
 */ 

function BOUCLE_sous_menu_testhtml_2956437593d29be5f9d6479e23733c39(&$Cache, &$Pile, &$doublons, &$Numrows, $SP) {

	static $command = array();
	static $connect;
	$command['connect'] = $connect = '';
	if (!isset($command['table'])) {
		$command['table'] = 'menus';
		$command['id'] = '_sous_menu_test';
		$command['from'] = array('menus' => 'spip_menus');
		$command['type'] = array();
		$command['groupby'] = array();
		$command['select'] = array("1");
		$command['orderby'] = array();
		$command['join'] = array();
		$command['limit'] = '';
		$command['having'] = 
			array();
	}
	$command['where'] = 
			array(
			array('=', 'menus.id_menus_entree', sql_quote($Pile[$SP-1]['id_menus_entree'], '','bigint(21) NOT NULL DEFAULT \'0\'')));
	if (defined("_BOUCLE_PROFILER")) $timer = time()+(float)microtime();
	$t0 = "";
	// REQUETE
	$iter = IterFactory::create(
		"SQL",
		$command,
		array('../plugins/auto/menus/v1.7.26/formulaires/inc-menus_entrees.html','html_2956437593d29be5f9d6479e23733c39','_sous_menu_test',18,$GLOBALS['spip_lang'])
	);
	if (!$iter->err()) {
	$SP++;
	// RESULTATS
	while ($Pile[$SP]=$iter->fetch()) {

		$t0 .= (
'
				
				');
	}
	$iter->free();
	}
	if (defined("_BOUCLE_PROFILER")
	AND 1000*($timer = (time()+(float)microtime())-$timer) > _BOUCLE_PROFILER)
		spip_log(intval(1000*$timer)."ms BOUCLE_sous_menu_test @ ../plugins/auto/menus/v1.7.26/formulaires/inc-menus_entrees.html","profiler"._LOG_AVERTISSEMENT);
	return $t0;
}


function BOUCLE_test_modifierhtml_2956437593d29be5f9d6479e23733c39(&$Cache, &$Pile, &$doublons, &$Numrows, $SP) {

	static $command = array();
	static $connect;
	$command['connect'] = $connect = '';
	if (!isset($si_init)) { $command['si'] = array(); $si_init = true; }
	$command['si'][] = interdire_scripts((((entites_html(table_valeur(@$Pile[0], (string)'id_menus_entree', null),true) == $Pile[$SP]['id_menus_entree'])) ?'' :' '));

	if (!isset($command['table'])) {
		$command['table'] = '';
		$command['id'] = '_test_modifier';
		$command['from'] = array();
		$command['type'] = array();
		$command['groupby'] = array();
		$command['select'] = array("1");
		$command['orderby'] = array();
		$command['where'] = 
			array();
		$command['join'] = array();
		$command['limit'] = '';
		$command['having'] = 
			array();
	}
	if (defined("_BOUCLE_PROFILER")) $timer = time()+(float)microtime();
	$t0 = "";
	// REQUETE
	$iter = IterFactory::create(
		"CONDITION",
		$command,
		array('../plugins/auto/menus/v1.7.26/formulaires/inc-menus_entrees.html','html_2956437593d29be5f9d6479e23733c39','_test_modifier',5,$GLOBALS['spip_lang'])
	);
	if (!$iter->err()) {
	
	$l1 = _T('menus:formulaire_ajouter_sous_menu');
	$l2 = _T('menus:editer_menus_entrees_editer');
	$l3 = _T('menus:formulaire_supprimer_entree');
	$l4 = _T('menus:confirmer_supprimer_entree');
	$l5 = _T('menus:formulaire_deplacer_bas');
	$l6 = _T('menus:formulaire_deplacer_haut');$SP++;
	// RESULTATS
	while ($Pile[$SP]=$iter->fetch()) {

		$t0 .= (
'
		<div class="ligne">
			<div class="description arial2">
				' .
vide($Pile['vars'][$_zzz=(string)'supprimer_seulement'] = 'non') .
(($t1 = strval(((find_in_path((	'menus/' .
	interdire_scripts($Pile[$SP-1]['type_entree']) .
	'.html'))) ?' ' :'')))!=='' ?
		($t1 . (	'
				' .
	((!$Pile[$SP-1]['id_menus_entree']) ? _T('zbug_champ_hors_motif', array('champ'=>'AFFICHER_ENTREE', 'motif'=>'MENUS_ENTREES')) : recuperer_fond(
		'menus/'.$Pile[$SP-1]['type_entree'],
		array_merge(is_array($params = unserialize($Pile[$SP-1]['parametres']))?$params:array(), array('appel_formulaire'=>true, 'env'=>$Pile[0]))
	)))) :
		'') .
'
				' .
(($t1 = strval(((find_in_path((	'menus/' .
	interdire_scripts($Pile[$SP-1]['type_entree']) .
	'.html'))) ?'' :' ')))!=='' ?
		($t1 . (	'
				' .
	vide($Pile['vars'][$_zzz=(string)'supprimer_seulement'] = 'oui') .
	recuperer_fond( 'formulaires/inc-menus_entrees_inexistantes' , array('appel_formulaire' => 'oui' ,
	'titre' => interdire_scripts($Pile[$SP-1]['type_entree']) ), array('compil'=>array('../plugins/auto/menus/v1.7.26/formulaires/inc-menus_entrees.html','html_2956437593d29be5f9d6479e23733c39','_test_modifier',11,$GLOBALS['spip_lang'])), _request('connect')))) :
		'') .
'
			</div>
			<div class="actions">
				
				' .
(($t1 = BOUCLE_sous_menu_testhtml_2956437593d29be5f9d6479e23733c39($Cache, $Pile, $doublons, $Numrows, $SP))!=='' ?
		$t1 :
		((	'
				' .
	(((table_valeur($Pile["vars"], (string)'supprimer_seulement', null) == 'non'))  ?
			(' ' . (	'
				' .
		(($t3 = strval(interdire_scripts(((table_valeur(table_valeur(entites_html(table_valeur(@$Pile[0], (string)'types_entrees', null),true),interdire_scripts($Pile[$SP-1]['type_entree'])),'refuser_sous_menu')) ?'' :' '))))!=='' ?
				($t3 . (	'
				<button type="submit" name="demander_sous_menu" value="' .
			$Pile[$SP-1]['id_menus_entree'] .
			'" title="' .
			$l1 .
			'">
					' .
			(($t4 = strval(find_in_path('images/menus-24.png')))!=='' ?
					('<img height="24" width="24" src="' . $t4 . '" alt="" />') :
					'') .
			'
				</button>
				')) :
				''))) :
			'') .
	'
				'))) .
'
				' .
(((table_valeur($Pile["vars"], (string)'supprimer_seulement', null) == 'non'))  ?
		(' ' . (	'
				<button type="submit" name="modifier_entree" value="' .
	$Pile[$SP-1]['id_menus_entree'] .
	'" title="' .
	$l2 .
	'">
					' .
	(($t2 = strval(find_in_path('images/menus_action_modifier.png')))!=='' ?
			('<img height="24" width="24" src="' . $t2 . '" alt="" />') :
			'') .
	'
				</button>')) :
		'') .
'
				<button type="submit" name="supprimer_entree" value="' .
$Pile[$SP-1]['id_menus_entree'] .
'" title="' .
$l3 .
'"
					onclick="return confirm(\'' .
$l4 .
'\')"
				>
					' .
(($t1 = strval(find_in_path('images/menus_action_supprimer.png')))!=='' ?
		('<img height="24" width="24" src="' . $t1 . '" alt="" />') :
		'') .
'
				</button>
				' .
(((table_valeur($Pile["vars"], (string)'supprimer_seulement', null) == 'non'))  ?
		(' ' . (	'
				<button type="submit" name="deplacer_entree" value="' .
	$Pile[$SP-1]['id_menus_entree'] .
	'-bas" title="' .
	$l5 .
	'">
					' .
	(($t2 = strval(find_in_path('images/menus_action_bas.png')))!=='' ?
			('<img height="24" width="24" src="' . $t2 . '" alt="" />') :
			'') .
	'
				</button>')) :
		'') .
'
				' .
(((table_valeur($Pile["vars"], (string)'supprimer_seulement', null) == 'non'))  ?
		(' ' . (	'
				<button type="submit" name="deplacer_entree" value="' .
	$Pile[$SP-1]['id_menus_entree'] .
	'-haut" title="' .
	$l6 .
	'">
					' .
	(($t2 = strval(find_in_path('images/menus_action_haut.png')))!=='' ?
			('<img height="24" width="24" src="' . $t2 . '" alt="" />') :
			'') .
	'
				</button>')) :
		'') .
'
			</div>
			<div class="nettoyeur"></div>
		</div>
		');
	}
	$iter->free();
	}
	if (defined("_BOUCLE_PROFILER")
	AND 1000*($timer = (time()+(float)microtime())-$timer) > _BOUCLE_PROFILER)
		spip_log(intval(1000*$timer)."ms BOUCLE_test_modifier @ ../plugins/auto/menus/v1.7.26/formulaires/inc-menus_entrees.html","profiler"._LOG_AVERTISSEMENT);
	return $t0;
}


function BOUCLE_sous_menuhtml_2956437593d29be5f9d6479e23733c39(&$Cache, &$Pile, &$doublons, &$Numrows, $SP) {

	static $command = array();
	static $connect;
	$command['connect'] = $connect = '';
	if (!isset($command['table'])) {
		$command['table'] = 'menus';
		$command['id'] = '_sous_menu';
		$command['from'] = array('menus' => 'spip_menus');
		$command['type'] = array();
		$command['groupby'] = array();
		$command['select'] = array("menus.id_menu");
		$command['orderby'] = array();
		$command['join'] = array();
		$command['limit'] = '';
		$command['having'] = 
			array();
	}
	$command['where'] = 
			array(
			array('=', 'menus.id_menus_entree', sql_quote($Pile[$SP]['id_menus_entree'], '','bigint(21) NOT NULL DEFAULT \'0\'')));
	if (defined("_BOUCLE_PROFILER")) $timer = time()+(float)microtime();
	$t0 = "";
	// REQUETE
	$iter = IterFactory::create(
		"SQL",
		$command,
		array('../plugins/auto/menus/v1.7.26/formulaires/inc-menus_entrees.html','html_2956437593d29be5f9d6479e23733c39','_sous_menu',53,$GLOBALS['spip_lang'])
	);
	if (!$iter->err()) {
	$SP++;
	// RESULTATS
	while ($Pile[$SP]=$iter->fetch()) {

		$t0 .= (
'
			' .
recuperer_fond( 'formulaires/inc-menus_entrees' , array_merge($Pile[0],array('id_menu' => $Pile[$SP]['id_menu'] ,
	'sous_menu' => 'oui' )), array('compil'=>array('../plugins/auto/menus/v1.7.26/formulaires/inc-menus_entrees.html','html_2956437593d29be5f9d6479e23733c39','_sous_menu',54,$GLOBALS['spip_lang'])), _request('connect')));
	}
	$iter->free();
	}
	if (defined("_BOUCLE_PROFILER")
	AND 1000*($timer = (time()+(float)microtime())-$timer) > _BOUCLE_PROFILER)
		spip_log(intval(1000*$timer)."ms BOUCLE_sous_menu @ ../plugins/auto/menus/v1.7.26/formulaires/inc-menus_entrees.html","profiler"._LOG_AVERTISSEMENT);
	return $t0;
}


function BOUCLE_entreeshtml_2956437593d29be5f9d6479e23733c39(&$Cache, &$Pile, &$doublons, &$Numrows, $SP) {

	static $command = array();
	static $connect;
	$command['connect'] = $connect = '';
	if (!isset($command['table'])) {
		$command['table'] = 'menus_entrees';
		$command['id'] = '_entrees';
		$command['from'] = array('menus_entrees' => 'spip_menus_entrees');
		$command['type'] = array();
		$command['groupby'] = array();
		$command['select'] = array("menus_entrees.id_menus_entree",
		"menus_entrees.type_entree",
		"menus_entrees.parametres",
		"menus_entrees.rang");
		$command['orderby'] = array('menus_entrees.rang');
		$command['join'] = array();
		$command['limit'] = '';
		$command['having'] = 
			array();
	}
	$command['where'] = 
			array(
			array('=', 'menus_entrees.id_menu', sql_quote(@$Pile[0]['id_menu'], '','bigint(21) NOT NULL DEFAULT \'0\'')));
	if (defined("_BOUCLE_PROFILER")) $timer = time()+(float)microtime();
	$t0 = "";
	// REQUETE
	$iter = IterFactory::create(
		"SQL",
		$command,
		array('../plugins/auto/menus/v1.7.26/formulaires/inc-menus_entrees.html','html_2956437593d29be5f9d6479e23733c39','_entrees',3,$GLOBALS['spip_lang'])
	);
	if (!$iter->err()) {
	$SP++;
	// RESULTATS
	while ($Pile[$SP]=$iter->fetch()) {

		$t0 .= (
'
	<li class="entree"' .
(($t1 = strval(interdire_scripts(extraire_attribut(filtrer('image_graver', filtrer('image_reduire',table_valeur(table_valeur(entites_html(table_valeur(@$Pile[0], (string)'types_entrees', null),true),interdire_scripts($Pile[$SP]['type_entree'])),'icone'),'24')),'src'))))!=='' ?
		(' style="background-repeat:no-repeat;background-position:5px 10px;background-image:url(' . $t1 . ')"') :
		'') .
'>
		' .
(($t1 = BOUCLE_test_modifierhtml_2956437593d29be5f9d6479e23733c39($Cache, $Pile, $doublons, $Numrows, $SP))!=='' ?
		$t1 :
		((	'
		' .
	recuperer_fond( 'formulaires/inc-nouvelle_entree-2' , array_merge($Pile[0],array('id_menus_entree' => $Pile[$SP]['id_menus_entree'] )), array('compil'=>array('../plugins/auto/menus/v1.7.26/formulaires/inc-menus_entrees.html','html_2956437593d29be5f9d6479e23733c39','_entrees',49,$GLOBALS['spip_lang'])), _request('connect'))))) .
'

		
		' .
BOUCLE_sous_menuhtml_2956437593d29be5f9d6479e23733c39($Cache, $Pile, $doublons, $Numrows, $SP) .
'
	</li>
	' .
vide($Pile['vars'][$_zzz=(string)'rang_suivant'] = plus($Pile[$SP]['rang'],'1')));
	}
	$iter->free();
	}
	if (defined("_BOUCLE_PROFILER")
	AND 1000*($timer = (time()+(float)microtime())-$timer) > _BOUCLE_PROFILER)
		spip_log(intval(1000*$timer)."ms BOUCLE_entrees @ ../plugins/auto/menus/v1.7.26/formulaires/inc-menus_entrees.html","profiler"._LOG_AVERTISSEMENT);
	return $t0;
}


function BOUCLE_pas_demande_entreehtml_2956437593d29be5f9d6479e23733c39(&$Cache, &$Pile, &$doublons, &$Numrows, $SP) {

	static $command = array();
	static $connect;
	$command['connect'] = $connect = '';
	if (!isset($si_init)) { $command['si'] = array(); $si_init = true; }
	$command['si'][] = (((table_valeur($Pile["vars"], (string)'id_menu_nouvelle_entree', null) == interdire_scripts(entites_html(table_valeur(@$Pile[0], (string)'id_menu', null),true)))) ?'' :' ');

	if (!isset($command['table'])) {
		$command['table'] = '';
		$command['id'] = '_pas_demande_entree';
		$command['from'] = array();
		$command['type'] = array();
		$command['groupby'] = array();
		$command['select'] = array("1");
		$command['orderby'] = array();
		$command['where'] = 
			array();
		$command['join'] = array();
		$command['limit'] = '';
		$command['having'] = 
			array();
	}
	if (defined("_BOUCLE_PROFILER")) $timer = time()+(float)microtime();
	$t0 = "";
	// REQUETE
	$iter = IterFactory::create(
		"CONDITION",
		$command,
		array('../plugins/auto/menus/v1.7.26/formulaires/inc-menus_entrees.html','html_2956437593d29be5f9d6479e23733c39','_pas_demande_entree',63,$GLOBALS['spip_lang'])
	);
	if (!$iter->err()) {
	
	$l1 = _T('menus:formulaire_supprimer_sous_menu');
	$l2 = _T('menus:confirmer_supprimer_sous_menu');
	$l3 = _T('menus:texte_ajouter_menu_entree');$SP++;
	// RESULTATS
	while ($Pile[$SP]=$iter->fetch()) {

		$t0 .= (
'
			<div class="boutons">
			' .
(($t1 = strval(interdire_scripts(((entites_html(table_valeur(@$Pile[0], (string)'sous_menu', null),true)) ?'' :' '))))!=='' ?
		($t1 . '<span class="image_loading"></span>') :
		'') .
'
			' .
(($t1 = strval(interdire_scripts((entites_html(table_valeur(@$Pile[0], (string)'sous_menu', null),true) ? $l1:''))))!=='' ?
		((	'<button type="submit" class="submit link" name="supprimer_menu" value="' .
	interdire_scripts(entites_html(table_valeur(@$Pile[0], (string)'id_menu', null),true)) .
	'"
				onclick="return confirm(\'' .
	$l2 .
	'\')"
			>
				' .
	(($t2 = strval(find_in_path('images/menus_action_supprimer.png')))!=='' ?
			('<img src="' . $t2 . '" alt="" />') :
			'') .
	'
				') . $t1 . '
			</button>') :
		'') .
'
			<button type="submit" class="submit" name="demander_nouvelle_entree" value="' .
interdire_scripts(entites_html(table_valeur(@$Pile[0], (string)'id_menu', null),true)) .
'">
				' .
(($t1 = strval(find_in_path('images/menus_action_ajouter.png')))!=='' ?
		('<img src="' . $t1 . '" alt="" />') :
		'') .
'
				' .
$l3 .
'
			</button>
			</div>
		');
	}
	$iter->free();
	}
	if (defined("_BOUCLE_PROFILER")
	AND 1000*($timer = (time()+(float)microtime())-$timer) > _BOUCLE_PROFILER)
		spip_log(intval(1000*$timer)."ms BOUCLE_pas_demande_entree @ ../plugins/auto/menus/v1.7.26/formulaires/inc-menus_entrees.html","profiler"._LOG_AVERTISSEMENT);
	return $t0;
}

//
// Fonction principale du squelette ../plugins/auto/menus/v1.7.26/formulaires/inc-menus_entrees.html
// Temps de compilation total: 3.911 ms
//

function html_2956437593d29be5f9d6479e23733c39($Cache, $Pile, $doublons = array(), $Numrows = array(), $SP = 0) {

	if (isset($Pile[0]["doublons"]) AND is_array($Pile[0]["doublons"]))
		$doublons = nettoyer_env_doublons($Pile[0]["doublons"]);

	$connect = '';
	$page = (
vide($Pile['vars'][$_zzz=(string)'rang_suivant'] = '1') .
'<ul id="menu-' .
interdire_scripts(entites_html(table_valeur(@$Pile[0], (string)'id_menu', null),true)) .
'" class="editer-groupe menus_entrees">
	' .
BOUCLE_entreeshtml_2956437593d29be5f9d6479e23733c39($Cache, $Pile, $doublons, $Numrows, $SP) .
'
	<li class="editer_entree fieldset">
		' .
vide($Pile['vars'][$_zzz=(string)'id_menu_nouvelle_entree'] = interdire_scripts(entites_html(table_valeur(@$Pile[0], (string)'id_menu_nouvelle_entree', null),true))) .
vide($Pile['vars'][$_zzz=(string)'type_entree'] = interdire_scripts(entites_html(table_valeur(@$Pile[0], (string)'type_entree', null),true))) .
(($t1 = BOUCLE_pas_demande_entreehtml_2956437593d29be5f9d6479e23733c39($Cache, $Pile, $doublons, $Numrows, $SP))!=='' ?
		$t1 :
		((	'
			' .
	(!(table_valeur($Pile["vars"], (string)'type_entree', null))  ?
			(' ' . (	'
				' .
		recuperer_fond( 'formulaires/inc-nouvelle_entree-1' , array_merge($Pile[0],array('id_menu_nouvelle_entree' => table_valeur($Pile["vars"], (string)'id_menu_nouvelle_entree', null) )), array('compil'=>array('../plugins/auto/menus/v1.7.26/formulaires/inc-menus_entrees.html','html_2956437593d29be5f9d6479e23733c39','',79,$GLOBALS['spip_lang'])), _request('connect')))) :
			'') .
	'
			' .
	((table_valeur($Pile["vars"], (string)'type_entree', null))  ?
			(' ' . (	'
				' .
		recuperer_fond( 'formulaires/inc-nouvelle_entree-2' , array_merge($Pile[0],array('id_menu_nouvelle_entree' => table_valeur($Pile["vars"], (string)'id_menu_nouvelle_entree', null) ,
	'rang_suivant' => table_valeur($Pile["vars"], (string)'rang_suivant', null) )), array('compil'=>array('../plugins/auto/menus/v1.7.26/formulaires/inc-menus_entrees.html','html_2956437593d29be5f9d6479e23733c39','',80,$GLOBALS['spip_lang'])), _request('connect')))) :
			'') .
	'
		'))) .
'
	</li>
</ul>
');

	return analyse_resultat_skel('html_2956437593d29be5f9d6479e23733c39', $Cache, $page, '../plugins/auto/menus/v1.7.26/formulaires/inc-menus_entrees.html');
}
?>