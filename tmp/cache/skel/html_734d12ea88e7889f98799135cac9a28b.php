<?php

/*
 * Squelette : ../plugins-dist/svp/prive/squelettes/navigation/charger_plugin.html
 * Date :      Tue, 16 Jun 2020 14:01:55 GMT
 * Compile :   Wed, 17 Jun 2020 07:19:03 GMT
 * Boucles :   
 */ 
//
// Fonction principale du squelette ../plugins-dist/svp/prive/squelettes/navigation/charger_plugin.html
// Temps de compilation total: 0.138 ms
//

function html_734d12ea88e7889f98799135cac9a28b($Cache, $Pile, $doublons = array(), $Numrows = array(), $SP = 0) {

	if (isset($Pile[0]["doublons"]) AND is_array($Pile[0]["doublons"]))
		$doublons = nettoyer_env_doublons($Pile[0]["doublons"]);

	$connect = '';
	$page = (
'
' .
recuperer_fond( 'prive/squelettes/inclure/menu-navigation' , array_merge($Pile[0],array('menu' => 'menu_configuration' ,
	'bloc' => 'navigation' )), array('compil'=>array('../plugins-dist/svp/prive/squelettes/navigation/charger_plugin.html','html_734d12ea88e7889f98799135cac9a28b','',2,$GLOBALS['spip_lang'])), _request('connect')) .
'
' .
boite_ouvrir('', 'info') .
_T('svp:info_boite_charger_plugin') .
'
' .
boite_fermer() .
'
');

	return analyse_resultat_skel('html_734d12ea88e7889f98799135cac9a28b', $Cache, $page, '../plugins-dist/svp/prive/squelettes/navigation/charger_plugin.html');
}
?>