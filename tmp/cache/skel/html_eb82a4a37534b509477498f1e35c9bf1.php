<?php

/*
 * Squelette : plugins/auto/z/v1.7.31/inclure/entete.html
 * Date :      Tue, 07 Nov 2017 17:27:36 GMT
 * Compile :   Wed, 17 Jun 2020 06:56:16 GMT
 * Boucles :   
 */ 
//
// Fonction principale du squelette plugins/auto/z/v1.7.31/inclure/entete.html
// Temps de compilation total: 2.080 ms
//

function html_eb82a4a37534b509477498f1e35c9bf1($Cache, $Pile, $doublons = array(), $Numrows = array(), $SP = 0) {

	if (isset($Pile[0]["doublons"]) AND is_array($Pile[0]["doublons"]))
		$doublons = nettoyer_env_doublons($Pile[0]["doublons"]);

	$connect = '';
	$page = (
'<div class="accueil">
	' .
(($t1 = strval(filtrer('image_graver',filtrer('image_reduire',
((!is_array($l = quete_logo('id_syndic', 'ON', "'0'",'', 0))) ? '':
 ("<img class=\"spip_logo spip_logos\" alt=\"\" src=\"$l[0]\"" . $l[2] .  ($l[1] ? " onmouseover=\"this.src='$l[1]'\" onmouseout=\"this.src='$l[0]'\"" : "") . ' />')),'300','100'))))!=='' ?
		((	'<a rel="start home" href="' .
	spip_htmlspecialchars(sinon($GLOBALS['meta']['adresse_site'],'.')) .
	'/" title="' .
	_T('public|spip|ecrire:accueil_site') .
	'">') . $t1 . '</a>') :
		'') .
'
	<strong id="nom_site_spip"><a rel="start home" href="' .
spip_htmlspecialchars(sinon($GLOBALS['meta']['adresse_site'],'.')) .
'/" title="' .
_T('public|spip|ecrire:accueil_site') .
'">' .
interdire_scripts(typo($GLOBALS['meta']['nom_site'], "TYPO", $connect, $Pile[0])) .
'</a></strong>
	' .
(($t1 = strval(interdire_scripts(PtoBR(typo($GLOBALS['meta']['slogan_site'], "TYPO", $connect, $Pile[0])))))!=='' ?
		('<div id=\'slogan_site_spip\'>' . $t1 . '</div>') :
		'') .
'
</div>
' .
executer_balise_dynamique('MENU_LANG',
	array(spip_htmlentities(@$Pile[0]['lang'] ? @$Pile[0]['lang'] : $GLOBALS['spip_lang'])),
	array('plugins/auto/z/v1.7.31/inclure/entete.html','html_eb82a4a37534b509477498f1e35c9bf1','',6,$GLOBALS['spip_lang'])) .
'

');

	return analyse_resultat_skel('html_eb82a4a37534b509477498f1e35c9bf1', $Cache, $page, 'plugins/auto/z/v1.7.31/inclure/entete.html');
}
?>