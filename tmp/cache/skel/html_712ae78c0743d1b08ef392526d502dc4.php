<?php

/*
 * Squelette : plugins/auto/menus/v1.7.26/inclure/menu.html
 * Date :      Mon, 08 Jun 2020 15:55:26 GMT
 * Compile :   Wed, 17 Jun 2020 06:56:16 GMT
 * Boucles :   _sous_menu, _entrees, _menu
 */ 

function BOUCLE_sous_menuhtml_712ae78c0743d1b08ef392526d502dc4(&$Cache, &$Pile, &$doublons, &$Numrows, $SP) {

	static $command = array();
	static $connect;
	$command['connect'] = $connect = '';
	if (!isset($si_init)) { $command['si'] = array(); $si_init = true; }
	$command['si'][] = table_valeur($Pile["vars"], (string)'entree', null);

	if (!isset($command['table'])) {
		$command['table'] = 'menus';
		$command['id'] = '_sous_menu';
		$command['from'] = array('menus' => 'spip_menus');
		$command['type'] = array();
		$command['groupby'] = array();
		$command['select'] = array("menus.id_menu",
		"menus.identifiant");
		$command['orderby'] = array();
		$command['join'] = array();
		$command['limit'] = '';
		$command['having'] = 
			array();
	}
	$command['where'] = 
			array(
			array('=', 'menus.id_menus_entree', sql_quote($Pile[$SP]['id_menus_entree'], '','bigint(21) NOT NULL DEFAULT \'0\'')));
	if (defined("_BOUCLE_PROFILER")) $timer = time()+(float)microtime();
	$t0 = "";
	// REQUETE
	$iter = IterFactory::create(
		"SQL",
		$command,
		array('plugins/auto/menus/v1.7.26/inclure/menu.html','html_712ae78c0743d1b08ef392526d502dc4','_sous_menu',6,$GLOBALS['spip_lang'])
	);
	if (!$iter->err()) {
	$SP++;
	// RESULTATS
	while ($Pile[$SP]=$iter->fetch()) {

		$t0 .= (
'
				' .

'<'.'?php echo recuperer_fond( ' . argumenter_squelette('inclure/menu') . ', array_merge('.var_export($Pile[0],1).',array(\'id_menu\' => ' . argumenter_squelette($Pile[$SP]['id_menu']) . ',
	\'identifiant\' => ' . argumenter_squelette($Pile[$SP]['identifiant']) . ',
	\'depth\' => ' . argumenter_squelette(interdire_scripts(plus(entites_html(sinon(table_valeur(@$Pile[0], (string)'depth', null), '0'),true),'1'))) . ',
	\'lang\' => ' . argumenter_squelette($GLOBALS["spip_lang"]) . ')), array("compil"=>array(\'plugins/auto/menus/v1.7.26/inclure/menu.html\',\'html_712ae78c0743d1b08ef392526d502dc4\',\'\',7,$GLOBALS[\'spip_lang\'])), _request("connect"));
?'.'>');
	}
	$iter->free();
	}
	if (defined("_BOUCLE_PROFILER")
	AND 1000*($timer = (time()+(float)microtime())-$timer) > _BOUCLE_PROFILER)
		spip_log(intval(1000*$timer)."ms BOUCLE_sous_menu @ plugins/auto/menus/v1.7.26/inclure/menu.html","profiler"._LOG_AVERTISSEMENT);
	return $t0;
}


function BOUCLE_entreeshtml_712ae78c0743d1b08ef392526d502dc4(&$Cache, &$Pile, &$doublons, &$Numrows, $SP) {

	static $command = array();
	static $connect;
	$command['connect'] = $connect = '';
	if (!isset($command['table'])) {
		$command['table'] = 'menus_entrees';
		$command['id'] = '_entrees';
		$command['from'] = array('menus_entrees' => 'spip_menus_entrees');
		$command['type'] = array();
		$command['groupby'] = array();
		$command['select'] = array("menus_entrees.id_menus_entree",
		"menus_entrees.rang",
		"menus_entrees.type_entree",
		"menus_entrees.parametres");
		$command['orderby'] = array('menus_entrees.rang');
		$command['join'] = array();
		$command['limit'] = '';
		$command['having'] = 
			array();
	}
	$command['where'] = 
			array(
			array('=', 'menus_entrees.id_menu', sql_quote($Pile[$SP]['id_menu'], '','bigint(21) NOT NULL DEFAULT \'0\'')));
	if (defined("_BOUCLE_PROFILER")) $timer = time()+(float)microtime();
	$t0 = "";
	// REQUETE
	$iter = IterFactory::create(
		"SQL",
		$command,
		array('plugins/auto/menus/v1.7.26/inclure/menu.html','html_712ae78c0743d1b08ef392526d502dc4','_entrees',2,$GLOBALS['spip_lang'])
	);
	if (!$iter->err()) {
	$SP++;
	// RESULTATS
	while ($Pile[$SP]=$iter->fetch()) {

		$t0 .= (
vide($Pile['vars'][$_zzz=(string)'entree'] = '') .
(($t1 = strval(((!$Pile[$SP]['id_menus_entree']) ? _T('zbug_champ_hors_motif', array('champ'=>'AFFICHER_ENTREE', 'motif'=>'MENUS_ENTREES')) : recuperer_fond(
		'menus/'.$Pile[$SP]['type_entree'],
		array_merge(is_array($params = unserialize($Pile[$SP]['parametres']))?$params:array(), array('appel_menu'=>true, 'env'=>$Pile[0]))
	))))!=='' ?
		($t1 . vide($Pile['vars'][$_zzz=(string)'entree'] = ' ')) :
		'') .
BOUCLE_sous_menuhtml_712ae78c0743d1b08ef392526d502dc4($Cache, $Pile, $doublons, $Numrows, $SP) .
(((table_valeur($Pile["vars"], (string)'entree', null)) AND (interdire_scripts(((menus_type_refuser_sous_menu($Pile[$SP]['type_entree'])) ?'' :' '))))  ?
		(' ' . '
		</li>') :
		'') .
'
		');
	}
	$iter->free();
	}
	if (defined("_BOUCLE_PROFILER")
	AND 1000*($timer = (time()+(float)microtime())-$timer) > _BOUCLE_PROFILER)
		spip_log(intval(1000*$timer)."ms BOUCLE_entrees @ plugins/auto/menus/v1.7.26/inclure/menu.html","profiler"._LOG_AVERTISSEMENT);
	return $t0;
}


function BOUCLE_menuhtml_712ae78c0743d1b08ef392526d502dc4(&$Cache, &$Pile, &$doublons, &$Numrows, $SP) {

	static $command = array();
	static $connect;
	$command['connect'] = $connect = '';
	$in = array();
	if (!(is_array($a = (@$Pile[0]['id_menu']))))
		$in[]= $a;
	else $in = array_merge($in, $a);
	$in1 = array();
	if (!(is_array($a = (@$Pile[0]['identifiant']))))
		$in1[]= $a;
	else $in1 = array_merge($in1, $a);
	if (!isset($command['table'])) {
		$command['table'] = 'menus';
		$command['id'] = '_menu';
		$command['from'] = array('menus' => 'spip_menus');
		$command['type'] = array();
		$command['groupby'] = array();
		$command['select'] = array("menus.id_menu",
		"menus.css");
		$command['orderby'] = array();
		$command['join'] = array();
		$command['limit'] = '';
		$command['having'] = 
			array();
	}
	$command['where'] = 
			array((!(is_array(@$Pile[0]['id_menu'])?count(@$Pile[0]['id_menu']):strlen(@$Pile[0]['id_menu'])) ? '' : ((is_array(@$Pile[0]['id_menu'])) ? sql_in('menus.id_menu',sql_quote($in)) : 
			array('=', 'menus.id_menu', sql_quote(@$Pile[0]['id_menu'], '','bigint(21) NOT NULL AUTO_INCREMENT')))), (!(is_array(@$Pile[0]['identifiant'])?count(@$Pile[0]['identifiant']):strlen(@$Pile[0]['identifiant'])) ? '' : ((is_array(@$Pile[0]['identifiant'])) ? sql_in('menus.identifiant',sql_quote($in1)) : 
			array('=', 'menus.identifiant', sql_quote(@$Pile[0]['identifiant'], '','varchar(255) NOT NULL DEFAULT \'\'')))));
	if (defined("_BOUCLE_PROFILER")) $timer = time()+(float)microtime();
	$t0 = "";
	// REQUETE
	$iter = IterFactory::create(
		"SQL",
		$command,
		array('plugins/auto/menus/v1.7.26/inclure/menu.html','html_712ae78c0743d1b08ef392526d502dc4','_menu',1,$GLOBALS['spip_lang'])
	);
	if (!$iter->err()) {
	$SP++;
	// RESULTATS
	while ($Pile[$SP]=$iter->fetch()) {

		$t0 .= (
'
	' .
(($t1 = BOUCLE_entreeshtml_712ae78c0743d1b08ef392526d502dc4($Cache, $Pile, $doublons, $Numrows, $SP))!=='' ?
		((	'
	<ul class="menu-liste menu-items' .
		(($t3 = strval(interdire_scripts(entites_html(table_valeur(@$Pile[0], (string)'class', null),true))))!=='' ?
				(' ' . $t3) :
				'') .
		((defined('_MENUS_EXTRA_CLASS'))  ?
				(' ' . constant('_MENUS_EXTRA_CLASS')) :
				'') .
		(($t3 = strval(interdire_scripts($Pile[$SP]['css'])))!=='' ?
				(' ' . $t3) :
				'') .
		'"' .
		(($t3 = strval(interdire_scripts(entites_html(sinon(table_valeur(@$Pile[0], (string)'depth', null), '0'),true))))!=='' ?
				(' data-depth="' . $t3 . '"') :
				'') .
		'>
		') . $t1 . '
	</ul>
	') :
		'') .
'
');
	}
	$iter->free();
	}
	if (defined("_BOUCLE_PROFILER")
	AND 1000*($timer = (time()+(float)microtime())-$timer) > _BOUCLE_PROFILER)
		spip_log(intval(1000*$timer)."ms BOUCLE_menu @ plugins/auto/menus/v1.7.26/inclure/menu.html","profiler"._LOG_AVERTISSEMENT);
	return $t0;
}

//
// Fonction principale du squelette plugins/auto/menus/v1.7.26/inclure/menu.html
// Temps de compilation total: 6.308 ms
//

function html_712ae78c0743d1b08ef392526d502dc4($Cache, $Pile, $doublons = array(), $Numrows = array(), $SP = 0) {

	if (isset($Pile[0]["doublons"]) AND is_array($Pile[0]["doublons"]))
		$doublons = nettoyer_env_doublons($Pile[0]["doublons"]);

	$connect = '';
	$page = (
BOUCLE_menuhtml_712ae78c0743d1b08ef392526d502dc4($Cache, $Pile, $doublons, $Numrows, $SP) .
'
' .
'<' . '?php header("X-Spip-Filtre: '.'trim' . '"); ?'.'>');

	return analyse_resultat_skel('html_712ae78c0743d1b08ef392526d502dc4', $Cache, $page, 'plugins/auto/menus/v1.7.26/inclure/menu.html');
}
?>