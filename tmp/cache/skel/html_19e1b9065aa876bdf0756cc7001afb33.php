<?php

/*
 * Squelette : plugins/auto/zcore/v2.8.7/404.html
 * Date :      Mon, 30 Mar 2020 16:36:24 GMT
 * Compile :   Wed, 17 Jun 2020 08:56:36 GMT
 * Boucles :   
 */ 
//
// Fonction principale du squelette plugins/auto/zcore/v2.8.7/404.html
// Temps de compilation total: 0.144 ms
//

function html_19e1b9065aa876bdf0756cc7001afb33($Cache, $Pile, $doublons = array(), $Numrows = array(), $SP = 0) {

	if (isset($Pile[0]["doublons"]) AND is_array($Pile[0]["doublons"]))
		$doublons = nettoyer_env_doublons($Pile[0]["doublons"]);

	$connect = '';
	$page = (
'<'.'?php header(' . _q(concat('HTTP/1.0 ',interdire_scripts(entites_html(sinon(table_valeur(@$Pile[0], (string)'code', null), '404 Not Found'),true)))) . '); ?'.'>' .
'<'.'?php header(' . _q('Cache-Control: no-store, no-cache, must-revalidate') . '); ?'.'>' .
'<'.'?php header(' . _q('Pragma: no-cache') . '); ?'.'>' .

'<'.'?php echo recuperer_fond( ' . argumenter_squelette('structure') . ', array_merge('.var_export($Pile[0],1).',array(\'type-page\' => ' . argumenter_squelette(interdire_scripts(entites_html(sinon(table_valeur(@$Pile[0], (string)'status', null), '404'),true))) . ',
	\'lang\' => ' . argumenter_squelette($GLOBALS["spip_lang"]) . ')), array("compil"=>array(\'plugins/auto/zcore/v2.8.7/404.html\',\'html_19e1b9065aa876bdf0756cc7001afb33\',\'\',4,$GLOBALS[\'spip_lang\'])), _request("connect"));
?'.'>
');

	return analyse_resultat_skel('html_19e1b9065aa876bdf0756cc7001afb33', $Cache, $page, 'plugins/auto/zcore/v2.8.7/404.html');
}
?>