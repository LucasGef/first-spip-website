<?php

/*
 * Squelette : plugins/auto/comments/v3.5.0/formulaires/forum.html
 * Date :      Thu, 26 Mar 2020 19:57:32 GMT
 * Compile :   Wed, 17 Jun 2020 09:02:58 GMT
 * Boucles :   
 */ 
//
// Fonction principale du squelette plugins/auto/comments/v3.5.0/formulaires/forum.html
// Temps de compilation total: 1.414 ms
//

function html_b47f69b252a03730accce63c77b3b341($Cache, $Pile, $doublons = array(), $Numrows = array(), $SP = 0) {

	if (isset($Pile[0]["doublons"]) AND is_array($Pile[0]["doublons"]))
		$doublons = nettoyer_env_doublons($Pile[0]["doublons"]);

	$connect = '';
	$page = (
'<div class="formulaire_spip formulaire_forum ajax" id="formulaire_forum">

	' .
(($t1 = strval(interdire_scripts(table_valeur(@$Pile[0], (string)'message_ok', null))))!=='' ?
		('<p class="reponse_formulaire reponse_formulaire_ok">' . $t1 . '</p>') :
		'') .
'
	' .
(($t1 = strval(interdire_scripts(table_valeur(@$Pile[0], (string)'message_erreur', null))))!=='' ?
		('<p class="reponse_formulaire reponse_formulaire_erreur">' . $t1 . '</p>') :
		'') .
'

	' .
(($t1 = strval(interdire_scripts(entites_html(table_valeur(@$Pile[0], (string)'login_forum_abo', null),true))))!=='' ?
		($t1 . (	'
	' .
	
'<'.'?php echo recuperer_fond( ' . argumenter_squelette('formulaires/inc-login_forum_abo') . ', array_merge('.var_export($Pile[0],1).',array(\'lang\' => ' . argumenter_squelette($GLOBALS["spip_lang"]) . ')), array("compil"=>array(\'plugins/auto/comments/v3.5.0/formulaires/forum.html\',\'html_b47f69b252a03730accce63c77b3b341\',\'\',7,$GLOBALS[\'spip_lang\'])), _request("connect"));
?'.'>
	')) :
		'') .
'

	' .
(($t1 = strval(interdire_scripts(entites_html(table_valeur(@$Pile[0], (string)'editable', null),true))))!=='' ?
		($t1 . (	'


	' .
	(($t2 = strval(interdire_scripts(table_valeur(table_valeur(@$Pile[0], (string)'erreurs', null),'previsu'))))!=='' ?
			((	'
	<form action="' .
		interdire_scripts(entites_html(table_valeur(@$Pile[0], (string)'action', null),true)) .
		'#formulaire_forum" method="post" class="preview">
		<div>
			' .
			'<div>' .
	form_hidden(interdire_scripts(entites_html(table_valeur(@$Pile[0], (string)'action', null),true))) .
	'<input name=\'formulaire_action\' type=\'hidden\'
		value=\'' . @$Pile[0]['form'] . '\' />' .
	'<input name=\'formulaire_action_args\' type=\'hidden\'
		value=\'' . @$Pile[0]['formulaire_args']. '\' />' .
	(!empty($Pile[0]['_hidden']) ? @$Pile[0]['_hidden'] : '') .
	'</div>' .
		'
			<input type=\'hidden\' name=\'titre\' value="' .
		interdire_scripts(entites_html(table_valeur(@$Pile[0], (string)'titre', null),true)) .
		'"/>
			<input type=\'hidden\' name=\'texte\' value="' .
		interdire_scripts(entites_html(table_valeur(@$Pile[0], (string)'texte', null),true)) .
		'"/>
			<input type=\'hidden\' name=\'url_site\' value="' .
		interdire_scripts(entites_html(table_valeur(@$Pile[0], (string)'url_site', null),true)) .
		'"/>
			<input type=\'hidden\' name=\'nom_site\' value="' .
		interdire_scripts(entites_html(table_valeur(@$Pile[0], (string)'nom_site', null),true)) .
		'"/>
			' .
		(($t3 = strval(interdire_scripts(table_valeur(@$Pile[0], (string)'id_forum', null))))!=='' ?
				('<input type="hidden" name="id_forum" value="' . $t3 . '"/>') :
				'') .
		'
			' .
		(($t3 = strval(interdire_scripts(table_valeur(@$Pile[0], (string)'notification', null))))!=='' ?
				('<input type="hidden" name="notification" value="' . $t3 . '"/>') :
				'') .
		'
			' .
		recuperer_fond( 'formulaires/inc-forum_ajouter_mot' , array('ajouter_mot' => @$Pile[0]['ajouter_mot'] ), array('compil'=>array('plugins/auto/comments/v3.5.0/formulaires/forum.html','html_b47f69b252a03730accce63c77b3b341','',14,$GLOBALS['spip_lang'])), _request('connect')) .
		'
			') . $t2 . '
		</div>
	</form>
	') :
			'') .
	'


	<form action="' .
	interdire_scripts(entites_html(table_valeur(@$Pile[0], (string)'action', null),true)) .
	'#formulaire_forum" method="post" enctype=\'multipart/form-data\'>
		<div>
			' .
		'<div>' .
	form_hidden(interdire_scripts(entites_html(table_valeur(@$Pile[0], (string)'action', null),true))) .
	'<input name=\'formulaire_action\' type=\'hidden\'
		value=\'' . @$Pile[0]['form'] . '\' />' .
	'<input name=\'formulaire_action_args\' type=\'hidden\'
		value=\'' . @$Pile[0]['formulaire_args']. '\' />' .
	(!empty($Pile[0]['_hidden']) ? @$Pile[0]['_hidden'] : '') .
	'</div>
			' .
	(($t2 = strval(interdire_scripts(table_valeur(@$Pile[0], (string)'id_forum', null))))!=='' ?
			('<input type="hidden" name="id_forum" value="' . $t2 . '"/>') :
			'') .
	'
			' .
	(($t2 = strval(interdire_scripts(entites_html(table_valeur(@$Pile[0], (string)'modere', null),true))))!=='' ?
			((	'
			<fieldset class="fieldset moderation_info">
				<legend>' .
		_T('forum:bouton_radio_modere_priori') .
		'</legend>
				<p class="explication">') . $t2 . (	'
					' .
		_T('comments:moderation_info') .
		'
				</p>
			</fieldset>
			')) :
			'') .
	'

			' .
	(($t2 = strval(choixsiegal(table_valeur(@$Pile[0], (string)'afficher_texte', null),'non',' ','')))!=='' ?
			($t2 . (	'
			' .
		(($t3 = strval(interdire_scripts(entites_html(table_valeur(@$Pile[0], (string)'titre', null),true))))!=='' ?
				('<input type="hidden" name="titre" value="' . $t3 . '"/>') :
				'') .
		'
			<p class="spip_bouton"><input type="submit" class="submit" value="' .
		_T('forum:forum_valider') .
		'"/></p>')) :
			'') .
	'

			' .
	(($t2 = strval(choixsiegal(table_valeur(@$Pile[0], (string)'afficher_texte', null),'non','',' ')))!=='' ?
			($t2 . (	'

			' .
		
'<'.'?php echo recuperer_fond( ' . argumenter_squelette('formulaires/inc-login_forum') . ', array_merge('.var_export($Pile[0],1).',array(\'lang\' => ' . argumenter_squelette($GLOBALS["spip_lang"]) . ')), array("compil"=>array(\'plugins/auto/comments/v3.5.0/formulaires/forum.html\',\'html_b47f69b252a03730accce63c77b3b341\',\'\',24,$GLOBALS[\'spip_lang\'])), _request("connect"));
?'.'>

			<fieldset class="fieldset fieldset-texte">
				<legend>' .
		_T('comments:saisie_texte_legend') .
		'</legend>
				<input type="hidden" name="titre" id="titre"' .
		(($t3 = strval(interdire_scripts(entites_html(table_valeur(@$Pile[0], (string)'titre', null),true))))!=='' ?
				(' value="' . $t3 . '"') :
				'') .
		' />
				<' .
		((($a = 'div') OR (is_string($a) AND strlen($a))) ? $a : 'ul') .
		' class="editer-groupe">
					' .
		interdire_scripts((((include_spip('inc/config')?lire_config('forums_texte',null,false):'') != 'non') ? (	'<' .
			((($a = 'div') OR (is_string($a) AND strlen($a))) ? $a : 'li') .
			' class=\'editer saisie_texte' .
			((table_valeur(table_valeur(@$Pile[0], (string)'erreurs', null),'texte'))  ?
					(' ' . ' ' . 'erreur') :
					'') .
			' obligatoire\'>
						<label for=\'texte\'>' .
			typo(_T('forum:forum_texte')) .
			'</label>
						' .
			(($t4 = strval(table_valeur(table_valeur(@$Pile[0], (string)'erreurs', null),'texte')))!=='' ?
					('<span class=\'erreur_message\'>' . $t4 . '</span>') :
					'') .
			'
						<p class="explication saisie_texte_info">
							' .
			_T('comments:saisie_texte_info') .
			'
						</p>
						<textarea name="texte" id="texte" rows="12" cols="60"' .
			(('')  ?
					(' ' . '
						required="required"') :
					'') .
			(($t4 = strval(interdire_scripts(((((include_spip('inc/config')?lire_config('forums_afficher_barre',null,false):'') == 'non')) ?' ' :''))))!=='' ?
					($t4 . '
						class="no_barre"') :
					'') .
			'>' .
			interdire_scripts(entites_html(table_valeur(@$Pile[0], (string)'texte', null),true)) .
			'</textarea>
					</' .
			((($a = 'div') OR (is_string($a) AND strlen($a))) ? $a : 'li') .
			'>
					'):'')) .
		'
					' .
		(($t3 = strval(recuperer_fond( 'formulaires/inc-forum_bloc_choix_mots' , array('table' => interdire_scripts(table_valeur(@$Pile[0], (string)'table', null)) ,
	'ajouter_mot' => @$Pile[0]['ajouter_mot'] ), array('compil'=>array('plugins/auto/comments/v3.5.0/formulaires/forum.html','html_b47f69b252a03730accce63c77b3b341','',0,$GLOBALS['spip_lang'])), _request('connect'))))!=='' ?
				((	'
					<' .
			((($a = 'div') OR (is_string($a) AND strlen($a))) ? $a : 'li') .
			' class=\'saisie_mots_forum\'>
						') . $t3 . (	'
					</' .
			((($a = 'div') OR (is_string($a) AND strlen($a))) ? $a : 'li') .
			'>
					')) :
				'') .
		'
					' .
		(($t3 = strval(interdire_scripts(((filtre_info_plugin_dist("notifications", "est_actif")) ?' ' :''))))!=='' ?
				($t3 . (	'
					' .
			vide($Pile['vars'][$_zzz=(string)'name'] = 'notification') .
			vide($Pile['vars'][$_zzz=(string)'obli'] = '') .
			vide($Pile['vars'][$_zzz=(string)'defaut'] = '1') .
			vide($Pile['vars'][$_zzz=(string)'erreurs'] = table_valeur(table_valeur(@$Pile[0], (string)'erreurs', null),table_valeur($Pile["vars"], (string)'name', null))) .
			'<' .
			((($a = 'div') OR (is_string($a) AND strlen($a))) ? $a : 'li') .
			' class="editer pleine_largeur editer_' .
			table_valeur($Pile["vars"], (string)'name', null) .
			(($t4 = strval(table_valeur($Pile["vars"], (string)'obli', null)))!=='' ?
					(' ' . $t4) :
					'') .
			((table_valeur($Pile["vars"], (string)'erreurs', null))  ?
					(' ' . ' ' . 'erreur') :
					'') .
			'">' .
			(($t4 = strval(table_valeur($Pile["vars"], (string)'erreurs', null)))!=='' ?
					('
						<span class=\'erreur_message\'>' . $t4 . '</span>
						') :
					'') .
			'<input type="hidden" name="' .
			table_valeur($Pile["vars"], (string)'name', null) .
			'" value="0" />
						' .
			vide($Pile['vars'][$_zzz=(string)'val'] = '1') .
			'<div class="choix">
							<input type="checkbox" name="' .
			table_valeur($Pile["vars"], (string)'name', null) .
			'" class="checkbox" id="' .
			table_valeur($Pile["vars"], (string)'name', null) .
			'_' .
			table_valeur($Pile["vars"], (string)'val', null) .
			'" value="' .
			table_valeur($Pile["vars"], (string)'val', null) .
			'"' .
			(((table_valeur($Pile["vars"], (string)'val', null) == interdire_scripts(entites_html(sinon(table_valeur(@$Pile[0], (string)table_valeur($Pile["vars"], (string)'name', null), null), table_valeur($Pile["vars"], (string)'defaut', null)),true))))  ?
					(' ' . 'checked="checked"') :
					'') .
			' />
							<label for="' .
			table_valeur($Pile["vars"], (string)'name', null) .
			'_' .
			table_valeur($Pile["vars"], (string)'val', null) .
			'">' .
			_T('comments:label_notification') .
			'</label>
						</div>
					</' .
			((($a = 'div') OR (is_string($a) AND strlen($a))) ? $a : 'li') .
			'>
					')) :
				'') .
		'
				</' .
		((($a = 'div') OR (is_string($a) AND strlen($a))) ? $a : 'ul') .
		'>
			</fieldset>

			' .
		(($t3 = strval(interdire_scripts(entites_html(table_valeur(@$Pile[0], (string)'cle_ajouter_document', null),true))))!=='' ?
				((	'
			<fieldset class="fieldset fieldset-documents">
				<legend>' .
			_T('medias:bouton_ajouter_document') .
			'</legend>
				<' .
			((($a = 'div') OR (is_string($a) AND strlen($a))) ? $a : 'ul') .
			' class="editer-groupe">
					<' .
			((($a = 'div') OR (is_string($a) AND strlen($a))) ? $a : 'li') .
			' class=\'editer saisie_document_forum' .
			((table_valeur(table_valeur(@$Pile[0], (string)'erreurs', null),'document_forum'))  ?
					(' ' . ' ' . 'erreur') :
					'') .
			'\'>
						' .
			(($t4 = strval(table_valeur(table_valeur(@$Pile[0], (string)'erreurs', null),'document_forum')))!=='' ?
					('<span class=\'erreur_message\'>' . $t4 . '</span>') :
					'') .
			'
						<input type="hidden" name="cle_ajouter_document" value="') . $t3 . (	'"/>
						' .
			(($t4 = strval(interdire_scripts(entites_html(table_valeur(@$Pile[0], (string)'ajouter_document', null),true))))!=='' ?
					('
						<div id="ajouter_document_up">' . $t4 . (	'
							<label for="supprimer_document_ajoute"><input type=\'checkbox\' name=\'supprimer_document_ajoute\'
							                                              id=\'supprimer_document_ajoute\'/>
								' .
				_T('public|spip|ecrire:lien_supprimer') .
				'
							</label>
						</div>
						')) :
					'') .
			'
						<div>
							' .
			(($t4 = strval(interdire_scripts((is_array(entites_html(table_valeur(@$Pile[0], (string)'formats_documents_forum', null),true)) ? interdire_scripts(filtre_implode_dist(entites_html(table_valeur(@$Pile[0], (string)'formats_documents_forum', null),true),', ')):interdire_scripts(entites_html(table_valeur(@$Pile[0], (string)'formats_documents_forum', null),true))))))!=='' ?
					('<label for="ajouter_document">' . $t4 . '</label>') :
					'') .
			'
							<input class=\'file\' type="file" name="ajouter_document" id="ajouter_document"' .
			(($t4 = strval(interdire_scripts((is_array(entites_html(table_valeur(@$Pile[0], (string)'formats_documents_forum', null),true)) ? interdire_scripts(filtre_implode_dist(entites_html(table_valeur(@$Pile[0], (string)'formats_documents_forum', null),true),', ')):''))))!=='' ?
					('
							accept="' . $t4 . '"') :
					'') .
			' />
						</div>

						<script type=\'text/javascript\'>/*<![CDATA[*/
						if (window.jQuery) jQuery(function (){
							jQuery(\'#ajouter_document_up\')
								.next().hide()
								.prev().find(\':checkbox\').bind(\'change\', function (){
									jQuery(\'#ajouter_document_up\').hide().next().show();
								});
						});
						/*]]>*/</script>
					</' .
			((($a = 'div') OR (is_string($a) AND strlen($a))) ? $a : 'li') .
			'>
				</' .
			((($a = 'div') OR (is_string($a) AND strlen($a))) ? $a : 'ul') .
			'>
			</fieldset>
			')) :
				'') .
		'

			
			<p style="display: none;">
				<label for="nobot_forum">' .
		_T('public|spip|ecrire:antispam_champ_vide') .
		'</label>
				<input type="text" class="text" name="nobot" id="nobot_forum" value="' .
		interdire_scripts(entites_html(table_valeur(@$Pile[0], (string)'nobot', null),true)) .
		'" size="10"/>
			</p>
			<p class="boutons"><input type="submit" class="submit" name="previsualiser_message" value="' .
		_T('comments:submit1') .
		'"/>' .
		(($t3 = strval(interdire_scripts(((((((entites_html(table_valeur(@$Pile[0], (string)'forcer_previsu', null),true) == 'non')) AND (interdire_scripts(((table_valeur(table_valeur(@$Pile[0], (string)'erreurs', null),'previsu')) ?'' :' ')))) ?' ' :'')) ?' ' :''))))!=='' ?
				(' 
			' . $t3 . (	'<input type="submit" class="submit" name="envoyer_message" value="' .
			_T('forum:forum_envoyer') .
			'" />')) :
				'') .
		'</p>
			')) :
			'') .
	'
		</div>
	</form>
	')) :
		'') .
'
</div>
');

	return analyse_resultat_skel('html_b47f69b252a03730accce63c77b3b341', $Cache, $page, 'plugins/auto/comments/v3.5.0/formulaires/forum.html');
}
?>