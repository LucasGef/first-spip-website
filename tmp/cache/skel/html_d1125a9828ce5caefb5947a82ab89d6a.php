<?php

/*
 * Squelette : ../plugins/auto/menus/v1.7.26/prive/squelettes/navigation/menu_edit.html
 * Date :      Mon, 08 Jun 2020 15:55:26 GMT
 * Compile :   Tue, 16 Jun 2020 14:50:01 GMT
 * Boucles :   _liens
 */ 

function BOUCLE_lienshtml_d1125a9828ce5caefb5947a82ab89d6a(&$Cache, &$Pile, &$doublons, &$Numrows, $SP) {

	static $command = array();
	static $connect;
	$command['connect'] = $connect = '';
	if (!isset($command['table'])) {
		$command['table'] = 'menus_liens';
		$command['id'] = '_liens';
		$command['from'] = array('menus_liens' => 'spip_menus_liens');
		$command['type'] = array();
		$command['groupby'] = array();
		$command['select'] = array("menus_liens.objet",
		"menus_liens.id_objet",
		"menus_liens.id_menu");
		$command['orderby'] = array();
		$command['join'] = array();
		$command['limit'] = '';
		$command['having'] = 
			array();
	}
	$command['where'] = 
			array(
			array('=', 'menus_liens.id_menu', sql_quote(@$Pile[0]['id_menu'], '','bigint(21) NOT NULL DEFAULT \'0\'')));
	if (defined("_BOUCLE_PROFILER")) $timer = time()+(float)microtime();
	$t0 = "";
	// REQUETE
	$iter = IterFactory::create(
		"SQL",
		$command,
		array('../plugins/auto/menus/v1.7.26/prive/squelettes/navigation/menu_edit.html','html_d1125a9828ce5caefb5947a82ab89d6a','_liens',29,$GLOBALS['spip_lang'])
	);
	if (!$iter->err()) {
	$Numrows['_liens']['total'] = @intval($iter->count());
	
	$l1 = _T('menus:retirer_lien_objet');$SP++;
	// RESULTATS
	while ($Pile[$SP]=$iter->fetch()) {

		$t0 .= (
'
		<li class=\'item ' .
interdire_scripts($Pile[$SP]['objet']) .
'\'>' .
(($t1 = strval(interdire_scripts(filtre_balise_img_dist(chemin_image(interdire_scripts(concat(objet_info($Pile[$SP]['objet'],'icone_objet'),'-16.png')))))))!=='' ?
		($t1 . ' ') :
		'') .
generer_lien_entite($Pile[$SP]['id_objet'],interdire_scripts($Pile[$SP]['objet'])) .
'
		' .
(($t1 = strval(invalideur_session($Cache, ((((function_exists("autoriser")||include_spip("inc/autoriser"))&&autoriser('dissociermenus', interdire_scripts(invalideur_session($Cache, $Pile[$SP]['objet'])), invalideur_session($Cache, $Pile[$SP]['id_objet']))?" ":"")) ?' ' :''))))!=='' ?
		($t1 . (	'
			' .
	bouton_action($l1,invalideur_session($Cache, generer_action_auteur('dissocier_menu',(	invalideur_session($Cache, $Pile[$SP]['id_menu']) .
			'-' .
			interdire_scripts(invalideur_session($Cache, $Pile[$SP]['objet'])) .
			'-' .
			invalideur_session($Cache, $Pile[$SP]['id_objet'])),invalideur_session($Cache, self()))),'ajax') .
	'
		')) :
		'') .
'
		</li>
		');
	}
	$iter->free();
	}
	if (defined("_BOUCLE_PROFILER")
	AND 1000*($timer = (time()+(float)microtime())-$timer) > _BOUCLE_PROFILER)
		spip_log(intval(1000*$timer)."ms BOUCLE_liens @ ../plugins/auto/menus/v1.7.26/prive/squelettes/navigation/menu_edit.html","profiler"._LOG_AVERTISSEMENT);
	return $t0;
}

//
// Fonction principale du squelette ../plugins/auto/menus/v1.7.26/prive/squelettes/navigation/menu_edit.html
// Temps de compilation total: 7.424 ms
//

function html_d1125a9828ce5caefb5947a82ab89d6a($Cache, $Pile, $doublons = array(), $Numrows = array(), $SP = 0) {

	if (isset($Pile[0]["doublons"]) AND is_array($Pile[0]["doublons"]))
		$doublons = nettoyer_env_doublons($Pile[0]["doublons"]);

	$connect = '';
	$page = (
(($t1 = strval(interdire_scripts(((intval(entites_html(sinon(table_valeur(@$Pile[0], (string)'id_menu', null), '0'),true))) ?' ' :''))))!=='' ?
		($t1 . (	'
' .
	boite_ouvrir('', 'info') .
	'
	<div class="infos">
		<div class="numero">
			' .
	_T('menus:info_numero_menu') .
	'
			<p>' .
	interdire_scripts(entites_html(table_valeur(@$Pile[0], (string)'id_menu', null),true)) .
	'</p>
		</div>
		' .
	filtre_icone_horizontale_dist(parametre_url(generer_url_action('redirect',(	'type=menu&id=' .
		interdire_scripts(entites_html(table_valeur(@$Pile[0], (string)'id_menu', null),true)))),'var_mode','calcul'),_T('public|spip|ecrire:icone_voir_en_ligne'),'racine') .
	'

		' .
	(($t2 = strval(interdire_scripts(((filtre_info_plugin_dist("yaml", "est_actif")) ?' ' :''))))!=='' ?
			($t2 . (	'
			' .
		invalideur_session($Cache, filtre_icone_horizontale_dist(generer_action_auteur('exporter_menu',interdire_scripts(invalideur_session($Cache, entites_html(table_valeur(@$Pile[0], (string)'id_menu', null),true)))),_T('menus:editer_menus_exporter'),'menu-export-24.png')) .
		'
		')) :
			'') .
	'

		' .
	bouton_action((	(($t3 = strval(interdire_scripts(filtre_balise_img_dist(chemin_image('menu-del-24')))))!=='' ?
				($t3 . ' ') :
				'') .
		_T('menus:formulaire_supprimer_menu')),invalideur_session($Cache, generer_action_auteur('supprimer_menu',interdire_scripts(invalideur_session($Cache, entites_html(table_valeur(@$Pile[0], (string)'id_menu', null),true))),interdire_scripts(invalideur_session($Cache, entites_html(sinon(table_valeur(@$Pile[0], (string)'redirect', null), invalideur_session($Cache, generer_url_ecrire('menus'))),true))))),'link icone horizontale danger',(	_T('menus:confirmer_supprimer_menu') .
		'
		')) .
	'

	</div>
' .
	boite_fermer() .
	'
')) :
		'') .
'

' .
(($t1 = BOUCLE_lienshtml_d1125a9828ce5caefb5947a82ab89d6a($Cache, $Pile, $doublons, $Numrows, $SP))!=='' ?
		((	'
' .
		boite_ouvrir(singulier_ou_pluriel($Numrows['_liens']['total'],'medias:une_utilisation','medias:des_utilisations'), 'menu utilisations simple') .
		'
	<ul class=\'liste_items\'>
		') . $t1 . (	'
	</ul>
' .
		boite_fermer() .
		'
')) :
		''));

	return analyse_resultat_skel('html_d1125a9828ce5caefb5947a82ab89d6a', $Cache, $page, '../plugins/auto/menus/v1.7.26/prive/squelettes/navigation/menu_edit.html');
}
?>