<?php

/*
 * Squelette : plugins/auto/spipr_dist/v2.2.6/cartouche/404.html
 * Date :      Mon, 30 Mar 2020 09:25:24 GMT
 * Compile :   Wed, 17 Jun 2020 08:56:36 GMT
 * Boucles :   
 */ 
//
// Fonction principale du squelette plugins/auto/spipr_dist/v2.2.6/cartouche/404.html
// Temps de compilation total: 0.048 ms
//

function html_41bb18d2b521bcb0e5ce108c44d8c6c3($Cache, $Pile, $doublons = array(), $Numrows = array(), $SP = 0) {

	if (isset($Pile[0]["doublons"]) AND is_array($Pile[0]["doublons"]))
		$doublons = nettoyer_env_doublons($Pile[0]["doublons"]);

	$connect = '';
	$page = (
'<header class="cartouche">
	<h1>' .
_T('public|spip|ecrire:pass_erreur') .
' 404</h1>
</header>');

	return analyse_resultat_skel('html_41bb18d2b521bcb0e5ce108c44d8c6c3', $Cache, $page, 'plugins/auto/spipr_dist/v2.2.6/cartouche/404.html');
}
?>