<?php

/*
 * Squelette : plugins/auto/spipr_dist/v2.2.6/cartouche/article.html
 * Date :      Mon, 30 Mar 2020 09:25:24 GMT
 * Compile :   Wed, 17 Jun 2020 09:02:58 GMT
 * Boucles :   _nb_commentaires, _combien, _tags, _cartouche
 */ 

function BOUCLE_nb_commentaireshtml_192662eb0fe4c74f7fcec6e7b8809bcc(&$Cache, &$Pile, &$doublons, &$Numrows, $SP) {

	static $command = array();
	static $connect;
	$command['connect'] = $connect = '';
	if (!isset($command['table'])) {
		$command['table'] = 'forum';
		$command['id'] = '_nb_commentaires';
		$command['from'] = array('forum' => 'spip_forum');
		$command['type'] = array();
		$command['groupby'] = array();
		$command['select'] = array("count(*)");
		$command['orderby'] = array();
		$command['join'] = array();
		$command['limit'] = '';
		$command['having'] = 
			array();
	}
	$command['where'] = 
			array(
quete_condition_statut('forum.statut','publie,prop','publie',''), 
			array('=', 'forum.id_objet', sql_quote($Pile[$SP]['id_article'], '','bigint(21) NOT NULL DEFAULT \'0\'')), 
			array('=', 'forum.objet', sql_quote('article')));
	if (defined("_BOUCLE_PROFILER")) $timer = time()+(float)microtime();
	$t0 = "";
	// REQUETE
	$iter = IterFactory::create(
		"SQL",
		$command,
		array('plugins/auto/spipr_dist/v2.2.6/cartouche/article.html','html_192662eb0fe4c74f7fcec6e7b8809bcc','_nb_commentaires',13,$GLOBALS['spip_lang'])
	);
	if (!$iter->err()) {
	$Numrows['_nb_commentaires']['total'] = @intval($iter->count());
	$SP++;
	// RESULTATS
	
	$iter->free();
	}
	if (defined("_BOUCLE_PROFILER")
	AND 1000*($timer = (time()+(float)microtime())-$timer) > _BOUCLE_PROFILER)
		spip_log(intval(1000*$timer)."ms BOUCLE_nb_commentaires @ plugins/auto/spipr_dist/v2.2.6/cartouche/article.html","profiler"._LOG_AVERTISSEMENT);
	return $t0;
}


function BOUCLE_combienhtml_192662eb0fe4c74f7fcec6e7b8809bcc(&$Cache, &$Pile, &$doublons, &$Numrows, $SP) {

	static $command = array();
	static $connect;
	$command['connect'] = $connect = '';
	if (!isset($command['table'])) {
		$command['table'] = 'signatures';
		$command['id'] = '_combien';
		$command['from'] = array('signatures' => 'spip_signatures','L1' => 'spip_petitions');
		$command['type'] = array();
		$command['groupby'] = array();
		$command['select'] = array("count(*)");
		$command['orderby'] = array();
		$command['join'] = array('L1' => array('signatures','id_petition'));
		$command['limit'] = '';
		$command['having'] = 
			array();
	}
	$command['where'] = 
			array(
quete_condition_statut('signatures.statut','publie','publie',''), 
			array('=', 'L1.id_article', sql_quote($Pile[$SP]['id_article'], '','bigint(21) NOT NULL DEFAULT \'0\'')));
	if (defined("_BOUCLE_PROFILER")) $timer = time()+(float)microtime();
	$t0 = "";
	// REQUETE
	$iter = IterFactory::create(
		"SQL",
		$command,
		array('plugins/auto/spipr_dist/v2.2.6/cartouche/article.html','html_192662eb0fe4c74f7fcec6e7b8809bcc','_combien',20,$GLOBALS['spip_lang'])
	);
	if (!$iter->err()) {
	$Numrows['_combien']['total'] = @intval($iter->count());
	$SP++;
	// RESULTATS
	
	$iter->free();
	}
	if (defined("_BOUCLE_PROFILER")
	AND 1000*($timer = (time()+(float)microtime())-$timer) > _BOUCLE_PROFILER)
		spip_log(intval(1000*$timer)."ms BOUCLE_combien @ plugins/auto/spipr_dist/v2.2.6/cartouche/article.html","profiler"._LOG_AVERTISSEMENT);
	return $t0;
}


function BOUCLE_tagshtml_192662eb0fe4c74f7fcec6e7b8809bcc(&$Cache, &$Pile, &$doublons, &$Numrows, $SP) {

	static $command = array();
	static $connect;
	$command['connect'] = $connect = '';
	if (!isset($command['table'])) {
		$command['table'] = 'mots';
		$command['id'] = '_tags';
		$command['from'] = array('mots' => 'spip_mots','L1' => 'spip_mots_liens');
		$command['type'] = array();
		$command['groupby'] = array();
		$command['select'] = array("mots.id_mot",
		"mots.titre");
		$command['orderby'] = array();
		$command['join'] = array('L1' => array('mots','id_mot'));
		$command['limit'] = '';
		$command['having'] = 
			array();
	}
	$command['where'] = 
			array(
			array('=', 'L1.id_objet', sql_quote($Pile[$SP]['id_article'], '','bigint(21) NOT NULL DEFAULT \'0\'')), 
			array('=', 'L1.objet', sql_quote('article')));
	if (defined("_BOUCLE_PROFILER")) $timer = time()+(float)microtime();
	$t0 = "";
	// REQUETE
	$iter = IterFactory::create(
		"SQL",
		$command,
		array('plugins/auto/spipr_dist/v2.2.6/cartouche/article.html','html_192662eb0fe4c74f7fcec6e7b8809bcc','_tags',27,$GLOBALS['spip_lang'])
	);
	if (!$iter->err()) {
	$SP++;
	// RESULTATS
	while ($Pile[$SP]=$iter->fetch()) {

		$t0 .= (
'<li class="list-inline-item"><a class="label label-default" href="' .
vider_url(urlencode_1738(generer_url_entite($Pile[$SP]['id_mot'], 'mot', '', '', true))) .
'">' .
afficher_icone_svg('tag', '', '') .
interdire_scripts(supprimer_numero(typo($Pile[$SP]['titre']), "TYPO", $connect, $Pile[0])) .
'</a></li>');
	}
	$iter->free();
	}
	if (defined("_BOUCLE_PROFILER")
	AND 1000*($timer = (time()+(float)microtime())-$timer) > _BOUCLE_PROFILER)
		spip_log(intval(1000*$timer)."ms BOUCLE_tags @ plugins/auto/spipr_dist/v2.2.6/cartouche/article.html","profiler"._LOG_AVERTISSEMENT);
	return $t0;
}


function BOUCLE_cartouchehtml_192662eb0fe4c74f7fcec6e7b8809bcc(&$Cache, &$Pile, &$doublons, &$Numrows, $SP) {

	static $command = array();
	static $connect;
	$command['connect'] = $connect = '';
	if (!isset($command['table'])) {
		$command['table'] = 'articles';
		$command['id'] = '_cartouche';
		$command['from'] = array('articles' => 'spip_articles');
		$command['type'] = array();
		$command['groupby'] = array();
		$command['select'] = array("articles.id_article",
		"articles.surtitre",
		"articles.titre",
		"articles.soustitre",
		"articles.date",
		"articles.lang");
		$command['orderby'] = array();
		$command['join'] = array();
		$command['limit'] = '';
		$command['having'] = 
			array();
	}
	$command['where'] = 
			array(
quete_condition_statut('articles.statut','publie,prop,prepa/auteur','publie',''), 
quete_condition_postdates('articles.date',''), 
			array('=', 'articles.id_article', sql_quote(@$Pile[0]['id_article'], '','bigint(21) NOT NULL AUTO_INCREMENT')));
	if (defined("_BOUCLE_PROFILER")) $timer = time()+(float)microtime();
	$t0 = "";
	// REQUETE
	$iter = IterFactory::create(
		"SQL",
		$command,
		array('plugins/auto/spipr_dist/v2.2.6/cartouche/article.html','html_192662eb0fe4c74f7fcec6e7b8809bcc','_cartouche',1,$GLOBALS['spip_lang'])
	);
	if (!$iter->err()) {
	lang_select($GLOBALS['spip_lang']);
	$SP++;
	// RESULTATS
	while ($Pile[$SP]=$iter->fetch()) {

		lang_select_public($Pile[$SP]['lang'], '', $Pile[$SP]['titre']);
		$t0 .= (
'
<header class="cartouche">
	' .
filtrer('image_graver',filtrer('image_reduire',
((!is_array($l = quete_logo('id_article', 'ON', $Pile[$SP]['id_article'],'', 0))) ? '':
 ("<img class=\"spip_logo spip_logos\" alt=\"\" src=\"$l[0]\"" . $l[2] .  ($l[1] ? " onmouseover=\"this.src='$l[1]'\" onmouseout=\"this.src='$l[0]'\"" : "") . ' />')),'200','200')) .
'
	' .
(($t1 = strval(interdire_scripts(typo($Pile[$SP]['surtitre'], "TYPO", $connect, $Pile[0]))))!=='' ?
		((	'<p class="surtitre">') . $t1 . '</p>') :
		'') .
'
	<h1><span class="">' .
interdire_scripts(supprimer_numero(typo($Pile[$SP]['titre']), "TYPO", $connect, $Pile[0])) .
'</span>' .
(($t1 = strval(interdire_scripts(typo($Pile[$SP]['soustitre'], "TYPO", $connect, $Pile[0]))))!=='' ?
		((	'
		<small class="soustitre">') . $t1 . '</small>
		') :
		'') .
'</h1>
	
	<p class="publication"><time pubdate="pubdate" datetime="' .
interdire_scripts(date_iso(normaliser_date($Pile[$SP]['date']))) .
'">' .
afficher_icone_svg('calendar', '', '') .
(($t1 = strval(interdire_scripts(nom_jour(normaliser_date($Pile[$SP]['date'])))))!=='' ?
		($t1 . ' ') :
		'') .
interdire_scripts(affdate(normaliser_date($Pile[$SP]['date']))) .
'</time>' .
(($t1 = strval(recuperer_fond('modeles/lesauteurs', array('objet'=>'article','id_objet' => $Pile[$SP]['id_article'],'id_article' => $Pile[$SP]['id_article']), array('trim'=>true, 'compil'=>array('plugins/auto/spipr_dist/v2.2.6/cartouche/article.html','html_192662eb0fe4c74f7fcec6e7b8809bcc','_cartouche',4,$GLOBALS['spip_lang'])), '')))!=='' ?
		((	'<span class="authors"><span class="sep">, </span>' .
	afficher_icone_svg('user', '', '') .
	_T('public|spip|ecrire:par_auteur') .
	' ') . $t1 . '</span>') :
		'') .
'</p>
	
	<div class="postmeta">
		' .

	((($recurs=(isset($Pile[0]['recurs'])?$Pile[0]['recurs']:0))>=5)? '' :
	recuperer_fond('modeles/article_traductions', array('lang' => $GLOBALS["spip_lang"] ,
	'id_article'=>$Pile[$SP]['id_article'],
	'id'=>$Pile[$SP]['id_article'],
	'recurs'=>(++$recurs)), array('compil'=>array('plugins/auto/spipr_dist/v2.2.6/cartouche/article.html','html_192662eb0fe4c74f7fcec6e7b8809bcc','_cartouche',10,$GLOBALS['spip_lang']), 'trim'=>true), ''))
 .
BOUCLE_nb_commentaireshtml_192662eb0fe4c74f7fcec6e7b8809bcc($Cache, $Pile, $doublons, $Numrows, $SP)
. (	(($Numrows['_nb_commentaires']['total'])  ?
			(' ' . (	'
		<span class="comments">
			<span class="sep">|</span>
			<a' .
		(($t3 = strval(ancre_url('','comments')))!=='' ?
				(' href="' . $t3 . '"') :
				'') .
		' ' .
		(($t3 = strval(attribut_html(singulier_ou_pluriel($Numrows['_nb_commentaires']['total'],'zcore:info_1_commentaire','zcore:info_nb_commentaires'))))!=='' ?
				('title="' . $t3 . '"') :
				'') .
		'>' .
		(($t3 = strval($Numrows['_nb_commentaires']['total']))!=='' ?
				(afficher_icone_svg('comment', '', '') . $t3) :
				'') .
		'</a>
			</span>
		')) :
			'') .
	'
	') .
'
	' .
BOUCLE_combienhtml_192662eb0fe4c74f7fcec6e7b8809bcc($Cache, $Pile, $doublons, $Numrows, $SP)
. (	(($Numrows['_combien']['total'])  ?
			(' ' . (	'
		<span class="signatures">
			<span class="sep">|</span>
			<a' .
		(($t3 = strval(ancre_url('','petition')))!=='' ?
				(' href="' . $t3 . '"') :
				'') .
		' ' .
		(($t3 = strval(attribut_html(singulier_ou_pluriel($Numrows['_combien']['total'],'zcore:info_1_signature','zcore:info_nb_signatures'))))!=='' ?
				('title="' . $t3 . '"') :
				'') .
		'>' .
		(($t3 = strval($Numrows['_combien']['total']))!=='' ?
				(afficher_icone_svg('ok-circle', '', '') . $t3) :
				'') .
		'</a>
		</span>
	')) :
			'') .
	'
') .
'
	' .
(($t1 = BOUCLE_tagshtml_192662eb0fe4c74f7fcec6e7b8809bcc($Cache, $Pile, $doublons, $Numrows, $SP))!=='' ?
		('
		<span class="tags">
			<span class="sep">|</span>
			<ul class="inline list-inline">
				' . $t1 . '
			</ul>
		</span>
	') :
		'') .
'
</div>
</header>
');
		lang_select();
	}
	lang_select();
	$iter->free();
	}
	if (defined("_BOUCLE_PROFILER")
	AND 1000*($timer = (time()+(float)microtime())-$timer) > _BOUCLE_PROFILER)
		spip_log(intval(1000*$timer)."ms BOUCLE_cartouche @ plugins/auto/spipr_dist/v2.2.6/cartouche/article.html","profiler"._LOG_AVERTISSEMENT);
	return $t0;
}

//
// Fonction principale du squelette plugins/auto/spipr_dist/v2.2.6/cartouche/article.html
// Temps de compilation total: 10.853 ms
//

function html_192662eb0fe4c74f7fcec6e7b8809bcc($Cache, $Pile, $doublons = array(), $Numrows = array(), $SP = 0) {

	if (isset($Pile[0]["doublons"]) AND is_array($Pile[0]["doublons"]))
		$doublons = nettoyer_env_doublons($Pile[0]["doublons"]);

	$connect = '';
	$page = BOUCLE_cartouchehtml_192662eb0fe4c74f7fcec6e7b8809bcc($Cache, $Pile, $doublons, $Numrows, $SP);

	return analyse_resultat_skel('html_192662eb0fe4c74f7fcec6e7b8809bcc', $Cache, $page, 'plugins/auto/spipr_dist/v2.2.6/cartouche/article.html');
}
?>