<?php

/*
 * Squelette : plugins/auto/spipr_dist/v2.2.6/breadcrumb/dist.html
 * Date :      Mon, 30 Mar 2020 09:25:24 GMT
 * Compile :   Wed, 17 Jun 2020 09:07:11 GMT
 * Boucles :   
 */ 
//
// Fonction principale du squelette plugins/auto/spipr_dist/v2.2.6/breadcrumb/dist.html
// Temps de compilation total: 0.430 ms
//

function html_f88727e5d5e3a11383a876eb6fd21516($Cache, $Pile, $doublons = array(), $Numrows = array(), $SP = 0) {

	if (isset($Pile[0]["doublons"]) AND is_array($Pile[0]["doublons"]))
		$doublons = nettoyer_env_doublons($Pile[0]["doublons"]);

	$connect = '';
	$page = (
'<ul class="breadcrumb">
	' .
vide($Pile['vars'][$_zzz=(string)'objet'] = '') .
vide($Pile['vars'][$_zzz=(string)'id_objet'] = '') .
(($t1 = strval(interdire_scripts(((entites_html(table_valeur(@$Pile[0], (string)'id_rubrique', null),true)) ?' ' :''))))!=='' ?
		($t1 . (	vide($Pile['vars'][$_zzz=(string)'objet'] = 'rubrique') .
	vide($Pile['vars'][$_zzz=(string)'id_objet'] = interdire_scripts(entites_html(table_valeur(@$Pile[0], (string)'id_rubrique', null),true))))) :
		'') .
'
	' .
(($t1 = strval(interdire_scripts(((entites_html(table_valeur(@$Pile[0], (string)'id_syndic', null),true)) ?' ' :''))))!=='' ?
		($t1 . (	vide($Pile['vars'][$_zzz=(string)'objet'] = 'site') .
	vide($Pile['vars'][$_zzz=(string)'id_objet'] = interdire_scripts(entites_html(table_valeur(@$Pile[0], (string)'id_syndic', null),true))))) :
		'') .
'
	' .
(($t1 = strval(interdire_scripts(((entites_html(table_valeur(@$Pile[0], (string)'id_breve', null),true)) ?' ' :''))))!=='' ?
		($t1 . (	vide($Pile['vars'][$_zzz=(string)'objet'] = 'breve') .
	vide($Pile['vars'][$_zzz=(string)'id_objet'] = interdire_scripts(entites_html(table_valeur(@$Pile[0], (string)'id_breve', null),true))))) :
		'') .
'
	' .
(($t1 = strval(interdire_scripts(((entites_html(table_valeur(@$Pile[0], (string)'id_article', null),true)) ?' ' :''))))!=='' ?
		($t1 . (	vide($Pile['vars'][$_zzz=(string)'objet'] = 'article') .
	vide($Pile['vars'][$_zzz=(string)'id_objet'] = interdire_scripts(entites_html(table_valeur(@$Pile[0], (string)'id_article', null),true))))) :
		'') .
'
	' .
(($t1 = strval(interdire_scripts(((entites_html(table_valeur(@$Pile[0], (string)'id_objet', null),true)) ?' ' :''))))!=='' ?
		($t1 . (	vide($Pile['vars'][$_zzz=(string)'objet'] = interdire_scripts(entites_html(table_valeur(@$Pile[0], (string)'objet', null),true))) .
	vide($Pile['vars'][$_zzz=(string)'id_objet'] = interdire_scripts(entites_html(table_valeur(@$Pile[0], (string)'id_objet', null),true))))) :
		'') .
'
	' .
(((((((table_valeur($Pile["vars"], (string)'objet', null)) ?'' :' ')) AND (interdire_scripts(entites_html(table_valeur(@$Pile[0], (string)interdire_scripts(id_table_objet(entites_html(table_valeur(@$Pile[0], (string)'type-page', null),true))), null),true)))) ?' ' :''))  ?
		(' ' . (	'
		' .
	vide($Pile['vars'][$_zzz=(string)'objet'] = interdire_scripts(entites_html(table_valeur(@$Pile[0], (string)'type-page', null),true))) .
	vide($Pile['vars'][$_zzz=(string)'id_objet'] = interdire_scripts(entites_html(table_valeur(@$Pile[0], (string)interdire_scripts(id_table_objet(entites_html(table_valeur(@$Pile[0], (string)'type-page', null),true))), null),true))))) :
		'') .
'
	' .
((table_valeur($Pile["vars"], (string)'objet', null))  ?
		(' ' . (	'
	' .
	recuperer_fond( 'breadcrumb/inc-objet' , array('id_objet' => table_valeur($Pile["vars"], (string)'id_objet', null) ,
	'objet' => table_valeur($Pile["vars"], (string)'objet', null) ), array('compil'=>array('plugins/auto/spipr_dist/v2.2.6/breadcrumb/dist.html','html_f88727e5d5e3a11383a876eb6fd21516','',11,$GLOBALS['spip_lang'])), _request('connect')))) :
		'') .
(!(table_valeur($Pile["vars"], (string)'objet', null))  ?
		(' ' . (	'
	<li class="breadcrumb-item"><a href="' .
	spip_htmlspecialchars(sinon($GLOBALS['meta']['adresse_site'],'.')) .
	'/">' .
	_T('public|spip|ecrire:accueil_site') .
	'</a></li>
	')) :
		'') .
'
</ul>');

	return analyse_resultat_skel('html_f88727e5d5e3a11383a876eb6fd21516', $Cache, $page, 'plugins/auto/spipr_dist/v2.2.6/breadcrumb/dist.html');
}
?>