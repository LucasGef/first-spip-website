<?php

/*
 * Squelette : plugins/auto/z/v1.7.31/plan.html
 * Date :      Tue, 07 Nov 2017 17:27:36 GMT
 * Compile :   Wed, 17 Jun 2020 07:10:32 GMT
 * Boucles :   
 */ 
//
// Fonction principale du squelette plugins/auto/z/v1.7.31/plan.html
// Temps de compilation total: 0.087 ms
//

function html_442e47df68cceadde2dfb29d61bf98b3($Cache, $Pile, $doublons = array(), $Numrows = array(), $SP = 0) {

	if (isset($Pile[0]["doublons"]) AND is_array($Pile[0]["doublons"]))
		$doublons = nettoyer_env_doublons($Pile[0]["doublons"]);

	$connect = '';
	$page = (

'<'.'?php echo recuperer_fond( ' . argumenter_squelette('structure') . ', array_merge('.var_export($Pile[0],1).',array(\'type\' => ' . argumenter_squelette('page') . ',
	\'composition\' => ' . argumenter_squelette('plan') . ',
	\'lang\' => ' . argumenter_squelette($GLOBALS["spip_lang"]) . ')), array("compil"=>array(\'plugins/auto/z/v1.7.31/plan.html\',\'html_442e47df68cceadde2dfb29d61bf98b3\',\'\',1,$GLOBALS[\'spip_lang\'])), _request("connect"));
?'.'>
');

	return analyse_resultat_skel('html_442e47df68cceadde2dfb29d61bf98b3', $Cache, $page, 'plugins/auto/z/v1.7.31/plan.html');
}
?>