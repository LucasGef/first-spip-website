<?php

/*
 * Squelette : ../plugins/auto/menus/v1.7.26/prive/squelettes/contenu/menus.html
 * Date :      Mon, 08 Jun 2020 15:55:26 GMT
 * Compile :   Wed, 17 Jun 2020 09:54:15 GMT
 * Boucles :   _menus_utiles
 */ 

function BOUCLE_menus_utileshtml_f76809bf07fbdf9c9ac7948f6baaa065(&$Cache, &$Pile, &$doublons, &$Numrows, $SP) {

	static $command = array();
	static $connect;
	$command['connect'] = $connect = '';
	$command['sourcemode'] = 'table';

	$command['source'] = array(menus_utiles(''));

	if (!isset($command['table'])) {
		$command['table'] = '';
		$command['id'] = '_menus_utiles';
		$command['from'] = array();
		$command['type'] = array();
		$command['groupby'] = array();
		$command['select'] = array(".valeur",
		".cle");
		$command['orderby'] = array();
		$command['where'] = 
			array();
		$command['join'] = array();
		$command['limit'] = '';
		$command['having'] = 
			array();
	}
	if (defined("_BOUCLE_PROFILER")) $timer = time()+(float)microtime();
	$t0 = "";
	// REQUETE
	$iter = IterFactory::create(
		"DATA",
		$command,
		array('../plugins/auto/menus/v1.7.26/prive/squelettes/contenu/menus.html','html_f76809bf07fbdf9c9ac7948f6baaa065','_menus_utiles',10,$GLOBALS['spip_lang'])
	);
	if (!$iter->err()) {
	$Numrows['_menus_utiles']['total'] = @intval($iter->count());
	$SP++;
	// RESULTATS
	while ($Pile[$SP]=$iter->fetch()) {

		$t0 .= (
'
	<li>
		' .
filtre_bouton_action_dist(_T('menus:utiles_generer_menu', array('titre' => interdire_scripts(safehtml($Pile[$SP]['valeur'])),
'identifiant' => interdire_scripts(safehtml($Pile[$SP]['cle'])))),invalideur_session($Cache, safehtml(generer_action_auteur('generer_menus',interdire_scripts(invalideur_session($Cache, safehtml($Pile[$SP]['cle']))),invalideur_session($Cache, self()))))) .
'
	</li>
	');
	}
	$iter->free();
	}
	if (defined("_BOUCLE_PROFILER")
	AND 1000*($timer = (time()+(float)microtime())-$timer) > _BOUCLE_PROFILER)
		spip_log(intval(1000*$timer)."ms BOUCLE_menus_utiles @ ../plugins/auto/menus/v1.7.26/prive/squelettes/contenu/menus.html","profiler"._LOG_AVERTISSEMENT);
	return $t0;
}

//
// Fonction principale du squelette ../plugins/auto/menus/v1.7.26/prive/squelettes/contenu/menus.html
// Temps de compilation total: 5.623 ms
//

function html_f76809bf07fbdf9c9ac7948f6baaa065($Cache, $Pile, $doublons = array(), $Numrows = array(), $SP = 0) {

	if (isset($Pile[0]["doublons"]) AND is_array($Pile[0]["doublons"]))
		$doublons = nettoyer_env_doublons($Pile[0]["doublons"]);

	$connect = '';
	$page = (
'<h1 class="grostitre">' .
_T('menus:editer_menus_titre') .
'</h1>
<p>' .
_T('menus:editer_menus_explication') .
'</p>

' .

'<'.'?php echo recuperer_fond( ' . argumenter_squelette('prive/objets/liste/menus') . ', array_merge('.var_export($Pile[0],1).',array(\'id_menus_entree\' => ' . argumenter_squelette('0') . ',
	\'lang\' => ' . argumenter_squelette($GLOBALS["spip_lang"]) . ')), array("compil"=>array(\'../plugins/auto/menus/v1.7.26/prive/squelettes/contenu/menus.html\',\'html_f76809bf07fbdf9c9ac7948f6baaa065\',\'\',4,$GLOBALS[\'spip_lang\'])), _request("connect"));
?'.'>

' .
filtre_icone_verticale_dist(parametre_url(generer_url_ecrire('menu_edit'),'new','oui'),_T('menus:editer_menus_nouveau'),'menus-24.png','new','right') .
'

<div class="nettoyeur"></div>

' .
(($t1 = BOUCLE_menus_utileshtml_f76809bf07fbdf9c9ac7948f6baaa065($Cache, $Pile, $doublons, $Numrows, $SP))!=='' ?
		((	'
<h2>' .
		_T('menus:utiles_titre') .
		'</h2>
<p class="explication">' .
		_T('menus:utiles_explication') .
		'</p>
<ul>
	') . $t1 . (	'
	' .
		((($Numrows['_menus_utiles']['total'] > '1'))  ?
				(' ' . (	'
	<li class="tous">
		' .
			filtre_bouton_action_dist(_T('menus:utiles_generer_menus'),invalideur_session($Cache, generer_action_auteur('generer_menus',invalideur_session($Cache, ''),invalideur_session($Cache, self())))) .
			'
	</li>
	')) :
				'') .
		'
</ul>
')) :
		'') .
'

<div class="nettoyeur"></div>
');

	return analyse_resultat_skel('html_f76809bf07fbdf9c9ac7948f6baaa065', $Cache, $page, '../plugins/auto/menus/v1.7.26/prive/squelettes/contenu/menus.html');
}
?>