<?php

/*
 * Squelette : plugins/auto/spipr_dist/v2.2.6/liste/rubriques-recherche.html
 * Date :      Mon, 30 Mar 2020 09:25:24 GMT
 * Compile :   Wed, 17 Jun 2020 07:36:22 GMT
 * Boucles :   _rubriques
 */ 

function BOUCLE_rubriqueshtml_f0396025319b6ce003a774cc6723faf3(&$Cache, &$Pile, &$doublons, &$Numrows, $SP) {

	static $command = array();
	static $connect;
	$command['connect'] = $connect = '';
	$in = array();
	if (!(is_array($a = (@$Pile[0]['id_secteur']))))
		$in[]= $a;
	else $in = array_merge($in, $a); 
	// RECHERCHE
	{
		$prepare_recherche = charger_fonction('prepare_recherche', 'inc');
		list($rech_select, $rech_where) = $prepare_recherche((isset($Pile[0]["recherche"])?$Pile[0]["recherche"]:(isset($GLOBALS["recherche"])?$GLOBALS["recherche"]:"")), "rubriques", "","",array (
),"id_rubrique");
	}
	
	$command['pagination'] = array((isset($Pile[0]['debut_rubriques']) ? $Pile[0]['debut_rubriques'] : null), (($a = intval(interdire_scripts(entites_html(sinon(table_valeur(@$Pile[0], (string)'pagination', null), '5'),true)))) ? $a : 10));
	if (!isset($command['table'])) {
		$command['table'] = 'rubriques';
		$command['id'] = '_rubriques';
		$command['from'] = array('rubriques' => 'spip_rubriques','resultats' => 'spip_resultats');
		$command['type'] = array();
		$command['groupby'] = array();
		$command['orderby'] = array('resultats.points DESC');
		$command['join'] = array('resultats' => array('rubriques','id','id_rubrique'));
		$command['limit'] = '';
		$command['having'] = 
			array();
	}
	$command['select'] = array("rubriques.id_rubrique",
		"$rech_select",
		"resultats.points",
		"rubriques.titre",
		"rubriques.lang");
	$command['where'] = 
			array(
quete_condition_statut('rubriques.statut','!','publie',''), $rech_where?$rech_where:'', (!(is_array(@$Pile[0]['id_secteur'])?count(@$Pile[0]['id_secteur']):strlen(@$Pile[0]['id_secteur'])) ? '' : ((is_array(@$Pile[0]['id_secteur'])) ? sql_in('rubriques.id_secteur',sql_quote($in)) : 
			array('=', 'rubriques.id_secteur', sql_quote(@$Pile[0]['id_secteur'], '','bigint(21) NOT NULL DEFAULT \'0\'')))));
	if (defined("_BOUCLE_PROFILER")) $timer = time()+(float)microtime();
	$t0 = "";
	// REQUETE
	$iter = IterFactory::create(
		"SQL",
		$command,
		array('plugins/auto/spipr_dist/v2.2.6/liste/rubriques-recherche.html','html_f0396025319b6ce003a774cc6723faf3','_rubriques',2,$GLOBALS['spip_lang'])
	);
	if (!$iter->err()) {
	
	// COMPTEUR
	$Numrows['_rubriques']['compteur_boucle'] = 0;
	$Numrows['_rubriques']['total'] = @intval($iter->count());
	$debut_boucle = isset($Pile[0]['debut_rubriques']) ? $Pile[0]['debut_rubriques'] : _request('debut_rubriques');
	if(substr($debut_boucle,0,1)=='@'){
		$debut_boucle = $Pile[0]['debut_rubriques'] = quete_debut_pagination('id_rubrique',$Pile[0]['@id_rubrique'] = substr($debut_boucle,1),(($a = intval(interdire_scripts(entites_html(sinon(table_valeur(@$Pile[0], (string)'pagination', null), '5'),true)))) ? $a : 10),$iter);
		$iter->seek(0);
	}
	$debut_boucle = intval($debut_boucle);
	$debut_boucle = (($tout=($debut_boucle == -1))?0:($debut_boucle));
	$debut_boucle = max(0,min($debut_boucle,floor(($Numrows['_rubriques']['total']-1)/((($a = intval(interdire_scripts(entites_html(sinon(table_valeur(@$Pile[0], (string)'pagination', null), '5'),true)))) ? $a : 10)))*((($a = intval(interdire_scripts(entites_html(sinon(table_valeur(@$Pile[0], (string)'pagination', null), '5'),true)))) ? $a : 10))));
	$debut_boucle = intval($debut_boucle);
	$fin_boucle = min(($tout ? $Numrows['_rubriques']['total'] : $debut_boucle+(($a = intval(interdire_scripts(entites_html(sinon(table_valeur(@$Pile[0], (string)'pagination', null), '5'),true)))) ? $a : 10) - 1), $Numrows['_rubriques']['total'] - 1);
	$Numrows['_rubriques']['grand_total'] = $Numrows['_rubriques']['total'];
	$Numrows['_rubriques']["total"] = max(0,$fin_boucle - $debut_boucle + 1);
	if ($debut_boucle>0 AND $debut_boucle < $Numrows['_rubriques']['grand_total'] AND $iter->seek($debut_boucle,'continue'))
		$Numrows['_rubriques']['compteur_boucle'] = $debut_boucle;
	
	lang_select($GLOBALS['spip_lang']);
	$SP++;
	// RESULTATS
	while ($Pile[$SP]=$iter->fetch()) {

		$Numrows['_rubriques']['compteur_boucle']++;
		if ($Numrows['_rubriques']['compteur_boucle'] <= $debut_boucle) continue;
		if ($Numrows['_rubriques']['compteur_boucle']-1 > $fin_boucle) break;
		lang_select_public($Pile[$SP]['lang'], '', $Pile[$SP]['titre']);
		$t0 .= (
'
		<li class="item"><a href="' .
vider_url(urlencode_1738(generer_url_entite($Pile[$SP]['id_rubrique'], 'rubrique', '', '', true))) .
'">' .
interdire_scripts(supprimer_numero(typo($Pile[$SP]['titre']), "TYPO", $connect, $Pile[0])) .
'</a></li>
		');
		lang_select();
	}
	lang_select();
	$iter->free();
	}
	if (defined("_BOUCLE_PROFILER")
	AND 1000*($timer = (time()+(float)microtime())-$timer) > _BOUCLE_PROFILER)
		spip_log(intval(1000*$timer)."ms BOUCLE_rubriques @ plugins/auto/spipr_dist/v2.2.6/liste/rubriques-recherche.html","profiler"._LOG_AVERTISSEMENT);
	return $t0;
}

//
// Fonction principale du squelette plugins/auto/spipr_dist/v2.2.6/liste/rubriques-recherche.html
// Temps de compilation total: 1.152 ms
//

function html_f0396025319b6ce003a774cc6723faf3($Cache, $Pile, $doublons = array(), $Numrows = array(), $SP = 0) {

	if (isset($Pile[0]["doublons"]) AND is_array($Pile[0]["doublons"]))
		$doublons = nettoyer_env_doublons($Pile[0]["doublons"]);

	$connect = '';
	$page = (
'
' .
(($t1 = BOUCLE_rubriqueshtml_f0396025319b6ce003a774cc6723faf3($Cache, $Pile, $doublons, $Numrows, $SP))!=='' ?
		((	'
<div class="liste resultats rubriques">
	' .
		filtre_pagination_dist($Numrows["_rubriques"]["grand_total"],
 		'_rubriques',
		isset($Pile[0]['debut_rubriques'])?$Pile[0]['debut_rubriques']:intval(_request('debut_rubriques')),
		(($a = intval(interdire_scripts(entites_html(sinon(table_valeur(@$Pile[0], (string)'pagination', null), '5'),true)))) ? $a : 10), false, '', '', array()) .
		'
	<h2 class="h2">' .
		(($t3 = strval(interdire_scripts(((($a = entites_html(table_valeur(@$Pile[0], (string)'titre', null),true)) OR (is_string($a) AND strlen($a))) ? $a : _T('public|spip|ecrire:rubriques')))))!=='' ?
				($t3 . ' ') :
				'') .
		'(' .
		(isset($Numrows['_rubriques']['grand_total'])
			? $Numrows['_rubriques']['grand_total'] : $Numrows['_rubriques']['total']) .
		')</h2>
	<ul class="liste-items">
		') . $t1 . (	'
	</ul>
	' .
		(($t3 = strval(filtre_pagination_dist($Numrows["_rubriques"]["grand_total"],
 		'_rubriques',
		isset($Pile[0]['debut_rubriques'])?$Pile[0]['debut_rubriques']:intval(_request('debut_rubriques')),
		(($a = intval(interdire_scripts(entites_html(sinon(table_valeur(@$Pile[0], (string)'pagination', null), '5'),true)))) ? $a : 10), true, '', '', array())))!=='' ?
				('<div class="pagination">' . $t3 . '</div>') :
				'') .
		'
</div>
')) :
		''));

	return analyse_resultat_skel('html_f0396025319b6ce003a774cc6723faf3', $Cache, $page, 'plugins/auto/spipr_dist/v2.2.6/liste/rubriques-recherche.html');
}
?>