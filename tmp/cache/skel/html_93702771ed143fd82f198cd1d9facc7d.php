<?php

/*
 * Squelette : ../plugins-dist/medias/prive/squelettes/contenu/document_edit.html
 * Date :      Tue, 16 Jun 2020 14:01:46 GMT
 * Compile :   Wed, 17 Jun 2020 06:57:21 GMT
 * Boucles :   
 */ 
//
// Fonction principale du squelette ../plugins-dist/medias/prive/squelettes/contenu/document_edit.html
// Temps de compilation total: 8.380 ms
//

function html_93702771ed143fd82f198cd1d9facc7d($Cache, $Pile, $doublons = array(), $Numrows = array(), $SP = 0) {

	if (isset($Pile[0]["doublons"]) AND is_array($Pile[0]["doublons"]))
		$doublons = nettoyer_env_doublons($Pile[0]["doublons"]);

	$connect = '';
	$page = (
sinon_interdire_acces(@$Pile[0]['id_document']) .
'
' .
invalideur_session($Cache, sinon_interdire_acces(((function_exists("autoriser")||include_spip("inc/autoriser"))&&autoriser('modifier', 'document', invalideur_session($Cache, @$Pile[0]['id_document']))?" ":""))) .
'
<div class="cadre-formulaire-editer' .
(($t1 = strval(interdire_scripts(((entites_html(sinon(table_valeur(@$Pile[0], (string)'popin', null), ''),true)) ?' ' :''))))!=='' ?
		($t1 . 'popin') :
		'') .
'">
<div class="entete-formulaire">
	' .
(($t1 = strval(interdire_scripts(((entites_html(sinon(table_valeur(@$Pile[0], (string)'popin', null), ''),true)) ?'' :' '))))!=='' ?
		($t1 . (	'
	' .
	interdire_scripts(filtre_icone_verticale_dist(((($a = entites_html(table_valeur(@$Pile[0], (string)'redirect', null),true)) OR (is_string($a) AND strlen($a))) ? $a : generer_url_ecrire('documents')),_T('public|spip|ecrire:icone_retour'),'document','','left')) .
	'
	')) :
		'') .
'
	' .
(($t1 = strval(interdire_scripts(((($a = generer_info_entite(@$Pile[0]['id_document'], 'document', 'titre')) OR (is_string($a) AND strlen($a))) ? $a : interdire_scripts(generer_info_entite(@$Pile[0]['id_document'], 'document', 'fichier','*'))))))!=='' ?
		((	_T('medias:info_modifier_document') .
	'
	<h1>') . $t1 . '</h1>') :
		'') .
'
</div>
<div class="formedit">
	<div class="ajax"><b class="reloaded"></b>
	' .
executer_balise_dynamique('FORMULAIRE_EDITER_DOCUMENT',
	array(@$Pile[0]['id_document'],interdire_scripts(entites_html(sinon(table_valeur(@$Pile[0], (string)'parent', null), ''),true)),interdire_scripts(entites_html(sinon(table_valeur(@$Pile[0], (string)'redirect', null), ''),true)),interdire_scripts(entites_html(table_valeur(@$Pile[0], (string)'lier_trad', null),true))),
	array('../plugins-dist/medias/prive/squelettes/contenu/document_edit.html','html_93702771ed143fd82f198cd1d9facc7d','',10,$GLOBALS['spip_lang'])) .
'</div>
</div>
<div class="ajax">
' .
executer_balise_dynamique('FORMULAIRE_ILLUSTRER_DOCUMENT',
	array(@$Pile[0]['id_document']),
	array('../plugins-dist/medias/prive/squelettes/contenu/document_edit.html','html_93702771ed143fd82f198cd1d9facc7d','',13,$GLOBALS['spip_lang'])) .
'</div>
</div>
<script type="text/javascript">
function reload_chemin(){
	if (!jQuery(\'.formedit\').find(\'.ajax>.reloaded:first\').length){
		jQuery(\'.formedit\').find(\'.ajax\').eq(0).prepend(\'<b class="reloaded"></b>\');
		jQuery(\'#chemin\').find(\'>:first\').ajaxReload();
	}
}
jQuery(function(){
	if (jQuery(\'body.document_edit\').length)
		onAjaxLoad(reload_chemin);
});
</script>
<!--affiche_milieu-->
' .
pipeline( 'afficher_contenu_objet' , array('args' => array('type' => 'document', 'id_objet' => @$Pile[0]['id_document']), 'data' => '') ));

	return analyse_resultat_skel('html_93702771ed143fd82f198cd1d9facc7d', $Cache, $page, '../plugins-dist/medias/prive/squelettes/contenu/document_edit.html');
}
?>