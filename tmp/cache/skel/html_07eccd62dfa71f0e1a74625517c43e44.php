<?php

/*
 * Squelette : plugins/auto/spipr_dist/v2.2.6/breadcrumb/recherche.html
 * Date :      Mon, 30 Mar 2020 09:25:24 GMT
 * Compile :   Wed, 17 Jun 2020 07:36:22 GMT
 * Boucles :   
 */ 
//
// Fonction principale du squelette plugins/auto/spipr_dist/v2.2.6/breadcrumb/recherche.html
// Temps de compilation total: 0.057 ms
//

function html_07eccd62dfa71f0e1a74625517c43e44($Cache, $Pile, $doublons = array(), $Numrows = array(), $SP = 0) {

	if (isset($Pile[0]["doublons"]) AND is_array($Pile[0]["doublons"]))
		$doublons = nettoyer_env_doublons($Pile[0]["doublons"]);

	$connect = '';
	$page = (
'<ul class="breadcrumb">
	<li class="breadcrumb-item"><a href="' .
spip_htmlspecialchars(sinon($GLOBALS['meta']['adresse_site'],'.')) .
'/">' .
_T('public|spip|ecrire:accueil_site') .
'</a></li>
	<li class="breadcrumb-item active"><span>' .
_T('public|spip|ecrire:info_rechercher') .
' ' .
(($t1 = strval(entites_html(_request("recherche"))))!=='' ?
		('&laquo; <strong class="on">' . $t1 . '</strong> &raquo;') :
		'') .
'</span></li>
</ul>');

	return analyse_resultat_skel('html_07eccd62dfa71f0e1a74625517c43e44', $Cache, $page, 'plugins/auto/spipr_dist/v2.2.6/breadcrumb/recherche.html');
}
?>