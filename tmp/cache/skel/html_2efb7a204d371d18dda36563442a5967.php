<?php

/*
 * Squelette : plugins/auto/spipr_dist/v2.2.6/header/dist.html
 * Date :      Mon, 30 Mar 2020 09:25:24 GMT
 * Compile :   Wed, 17 Jun 2020 09:07:11 GMT
 * Boucles :   
 */ 
//
// Fonction principale du squelette plugins/auto/spipr_dist/v2.2.6/header/dist.html
// Temps de compilation total: 2.233 ms
//

function html_2efb7a204d371d18dda36563442a5967($Cache, $Pile, $doublons = array(), $Numrows = array(), $SP = 0) {

	if (isset($Pile[0]["doublons"]) AND is_array($Pile[0]["doublons"]))
		$doublons = nettoyer_env_doublons($Pile[0]["doublons"]);

	$connect = '';
	$page = (
'<div class="row justify-content-between">
<header class="accueil clearfix col-sm-9 col-lg-8">
	' .
(($t1 = strval(((((
	(($zp='sommaire') AND isset($Pile[0][_SPIP_PAGE]) AND ($Pile[0][_SPIP_PAGE]==$zp))
	OR (isset($Pile[0]['type-page']) AND $Pile[0]['type-page']==$zp)
	OR (isset($Pile[0]['composition']) AND $Pile[0]['composition']==$zp AND $Pile[0]['type-page']=='page'))?' ':'')
) ?'' :' ')))!=='' ?
		($t1 . (	'<a rel="start home" href="' .
	spip_htmlspecialchars(sinon($GLOBALS['meta']['adresse_site'],'.')) .
	'/" title="' .
	_T('public|spip|ecrire:accueil_site') .
	'"
	>')) :
		'') .
'<h1 id="logo_site_spip">' .
(($t1 = strval(filtrer('image_graver',filtrer('image_reduire',
((!is_array($l = quete_logo('id_syndic', 'ON', "'0'",'', 0))) ? '':
 ("<img class=\"spip_logo spip_logos\" alt=\"\" src=\"$l[0]\"" . $l[2] .  ($l[1] ? " onmouseover=\"this.src='$l[1]'\" onmouseout=\"this.src='$l[0]'\"" : "") . ' />')),'300','100'))))!=='' ?
		($t1 . ' ') :
		'') .
interdire_scripts(typo($GLOBALS['meta']['nom_site'], "TYPO", $connect, $Pile[0])) .
'
	' .
(($t1 = strval(interdire_scripts(typo($GLOBALS['meta']['slogan_site'], "TYPO", $connect, $Pile[0]))))!=='' ?
		((	'<small id="slogan_site_spip">') . $t1 . '</small>') :
		'') .
'
	</h1>' .
(($t1 = strval(((((
	(($zp='sommaire') AND isset($Pile[0][_SPIP_PAGE]) AND ($Pile[0][_SPIP_PAGE]==$zp))
	OR (isset($Pile[0]['type-page']) AND $Pile[0]['type-page']==$zp)
	OR (isset($Pile[0]['composition']) AND $Pile[0]['composition']==$zp AND $Pile[0]['type-page']=='page'))?' ':'')
) ?'' :' ')))!=='' ?
		($t1 . '</a>') :
		'') .
'
</header>
<div class="menu-lang col-sm-3">
' .
executer_balise_dynamique('MENU_LANG',
	array(spip_htmlentities(@$Pile[0]['lang'] ? @$Pile[0]['lang'] : $GLOBALS['spip_lang'])),
	array('plugins/auto/spipr_dist/v2.2.6/header/dist.html','html_2efb7a204d371d18dda36563442a5967','',8,$GLOBALS['spip_lang'])) .
'
</div>
</div>
');

	return analyse_resultat_skel('html_2efb7a204d371d18dda36563442a5967', $Cache, $page, 'plugins/auto/spipr_dist/v2.2.6/header/dist.html');
}
?>