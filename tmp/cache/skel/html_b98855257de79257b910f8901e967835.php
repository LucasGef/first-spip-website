<?php

/*
 * Squelette : ../prive/squelettes/top/dist.html
 * Date :      Tue, 16 Jun 2020 14:01:35 GMT
 * Compile :   Wed, 17 Jun 2020 07:17:09 GMT
 * Boucles :   
 */ 
//
// Fonction principale du squelette ../prive/squelettes/top/dist.html
// Temps de compilation total: 0.026 ms
//

function html_b98855257de79257b910f8901e967835($Cache, $Pile, $doublons = array(), $Numrows = array(), $SP = 0) {

	if (isset($Pile[0]["doublons"]) AND is_array($Pile[0]["doublons"]))
		$doublons = nettoyer_env_doublons($Pile[0]["doublons"]);

	$connect = '';
	$page = '<!-- top -->';

	return analyse_resultat_skel('html_b98855257de79257b910f8901e967835', $Cache, $page, '../prive/squelettes/top/dist.html');
}
?>