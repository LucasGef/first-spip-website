<?php

/*
 * Squelette : ../prive/formulaires/selecteur/articles.html
 * Date :      Tue, 16 Jun 2020 14:01:34 GMT
 * Compile :   Wed, 17 Jun 2020 06:57:21 GMT
 * Boucles :   _art
 */ 

function BOUCLE_arthtml_0ed46afd0703f60167844a98858defc6(&$Cache, &$Pile, &$doublons, &$Numrows, $SP) {

	static $command = array();
	static $connect;
	$command['connect'] = $connect = '';
	$in = array();
	if (!(is_array($a = (@$Pile[0]['statut']))))
		$in[]= $a;
	else $in = array_merge($in, $a);
	if (!isset($command['table'])) {
		$command['table'] = 'articles';
		$command['id'] = '_art';
		$command['from'] = array('articles' => 'spip_articles');
		$command['type'] = array();
		$command['groupby'] = array();
		$command['select'] = array("articles.id_rubrique",
		"articles.lang",
		"articles.titre");
		$command['orderby'] = array();
		$command['join'] = array();
		$command['limit'] = '';
		$command['having'] = 
			array();
	}
	$command['where'] = 
			array((!(is_array(@$Pile[0]['statut'])?count(@$Pile[0]['statut']):strlen(@$Pile[0]['statut'])) ? '' : ((is_array(@$Pile[0]['statut'])) ? sql_in('articles.statut',sql_quote($in)) : 
			array('=', 'articles.statut', sql_quote(@$Pile[0]['statut'], '','varchar(10) NOT NULL DEFAULT \'0\'')))), 
			array('=', 'articles.id_article', sql_quote(@$Pile[0]['id_article'], '','bigint(21) NOT NULL AUTO_INCREMENT')));
	if (defined("_BOUCLE_PROFILER")) $timer = time()+(float)microtime();
	$t0 = "";
	// REQUETE
	$iter = IterFactory::create(
		"SQL",
		$command,
		array('../prive/formulaires/selecteur/articles.html','html_0ed46afd0703f60167844a98858defc6','_art',29,$GLOBALS['spip_lang'])
	);
	if (!$iter->err()) {
	lang_select($GLOBALS['spip_lang']);
	$SP++;
	// RESULTATS
	while ($Pile[$SP]=$iter->fetch()) {

		lang_select_public($Pile[$SP]['lang'], '', $Pile[$SP]['titre']);
		$t0 .= vide($Pile['vars'][$_zzz=(string)'id_rubrique'] = $Pile[$SP]['id_rubrique']);
		lang_select();
	}
	lang_select();
	$iter->free();
	}
	if (defined("_BOUCLE_PROFILER")
	AND 1000*($timer = (time()+(float)microtime())-$timer) > _BOUCLE_PROFILER)
		spip_log(intval(1000*$timer)."ms BOUCLE_art @ ../prive/formulaires/selecteur/articles.html","profiler"._LOG_AVERTISSEMENT);
	return $t0;
}

//
// Fonction principale du squelette ../prive/formulaires/selecteur/articles.html
// Temps de compilation total: 4.746 ms
//

function html_0ed46afd0703f60167844a98858defc6($Cache, $Pile, $doublons = array(), $Numrows = array(), $SP = 0) {

	if (isset($Pile[0]["doublons"]) AND is_array($Pile[0]["doublons"]))
		$doublons = nettoyer_env_doublons($Pile[0]["doublons"]);

	$connect = '';
	$page = (
'<script type=\'text/javascript\'>var img_unpick=\'' .
interdire_scripts(chemin_image('supprimer-12.png')) .
'\';
jQuery.getScript(\'' .
timestamp(find_in_path('formulaires/selecteur/jquery.picker.js')) .
'\');
</script>
' .
'<ul
	class=\'item_picked' .
(($t1 = strval(interdire_scripts((entites_html(sinon(table_valeur(@$Pile[0], (string)'select', null), ''),true) ? 'select':''))))!=='' ?
		(' ' . $t1) :
		'') .
'\'>
' .
(($t1 = strval(interdire_scripts(((entites_html(sinon(table_valeur(@$Pile[0], (string)'rubriques', null), '0'),true)) ?' ' :''))))!=='' ?
		($t1 . (	'
' .
	
'<'.'?php echo recuperer_fond( ' . argumenter_squelette('formulaires/selecteur/inc-sel-rubriques') . ', array(\'name\' => ' . argumenter_squelette(@$Pile[0]['name']) . ',
	\'selected\' => ' . argumenter_squelette(@$Pile[0]['selected']) . ',
	\'select\' => ' . argumenter_squelette(@$Pile[0]['select']) . ',
	\'img_unpick\' => ' . argumenter_squelette(interdire_scripts(chemin_image('supprimer-12.png'))) . ',
	\'afficher_langue\' => ' . argumenter_squelette(@$Pile[0]['afficher_langue']) . ',
	\'lang\' => ' . argumenter_squelette($GLOBALS["spip_lang"]) . '), array("compil"=>array(\'../prive/formulaires/selecteur/articles.html\',\'html_0ed46afd0703f60167844a98858defc6\',\'\',5,$GLOBALS[\'spip_lang\'])), _request("connect"));
?'.'>
')) :
		'') .
'
' .

'<'.'?php echo recuperer_fond( ' . argumenter_squelette('formulaires/selecteur/inc-sel-articles') . ', array(\'name\' => ' . argumenter_squelette(@$Pile[0]['name']) . ',
	\'selected\' => ' . argumenter_squelette(@$Pile[0]['selected']) . ',
	\'select\' => ' . argumenter_squelette(@$Pile[0]['select']) . ',
	\'img_unpick\' => ' . argumenter_squelette(interdire_scripts(chemin_image('supprimer-12.png'))) . ',
	\'afficher_langue\' => ' . argumenter_squelette(@$Pile[0]['afficher_langue']) . ',
	\'lang\' => ' . argumenter_squelette($GLOBALS["spip_lang"]) . '), array("compil"=>array(\'../prive/formulaires/selecteur/articles.html\',\'html_0ed46afd0703f60167844a98858defc6\',\'\',7,$GLOBALS[\'spip_lang\'])), _request("connect"));
?'.'>
</ul>

' .
vide($Pile['vars'][$_zzz=(string)'id_rubrique'] = interdire_scripts(entites_html(table_valeur(@$Pile[0], (string)'id_rubrique', null),true))) .
BOUCLE_arthtml_0ed46afd0703f60167844a98858defc6($Cache, $Pile, $doublons, $Numrows, $SP) .
'
' .
'
<div class=\'item_picker\'>
' .

'<'.'?php echo recuperer_fond( ' . argumenter_squelette('formulaires/selecteur/picker-ajax') . ', array_merge('.var_export($Pile[0],1).',array(\'id_rubrique\' => ' . argumenter_squelette(table_valeur($Pile["vars"], (string)'id_rubrique', null)) . ',
	\'id_article\' => ' . argumenter_squelette(@$Pile[0]['id_article']) . ',
	\'articles\' => ' . argumenter_squelette('1') . ',
	\'lang\' => ' . argumenter_squelette($GLOBALS["spip_lang"]) . ')), array("compil"=>array(\'../prive/formulaires/selecteur/articles.html\',\'html_0ed46afd0703f60167844a98858defc6\',\'\',32,$GLOBALS[\'spip_lang\']),\'ajax\' => ($v=( ' . argumenter_squelette(@$Pile[0]['ajax']) . '))?$v:true), _request("connect"));
?'.'>
</div>
');

	return analyse_resultat_skel('html_0ed46afd0703f60167844a98858defc6', $Cache, $page, '../prive/formulaires/selecteur/articles.html');
}
?>