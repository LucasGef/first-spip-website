<?php

/*
 * Squelette : plugins/auto/bootstrap4/v4.4.1.7/bootstrap2spip/formulaires/charter.html
 * Date :      Tue, 14 Apr 2020 15:39:44 GMT
 * Compile :   Wed, 17 Jun 2020 10:10:20 GMT
 * Boucles :   
 */ 
//
// Fonction principale du squelette plugins/auto/bootstrap4/v4.4.1.7/bootstrap2spip/formulaires/charter.html
// Temps de compilation total: 4.541 ms
//

function html_1dfa09028ec2cf88d66346feb02869c0($Cache, $Pile, $doublons = array(), $Numrows = array(), $SP = 0) {

	if (isset($Pile[0]["doublons"]) AND is_array($Pile[0]["doublons"]))
		$doublons = nettoyer_env_doublons($Pile[0]["doublons"]);

	$connect = '';
	$page = (
'<div class="ajax formulaire_spip formulaire_configurer formulaire_' .
interdire_scripts(@$Pile[0]['form']) .
' formulaire_' .
interdire_scripts(@$Pile[0]['form']) .
'-' .
interdire_scripts(entites_html(sinon(table_valeur(@$Pile[0], (string)'id', null), 'nouveau'),true)) .
'">
	<h3 class="titrem">Titre du formulaire</h3>
	' .
(($t1 = strval(table_valeur(@$Pile[0], (string)'message_ok', null)))!=='' ?
		('<p class="reponse_formulaire reponse_formulaire_ok">' . $t1 . '</p>') :
		'') .
'
	' .
(($t1 = strval(table_valeur(@$Pile[0], (string)'message_erreur', null)))!=='' ?
		('<p class="reponse_formulaire reponse_formulaire_erreur">' . $t1 . '</p>') :
		'') .
'
	<h4>Des fois on utilise un sous-titre (Étape 1/N)</h4>
	<p>Un texte d\'introduction, qui peut parfois être sur plusieurs lignes.
		Un texte d\'introduction, qui peut parfois être sur plusieurs lignes.
		Un texte d\'introduction, qui peut parfois être sur plusieurs lignes.
		Un texte d\'introduction, qui peut parfois être sur plusieurs lignes.
	</p>
	<ul class="spip">
		<li>avec une énumération</li>
		<li>de plusieurs items</li>
		<li>qui doivent «bien tomber»</li>
	</ul>
	' .
(($t1 = strval(interdire_scripts(entites_html(table_valeur(@$Pile[0], (string)'editable', null),true))))!=='' ?
		($t1 . (	'
	<form method=\'post\' action=\'' .
	interdire_scripts(entites_html(table_valeur(@$Pile[0], (string)'action', null),true)) .
	'\'><div>
		
		' .
		'<div>' .
	form_hidden(@$Pile[0]['action']) .
	'<input name=\'formulaire_action\' type=\'hidden\'
		value=\'' . @$Pile[0]['form'] . '\' />' .
	'<input name=\'formulaire_action_args\' type=\'hidden\'
		value=\'' . @$Pile[0]['formulaire_args']. '\' />' .
	(!empty($Pile[0]['_hidden']) ? @$Pile[0]['_hidden'] : '') .
	'</div>
		' .
	vide($Pile['vars'][$_zzz=(string)'fl'] = 'charter') .
	'<p class="explication">Des explications préliminaires, en début de formulaire</p>
		<fieldset>
			<legend>' .
	_T('charter:legend') .
	'</legend>
				<p class="explication">Des explications dans un fieldset</p>
				<div class="editer-groupe">
				<!--EX01-->
				' .
	vide($Pile['vars'][$_zzz=(string)'name'] = 'text') .
	vide($Pile['vars'][$_zzz=(string)'obli'] = '') .
	vide($Pile['vars'][$_zzz=(string)'defaut'] = '') .
	vide($Pile['vars'][$_zzz=(string)'erreurs'] = table_valeur(table_valeur(@$Pile[0], (string)'erreurs', null),table_valeur($Pile["vars"], (string)'name', null))) .
	'<div class="editer editer_' .
	table_valeur($Pile["vars"], (string)'name', null) .
	(($t2 = strval(table_valeur($Pile["vars"], (string)'obli', null)))!=='' ?
			(' ' . $t2) :
			'') .
	((table_valeur($Pile["vars"], (string)'erreurs', null))  ?
			(' ' . ' ' . 'erreur') :
			'') .
	'">
					<label for="' .
	table_valeur($Pile["vars"], (string)'name', null) .
	'">' .
	_T(concat(table_valeur($Pile["vars"], (string)'fl', null),':label_',table_valeur($Pile["vars"], (string)'name', null))) .
	'</label>' .
	(($t2 = strval(table_valeur($Pile["vars"], (string)'erreurs', null)))!=='' ?
			('
					<span class=\'erreur_message\'>' . $t2 . '</span>
					') :
			'') .
	'<input type="text" name="' .
	table_valeur($Pile["vars"], (string)'name', null) .
	'" class="text" value="' .
	interdire_scripts(sinon(table_valeur(@$Pile[0], (string)table_valeur($Pile["vars"], (string)'name', null), null), table_valeur($Pile["vars"], (string)'defaut', null))) .
	'" id="' .
	table_valeur($Pile["vars"], (string)'name', null) .
	'" ' .
	((('') AND (table_valeur($Pile["vars"], (string)'obli', null)))  ?
			(' ' . 'required=\'required\'') :
			'') .
	'/>
				</div>
				<!--EX02-->
				' .
	vide($Pile['vars'][$_zzz=(string)'name'] = 'select') .
	vide($Pile['vars'][$_zzz=(string)'obli'] = '') .
	vide($Pile['vars'][$_zzz=(string)'defaut'] = '') .
	vide($Pile['vars'][$_zzz=(string)'erreurs'] = table_valeur(table_valeur(@$Pile[0], (string)'erreurs', null),table_valeur($Pile["vars"], (string)'name', null))) .
	'<div class="editer editer_' .
	table_valeur($Pile["vars"], (string)'name', null) .
	(($t2 = strval(table_valeur($Pile["vars"], (string)'obli', null)))!=='' ?
			(' ' . $t2) :
			'') .
	((table_valeur($Pile["vars"], (string)'erreurs', null))  ?
			(' ' . ' ' . 'erreur') :
			'') .
	'">
					<label for="' .
	table_valeur($Pile["vars"], (string)'name', null) .
	'">' .
	_T(concat(table_valeur($Pile["vars"], (string)'fl', null),':label_',table_valeur($Pile["vars"], (string)'name', null))) .
	'</label>' .
	(($t2 = strval(table_valeur($Pile["vars"], (string)'erreurs', null)))!=='' ?
			('
					<span class=\'erreur_message\'>' . $t2 . '</span>
					') :
			'') .
	'<select name="' .
	table_valeur($Pile["vars"], (string)'name', null) .
	'" class="select" id="' .
	table_valeur($Pile["vars"], (string)'name', null) .
	'">
						' .
	vide($Pile['vars'][$_zzz=(string)'val'] = 'oui') .
	'<option value="' .
	table_valeur($Pile["vars"], (string)'val', null) .
	'"' .
	(($t2 = strval(interdire_scripts((((entites_html(sinon(table_valeur(@$Pile[0], (string)table_valeur($Pile["vars"], (string)'name', null), null), table_valeur($Pile["vars"], (string)'defaut', null)),true) == table_valeur($Pile["vars"], (string)'val', null))) ?' ' :''))))!=='' ?
			($t2 . 'selected="selected"') :
			'') .
	'>' .
	_T(concat(table_valeur($Pile["vars"], (string)'fl', null),':label_',table_valeur($Pile["vars"], (string)'name', null),'_',table_valeur($Pile["vars"], (string)'val', null))) .
	'</option>
						' .
	vide($Pile['vars'][$_zzz=(string)'val'] = 'non') .
	'<option value="' .
	table_valeur($Pile["vars"], (string)'val', null) .
	'"' .
	(($t2 = strval(interdire_scripts((((entites_html(sinon(table_valeur(@$Pile[0], (string)table_valeur($Pile["vars"], (string)'name', null), null), table_valeur($Pile["vars"], (string)'defaut', null)),true) == table_valeur($Pile["vars"], (string)'val', null))) ?' ' :''))))!=='' ?
			($t2 . 'selected="selected"') :
			'') .
	'>' .
	_T(concat(table_valeur($Pile["vars"], (string)'fl', null),':label_',table_valeur($Pile["vars"], (string)'name', null),'_',table_valeur($Pile["vars"], (string)'val', null))) .
	'</option>
					</select>
				</div>
				<!--EX03-->
				' .
	vide($Pile['vars'][$_zzz=(string)'name'] = 'text_obli') .
	vide($Pile['vars'][$_zzz=(string)'obli'] = 'obligatoire') .
	vide($Pile['vars'][$_zzz=(string)'defaut'] = '') .
	vide($Pile['vars'][$_zzz=(string)'erreurs'] = table_valeur(table_valeur(@$Pile[0], (string)'erreurs', null),table_valeur($Pile["vars"], (string)'name', null))) .
	'<div class="editer editer_' .
	table_valeur($Pile["vars"], (string)'name', null) .
	(($t2 = strval(table_valeur($Pile["vars"], (string)'obli', null)))!=='' ?
			(' ' . $t2) :
			'') .
	((table_valeur($Pile["vars"], (string)'erreurs', null))  ?
			(' ' . ' ' . 'erreur') :
			'') .
	'">
					<label for="' .
	table_valeur($Pile["vars"], (string)'name', null) .
	'">' .
	_T(concat(table_valeur($Pile["vars"], (string)'fl', null),':label_',table_valeur($Pile["vars"], (string)'name', null))) .
	'</label>' .
	(($t2 = strval(table_valeur($Pile["vars"], (string)'erreurs', null)))!=='' ?
			('
					<span class=\'erreur_message\'>' . $t2 . '</span>
					') :
			'') .
	'<input type="text" name="' .
	table_valeur($Pile["vars"], (string)'name', null) .
	'" class="text" value="' .
	interdire_scripts(sinon(table_valeur(@$Pile[0], (string)table_valeur($Pile["vars"], (string)'name', null), null), table_valeur($Pile["vars"], (string)'defaut', null))) .
	'" id="' .
	table_valeur($Pile["vars"], (string)'name', null) .
	'" ' .
	((('') AND (table_valeur($Pile["vars"], (string)'obli', null)))  ?
			(' ' . 'required=\'required\'') :
			'') .
	'/>
				</div>
			</div>
		</fieldset>
		<fieldset>
			<legend>' .
	_T('charter:legend') .
	'</legend>
			<div class="editer-groupe">
				<!--EX04-->
				' .
	vide($Pile['vars'][$_zzz=(string)'name'] = 'text') .
	vide($Pile['vars'][$_zzz=(string)'obli'] = '') .
	vide($Pile['vars'][$_zzz=(string)'defaut'] = '') .
	vide($Pile['vars'][$_zzz=(string)'erreurs'] = table_valeur(table_valeur(@$Pile[0], (string)'erreurs', null),table_valeur($Pile["vars"], (string)'name', null))) .
	'<div class="editer editer_' .
	table_valeur($Pile["vars"], (string)'name', null) .
	(($t2 = strval(table_valeur($Pile["vars"], (string)'obli', null)))!=='' ?
			(' ' . $t2) :
			'') .
	((table_valeur($Pile["vars"], (string)'erreurs', null))  ?
			(' ' . ' ' . 'erreur') :
			'') .
	'">
					<label for="' .
	table_valeur($Pile["vars"], (string)'name', null) .
	'">' .
	_T(concat(table_valeur($Pile["vars"], (string)'fl', null),':label_',table_valeur($Pile["vars"], (string)'name', null))) .
	'</label>' .
	(($t2 = strval(table_valeur($Pile["vars"], (string)'erreurs', null)))!=='' ?
			('
					<span class=\'erreur_message\'>' . $t2 . '</span>
					') :
			'') .
	'
					<p class="explication" id="' .
	table_valeur($Pile["vars"], (string)'name', null) .
	'_explication">Des explications au dessus d\'un champ de saisie</p>
					<input type="text" name="' .
	table_valeur($Pile["vars"], (string)'name', null) .
	'" class="text" value="' .
	interdire_scripts(sinon(table_valeur(@$Pile[0], (string)table_valeur($Pile["vars"], (string)'name', null), null), table_valeur($Pile["vars"], (string)'defaut', null))) .
	'" id="' .
	table_valeur($Pile["vars"], (string)'name', null) .
	'" aria-describedby="' .
	table_valeur($Pile["vars"], (string)'name', null) .
	'_explication" ' .
	((('') AND (table_valeur($Pile["vars"], (string)'obli', null)))  ?
			(' ' . 'required=\'required\'') :
			'') .
	'/>
				</div>
				<!--EX05-->
				' .
	vide($Pile['vars'][$_zzz=(string)'name'] = 'text_obli') .
	vide($Pile['vars'][$_zzz=(string)'obli'] = 'obligatoire') .
	vide($Pile['vars'][$_zzz=(string)'defaut'] = '') .
	vide($Pile['vars'][$_zzz=(string)'erreurs'] = table_valeur(table_valeur(@$Pile[0], (string)'erreurs', null),table_valeur($Pile["vars"], (string)'name', null))) .
	'<div class="editer editer_' .
	table_valeur($Pile["vars"], (string)'name', null) .
	(($t2 = strval(table_valeur($Pile["vars"], (string)'obli', null)))!=='' ?
			(' ' . $t2) :
			'') .
	((table_valeur($Pile["vars"], (string)'erreurs', null))  ?
			(' ' . ' ' . 'erreur') :
			'') .
	'">
					<label for="' .
	table_valeur($Pile["vars"], (string)'name', null) .
	'">' .
	_T(concat(table_valeur($Pile["vars"], (string)'fl', null),':label_',table_valeur($Pile["vars"], (string)'name', null))) .
	'</label>' .
	(($t2 = strval(table_valeur($Pile["vars"], (string)'erreurs', null)))!=='' ?
			('
					<span class=\'erreur_message\'>' . $t2 . '</span>
					') :
			'') .
	'<input type="text" name="' .
	table_valeur($Pile["vars"], (string)'name', null) .
	'" class="text" value="' .
	interdire_scripts(sinon(table_valeur(@$Pile[0], (string)table_valeur($Pile["vars"], (string)'name', null), null), table_valeur($Pile["vars"], (string)'defaut', null))) .
	'" id="' .
	table_valeur($Pile["vars"], (string)'name', null) .
	'" aria-describedby="' .
	table_valeur($Pile["vars"], (string)'name', null) .
	'_explication" ' .
	((('') AND (table_valeur($Pile["vars"], (string)'obli', null)))  ?
			(' ' . 'required=\'required\'') :
			'') .
	'/>
					<p class="explication" id="' .
	table_valeur($Pile["vars"], (string)'name', null) .
	'_explication">Des explications après un champ de saisie</p>
				</div>
				<!--EX06-->
				' .
	vide($Pile['vars'][$_zzz=(string)'name'] = 'textarea') .
	vide($Pile['vars'][$_zzz=(string)'obli'] = '') .
	vide($Pile['vars'][$_zzz=(string)'defaut'] = '') .
	vide($Pile['vars'][$_zzz=(string)'erreurs'] = table_valeur(table_valeur(@$Pile[0], (string)'erreurs', null),table_valeur($Pile["vars"], (string)'name', null))) .
	'<div class="editer editer_' .
	table_valeur($Pile["vars"], (string)'name', null) .
	(($t2 = strval(table_valeur($Pile["vars"], (string)'obli', null)))!=='' ?
			(' ' . $t2) :
			'') .
	((table_valeur($Pile["vars"], (string)'erreurs', null))  ?
			(' ' . ' ' . 'erreur') :
			'') .
	'">
					<label for="' .
	table_valeur($Pile["vars"], (string)'name', null) .
	'">' .
	_T(concat(table_valeur($Pile["vars"], (string)'fl', null),':label_',table_valeur($Pile["vars"], (string)'name', null))) .
	'</label>' .
	(($t2 = strval(table_valeur($Pile["vars"], (string)'erreurs', null)))!=='' ?
			('
					<span class=\'erreur_message\'>' . $t2 . '</span>
					') :
			'') .
	'<textarea name="' .
	table_valeur($Pile["vars"], (string)'name', null) .
	'" id="' .
	table_valeur($Pile["vars"], (string)'name', null) .
	'" class="textarea" rows="2">
' .
	interdire_scripts(sinon(table_valeur(@$Pile[0], (string)table_valeur($Pile["vars"], (string)'name', null), null), table_valeur($Pile["vars"], (string)'defaut', null))) .
	'</textarea>
				</div>
				<!--EX07-->
				' .
	vide($Pile['vars'][$_zzz=(string)'name'] = 'textarea_pleine_largeur') .
	vide($Pile['vars'][$_zzz=(string)'obli'] = '') .
	vide($Pile['vars'][$_zzz=(string)'defaut'] = '') .
	vide($Pile['vars'][$_zzz=(string)'erreurs'] = table_valeur(table_valeur(@$Pile[0], (string)'erreurs', null),table_valeur($Pile["vars"], (string)'name', null))) .
	'<div class="editer pleine_largeur editer_' .
	table_valeur($Pile["vars"], (string)'name', null) .
	(($t2 = strval(table_valeur($Pile["vars"], (string)'obli', null)))!=='' ?
			(' ' . $t2) :
			'') .
	((table_valeur($Pile["vars"], (string)'erreurs', null))  ?
			(' ' . ' ' . 'erreur') :
			'') .
	'">
					<label for="' .
	table_valeur($Pile["vars"], (string)'name', null) .
	'">' .
	_T(concat(table_valeur($Pile["vars"], (string)'fl', null),':label_',table_valeur($Pile["vars"], (string)'name', null))) .
	'</label>' .
	(($t2 = strval(table_valeur($Pile["vars"], (string)'erreurs', null)))!=='' ?
			('
					<span class=\'erreur_message\'>' . $t2 . '</span>
					') :
			'') .
	'<textarea name="' .
	table_valeur($Pile["vars"], (string)'name', null) .
	'" id="' .
	table_valeur($Pile["vars"], (string)'name', null) .
	'" class="textarea" rows="4">
' .
	interdire_scripts(sinon(table_valeur(@$Pile[0], (string)table_valeur($Pile["vars"], (string)'name', null), null), table_valeur($Pile["vars"], (string)'defaut', null))) .
	'</textarea>
				</div>
				' .
	vide($Pile['vars'][$_zzz=(string)'name'] = 'textarea_pleine_largeur_obli') .
	vide($Pile['vars'][$_zzz=(string)'obli'] = 'obligatoire') .
	vide($Pile['vars'][$_zzz=(string)'defaut'] = '') .
	vide($Pile['vars'][$_zzz=(string)'erreurs'] = table_valeur(table_valeur(@$Pile[0], (string)'erreurs', null),table_valeur($Pile["vars"], (string)'name', null))) .
	'<div class="editer pleine_largeur editer_' .
	table_valeur($Pile["vars"], (string)'name', null) .
	(($t2 = strval(table_valeur($Pile["vars"], (string)'obli', null)))!=='' ?
			(' ' . $t2) :
			'') .
	((table_valeur($Pile["vars"], (string)'erreurs', null))  ?
			(' ' . ' ' . 'erreur') :
			'') .
	'">
					<label for="' .
	table_valeur($Pile["vars"], (string)'name', null) .
	'">' .
	_T(concat(table_valeur($Pile["vars"], (string)'fl', null),':label_',table_valeur($Pile["vars"], (string)'name', null))) .
	'</label>' .
	(($t2 = strval(table_valeur($Pile["vars"], (string)'erreurs', null)))!=='' ?
			('
					<span class=\'erreur_message\'>' . $t2 . '</span>
					') :
			'') .
	'<textarea name="' .
	table_valeur($Pile["vars"], (string)'name', null) .
	'" id="' .
	table_valeur($Pile["vars"], (string)'name', null) .
	'" class="textarea" rows="6">
' .
	interdire_scripts(sinon(table_valeur(@$Pile[0], (string)table_valeur($Pile["vars"], (string)'name', null), null), table_valeur($Pile["vars"], (string)'defaut', null))) .
	'</textarea>
				</div>
				<!--EX08-->
				' .
	vide($Pile['vars'][$_zzz=(string)'name'] = 'text_long_label') .
	vide($Pile['vars'][$_zzz=(string)'obli'] = '') .
	vide($Pile['vars'][$_zzz=(string)'defaut'] = '') .
	vide($Pile['vars'][$_zzz=(string)'erreurs'] = table_valeur(table_valeur(@$Pile[0], (string)'erreurs', null),table_valeur($Pile["vars"], (string)'name', null))) .
	'<div class="editer long_label editer_' .
	table_valeur($Pile["vars"], (string)'name', null) .
	(($t2 = strval(table_valeur($Pile["vars"], (string)'obli', null)))!=='' ?
			(' ' . $t2) :
			'') .
	((table_valeur($Pile["vars"], (string)'erreurs', null))  ?
			(' ' . ' ' . 'erreur') :
			'') .
	'">
					<label for="' .
	table_valeur($Pile["vars"], (string)'name', null) .
	'">' .
	_T(concat(table_valeur($Pile["vars"], (string)'fl', null),':label_',table_valeur($Pile["vars"], (string)'name', null))) .
	'</label>' .
	(($t2 = strval(table_valeur($Pile["vars"], (string)'erreurs', null)))!=='' ?
			('
					<span class=\'erreur_message\'>' . $t2 . '</span>
					') :
			'') .
	'<input type="text" name="' .
	table_valeur($Pile["vars"], (string)'name', null) .
	'" class="text" value="' .
	interdire_scripts(sinon(table_valeur(@$Pile[0], (string)table_valeur($Pile["vars"], (string)'name', null), null), table_valeur($Pile["vars"], (string)'defaut', null))) .
	'" id="' .
	table_valeur($Pile["vars"], (string)'name', null) .
	'" ' .
	((('') AND (table_valeur($Pile["vars"], (string)'obli', null)))  ?
			(' ' . 'required=\'required\'') :
			'') .
	'/>
				</div>
				<!--EX09-->
				' .
	vide($Pile['vars'][$_zzz=(string)'name'] = 'radio') .
	vide($Pile['vars'][$_zzz=(string)'obli'] = '') .
	vide($Pile['vars'][$_zzz=(string)'defaut'] = '') .
	vide($Pile['vars'][$_zzz=(string)'erreurs'] = table_valeur(table_valeur(@$Pile[0], (string)'erreurs', null),table_valeur($Pile["vars"], (string)'name', null))) .
	'<div class="editer editer_' .
	table_valeur($Pile["vars"], (string)'name', null) .
	(($t2 = strval(table_valeur($Pile["vars"], (string)'obli', null)))!=='' ?
			(' ' . $t2) :
			'') .
	((table_valeur($Pile["vars"], (string)'erreurs', null))  ?
			(' ' . ' ' . 'erreur') :
			'') .
	'">
					<label>' .
	_T(concat(table_valeur($Pile["vars"], (string)'fl', null),':label_',table_valeur($Pile["vars"], (string)'name', null))) .
	'</label>' .
	(($t2 = strval(table_valeur($Pile["vars"], (string)'erreurs', null)))!=='' ?
			('
					<span class=\'erreur_message\'>' . $t2 . '</span>
					') :
			'') .
	'
					<p class="explication" id="' .
	table_valeur($Pile["vars"], (string)'name', null) .
	'_explication">Des explications au-dessus d\'un choix</p>
					' .
	vide($Pile['vars'][$_zzz=(string)'val'] = 'oui') .
	'<div class="choix">
						<input type="radio" name="' .
	table_valeur($Pile["vars"], (string)'name', null) .
	'" class="radio" id="' .
	table_valeur($Pile["vars"], (string)'name', null) .
	'_' .
	table_valeur($Pile["vars"], (string)'val', null) .
	'" value="' .
	table_valeur($Pile["vars"], (string)'val', null) .
	'" aria-describedby="' .
	table_valeur($Pile["vars"], (string)'name', null) .
	'_explication" ' .
	(($t2 = strval(interdire_scripts((((entites_html(sinon(table_valeur(@$Pile[0], (string)table_valeur($Pile["vars"], (string)'name', null), null), table_valeur($Pile["vars"], (string)'defaut', null)),true) == table_valeur($Pile["vars"], (string)'val', null))) ?' ' :''))))!=='' ?
			($t2 . 'checked="checked"') :
			'') .
	' />
						<label for="' .
	table_valeur($Pile["vars"], (string)'name', null) .
	'_' .
	table_valeur($Pile["vars"], (string)'val', null) .
	'">' .
	_T(concat(table_valeur($Pile["vars"], (string)'fl', null),':label_',table_valeur($Pile["vars"], (string)'name', null),'_',table_valeur($Pile["vars"], (string)'val', null))) .
	'</label>
					</div>
					' .
	vide($Pile['vars'][$_zzz=(string)'val'] = 'non') .
	'<div class="choix">
						<input type="radio" name="' .
	table_valeur($Pile["vars"], (string)'name', null) .
	'" class="radio" id="' .
	table_valeur($Pile["vars"], (string)'name', null) .
	'_' .
	table_valeur($Pile["vars"], (string)'val', null) .
	'" value="' .
	table_valeur($Pile["vars"], (string)'val', null) .
	'" aria-describedby="' .
	table_valeur($Pile["vars"], (string)'name', null) .
	'_explication" ' .
	(($t2 = strval(interdire_scripts((((entites_html(sinon(table_valeur(@$Pile[0], (string)table_valeur($Pile["vars"], (string)'name', null), null), table_valeur($Pile["vars"], (string)'defaut', null)),true) == table_valeur($Pile["vars"], (string)'val', null))) ?' ' :''))))!=='' ?
			($t2 . 'checked="checked"') :
			'') .
	' />
						<label for="' .
	table_valeur($Pile["vars"], (string)'name', null) .
	'_' .
	table_valeur($Pile["vars"], (string)'val', null) .
	'">' .
	_T(concat(table_valeur($Pile["vars"], (string)'fl', null),':label_',table_valeur($Pile["vars"], (string)'name', null),'_',table_valeur($Pile["vars"], (string)'val', null))) .
	'</label>
					</div>
				</div>
				<!--EX09b-->
				' .
	vide($Pile['vars'][$_zzz=(string)'name'] = 'checkbox_ouiounon') .
	vide($Pile['vars'][$_zzz=(string)'obli'] = '') .
	vide($Pile['vars'][$_zzz=(string)'defaut'] = '') .
	vide($Pile['vars'][$_zzz=(string)'erreurs'] = table_valeur(table_valeur(@$Pile[0], (string)'erreurs', null),table_valeur($Pile["vars"], (string)'name', null))) .
	'<div class="editer editer_' .
	table_valeur($Pile["vars"], (string)'name', null) .
	(($t2 = strval(table_valeur($Pile["vars"], (string)'obli', null)))!=='' ?
			(' ' . $t2) :
			'') .
	((table_valeur($Pile["vars"], (string)'erreurs', null))  ?
			(' ' . ' ' . 'erreur') :
			'') .
	'">
					<label>' .
	_T(concat(table_valeur($Pile["vars"], (string)'fl', null),':label_',table_valeur($Pile["vars"], (string)'name', null))) .
	'</label>' .
	(($t2 = strval(table_valeur($Pile["vars"], (string)'erreurs', null)))!=='' ?
			('
					<span class=\'erreur_message\'>' . $t2 . '</span>
					') :
			'') .
	'
					<p class="explication" id="' .
	table_valeur($Pile["vars"], (string)'name', null) .
	'_explication">Des explications au-dessus d\'un choix</p>
					<div class="choix">
						' .
	vide($Pile['vars'][$_zzz=(string)'val'] = 'non') .
	'<input type="hidden" name="' .
	table_valeur($Pile["vars"], (string)'name', null) .
	'" value="' .
	table_valeur($Pile["vars"], (string)'val', null) .
	'" />
						' .
	vide($Pile['vars'][$_zzz=(string)'val'] = 'oui') .
	'<input type="checkbox" name="' .
	table_valeur($Pile["vars"], (string)'name', null) .
	'" class="checkbox" id="' .
	table_valeur($Pile["vars"], (string)'name', null) .
	'_' .
	table_valeur($Pile["vars"], (string)'val', null) .
	'" value="' .
	table_valeur($Pile["vars"], (string)'val', null) .
	'" aria-describedby="' .
	table_valeur($Pile["vars"], (string)'name', null) .
	'_explication" ' .
	(($t2 = strval(interdire_scripts((((entites_html(sinon(table_valeur(@$Pile[0], (string)table_valeur($Pile["vars"], (string)'name', null), null), table_valeur($Pile["vars"], (string)'defaut', null)),true) == table_valeur($Pile["vars"], (string)'val', null))) ?' ' :''))))!=='' ?
			($t2 . 'checked="checked"') :
			'') .
	' />
						<label for="' .
	table_valeur($Pile["vars"], (string)'name', null) .
	'_' .
	table_valeur($Pile["vars"], (string)'val', null) .
	'">' .
	_T(concat(table_valeur($Pile["vars"], (string)'fl', null),':label_',table_valeur($Pile["vars"], (string)'name', null),'_',table_valeur($Pile["vars"], (string)'val', null))) .
	'</label>
					</div>
				</div>
				<!--EX10-->
				' .
	vide($Pile['vars'][$_zzz=(string)'name'] = 'checkbox') .
	vide($Pile['vars'][$_zzz=(string)'obli'] = '') .
	vide($Pile['vars'][$_zzz=(string)'defaut'] = '') .
	vide($Pile['vars'][$_zzz=(string)'erreurs'] = table_valeur(table_valeur(@$Pile[0], (string)'erreurs', null),table_valeur($Pile["vars"], (string)'name', null))) .
	'<div class="editer editer_' .
	table_valeur($Pile["vars"], (string)'name', null) .
	(($t2 = strval(table_valeur($Pile["vars"], (string)'obli', null)))!=='' ?
			(' ' . $t2) :
			'') .
	((table_valeur($Pile["vars"], (string)'erreurs', null))  ?
			(' ' . ' ' . 'erreur') :
			'') .
	'">
					<label>' .
	_T(concat(table_valeur($Pile["vars"], (string)'fl', null),':label_',table_valeur($Pile["vars"], (string)'name', null))) .
	'</label>' .
	(($t2 = strval(table_valeur($Pile["vars"], (string)'erreurs', null)))!=='' ?
			('
					<span class=\'erreur_message\'>' . $t2 . '</span>
					') :
			'') .
	'
					' .
	vide($Pile['vars'][$_zzz=(string)'val'] = '1') .
	'<div class="choix">
						<input type="checkbox" name="' .
	table_valeur($Pile["vars"], (string)'name', null) .
	'[]" class="checkbox" id="' .
	table_valeur($Pile["vars"], (string)'name', null) .
	'_' .
	table_valeur($Pile["vars"], (string)'val', null) .
	'" value="' .
	table_valeur($Pile["vars"], (string)'val', null) .
	'" aria-describedby="' .
	table_valeur($Pile["vars"], (string)'name', null) .
	'_explication" ' .
	((in_any(table_valeur($Pile["vars"], (string)'val', null),interdire_scripts(entites_html(sinon(table_valeur(@$Pile[0], (string)table_valeur($Pile["vars"], (string)'name', null), null), table_valeur($Pile["vars"], (string)'defaut', null)),true))))  ?
			(' ' . 'checked="checked"') :
			'') .
	' />
						<label for="' .
	table_valeur($Pile["vars"], (string)'name', null) .
	'_' .
	table_valeur($Pile["vars"], (string)'val', null) .
	'">' .
	_T(concat(table_valeur($Pile["vars"], (string)'fl', null),':label_',table_valeur($Pile["vars"], (string)'name', null),'_',table_valeur($Pile["vars"], (string)'val', null))) .
	'</label>
					</div>
					' .
	vide($Pile['vars'][$_zzz=(string)'val'] = '2') .
	'<div class="choix">
						<input type="checkbox" name="' .
	table_valeur($Pile["vars"], (string)'name', null) .
	'[]" class="checkbox" id="' .
	table_valeur($Pile["vars"], (string)'name', null) .
	'_' .
	table_valeur($Pile["vars"], (string)'val', null) .
	'" value="' .
	table_valeur($Pile["vars"], (string)'val', null) .
	'" aria-describedby="' .
	table_valeur($Pile["vars"], (string)'name', null) .
	'_explication" ' .
	((in_any(table_valeur($Pile["vars"], (string)'val', null),interdire_scripts(entites_html(sinon(table_valeur(@$Pile[0], (string)table_valeur($Pile["vars"], (string)'name', null), null), table_valeur($Pile["vars"], (string)'defaut', null)),true))))  ?
			(' ' . 'checked="checked"') :
			'') .
	' />
						<label for="' .
	table_valeur($Pile["vars"], (string)'name', null) .
	'_' .
	table_valeur($Pile["vars"], (string)'val', null) .
	'">' .
	_T(concat(table_valeur($Pile["vars"], (string)'fl', null),':label_',table_valeur($Pile["vars"], (string)'name', null),'_',table_valeur($Pile["vars"], (string)'val', null))) .
	'</label>
					</div>
					<p class="explication" id="' .
	table_valeur($Pile["vars"], (string)'name', null) .
	'_explication">Des explications au-dessous d\'un choix</p>
				</div>

				<!--EX11-->
				' .
	vide($Pile['vars'][$_zzz=(string)'name'] = 'checkbox_long_label') .
	vide($Pile['vars'][$_zzz=(string)'obli'] = '') .
	vide($Pile['vars'][$_zzz=(string)'defaut'] = '') .
	vide($Pile['vars'][$_zzz=(string)'erreurs'] = table_valeur(table_valeur(@$Pile[0], (string)'erreurs', null),table_valeur($Pile["vars"], (string)'name', null))) .
	'<div class="editer long_label editer_' .
	table_valeur($Pile["vars"], (string)'name', null) .
	(($t2 = strval(table_valeur($Pile["vars"], (string)'obli', null)))!=='' ?
			(' ' . $t2) :
			'') .
	((table_valeur($Pile["vars"], (string)'erreurs', null))  ?
			(' ' . ' ' . 'erreur') :
			'') .
	'">
					<label>' .
	_T(concat(table_valeur($Pile["vars"], (string)'fl', null),':label_',table_valeur($Pile["vars"], (string)'name', null))) .
	'</label>' .
	(($t2 = strval(table_valeur($Pile["vars"], (string)'erreurs', null)))!=='' ?
			('
					<span class=\'erreur_message\'>' . $t2 . '</span>
					') :
			'') .
	'
					' .
	vide($Pile['vars'][$_zzz=(string)'val'] = '1') .
	'<div class="choix">
						<input type="checkbox" name="' .
	table_valeur($Pile["vars"], (string)'name', null) .
	'[]" class="checkbox" id="' .
	table_valeur($Pile["vars"], (string)'name', null) .
	'_' .
	table_valeur($Pile["vars"], (string)'val', null) .
	'" value="' .
	table_valeur($Pile["vars"], (string)'val', null) .
	'"' .
	((in_any(table_valeur($Pile["vars"], (string)'val', null),interdire_scripts(entites_html(sinon(table_valeur(@$Pile[0], (string)table_valeur($Pile["vars"], (string)'name', null), null), table_valeur($Pile["vars"], (string)'defaut', null)),true))))  ?
			(' ' . 'checked="checked"') :
			'') .
	' />
						<label for="' .
	table_valeur($Pile["vars"], (string)'name', null) .
	'_' .
	table_valeur($Pile["vars"], (string)'val', null) .
	'">' .
	_T(concat(table_valeur($Pile["vars"], (string)'fl', null),':label_',table_valeur($Pile["vars"], (string)'name', null),'_',table_valeur($Pile["vars"], (string)'val', null))) .
	'</label>
					</div>
					' .
	vide($Pile['vars'][$_zzz=(string)'val'] = '2') .
	'<div class="choix">
						<input type="checkbox" name="' .
	table_valeur($Pile["vars"], (string)'name', null) .
	'[]" class="checkbox" id="' .
	table_valeur($Pile["vars"], (string)'name', null) .
	'_' .
	table_valeur($Pile["vars"], (string)'val', null) .
	'" value="' .
	table_valeur($Pile["vars"], (string)'val', null) .
	'"' .
	((in_any(table_valeur($Pile["vars"], (string)'val', null),interdire_scripts(entites_html(sinon(table_valeur(@$Pile[0], (string)table_valeur($Pile["vars"], (string)'name', null), null), table_valeur($Pile["vars"], (string)'defaut', null)),true))))  ?
			(' ' . 'checked="checked"') :
			'') .
	' />
						<label for="' .
	table_valeur($Pile["vars"], (string)'name', null) .
	'_' .
	table_valeur($Pile["vars"], (string)'val', null) .
	'">' .
	_T(concat(table_valeur($Pile["vars"], (string)'fl', null),':label_',table_valeur($Pile["vars"], (string)'name', null),'_',table_valeur($Pile["vars"], (string)'val', null))) .
	'</label>
					</div>
				</div>

				<div class="fieldset">
					<fieldset>
						<legend>' .
	_T('charter:legend') .
	'</legend>
						<div class="editer-groupe">
							<!--EX12-->
							' .
	vide($Pile['vars'][$_zzz=(string)'name'] = 'text') .
	vide($Pile['vars'][$_zzz=(string)'obli'] = '') .
	vide($Pile['vars'][$_zzz=(string)'defaut'] = '') .
	vide($Pile['vars'][$_zzz=(string)'erreurs'] = table_valeur(table_valeur(@$Pile[0], (string)'erreurs', null),table_valeur($Pile["vars"], (string)'name', null))) .
	'<div class="editer editer_' .
	table_valeur($Pile["vars"], (string)'name', null) .
	(($t2 = strval(table_valeur($Pile["vars"], (string)'obli', null)))!=='' ?
			(' ' . $t2) :
			'') .
	((table_valeur($Pile["vars"], (string)'erreurs', null))  ?
			(' ' . ' ' . 'erreur') :
			'') .
	'">
								<label for="' .
	table_valeur($Pile["vars"], (string)'name', null) .
	'">' .
	_T(concat(table_valeur($Pile["vars"], (string)'fl', null),':label_',table_valeur($Pile["vars"], (string)'name', null))) .
	'</label>' .
	(($t2 = strval(table_valeur($Pile["vars"], (string)'erreurs', null)))!=='' ?
			('
								<span class=\'erreur_message\'>' . $t2 . '</span>
								') :
			'') .
	'<input type="text" name="' .
	table_valeur($Pile["vars"], (string)'name', null) .
	'" class="text" value="' .
	interdire_scripts(sinon(table_valeur(@$Pile[0], (string)table_valeur($Pile["vars"], (string)'name', null), null), table_valeur($Pile["vars"], (string)'defaut', null))) .
	'" id="' .
	table_valeur($Pile["vars"], (string)'name', null) .
	'" ' .
	((('') AND (table_valeur($Pile["vars"], (string)'obli', null)))  ?
			(' ' . 'required=\'required\'') :
			'') .
	'/>
							</div>
							<!--EX13--> 
							' .
	vide($Pile['vars'][$_zzz=(string)'name'] = 'text_obli') .
	vide($Pile['vars'][$_zzz=(string)'obli'] = 'obligatoire') .
	vide($Pile['vars'][$_zzz=(string)'defaut'] = '') .
	vide($Pile['vars'][$_zzz=(string)'erreurs'] = table_valeur(table_valeur(@$Pile[0], (string)'erreurs', null),table_valeur($Pile["vars"], (string)'name', null))) .
	'<div class="editer editer_' .
	table_valeur($Pile["vars"], (string)'name', null) .
	(($t2 = strval(table_valeur($Pile["vars"], (string)'obli', null)))!=='' ?
			(' ' . $t2) :
			'') .
	((table_valeur($Pile["vars"], (string)'erreurs', null))  ?
			(' ' . ' ' . 'erreur') :
			'') .
	'">
								<label for="' .
	table_valeur($Pile["vars"], (string)'name', null) .
	'">' .
	_T(concat(table_valeur($Pile["vars"], (string)'fl', null),':label_',table_valeur($Pile["vars"], (string)'name', null))) .
	'</label>' .
	(($t2 = strval(table_valeur($Pile["vars"], (string)'erreurs', null)))!=='' ?
			('
								<span class=\'erreur_message\'>' . $t2 . '</span>
								') :
			'') .
	'<input type="text" name="' .
	table_valeur($Pile["vars"], (string)'name', null) .
	'" class="text" value="' .
	interdire_scripts(sinon(table_valeur(@$Pile[0], (string)table_valeur($Pile["vars"], (string)'name', null), null), table_valeur($Pile["vars"], (string)'defaut', null))) .
	'" id="' .
	table_valeur($Pile["vars"], (string)'name', null) .
	'" ' .
	((('') AND (table_valeur($Pile["vars"], (string)'obli', null)))  ?
			(' ' . 'required=\'required\'') :
			'') .
	'/>
							</div>
						</div>
					</fieldset>
				</div>
			</div>
		</fieldset>
	  ' .
	'
	  <!--extra-->
		<p class="explication">Cliquez sur <q>Annuler</q> pour tester les messages d\'erreur et sur <q>Enregistrer</q> pour tester les messages de succès.</p>
	  <p class=\'boutons\'><span class=\'image_loading\'>&nbsp;</span>
			<input type=\'submit\' name="cancel" class=\'submit\' value=\'' .
	attribut_html(_T('public|spip|ecrire:bouton_annuler')) .
	'\' />
			<input type=\'submit\' class=\'submit\' value=\'' .
	attribut_html(_T('public|spip|ecrire:bouton_enregistrer')) .
	'\' /></p>
	</div></form>
	')) :
		'') .
'
	' .
(($t1 = strval(interdire_scripts(((entites_html(table_valeur(@$Pile[0], (string)'editable', null),true)) ?'' :' '))))!=='' ?
		($t1 . (	'
	  <p class=\'boutons\'><span class=\'image_loading\'>&nbsp;</span>
			<input type=\'submit\' name="cancel" class=\'submit\' value=\'' .
	attribut_html(_T('public|spip|ecrire:bouton_fermer')) .
	'\' onclick="$.modalboxclose();return false;" />
	')) :
		'') .
'
</div>');

	return analyse_resultat_skel('html_1dfa09028ec2cf88d66346feb02869c0', $Cache, $page, 'plugins/auto/bootstrap4/v4.4.1.7/bootstrap2spip/formulaires/charter.html');
}
?>