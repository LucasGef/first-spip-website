<?php

/*
 * Squelette : plugins/auto/z/v1.7.31/inclure/forum.html
 * Date :      Tue, 07 Nov 2017 17:27:36 GMT
 * Compile :   Wed, 17 Jun 2020 06:56:17 GMT
 * Boucles :   _decompte, _doc, _doc2, _forums_boucle, _forums_fils, _forums
 */ 

function BOUCLE_decomptehtml_2cd38e156bfa819301815ef4fc63ef30(&$Cache, &$Pile, &$doublons, &$Numrows, $SP) {

	static $command = array();
	static $connect;
	$command['connect'] = $connect = '';
	$in = array();
	if (!(is_array($a = (@$Pile[0]['id_rubrique']))))
		$in[]= $a;
	else $in = array_merge($in, $a);
	$in1 = array();
	if (!(is_array($a = (@$Pile[0]['id_article']))))
		$in1[]= $a;
	else $in1 = array_merge($in1, $a);
	$in2 = array();
	if (!(is_array($a = (@$Pile[0]['id_breve']))))
		$in2[]= $a;
	else $in2 = array_merge($in2, $a);
	$in3 = array();
	if (!(is_array($a = (@$Pile[0]['id_syndic']))))
		$in3[]= $a;
	else $in3 = array_merge($in3, $a);
	if (!isset($command['table'])) {
		$command['table'] = 'forum';
		$command['id'] = '_decompte';
		$command['from'] = array('forum' => 'spip_forum');
		$command['type'] = array();
		$command['groupby'] = array();
		$command['select'] = array("count(*)");
		$command['orderby'] = array();
		$command['join'] = array();
		$command['limit'] = '';
		$command['having'] = 
			array();
	}
	$command['where'] = 
			array(
quete_condition_statut('forum.statut','publie,prop','publie',''), (!(is_array(@$Pile[0]['id_rubrique'])?count(@$Pile[0]['id_rubrique']):strlen(@$Pile[0]['id_rubrique'])) ? '' : ((is_array(@$Pile[0]['id_rubrique'])) ? sql_in('forum.id_objet',sql_quote($in)) : 
			array('=', 'forum.id_objet', sql_quote(@$Pile[0]['id_rubrique'], '','bigint(21) NOT NULL DEFAULT \'0\'')))), (!(is_array(@$Pile[0]['id_rubrique'])?count(@$Pile[0]['id_rubrique']):strlen(@$Pile[0]['id_rubrique'])) ? '' : 
			array('=', 'forum.objet', sql_quote('rubrique'))), (!(is_array(@$Pile[0]['id_article'])?count(@$Pile[0]['id_article']):strlen(@$Pile[0]['id_article'])) ? '' : ((is_array(@$Pile[0]['id_article'])) ? sql_in('forum.id_objet',sql_quote($in1)) : 
			array('=', 'forum.id_objet', sql_quote(@$Pile[0]['id_article'], '','bigint(21) NOT NULL DEFAULT \'0\'')))), (!(is_array(@$Pile[0]['id_article'])?count(@$Pile[0]['id_article']):strlen(@$Pile[0]['id_article'])) ? '' : 
			array('=', 'forum.objet', sql_quote('article'))), (!(is_array(@$Pile[0]['id_breve'])?count(@$Pile[0]['id_breve']):strlen(@$Pile[0]['id_breve'])) ? '' : ((is_array(@$Pile[0]['id_breve'])) ? sql_in('forum.id_objet',sql_quote($in2)) : 
			array('=', 'forum.id_objet', sql_quote(@$Pile[0]['id_breve'], '','bigint(21) NOT NULL DEFAULT \'0\'')))), (!(is_array(@$Pile[0]['id_breve'])?count(@$Pile[0]['id_breve']):strlen(@$Pile[0]['id_breve'])) ? '' : 
			array('=', 'forum.objet', sql_quote('breve'))), (!(is_array(@$Pile[0]['id_syndic'])?count(@$Pile[0]['id_syndic']):strlen(@$Pile[0]['id_syndic'])) ? '' : ((is_array(@$Pile[0]['id_syndic'])) ? sql_in('forum.id_objet',sql_quote($in3)) : 
			array('=', 'forum.id_objet', sql_quote(@$Pile[0]['id_syndic'], '','bigint(21) NOT NULL DEFAULT \'0\'')))), (!(is_array(@$Pile[0]['id_syndic'])?count(@$Pile[0]['id_syndic']):strlen(@$Pile[0]['id_syndic'])) ? '' : 
			array('=', 'forum.objet', sql_quote('site'))));
	if (defined("_BOUCLE_PROFILER")) $timer = time()+(float)microtime();
	$t0 = "";
	// REQUETE
	$iter = IterFactory::create(
		"SQL",
		$command,
		array('plugins/auto/z/v1.7.31/inclure/forum.html','html_2cd38e156bfa819301815ef4fc63ef30','_decompte',7,$GLOBALS['spip_lang'])
	);
	if (!$iter->err()) {
	$Numrows['_decompte']['total'] = @intval($iter->count());
	$SP++;
	// RESULTATS
	
	$iter->free();
	}
	if (defined("_BOUCLE_PROFILER")
	AND 1000*($timer = (time()+(float)microtime())-$timer) > _BOUCLE_PROFILER)
		spip_log(intval(1000*$timer)."ms BOUCLE_decompte @ plugins/auto/z/v1.7.31/inclure/forum.html","profiler"._LOG_AVERTISSEMENT);
	return $t0;
}


function BOUCLE_dochtml_2cd38e156bfa819301815ef4fc63ef30(&$Cache, &$Pile, &$doublons, &$Numrows, $SP) {

	static $command = array();
	static $connect;
	$command['connect'] = $connect = '';
	if (!isset($command['table'])) {
		$command['table'] = 'documents';
		$command['id'] = '_doc';
		$command['from'] = array('documents' => 'spip_documents','L1' => 'spip_documents_liens');
		$command['type'] = array();
		$command['groupby'] = array();
		$command['select'] = array("documents.extension",
		"documents.id_document");
		$command['orderby'] = array();
		$command['join'] = array('L1' => array('documents','id_document'));
		$command['limit'] = '';
		$command['having'] = 
			array();
	}
	$command['where'] = 
			array(
quete_condition_statut('documents.statut','publie,prop,prepa','publie',''), 
quete_condition_postdates('documents.date_publication',''), 
			array('IN', 'documents.mode', '(\'image\',\'document\')'), 
			array('(documents.taille > 0 OR documents.distant=\'oui\')'), 
			array('=', 'L1.id_objet', sql_quote($Pile[$SP]['id_forum'], '','bigint(21) NOT NULL DEFAULT \'0\'')), 
			array('=', 'L1.objet', sql_quote('forum')));
	if (defined("_BOUCLE_PROFILER")) $timer = time()+(float)microtime();
	$t0 = "";
	// REQUETE
	$iter = IterFactory::create(
		"SQL",
		$command,
		array('plugins/auto/z/v1.7.31/inclure/forum.html','html_2cd38e156bfa819301815ef4fc63ef30','_doc',31,$GLOBALS['spip_lang'])
	);
	if (!$iter->err()) {
	$SP++;
	// RESULTATS
	while ($Pile[$SP]=$iter->fetch()) {

		$t0 .= (
'
					' .
interdire_scripts((match($Pile[$SP]['extension'],'^(gif|jpg|png)$') ? (	filtrer('image_graver',filtrer('image_reduire',
	((($recurs=(isset($Pile[0]['recurs'])?$Pile[0]['recurs']:0))>=5)? '' :
	recuperer_fond('modeles/emb', array('lang' => $GLOBALS["spip_lang"] ,
	'id_document'=>$Pile[$SP]['id_document'],
	'id'=>$Pile[$SP]['id_document'],
	'recurs'=>(++$recurs)), array('compil'=>array('plugins/auto/z/v1.7.31/inclure/forum.html','html_2cd38e156bfa819301815ef4fc63ef30','_doc',33,$GLOBALS['spip_lang']), 'trim'=>true), ''))
,'300')) .
	'
					'):(	quete_logo_document(quete_document($Pile[$SP]['id_document'], ''), vider_url(urlencode_1738(generer_url_entite($Pile[$SP]['id_document'], 'document', '', '', true))), '', '', 0, 0, '') .
	'
					'))) .
'
					');
	}
	$iter->free();
	}
	if (defined("_BOUCLE_PROFILER")
	AND 1000*($timer = (time()+(float)microtime())-$timer) > _BOUCLE_PROFILER)
		spip_log(intval(1000*$timer)."ms BOUCLE_doc @ plugins/auto/z/v1.7.31/inclure/forum.html","profiler"._LOG_AVERTISSEMENT);
	return $t0;
}


function BOUCLE_doc2html_2cd38e156bfa819301815ef4fc63ef30(&$Cache, &$Pile, &$doublons, &$Numrows, $SP) {

	static $command = array();
	static $connect;
	$command['connect'] = $connect = '';
	if (!isset($command['table'])) {
		$command['table'] = 'documents';
		$command['id'] = '_doc2';
		$command['from'] = array('documents' => 'spip_documents','L1' => 'spip_documents_liens');
		$command['type'] = array();
		$command['groupby'] = array();
		$command['select'] = array("documents.extension",
		"documents.id_document");
		$command['orderby'] = array();
		$command['join'] = array('L1' => array('documents','id_document'));
		$command['limit'] = '';
		$command['having'] = 
			array();
	}
	$command['where'] = 
			array(
quete_condition_statut('documents.statut','publie,prop,prepa','publie',''), 
quete_condition_postdates('documents.date_publication',''), 
			array('IN', 'documents.mode', '(\'image\',\'document\')'), 
			array('(documents.taille > 0 OR documents.distant=\'oui\')'), 
			array('=', 'L1.id_objet', sql_quote($Pile[$SP]['id_forum'], '','bigint(21) NOT NULL DEFAULT \'0\'')), 
			array('=', 'L1.objet', sql_quote('forum')));
	if (defined("_BOUCLE_PROFILER")) $timer = time()+(float)microtime();
	$t0 = "";
	// REQUETE
	$iter = IterFactory::create(
		"SQL",
		$command,
		array('plugins/auto/z/v1.7.31/inclure/forum.html','html_2cd38e156bfa819301815ef4fc63ef30','_doc2',63,$GLOBALS['spip_lang'])
	);
	if (!$iter->err()) {
	$SP++;
	// RESULTATS
	while ($Pile[$SP]=$iter->fetch()) {

		$t0 .= (
'
							' .
interdire_scripts((match($Pile[$SP]['extension'],'^(gif|jpg|png)$') ? (	filtrer('image_graver',filtrer('image_reduire',
	((($recurs=(isset($Pile[0]['recurs'])?$Pile[0]['recurs']:0))>=5)? '' :
	recuperer_fond('modeles/emb', array('lang' => $GLOBALS["spip_lang"] ,
	'id_document'=>$Pile[$SP]['id_document'],
	'id'=>$Pile[$SP]['id_document'],
	'recurs'=>(++$recurs)), array('compil'=>array('plugins/auto/z/v1.7.31/inclure/forum.html','html_2cd38e156bfa819301815ef4fc63ef30','_doc2',65,$GLOBALS['spip_lang']), 'trim'=>true), ''))
,'300')) .
	'
							'):(	quete_logo_document(quete_document($Pile[$SP]['id_document'], ''), vider_url(urlencode_1738(generer_url_entite($Pile[$SP]['id_document'], 'document', '', '', true))), '', '', 0, 0, '') .
	'
							'))) .
'
							');
	}
	$iter->free();
	}
	if (defined("_BOUCLE_PROFILER")
	AND 1000*($timer = (time()+(float)microtime())-$timer) > _BOUCLE_PROFILER)
		spip_log(intval(1000*$timer)."ms BOUCLE_doc2 @ plugins/auto/z/v1.7.31/inclure/forum.html","profiler"._LOG_AVERTISSEMENT);
	return $t0;
}


function BOUCLE_forums_bouclehtml_2cd38e156bfa819301815ef4fc63ef30(&$Cache, &$Pile, &$doublons, &$Numrows, $SP) {

	static $command = array();
	static $connect;
	$command['connect'] = $connect = '';
	$save_numrows = (isset($Numrows['_forums_fils']) ? $Numrows['_forums_fils'] : array());
	$t0 = (($t1 = BOUCLE_forums_filshtml_2cd38e156bfa819301815ef4fc63ef30($Cache, $Pile, $doublons, $Numrows, $SP))!=='' ?
		('
			<ul>
				' . $t1 . '
			</ul>
			') :
		'');
	$Numrows['_forums_fils'] = ($save_numrows);
	return $t0;
}


function BOUCLE_forums_filshtml_2cd38e156bfa819301815ef4fc63ef30(&$Cache, &$Pile, &$doublons, &$Numrows, $SP) {

	static $command = array();
	static $connect;
	$command['connect'] = $connect = '';
	if (!isset($command['table'])) {
		$command['table'] = 'forum';
		$command['id'] = '_forums_fils';
		$command['from'] = array('forum' => 'spip_forum');
		$command['type'] = array();
		$command['groupby'] = array();
		$command['select'] = array("forum.id_forum",
		"forum.date_heure",
		"forum.titre",
		"forum.date_heure AS date",
		"forum.auteur AS nom",
		"forum.id_auteur",
		"forum.texte",
		"forum.url_site",
		"forum.nom_site",
		"forum.id_objet AS id_article");
		$command['orderby'] = array('forum.date_heure');
		$command['join'] = array();
		$command['limit'] = '';
		$command['having'] = 
			array();
	}
	$command['where'] = 
			array(
quete_condition_statut('forum.statut','publie,prop','publie',''), 
			array('=', 'forum.id_parent', sql_quote($Pile[$SP]['id_forum'], '','bigint(21) NOT NULL DEFAULT \'0\'')));
	if (defined("_BOUCLE_PROFILER")) $timer = time()+(float)microtime();
	$t0 = "";
	// REQUETE
	$iter = IterFactory::create(
		"SQL",
		$command,
		array('plugins/auto/z/v1.7.31/inclure/forum.html','html_2cd38e156bfa819301815ef4fc63ef30','_forums_fils',43,$GLOBALS['spip_lang'])
	);
	if (!$iter->err()) {
	
	// COMPTEUR
	$Numrows['_forums_fils']['compteur_boucle'] = 0;
	
	$l1 = _T('public|spip|ecrire:par_auteur');
	$l2 = _T('public|spip|ecrire:voir_en_ligne');
	$l3 = _T('public|spip|ecrire:lien_repondre_message');$SP++;
	// RESULTATS
	while ($Pile[$SP]=$iter->fetch()) {

		$Numrows['_forums_fils']['compteur_boucle']++;
		$t0 .= (
'

				<li class="forum-fil comment' .
(($t1 = strval(alterner($Numrows['_forums_fils']['compteur_boucle'],'odd','even')))!=='' ?
		(' ' . $t1) :
		'') .
((($Numrows['_forums_fils']['compteur_boucle'] == '1'))  ?
		(' ' . ' ' . 'first') :
		'') .
(calcul_exposer($Pile[$SP]['id_forum'], 'id_forum', $Pile[0], 0, 'id_forum', '')  ?
		(' ' . 'on') :
		'') .
'">
					<div class="comment-message forum-message">
						<div class="comment-meta forum-chapo">
							<a href="#forum' .
$Pile[$SP]['id_forum'] .
'" title="' .
$Pile[$SP]['id_forum'] .
'" class="ancre permalink comment-number">#</a>
							<strong class="forum-titre"><a href="#forum' .
$Pile[$SP]['id_forum'] .
'" name="forum' .
$Pile[$SP]['id_forum'] .
'" id="forum' .
$Pile[$SP]['id_forum'] .
'">' .
interdire_scripts(liens_nofollow(safehtml(typo(interdit_html($Pile[$SP]['titre']), "TYPO", $connect, $Pile[0])))) .
'</a></strong>
							' .
(($t1 = strval(interdire_scripts(((normaliser_date($Pile[$SP]['date'])) ?' ' :''))))!=='' ?
		($t1 . (	'<abbr class="date"' .
	(($t2 = strval(interdire_scripts(date_iso(normaliser_date($Pile[$SP]['date'])))))!=='' ?
			(' title="' . $t2 . '"') :
			'') .
	'>
								' .
	vide($Pile['vars'][$_zzz=(string)'date'] = interdire_scripts(affdate_jourcourt(normaliser_date($Pile[$SP]['date'])))) .
	'
								' .
	vide($Pile['vars'][$_zzz=(string)'heure'] = (	interdire_scripts(heures(normaliser_date($Pile[$SP]['date']))) .
		(($t3 = strval(interdire_scripts(minutes(normaliser_date($Pile[$SP]['date'])))))!=='' ?
				(':' . $t3) :
				''))) .
	'
								' .
	_T('zpip:date_forum', array('date' => table_valeur($Pile["vars"], (string)'date', null),
'heure' => table_valeur($Pile["vars"], (string)'heure', null))) .
	'</abbr>')) :
		'') .
(($t1 = strval(interdire_scripts(supprimer_numero(typo($Pile[$SP]['nom']), "TYPO", $connect, $Pile[0]))))!=='' ?
		((	'<span class="comment-author vcard">, ' .
	$l1 .
	' <strong class="fn n ">') . $t1 . '</strong></span>') :
		'') .
'
						</div>
						<div class="comment-content forum-texte">
							' .

((!is_array($l = quete_logo('id_auteur', 'ON', $Pile[$SP]['id_auteur'],'', 0))) ? '':
 ("<img class=\"spip_logo spip_logos\" alt=\"\" src=\"$l[0]\"" . $l[2] .  ($l[1] ? " onmouseover=\"this.src='$l[1]'\" onmouseout=\"this.src='$l[0]'\"" : "") . ' />')) .
'
							' .
(($t1 = strval(interdire_scripts(lignes_longues(liens_nofollow(safehtml(propre(interdit_html($Pile[$SP]['texte']), $connect, $Pile[0])))))))!=='' ?
		((	'<div class="comment-texte">') . $t1 . '</div>') :
		'') .
'
							' .
(($t1 = strval(interdire_scripts(lignes_longues(safehtml(liens_nofollow(safehtml(propre(interdit_html(calculer_notes()), $connect, $Pile[0]))))))))!=='' ?
		('<div class="comment-notes">' . $t1 . '</div>') :
		'') .
'
							' .
(($t1 = strval(safehtml(vider_url(calculer_url($Pile[$SP]['url_site'],'','url', $connect)))))!=='' ?
		((	'<p class="comment-external-link hyperlien">' .
	$l2 .
	' : <a href="') . $t1 . (	'" class="spip_out">' .
	interdire_scripts(((($a = liens_nofollow(safehtml(typo(interdit_html(supprimer_numero(calculer_url($Pile[$SP]['url_site'],$Pile[$SP]['nom_site'], 'titre', $connect, false))), "TYPO", $connect, $Pile[0])))) OR (is_string($a) AND strlen($a))) ? $a : couper(safehtml(vider_url(calculer_url($Pile[$SP]['url_site'],'','url', $connect))),'80'))) .
	'</a></p>')) :
		'') .
'

							' .
BOUCLE_doc2html_2cd38e156bfa819301815ef4fc63ef30($Cache, $Pile, $doublons, $Numrows, $SP) .
'

							' .
(($t1 = strval(filtre_url_reponse_forum(spip_htmlspecialchars(
		// refus des forums ?
		(quete_accepter_forum($Pile[$SP]['id_article'])=="non" OR
		($GLOBALS["meta"]["forums_publics"] == "non"
		AND quete_accepter_forum($Pile[$SP]['id_article']) == ""))
		? "" : // sinon:
		(calcul_parametres_forum($Pile[0],$Pile[$SP]['id_forum'],'forums',$Pile[$SP]['id_forum']).
	(($lien = (_request("retour") ? _request("retour") : str_replace("&amp;", "&", ''))) ? "&retour=".rawurlencode($lien) : ""))))))!=='' ?
		('<p class="comment-reply repondre"><a href="' . $t1 . (	'" rel="noindex nofollow">' .
	$l3 .
	'</a></p>')) :
		'') .
'
						</div>
					</div>

					' .
BOUCLE_forums_bouclehtml_2cd38e156bfa819301815ef4fc63ef30($Cache, $Pile, $doublons, $Numrows, $SP) .
'

				</li>

				');
	}
	$iter->free();
	}
	if (defined("_BOUCLE_PROFILER")
	AND 1000*($timer = (time()+(float)microtime())-$timer) > _BOUCLE_PROFILER)
		spip_log(intval(1000*$timer)."ms BOUCLE_forums_fils @ plugins/auto/z/v1.7.31/inclure/forum.html","profiler"._LOG_AVERTISSEMENT);
	return $t0;
}


function BOUCLE_forumshtml_2cd38e156bfa819301815ef4fc63ef30(&$Cache, &$Pile, &$doublons, &$Numrows, $SP) {

	static $command = array();
	static $connect;
	$command['connect'] = $connect = '';
	$in = array();
	if (!(is_array($a = (@$Pile[0]['id_rubrique']))))
		$in[]= $a;
	else $in = array_merge($in, $a);
	$in1 = array();
	if (!(is_array($a = (@$Pile[0]['id_article']))))
		$in1[]= $a;
	else $in1 = array_merge($in1, $a);
	$in2 = array();
	if (!(is_array($a = (@$Pile[0]['id_breve']))))
		$in2[]= $a;
	else $in2 = array_merge($in2, $a);
	$in3 = array();
	if (!(is_array($a = (@$Pile[0]['id_syndic']))))
		$in3[]= $a;
	else $in3 = array_merge($in3, $a);
	if (!isset($command['table'])) {
		$command['table'] = 'forum';
		$command['id'] = '_forums';
		$command['from'] = array('forum' => 'spip_forum');
		$command['type'] = array();
		$command['groupby'] = array();
		$command['select'] = array("forum.id_forum",
		"forum.date_heure",
		"forum.titre",
		"forum.date_heure AS date",
		"forum.auteur AS nom",
		"forum.id_auteur",
		"forum.texte",
		"forum.url_site",
		"forum.nom_site",
		"forum.id_objet AS id_article");
		$command['orderby'] = array('forum.date_heure');
		$command['join'] = array();
		$command['limit'] = '';
		$command['having'] = 
			array();
	}
	$command['where'] = 
			array(
quete_condition_statut('forum.statut','publie,prop','publie',''), 
			array('=', 'forum.id_parent', 0), (!(is_array(@$Pile[0]['id_rubrique'])?count(@$Pile[0]['id_rubrique']):strlen(@$Pile[0]['id_rubrique'])) ? '' : ((is_array(@$Pile[0]['id_rubrique'])) ? sql_in('forum.id_objet',sql_quote($in)) : 
			array('=', 'forum.id_objet', sql_quote(@$Pile[0]['id_rubrique'], '','bigint(21) NOT NULL DEFAULT \'0\'')))), (!(is_array(@$Pile[0]['id_rubrique'])?count(@$Pile[0]['id_rubrique']):strlen(@$Pile[0]['id_rubrique'])) ? '' : 
			array('=', 'forum.objet', sql_quote('rubrique'))), (!(is_array(@$Pile[0]['id_article'])?count(@$Pile[0]['id_article']):strlen(@$Pile[0]['id_article'])) ? '' : ((is_array(@$Pile[0]['id_article'])) ? sql_in('forum.id_objet',sql_quote($in1)) : 
			array('=', 'forum.id_objet', sql_quote(@$Pile[0]['id_article'], '','bigint(21) NOT NULL DEFAULT \'0\'')))), (!(is_array(@$Pile[0]['id_article'])?count(@$Pile[0]['id_article']):strlen(@$Pile[0]['id_article'])) ? '' : 
			array('=', 'forum.objet', sql_quote('article'))), (!(is_array(@$Pile[0]['id_breve'])?count(@$Pile[0]['id_breve']):strlen(@$Pile[0]['id_breve'])) ? '' : ((is_array(@$Pile[0]['id_breve'])) ? sql_in('forum.id_objet',sql_quote($in2)) : 
			array('=', 'forum.id_objet', sql_quote(@$Pile[0]['id_breve'], '','bigint(21) NOT NULL DEFAULT \'0\'')))), (!(is_array(@$Pile[0]['id_breve'])?count(@$Pile[0]['id_breve']):strlen(@$Pile[0]['id_breve'])) ? '' : 
			array('=', 'forum.objet', sql_quote('breve'))), (!(is_array(@$Pile[0]['id_syndic'])?count(@$Pile[0]['id_syndic']):strlen(@$Pile[0]['id_syndic'])) ? '' : ((is_array(@$Pile[0]['id_syndic'])) ? sql_in('forum.id_objet',sql_quote($in3)) : 
			array('=', 'forum.id_objet', sql_quote(@$Pile[0]['id_syndic'], '','bigint(21) NOT NULL DEFAULT \'0\'')))), (!(is_array(@$Pile[0]['id_syndic'])?count(@$Pile[0]['id_syndic']):strlen(@$Pile[0]['id_syndic'])) ? '' : 
			array('=', 'forum.objet', sql_quote('site'))));
	if (defined("_BOUCLE_PROFILER")) $timer = time()+(float)microtime();
	$t0 = "";
	// REQUETE
	$iter = IterFactory::create(
		"SQL",
		$command,
		array('plugins/auto/z/v1.7.31/inclure/forum.html','html_2cd38e156bfa819301815ef4fc63ef30','_forums',11,$GLOBALS['spip_lang'])
	);
	if (!$iter->err()) {
	
	// COMPTEUR
	$Numrows['_forums']['compteur_boucle'] = 0;
	
	$l1 = _T('public|spip|ecrire:par_auteur');
	$l2 = _T('public|spip|ecrire:voir_en_ligne');
	$l3 = _T('public|spip|ecrire:lien_repondre_message');$SP++;
	// RESULTATS
	while ($Pile[$SP]=$iter->fetch()) {

		$Numrows['_forums']['compteur_boucle']++;
		$t0 .= (
'

		<li class="forum-fil comment' .
(($t1 = strval(alterner($Numrows['_forums']['compteur_boucle'],'odd','even')))!=='' ?
		(' ' . $t1) :
		'') .
((($Numrows['_forums']['compteur_boucle'] == '1'))  ?
		(' ' . ' ' . 'first') :
		'') .
(calcul_exposer($Pile[$SP]['id_forum'], 'id_forum', $Pile[0], 0, 'id_forum', '')  ?
		(' ' . 'on') :
		'') .
'">
			<div class="comment-message forum-message">
				<div class="comment-meta forum-chapo">
					<a href="#forum' .
$Pile[$SP]['id_forum'] .
'" title="' .
$Pile[$SP]['id_forum'] .
'" class="ancre permalink comment-number">#</a>
					<strong class="forum-titre"><a href="#forum' .
$Pile[$SP]['id_forum'] .
'" name="forum' .
$Pile[$SP]['id_forum'] .
'" id="forum' .
$Pile[$SP]['id_forum'] .
'">' .
interdire_scripts(liens_nofollow(safehtml(typo(interdit_html($Pile[$SP]['titre']), "TYPO", $connect, $Pile[0])))) .
'</a></strong>
					' .
(($t1 = strval(interdire_scripts(((normaliser_date($Pile[$SP]['date'])) ?' ' :''))))!=='' ?
		($t1 . (	'<abbr class="date"' .
	(($t2 = strval(interdire_scripts(date_iso(normaliser_date($Pile[$SP]['date'])))))!=='' ?
			(' title="' . $t2 . '"') :
			'') .
	'>
						' .
	vide($Pile['vars'][$_zzz=(string)'date'] = interdire_scripts(affdate_jourcourt(normaliser_date($Pile[$SP]['date'])))) .
	'
						' .
	vide($Pile['vars'][$_zzz=(string)'heure'] = (	interdire_scripts(heures(normaliser_date($Pile[$SP]['date']))) .
		(($t3 = strval(interdire_scripts(minutes(normaliser_date($Pile[$SP]['date'])))))!=='' ?
				(':' . $t3) :
				''))) .
	'
						' .
	_T('zpip:date_forum', array('date' => table_valeur($Pile["vars"], (string)'date', null),
'heure' => table_valeur($Pile["vars"], (string)'heure', null))) .
	'</abbr>')) :
		'') .
(($t1 = strval(interdire_scripts(supprimer_numero(typo($Pile[$SP]['nom']), "TYPO", $connect, $Pile[0]))))!=='' ?
		((	'<span class="comment-author vcard">, ' .
	$l1 .
	' <strong class="fn n ">') . $t1 . '</strong></span>') :
		'') .
'
				</div>
				<div class="comment-content forum-texte">
					' .

((!is_array($l = quete_logo('id_auteur', 'ON', $Pile[$SP]['id_auteur'],'', 0))) ? '':
 ("<img class=\"spip_logo spip_logos\" alt=\"\" src=\"$l[0]\"" . $l[2] .  ($l[1] ? " onmouseover=\"this.src='$l[1]'\" onmouseout=\"this.src='$l[0]'\"" : "") . ' />')) .
'
					' .
(($t1 = strval(interdire_scripts(lignes_longues(liens_nofollow(safehtml(propre(interdit_html($Pile[$SP]['texte']), $connect, $Pile[0])))))))!=='' ?
		((	'<div class="comment-texte">') . $t1 . '</div>') :
		'') .
'
					' .
(($t1 = strval(interdire_scripts(lignes_longues(safehtml(liens_nofollow(safehtml(propre(interdit_html(calculer_notes()), $connect, $Pile[0]))))))))!=='' ?
		('<div class="comment-notes">' . $t1 . '</div>') :
		'') .
'
					' .
(($t1 = strval(safehtml(vider_url(calculer_url($Pile[$SP]['url_site'],'','url', $connect)))))!=='' ?
		((	'<p class="comment-external-link hyperlien">' .
	$l2 .
	' : <a href="') . $t1 . (	'" class="spip_out">' .
	interdire_scripts(((($a = liens_nofollow(safehtml(typo(interdit_html(supprimer_numero(calculer_url($Pile[$SP]['url_site'],$Pile[$SP]['nom_site'], 'titre', $connect, false))), "TYPO", $connect, $Pile[0])))) OR (is_string($a) AND strlen($a))) ? $a : couper(safehtml(vider_url(calculer_url($Pile[$SP]['url_site'],'','url', $connect))),'80'))) .
	'</a></p>')) :
		'') .
'
					' .
BOUCLE_dochtml_2cd38e156bfa819301815ef4fc63ef30($Cache, $Pile, $doublons, $Numrows, $SP) .
'

					' .
(($t1 = strval(filtre_url_reponse_forum(spip_htmlspecialchars(
		// refus des forums ?
		(quete_accepter_forum($Pile[$SP]['id_article'])=="non" OR
		($GLOBALS["meta"]["forums_publics"] == "non"
		AND quete_accepter_forum($Pile[$SP]['id_article']) == ""))
		? "" : // sinon:
		(calcul_parametres_forum($Pile[0],$Pile[$SP]['id_forum'],'forums',$Pile[$SP]['id_forum']).
	(($lien = (_request("retour") ? _request("retour") : str_replace("&amp;", "&", ''))) ? "&retour=".rawurlencode($lien) : ""))))))!=='' ?
		('<p class="comment-reply repondre"><a href="' . $t1 . (	'" rel="noindex nofollow">' .
	$l3 .
	'</a></p>')) :
		'') .
'
				</div>
			</div>

			' .
(($t1 = BOUCLE_forums_filshtml_2cd38e156bfa819301815ef4fc63ef30($Cache, $Pile, $doublons, $Numrows, $SP))!=='' ?
		('
			<ul>
				' . $t1 . '
			</ul>
			') :
		'') .
'

		</li>
		');
	}
	$iter->free();
	}
	if (defined("_BOUCLE_PROFILER")
	AND 1000*($timer = (time()+(float)microtime())-$timer) > _BOUCLE_PROFILER)
		spip_log(intval(1000*$timer)."ms BOUCLE_forums @ plugins/auto/z/v1.7.31/inclure/forum.html","profiler"._LOG_AVERTISSEMENT);
	return $t0;
}

//
// Fonction principale du squelette plugins/auto/z/v1.7.31/inclure/forum.html
// Temps de compilation total: 5.002 ms
//

function html_2cd38e156bfa819301815ef4fc63ef30($Cache, $Pile, $doublons = array(), $Numrows = array(), $SP = 0) {

	if (isset($Pile[0]["doublons"]) AND is_array($Pile[0]["doublons"]))
		$doublons = nettoyer_env_doublons($Pile[0]["doublons"]);

	$connect = '';
	$page = (
'
' .
(spip_htmlspecialchars(
		// refus des forums ?
		(quete_accepter_forum(@$Pile[0]['id_article'])=="non" OR
		($GLOBALS["meta"]["forums_publics"] == "non"
		AND quete_accepter_forum(@$Pile[0]['id_article']) == ""))
		? "" : // sinon:
		(calcul_parametres_forum($Pile[0],null,null,null).
	(($lien = (_request("retour") ? _request("retour") : str_replace("&amp;", "&", ''))) ? "&retour=".rawurlencode($lien) : ""))) ? '':'') .
'

<div class="comments" id="comments">
	
	' .
BOUCLE_decomptehtml_2cd38e156bfa819301815ef4fc63ef30($Cache, $Pile, $doublons, $Numrows, $SP)
. (($t2 = strval((($Numrows['_decompte']['total'] > '0') ? $Numrows['_decompte']['total']:'')))!=='' ?
			('<h2 class="h2">' . $t2 . (	'
	' .
		(($Numrows['_decompte']['total'] == '1') ? _T('public|spip|ecrire:message'):_T('public|spip|ecrire:messages_forum')) .
		'</h2>')) :
			'') .
'

	
	' .
(($t1 = BOUCLE_forumshtml_2cd38e156bfa819301815ef4fc63ef30($Cache, $Pile, $doublons, $Numrows, $SP))!=='' ?
		('
	<ul class="forum comments-list">

		' . $t1 . '

	</ul>
	') :
		'') .
'
</div>
');

	return analyse_resultat_skel('html_2cd38e156bfa819301815ef4fc63ef30', $Cache, $page, 'plugins/auto/z/v1.7.31/inclure/forum.html');
}
?>