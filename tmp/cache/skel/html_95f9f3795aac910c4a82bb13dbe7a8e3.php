<?php

/*
 * Squelette : ../prive/objets/infos/objet.html
 * Date :      Tue, 16 Jun 2020 14:01:32 GMT
 * Compile :   Wed, 17 Jun 2020 06:58:47 GMT
 * Boucles :   
 */ 
//
// Fonction principale du squelette ../prive/objets/infos/objet.html
// Temps de compilation total: 2.252 ms
//

function html_95f9f3795aac910c4a82bb13dbe7a8e3($Cache, $Pile, $doublons = array(), $Numrows = array(), $SP = 0) {

	if (isset($Pile[0]["doublons"]) AND is_array($Pile[0]["doublons"]))
		$doublons = nettoyer_env_doublons($Pile[0]["doublons"]);

	$connect = '';
	$page = (
'<div class=\'infos\'>
' .
vide($Pile['vars'][$_zzz=(string)'texte_objet'] = interdire_scripts(_T(objet_info(entites_html(table_valeur(@$Pile[0], (string)'type', null),true),'texte_objet')))) .
'<div class=\'numero\'>' .
_T('public|spip|ecrire:titre_cadre_numero_objet', array('objet' => table_valeur($Pile["vars"], (string)'texte_objet', null))) .
'<p>' .
interdire_scripts(generer_info_entite(entites_html(table_valeur(@$Pile[0], (string)'id', null),true),interdire_scripts(entites_html(table_valeur(@$Pile[0], (string)'type', null),true)),interdire_scripts(id_table_objet(entites_html(table_valeur(@$Pile[0], (string)'type', null),true))),'**')) .
'</p></div>


' .
executer_balise_dynamique('FORMULAIRE_INSTITUER_OBJET',
	array(interdire_scripts(entites_html(table_valeur(@$Pile[0], (string)'type', null),true)),interdire_scripts(entites_html(table_valeur(@$Pile[0], (string)'id', null),true)),'',interdire_scripts(objet_info(entites_html(table_valeur(@$Pile[0], (string)'type', null),true),'editable'))),
	array('../prive/objets/infos/objet.html','html_95f9f3795aac910c4a82bb13dbe7a8e3','',4,$GLOBALS['spip_lang'])) .
'


' .
(($t1 = strval(interdire_scripts(((objet_info(entites_html(table_valeur(@$Pile[0], (string)'type', null),true),'page')) ?' ' :''))))!=='' ?
		($t1 . (	'
	' .
	(((objet_test_si_publie(interdire_scripts(entites_html(table_valeur(@$Pile[0], (string)'type', null),true)),intval(interdire_scripts(entites_html(table_valeur(@$Pile[0], (string)'id', null),true))),'')?' ':''))  ?
			(' ' . (	'
		' .
		filtre_icone_horizontale_dist(parametre_url(generer_url_action('redirect',(	'type=' .
			interdire_scripts(entites_html(table_valeur(@$Pile[0], (string)'type', null),true)) .
			'&id=' .
			interdire_scripts(entites_html(table_valeur(@$Pile[0], (string)'id', null),true)))),'var_mode','calcul'),_T('public|spip|ecrire:icone_voir_en_ligne'),'racine') .
		'
	')) :
			'') .
	'
	' .
	(!((objet_test_si_publie(interdire_scripts(entites_html(table_valeur(@$Pile[0], (string)'type', null),true)),intval(interdire_scripts(entites_html(table_valeur(@$Pile[0], (string)'id', null),true))),'')?' ':''))  ?
			(' ' . (	'
		' .
		vide($Pile['vars'][$_zzz=(string)'champ_statut'] = interdire_scripts(table_valeur(objet_info(entites_html(table_valeur(@$Pile[0], (string)'type', null),true),'statut'),'0/champ'))) .
		(((((table_valeur($Pile["vars"], (string)'champ_statut', null)) ?' ' :'')) AND (invalideur_session($Cache, ((function_exists("autoriser")||include_spip("inc/autoriser"))&&autoriser('previsualiser', interdire_scripts(invalideur_session($Cache, entites_html(table_valeur(@$Pile[0], (string)'type', null),true))), interdire_scripts(invalideur_session($Cache, entites_html(table_valeur(@$Pile[0], (string)'id', null),true))), '', invalideur_session($Cache, array(invalideur_session($Cache, table_valeur($Pile["vars"], (string)'champ_statut', null)) => interdire_scripts(invalideur_session($Cache, generer_info_entite(entites_html(table_valeur(@$Pile[0], (string)'id', null),true),interdire_scripts(invalideur_session($Cache, entites_html(table_valeur(@$Pile[0], (string)'type', null),true))),invalideur_session($Cache, table_valeur($Pile["vars"], (string)'champ_statut', null))))))))?" ":""))))  ?
				(' ' . (	'
			' .
			filtre_icone_horizontale_dist(parametre_url(generer_url_action('redirect',(	'type=' .
				interdire_scripts(entites_html(table_valeur(@$Pile[0], (string)'type', null),true)) .
				'&id=' .
				interdire_scripts(entites_html(table_valeur(@$Pile[0], (string)'id', null),true)))),'var_mode','preview'),_T('public|spip|ecrire:previsualiser'),'preview') .
			'
		')) :
				'') .
		'
	')) :
			'') .
	'
')) :
		'') .
'
</div>
');

	return analyse_resultat_skel('html_95f9f3795aac910c4a82bb13dbe7a8e3', $Cache, $page, '../prive/objets/infos/objet.html');
}
?>