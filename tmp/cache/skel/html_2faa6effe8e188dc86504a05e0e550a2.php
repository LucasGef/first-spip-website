<?php

/*
 * Squelette : plugins/auto/spipr_dist/v2.2.6/content/404.html
 * Date :      Mon, 30 Mar 2020 09:25:24 GMT
 * Compile :   Wed, 17 Jun 2020 08:56:36 GMT
 * Boucles :   
 */ 
//
// Fonction principale du squelette plugins/auto/spipr_dist/v2.2.6/content/404.html
// Temps de compilation total: 0.134 ms
//

function html_2faa6effe8e188dc86504a05e0e550a2($Cache, $Pile, $doublons = array(), $Numrows = array(), $SP = 0) {

	if (isset($Pile[0]["doublons"]) AND is_array($Pile[0]["doublons"]))
		$doublons = nettoyer_env_doublons($Pile[0]["doublons"]);

	$connect = '';
	$page = (
'
<section>
	' .
(($t1 = strval(interdire_scripts(((entites_html(sinon(table_valeur(@$Pile[0], (string)'cartouche', null), '1'),true)) ?' ' :''))))!=='' ?
		($t1 . 
'<'.'?php echo recuperer_fond( ' . argumenter_squelette((	'cartouche/' .
		interdire_scripts(entites_html(table_valeur(@$Pile[0], (string)'type-page', null),true)))) . ', array_merge('.var_export($Pile[0],1).',array(\'lang\' => ' . argumenter_squelette($GLOBALS["spip_lang"]) . ')), array("compil"=>array(\'plugins/auto/spipr_dist/v2.2.6/content/404.html\',\'html_2faa6effe8e188dc86504a05e0e550a2\',\'\',3,$GLOBALS[\'spip_lang\'])), _request("connect"));
?'.'>') :
		'') .
'
	<div class="main">
		' .
(($t1 = strval(interdire_scripts(entites_html(table_valeur(@$Pile[0], (string)'erreur', null),true))))!=='' ?
		('<div class="chapo">' . $t1 . '</div>') :
		'') .
'
	</div>
</section>
');

	return analyse_resultat_skel('html_2faa6effe8e188dc86504a05e0e550a2', $Cache, $page, 'plugins/auto/spipr_dist/v2.2.6/content/404.html');
}
?>