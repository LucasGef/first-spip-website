<?php

/*
 * Squelette : plugins/auto/spipr_dist/v2.2.6/breadcrumb/inc-objet.html
 * Date :      Mon, 30 Mar 2020 09:25:24 GMT
 * Compile :   Wed, 17 Jun 2020 09:07:11 GMT
 * Boucles :   _ariane_hier, _contexte_rubrique
 */ 

function BOUCLE_ariane_hierhtml_89d8861b2f7c18c9e81c62ba1c64b137(&$Cache, &$Pile, &$doublons, &$Numrows, $SP) {

	static $command = array();
	static $connect;
	$command['connect'] = $connect = '';
	if (!($id_rubrique = intval($Pile[$SP]['id_rubrique'])))
		return '';
	include_spip('inc/rubriques');
	$hierarchie = calcul_hierarchie_in($id_rubrique,true);
	if (!$hierarchie) return "";
	
	if (!isset($command['table'])) {
		$command['table'] = 'rubriques';
		$command['id'] = '_ariane_hier';
		$command['from'] = array('rubriques' => 'spip_rubriques');
		$command['type'] = array();
		$command['groupby'] = array();
		$command['select'] = array("rubriques.id_rubrique",
		"rubriques.titre",
		"rubriques.lang");
		$command['join'] = array();
		$command['limit'] = '';
		$command['having'] = 
			array();
	}
	$command['orderby'] = array("FIELD(rubriques.id_rubrique, $hierarchie)");
	$command['where'] = 
			array(
			array('IN', 'rubriques.id_rubrique', "($hierarchie)"));
	if (defined("_BOUCLE_PROFILER")) $timer = time()+(float)microtime();
	$t0 = "";
	// REQUETE
	$iter = IterFactory::create(
		"SQL",
		$command,
		array('plugins/auto/spipr_dist/v2.2.6/breadcrumb/inc-objet.html','html_89d8861b2f7c18c9e81c62ba1c64b137','_ariane_hier',3,$GLOBALS['spip_lang'])
	);
	if (!$iter->err()) {
	lang_select($GLOBALS['spip_lang']);
	$SP++;
	// RESULTATS
	while ($Pile[$SP]=$iter->fetch()) {

		lang_select_public($Pile[$SP]['lang'], '', $Pile[$SP]['titre']);
		$t0 .= (
'
<li class="breadcrumb-item"><a href="' .
vider_url(urlencode_1738(generer_url_entite($Pile[$SP]['id_rubrique'], 'rubrique', '', '', true))) .
'">' .
interdire_scripts(couper(supprimer_numero(typo($Pile[$SP]['titre']), "TYPO", $connect, $Pile[0]),'80')) .
'</a></li>
');
		lang_select();
	}
	lang_select();
	$iter->free();
	}
	if (defined("_BOUCLE_PROFILER")
	AND 1000*($timer = (time()+(float)microtime())-$timer) > _BOUCLE_PROFILER)
		spip_log(intval(1000*$timer)."ms BOUCLE_ariane_hier @ plugins/auto/spipr_dist/v2.2.6/breadcrumb/inc-objet.html","profiler"._LOG_AVERTISSEMENT);
	return $t0;
}


function BOUCLE_contexte_rubriquehtml_89d8861b2f7c18c9e81c62ba1c64b137(&$Cache, &$Pile, &$doublons, &$Numrows, $SP) {

	static $command = array();
	static $connect;
	$command['connect'] = $connect = '';
	if (!isset($command['table'])) {
		$command['table'] = 'rubriques';
		$command['id'] = '_contexte_rubrique';
		$command['from'] = array('rubriques' => 'spip_rubriques');
		$command['type'] = array();
		$command['groupby'] = array();
		$command['select'] = array("rubriques.id_rubrique",
		"rubriques.lang",
		"rubriques.titre");
		$command['orderby'] = array();
		$command['join'] = array();
		$command['limit'] = '';
		$command['having'] = 
			array();
	}
	$command['where'] = 
			array(
quete_condition_statut('rubriques.statut','!','publie',''), 
			array('=', 'rubriques.id_rubrique', sql_quote(interdire_scripts(((@$Pile[0]['objet'] == 'rubrique') ? interdire_scripts(generer_info_entite(@$Pile[0]['id_objet'], interdire_scripts(@$Pile[0]['objet']), 'id_parent')):interdire_scripts(generer_info_entite(@$Pile[0]['id_objet'], interdire_scripts(@$Pile[0]['objet']), 'id_rubrique')))), '', 'bigint(21) NOT NULL AUTO_INCREMENT')));
	if (defined("_BOUCLE_PROFILER")) $timer = time()+(float)microtime();
	$t0 = "";
	// REQUETE
	$iter = IterFactory::create(
		"SQL",
		$command,
		array('plugins/auto/spipr_dist/v2.2.6/breadcrumb/inc-objet.html','html_89d8861b2f7c18c9e81c62ba1c64b137','_contexte_rubrique',2,$GLOBALS['spip_lang'])
	);
	if (!$iter->err()) {
	lang_select($GLOBALS['spip_lang']);
	$SP++;
	// RESULTATS
	while ($Pile[$SP]=$iter->fetch()) {

		lang_select_public($Pile[$SP]['lang'], '', $Pile[$SP]['titre']);
		$t0 .= (
'
' .
BOUCLE_ariane_hierhtml_89d8861b2f7c18c9e81c62ba1c64b137($Cache, $Pile, $doublons, $Numrows, $SP) .
'
');
		lang_select();
	}
	lang_select();
	$iter->free();
	}
	if (defined("_BOUCLE_PROFILER")
	AND 1000*($timer = (time()+(float)microtime())-$timer) > _BOUCLE_PROFILER)
		spip_log(intval(1000*$timer)."ms BOUCLE_contexte_rubrique @ plugins/auto/spipr_dist/v2.2.6/breadcrumb/inc-objet.html","profiler"._LOG_AVERTISSEMENT);
	return $t0;
}

//
// Fonction principale du squelette plugins/auto/spipr_dist/v2.2.6/breadcrumb/inc-objet.html
// Temps de compilation total: 2.102 ms
//

function html_89d8861b2f7c18c9e81c62ba1c64b137($Cache, $Pile, $doublons = array(), $Numrows = array(), $SP = 0) {

	if (isset($Pile[0]["doublons"]) AND is_array($Pile[0]["doublons"]))
		$doublons = nettoyer_env_doublons($Pile[0]["doublons"]);

	$connect = '';
	$page = (
'<li class="breadcrumb-item"><a href="' .
spip_htmlspecialchars(sinon($GLOBALS['meta']['adresse_site'],'.')) .
'/">' .
_T('public|spip|ecrire:accueil_site') .
'</a></li>
' .
BOUCLE_contexte_rubriquehtml_89d8861b2f7c18c9e81c62ba1c64b137($Cache, $Pile, $doublons, $Numrows, $SP) .
'
<li class="breadcrumb-item' .
(($t1 = strval(interdire_scripts(((entites_html(sinon(table_valeur(@$Pile[0], (string)'expose', null), ' '),true)) ?' ' :''))))!=='' ?
		($t1 . 'active') :
		'') .
'">' .
filtre_lien_ou_expose_dist(generer_url_entite(@$Pile[0]['id_objet'],interdire_scripts(@$Pile[0]['objet'])),interdire_scripts(couper(((($a = generer_info_entite(@$Pile[0]['id_objet'], interdire_scripts(@$Pile[0]['objet']), 'titre')) OR (is_string($a) AND strlen($a))) ? $a : '?'),'80')),interdire_scripts((entites_html(sinon(table_valeur(@$Pile[0], (string)'expose', null), ' '),true) ? 'span':''))) .
'</li>
');

	return analyse_resultat_skel('html_89d8861b2f7c18c9e81c62ba1c64b137', $Cache, $page, 'plugins/auto/spipr_dist/v2.2.6/breadcrumb/inc-objet.html');
}
?>