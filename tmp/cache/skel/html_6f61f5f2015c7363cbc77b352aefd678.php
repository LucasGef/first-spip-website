<?php

/*
 * Squelette : ../plugins-dist/dump/prive/style_prive_plugin_dump.html
 * Date :      Tue, 16 Jun 2020 14:01:53 GMT
 * Compile :   Wed, 17 Jun 2020 07:16:17 GMT
 * Boucles :   
 */ 
//
// Fonction principale du squelette ../plugins-dist/dump/prive/style_prive_plugin_dump.html
// Temps de compilation total: 0.425 ms
//

function html_6f61f5f2015c7363cbc77b352aefd678($Cache, $Pile, $doublons = array(), $Numrows = array(), $SP = 0) {

	if (isset($Pile[0]["doublons"]) AND is_array($Pile[0]["doublons"]))
		$doublons = nettoyer_env_doublons($Pile[0]["doublons"]);

	$connect = '';
	$page = (
'
' .
'<'.'?php header("X-Spip-Cache: 360000"); ?'.'>'.'<'.'?php header("Cache-Control: max-age=360000"); ?'.'>'.'<'.'?php header("X-Spip-Statique: oui"); ?'.'>' .
'<'.'?php header(' . _q('Content-Type: text/css; charset=iso-8859-15') . '); ?'.'>' .
'<'.'?php header(' . _q('Vary: Accept-Encoding') . '); ?'.'>' .
vide($Pile['vars'][$_zzz=(string)'claire'] = (	'#' .
	interdire_scripts(entites_html(sinon(table_valeur(@$Pile[0], (string)'couleur_claire', null), 'edf3fe'),true)))) .
vide($Pile['vars'][$_zzz=(string)'foncee'] = (	'#' .
	interdire_scripts(entites_html(sinon(table_valeur(@$Pile[0], (string)'couleur_foncee', null), '3874b0'),true)))) .
vide($Pile['vars'][$_zzz=(string)'left'] = interdire_scripts(choixsiegal(entites_html(table_valeur(@$Pile[0], (string)'ltr', null),true),'left','left','right'))) .
vide($Pile['vars'][$_zzz=(string)'right'] = interdire_scripts(choixsiegal(entites_html(table_valeur(@$Pile[0], (string)'ltr', null),true),'left','right','left'))) .
'.liste-objets.dump tr .fichier label {display:block; width: 260px;word-wrap:break-word;}
.liste-objets.dump tr .taille {text-align:right;}

.formulaire_restaurer .editer div.choix {border:0;background: none;padding: 0;}

');

	return analyse_resultat_skel('html_6f61f5f2015c7363cbc77b352aefd678', $Cache, $page, '../plugins-dist/dump/prive/style_prive_plugin_dump.html');
}
?>