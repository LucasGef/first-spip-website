<?php

/*
 * Squelette : plugins/auto/spipr_dist/v2.2.6/content/article.html
 * Date :      Wed, 17 Jun 2020 08:56:49 GMT
 * Compile :   Wed, 17 Jun 2020 09:02:58 GMT
 * Boucles :   _content
 */ 

function BOUCLE_contenthtml_b5ad0cd23e7c931bbcf8f56c3ecb9c2b(&$Cache, &$Pile, &$doublons, &$Numrows, $SP) {

	static $command = array();
	static $connect;
	$command['connect'] = $connect = '';
	if (!isset($command['table'])) {
		$command['table'] = 'articles';
		$command['id'] = '_content';
		$command['from'] = array('articles' => 'spip_articles');
		$command['type'] = array();
		$command['groupby'] = array();
		$command['select'] = array("articles.chapo",
		"articles.texte",
		"articles.url_site",
		"articles.nom_site",
		"articles.ps",
		"articles.id_article",
		"articles.lang",
		"articles.titre");
		$command['orderby'] = array();
		$command['join'] = array();
		$command['limit'] = '';
		$command['having'] = 
			array();
	}
	$command['where'] = 
			array(
quete_condition_statut('articles.statut','publie,prop,prepa/auteur','publie',''), 
quete_condition_postdates('articles.date',''), 
			array('=', 'articles.id_article', sql_quote(@$Pile[0]['id_article'], '','bigint(21) NOT NULL AUTO_INCREMENT')));
	if (defined("_BOUCLE_PROFILER")) $timer = time()+(float)microtime();
	$t0 = "";
	// REQUETE
	$iter = IterFactory::create(
		"SQL",
		$command,
		array('plugins/auto/spipr_dist/v2.2.6/content/article.html','html_b5ad0cd23e7c931bbcf8f56c3ecb9c2b','_content',1,$GLOBALS['spip_lang'])
	);
	if (!$iter->err()) {
	lang_select($GLOBALS['spip_lang']);
	$SP++;
	// RESULTATS
	while ($Pile[$SP]=$iter->fetch()) {

		lang_select_public($Pile[$SP]['lang'], '', $Pile[$SP]['titre']);
		$t0 .= (
'
<article>
	' .
(($t1 = strval(interdire_scripts(((entites_html(sinon(table_valeur(@$Pile[0], (string)'cartouche', null), '1'),true)) ?' ' :''))))!=='' ?
		($t1 . 
'<'.'?php echo recuperer_fond( ' . argumenter_squelette((	'cartouche/' .
		interdire_scripts(entites_html(table_valeur(@$Pile[0], (string)'type-page', null),true)))) . ', array_merge('.var_export($Pile[0],1).',array(\'lang\' => ' . argumenter_squelette($GLOBALS["spip_lang"]) . ')), array("compil"=>array(\'plugins/auto/spipr_dist/v2.2.6/content/article.html\',\'html_b5ad0cd23e7c931bbcf8f56c3ecb9c2b\',\'\',3,$GLOBALS[\'spip_lang\'])), _request("connect"));
?'.'>') :
		'') .
'

	<div class="main">
		' .
(($t1 = strval(interdire_scripts(adaptive_images(propre($Pile[$SP]['chapo'], $connect, $Pile[0])))))!=='' ?
		((	'<div class="chapo surlignable">') . $t1 . '</div>') :
		'') .
'
		' .
(($t1 = strval(interdire_scripts(adaptive_images(propre($Pile[$SP]['texte'], $connect, $Pile[0])))))!=='' ?
		((	'<div class="texte surlignable">') . $t1 . '</div>') :
		'') .
'

		' .
(($t1 = strval(calculer_url($Pile[$SP]['url_site'],'','url', $connect)))!=='' ?
		((	'<p class="hyperlien">' .
	_T('public|spip|ecrire:voir_en_ligne') .
	' : <a href="') . $t1 . (	'" class="spip_out">' .
	interdire_scripts(((($a = typo(supprimer_numero(calculer_url($Pile[$SP]['url_site'],$Pile[$SP]['nom_site'], 'titre', $connect, false)), "TYPO", $connect, $Pile[0])) OR (is_string($a) AND strlen($a))) ? $a : couper(calculer_url($Pile[$SP]['url_site'],'','url', $connect),'80'))) .
	'</a></p>')) :
		'') .
'
	</div>

	<footer>
		' .
(($t1 = strval(interdire_scripts(adaptive_images(propre($Pile[$SP]['ps'], $connect, $Pile[0])))))!=='' ?
		((	'<div class="ps surlignable"><h2>' .
	_T('public|spip|ecrire:info_ps') .
	'</h2>') . $t1 . '</div>') :
		'') .
'
		' .
(($t1 = strval(interdire_scripts(calculer_notes())))!=='' ?
		((	'<div class="notes"><h2>' .
	_T('public|spip|ecrire:info_notes') .
	'</h2>') . $t1 . '</div>') :
		'') .
'
	</footer>

	<aside>
		
		' .
recuperer_fond( 'inclure/documents' , array('id_article' => $Pile[$SP]['id_article'] ), array('compil'=>array('plugins/auto/spipr_dist/v2.2.6/content/article.html','html_b5ad0cd23e7c931bbcf8f56c3ecb9c2b','_content',14,$GLOBALS['spip_lang'])), _request('connect')) .
'

		' .
((quete_petitions($Pile[$SP]['id_article'],'articles','_content','', $Cache))  ?
		(' ' . 
'<'.'?php echo recuperer_fond( ' . argumenter_squelette('inclure/petition') . ', array_merge('.var_export($Pile[0],1).',array(\'id_article\' => ' . argumenter_squelette($Pile[$SP]['id_article']) . ',
	\'lang\' => ' . argumenter_squelette($GLOBALS["spip_lang"]) . ')), array("compil"=>array(\'plugins/auto/spipr_dist/v2.2.6/content/article.html\',\'html_b5ad0cd23e7c931bbcf8f56c3ecb9c2b\',\'\',15,$GLOBALS[\'spip_lang\']),\'ajax\' => ($v=( ' . argumenter_squelette(@$Pile[0]['ajax']) . '))?$v:true), _request("connect"));
?'.'>') :
		'') .
'

		
		' .

'<'.'?php echo recuperer_fond( ' . argumenter_squelette('inclure/forum') . ', array(\'id_article\' => ' . argumenter_squelette($Pile[$SP]['id_article']) . ',
	\'repondre_url\' => ' . argumenter_squelette(filtre_url_reponse_forum(spip_htmlspecialchars(
		// refus des forums ?
		(quete_accepter_forum($Pile[$SP]['id_article'])=="non" OR
		($GLOBALS["meta"]["forums_publics"] == "non"
		AND quete_accepter_forum($Pile[$SP]['id_article']) == ""))
		? "" : // sinon:
		(calcul_parametres_forum($Pile[0],null,'articles',$Pile[$SP]['id_article']).
	(($lien = (_request("retour") ? _request("retour") : str_replace("&amp;", "&", ''))) ? "&retour=".rawurlencode($lien) : ""))))) . ',
	\'repondre_titre\' => ' . argumenter_squelette(_T('public|spip|ecrire:repondre_article')) . ',
	\'lang\' => ' . argumenter_squelette($GLOBALS["spip_lang"]) . '), array("compil"=>array(\'plugins/auto/spipr_dist/v2.2.6/content/article.html\',\'html_b5ad0cd23e7c931bbcf8f56c3ecb9c2b\',\'\',24,$GLOBALS[\'spip_lang\'])), _request("connect"));
?'.'>
	</aside>

</article>
');
		lang_select();
	}
	lang_select();
	$iter->free();
	}
	if (defined("_BOUCLE_PROFILER")
	AND 1000*($timer = (time()+(float)microtime())-$timer) > _BOUCLE_PROFILER)
		spip_log(intval(1000*$timer)."ms BOUCLE_content @ plugins/auto/spipr_dist/v2.2.6/content/article.html","profiler"._LOG_AVERTISSEMENT);
	return $t0;
}

//
// Fonction principale du squelette plugins/auto/spipr_dist/v2.2.6/content/article.html
// Temps de compilation total: 1.888 ms
//

function html_b5ad0cd23e7c931bbcf8f56c3ecb9c2b($Cache, $Pile, $doublons = array(), $Numrows = array(), $SP = 0) {

	if (isset($Pile[0]["doublons"]) AND is_array($Pile[0]["doublons"]))
		$doublons = nettoyer_env_doublons($Pile[0]["doublons"]);

	$connect = '';
	$page = BOUCLE_contenthtml_b5ad0cd23e7c931bbcf8f56c3ecb9c2b($Cache, $Pile, $doublons, $Numrows, $SP);

	return analyse_resultat_skel('html_b5ad0cd23e7c931bbcf8f56c3ecb9c2b', $Cache, $page, 'plugins/auto/spipr_dist/v2.2.6/content/article.html');
}
?>