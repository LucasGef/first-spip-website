<?php

/*
 * Squelette : ../plugins/auto/menus/v1.7.26/prive/objets/liste/menus.html
 * Date :      Mon, 08 Jun 2020 15:55:26 GMT
 * Compile :   Wed, 17 Jun 2020 09:54:15 GMT
 * Boucles :   _liste_menus
 */ 

function BOUCLE_liste_menushtml_b314c9a58ed1e35cc73d803cbee96908(&$Cache, &$Pile, &$doublons, &$Numrows, $SP) {

	static $command = array();
	static $connect;
	$command['connect'] = $connect = '';
	$in = array();
	if (!(is_array($a = (@$Pile[0]['id_menu']))))
		$in[]= $a;
	else $in = array_merge($in, $a);
	$in1 = array();
	if (!(is_array($a = (@$Pile[0]['id_menus_entree']))))
		$in1[]= $a;
	else $in1 = array_merge($in1, $a);
	$in2 = array();
	if (!(is_array($a = (@$Pile[0]['identifiant']))))
		$in2[]= $a;
	else $in2 = array_merge($in2, $a);
	// RECHERCHE
	if (!strlen((isset($Pile[0]["recherche"])?$Pile[0]["recherche"]:(isset($GLOBALS["recherche"])?$GLOBALS["recherche"]:"")))){
		list($rech_select, $rech_where) = array("0 as points","");
	} else
	{
		$prepare_recherche = charger_fonction('prepare_recherche', 'inc');
		list($rech_select, $rech_where) = $prepare_recherche((isset($Pile[0]["recherche"])?$Pile[0]["recherche"]:(isset($GLOBALS["recherche"])?$GLOBALS["recherche"]:"")), "menus", "?","",array (
  'criteres' => 
  array (
    'id_menu' => true,
    'id_menus_entree' => true,
    'identifiant' => true,
  ),
),"id_menu");
	}
	
	$senstri = '';
	$tri = (($t=(isset($Pile[0]['tri'.'_liste_menus']))?$Pile[0]['tri'.'_liste_menus']:((strncmp('_liste_menus','session',7)==0 AND session_get('tri'.'_liste_menus'))?session_get('tri'.'_liste_menus'):interdire_scripts(entites_html(sinon(table_valeur(@$Pile[0], (string)'par', null), 'multi titre'),true))))?tri_protege_champ($t):'');
	if ($tri){
		$senstri = ((intval($t=(isset($Pile[0]['sens'.'_liste_menus']))?$Pile[0]['sens'.'_liste_menus']:((strncmp('_liste_menus','session',7)==0 AND session_get('sens'.'_liste_menus'))?session_get('sens'.'_liste_menus'):(is_array($s=table_valeur($Pile["vars"], (string)'defaut_tri', null))?(isset($s[$st=(($t=(isset($Pile[0]['tri'.'_liste_menus']))?$Pile[0]['tri'.'_liste_menus']:((strncmp('_liste_menus','session',7)==0 AND session_get('tri'.'_liste_menus'))?session_get('tri'.'_liste_menus'):interdire_scripts(entites_html(sinon(table_valeur(@$Pile[0], (string)'par', null), 'multi titre'),true))))?tri_protege_champ($t):'')])?$s[$st]:reset($s)):$s)))==-1 OR $t=='inverse')?-1:1);
		$senstri = ($senstri<0)?' DESC':'';
	};
	
	$command['pagination'] = array((isset($Pile[0]['debutmenl']) ? $Pile[0]['debutmenl'] : null), (($a = intval(interdire_scripts(entites_html(sinon(table_valeur(@$Pile[0], (string)'nb', null), '10'),true)))) ? $a : 10));
	if (!isset($command['table'])) {
		$command['table'] = 'menus';
		$command['id'] = '_liste_menus';
		$command['from'] = array('menus' => 'spip_menus','resultats' => 'spip_resultats');
		$command['type'] = array();
		$command['groupby'] = array();
		$command['join'] = array('resultats' => array('menus','id','id_menu'));
		$command['limit'] = '';
		$command['having'] = 
			array();
	}
	$command['select'] = array("menus.id_menu",
		"$rech_select",
		"".tri_champ_select($tri)."",
		"menus.titre",
		"menus.identifiant");
	$command['orderby'] = array(tri_champ_order($tri,$command['from']).$senstri);
	$command['where'] = 
			array((!(is_array(@$Pile[0]['id_menu'])?count(@$Pile[0]['id_menu']):strlen(@$Pile[0]['id_menu'])) ? '' : ((is_array(@$Pile[0]['id_menu'])) ? sql_in('menus.id_menu',sql_quote($in)) : 
			array('=', 'menus.id_menu', sql_quote(@$Pile[0]['id_menu'], '','bigint(21) NOT NULL AUTO_INCREMENT')))), (!(is_array(@$Pile[0]['id_menus_entree'])?count(@$Pile[0]['id_menus_entree']):strlen(@$Pile[0]['id_menus_entree'])) ? '' : ((is_array(@$Pile[0]['id_menus_entree'])) ? sql_in('menus.id_menus_entree',sql_quote($in1)) : 
			array('=', 'menus.id_menus_entree', sql_quote(@$Pile[0]['id_menus_entree'], '','bigint(21) NOT NULL DEFAULT \'0\'')))), (!(is_array(@$Pile[0]['identifiant'])?count(@$Pile[0]['identifiant']):strlen(@$Pile[0]['identifiant'])) ? '' : ((is_array(@$Pile[0]['identifiant'])) ? sql_in('menus.identifiant',sql_quote($in2)) : 
			array('=', 'menus.identifiant', sql_quote(@$Pile[0]['identifiant'], '','varchar(255) NOT NULL DEFAULT \'\'')))), $rech_where?$rech_where:'', ((@$Pile[0]["where"]) ? (@$Pile[0]["where"]) : ''));
	if (defined("_BOUCLE_PROFILER")) $timer = time()+(float)microtime();
	$t0 = "";
	// REQUETE
	$iter = IterFactory::create(
		"SQL",
		$command,
		array('../plugins/auto/menus/v1.7.26/prive/objets/liste/menus.html','html_b314c9a58ed1e35cc73d803cbee96908','_liste_menus',8,$GLOBALS['spip_lang'])
	);
	if (!$iter->err()) {
	
	// COMPTEUR
	$Numrows['_liste_menus']['compteur_boucle'] = 0;
	$Numrows['_liste_menus']['total'] = @intval($iter->count());
	$debut_boucle = isset($Pile[0]['debutmenl']) ? $Pile[0]['debutmenl'] : _request('debutmenl');
	if(substr($debut_boucle,0,1)=='@'){
		$debut_boucle = $Pile[0]['debutmenl'] = quete_debut_pagination('id_menu',$Pile[0]['@id_menu'] = substr($debut_boucle,1),(($a = intval(interdire_scripts(entites_html(sinon(table_valeur(@$Pile[0], (string)'nb', null), '10'),true)))) ? $a : 10),$iter);
		$iter->seek(0);
	}
	$debut_boucle = intval($debut_boucle);
	$debut_boucle = (($tout=($debut_boucle == -1))?0:($debut_boucle));
	$debut_boucle = max(0,min($debut_boucle,floor(($Numrows['_liste_menus']['total']-1)/((($a = intval(interdire_scripts(entites_html(sinon(table_valeur(@$Pile[0], (string)'nb', null), '10'),true)))) ? $a : 10)))*((($a = intval(interdire_scripts(entites_html(sinon(table_valeur(@$Pile[0], (string)'nb', null), '10'),true)))) ? $a : 10))));
	$debut_boucle = intval($debut_boucle);
	$fin_boucle = min(($tout ? $Numrows['_liste_menus']['total'] : $debut_boucle+(($a = intval(interdire_scripts(entites_html(sinon(table_valeur(@$Pile[0], (string)'nb', null), '10'),true)))) ? $a : 10) - 1), $Numrows['_liste_menus']['total'] - 1);
	$Numrows['_liste_menus']['grand_total'] = $Numrows['_liste_menus']['total'];
	$Numrows['_liste_menus']["total"] = max(0,$fin_boucle - $debut_boucle + 1);
	if ($debut_boucle>0 AND $debut_boucle < $Numrows['_liste_menus']['grand_total'] AND $iter->seek($debut_boucle,'continue'))
		$Numrows['_liste_menus']['compteur_boucle'] = $debut_boucle;
	
	
	$l1 = _T('menus:editer_menus_editer');$SP++;
	// RESULTATS
	while ($Pile[$SP]=$iter->fetch()) {

		$Numrows['_liste_menus']['compteur_boucle']++;
		if ($Numrows['_liste_menus']['compteur_boucle'] <= $debut_boucle) continue;
		if ($Numrows['_liste_menus']['compteur_boucle']-1 > $fin_boucle) break;
		$t0 .= (
'
		<tr class="' .
alterner($Numrows['_liste_menus']['compteur_boucle'],'row_odd','row_even') .
(($t1 = strval(unique((calcul_exposer($Pile[$SP]['id_menu'], 'id_menu', $Pile[0], '', 'id_menu', '') ? 'on' : ''))))!=='' ?
		(' ' . $t1) :
		'') .
(($t1 = strval(unique(((filtre_initiale(extraire_multi($Pile[$SP]['titre'])) == interdire_scripts(entites_html(table_valeur(@$Pile[0], (string)'i', null),true))) ? 'on':''))))!=='' ?
		(' ' . $t1) :
		'') .
((($Pile[$SP]['id_menu'] == interdire_scripts(entites_html(table_valeur(@$Pile[0], (string)'id_lien_ajoute', null),true))))  ?
		(' ' . 'append') :
		'') .
'">
			<td class="titre principal">' .
filtrer('image_graver',filtrer('image_reduire',
((!is_array($l = quete_logo('id_menu', 'ON', $Pile[$SP]['id_menu'],'', 0))) ? '':
 ("<img class=\"spip_logo spip_logos\" alt=\"\" src=\"$l[0]\"" . $l[2] .  ($l[1] ? " onmouseover=\"this.src='$l[1]'\" onmouseout=\"this.src='$l[0]'\"" : "") . ' />')),'20','20')) .
'<a href="' .
generer_url_ecrire('menu_edit',(	'id_menu=' .
	$Pile[$SP]['id_menu'])) .
'">' .
interdire_scripts(supprimer_numero(typo($Pile[$SP]['titre']), "TYPO", $connect, $Pile[0])) .
'</a></td>
			<td class="identifiant secondaire">' .
interdire_scripts($Pile[$SP]['identifiant']) .
'</td>
			<td class="action">
				' .
filtre_icone_horizontale_dist(generer_url_ecrire('menu_edit',(	'id_menu=' .
	$Pile[$SP]['id_menu'])),$l1,'menu-24','edit') .
'
			</td>
			<td class="id"><a href="' .
generer_url_ecrire('menu_edit',(	'id_menu=' .
	$Pile[$SP]['id_menu'])) .
'">' .
$Pile[$SP]['id_menu'] .
'</a></td>
		</tr>
	');
	}
	$iter->free();
	}
	if (defined("_BOUCLE_PROFILER")
	AND 1000*($timer = (time()+(float)microtime())-$timer) > _BOUCLE_PROFILER)
		spip_log(intval(1000*$timer)."ms BOUCLE_liste_menus @ ../plugins/auto/menus/v1.7.26/prive/objets/liste/menus.html","profiler"._LOG_AVERTISSEMENT);
	return $t0;
}

//
// Fonction principale du squelette ../plugins/auto/menus/v1.7.26/prive/objets/liste/menus.html
// Temps de compilation total: 7.657 ms
//

function html_b314c9a58ed1e35cc73d803cbee96908($Cache, $Pile, $doublons = array(), $Numrows = array(), $SP = 0) {

	if (isset($Pile[0]["doublons"]) AND is_array($Pile[0]["doublons"]))
		$doublons = nettoyer_env_doublons($Pile[0]["doublons"]);

	$connect = '';
	$page = (
(($t1 = strval(vide($Pile['vars'][$_zzz=(string)'defaut_tri'] = array('multi titre' => '1', 'titre' => '1', 'identifiant' => '1', 'id_menu' => '1', 'points' => '-1
'))))!=='' ?
		($t1 . '
') :
		'') .
(($t1 = BOUCLE_liste_menushtml_b314c9a58ed1e35cc73d803cbee96908($Cache, $Pile, $doublons, $Numrows, $SP))!=='' ?
		((	'
' .
		filtre_pagination_dist($Numrows["_liste_menus"]["grand_total"],
 		'menl',
		isset($Pile[0]['debutmenl'])?$Pile[0]['debutmenl']:intval(_request('debutmenl')),
		(($a = intval(interdire_scripts(entites_html(sinon(table_valeur(@$Pile[0], (string)'nb', null), '10'),true)))) ? $a : 10), false, '', '', array()) .
		'
<div class="liste-objets menus">
<table class="spip liste">
' .
		(($t3 = strval(interdire_scripts(sinon(table_valeur(@$Pile[0], (string)'titre', null), singulier_ou_pluriel((isset($Numrows['_liste_menus']['grand_total'])
			? $Numrows['_liste_menus']['grand_total'] : $Numrows['_liste_menus']['total']),'menus:info_1_menu','menus:info_nb_menus')))))!=='' ?
				('<caption><strong class="caption">' . $t3 . '</strong></caption>') :
				'') .
		'
	<thead>
		<tr class="first_row">
			<th class="titre" scope="col">' .
		lien_ou_expose(parametre_url(parametre_url(self(),(($s=in_array('multi titre',array('>','<')))?'sens':'tri').'_liste_menus',$s?(strpos('< >','multi titre')-1):'multi titre'),'var_memotri',strncmp('_liste_menus','session',7)==0?(($s=in_array('multi titre',array('>','<')))?'sens':'tri').'_liste_menus':''),_T('menus:formulaire_titre'),$s?(((intval($t=(isset($Pile[0]['sens'.'_liste_menus']))?$Pile[0]['sens'.'_liste_menus']:((strncmp('_liste_menus','session',7)==0 AND session_get('sens'.'_liste_menus'))?session_get('sens'.'_liste_menus'):(is_array($s=table_valeur($Pile["vars"], (string)'defaut_tri', null))?(isset($s[$st=(($t=(isset($Pile[0]['tri'.'_liste_menus']))?$Pile[0]['tri'.'_liste_menus']:((strncmp('_liste_menus','session',7)==0 AND session_get('tri'.'_liste_menus'))?session_get('tri'.'_liste_menus'):interdire_scripts(entites_html(sinon(table_valeur(@$Pile[0], (string)'par', null), 'multi titre'),true))))?tri_protege_champ($t):'')])?$s[$st]:reset($s)):$s)))==-1 OR $t=='inverse')?-1:1)==(strpos('< >','multi titre')-1)):((($t=(isset($Pile[0]['tri'.'_liste_menus']))?$Pile[0]['tri'.'_liste_menus']:((strncmp('_liste_menus','session',7)==0 AND session_get('tri'.'_liste_menus'))?session_get('tri'.'_liste_menus'):interdire_scripts(entites_html(sinon(table_valeur(@$Pile[0], (string)'par', null), 'multi titre'),true))))?tri_protege_champ($t):'')=='multi titre'),'ajax') .
		'</th>
			<th class="titre" scope="col">' .
		lien_ou_expose(parametre_url(parametre_url(self(),(($s=in_array('identifiant',array('>','<')))?'sens':'tri').'_liste_menus',$s?(strpos('< >','identifiant')-1):'identifiant'),'var_memotri',strncmp('_liste_menus','session',7)==0?(($s=in_array('identifiant',array('>','<')))?'sens':'tri').'_liste_menus':''),_T('menus:formulaire_identifiant'),$s?(((intval($t=(isset($Pile[0]['sens'.'_liste_menus']))?$Pile[0]['sens'.'_liste_menus']:((strncmp('_liste_menus','session',7)==0 AND session_get('sens'.'_liste_menus'))?session_get('sens'.'_liste_menus'):(is_array($s=table_valeur($Pile["vars"], (string)'defaut_tri', null))?(isset($s[$st=(($t=(isset($Pile[0]['tri'.'_liste_menus']))?$Pile[0]['tri'.'_liste_menus']:((strncmp('_liste_menus','session',7)==0 AND session_get('tri'.'_liste_menus'))?session_get('tri'.'_liste_menus'):interdire_scripts(entites_html(sinon(table_valeur(@$Pile[0], (string)'par', null), 'multi titre'),true))))?tri_protege_champ($t):'')])?$s[$st]:reset($s)):$s)))==-1 OR $t=='inverse')?-1:1)==(strpos('< >','identifiant')-1)):((($t=(isset($Pile[0]['tri'.'_liste_menus']))?$Pile[0]['tri'.'_liste_menus']:((strncmp('_liste_menus','session',7)==0 AND session_get('tri'.'_liste_menus'))?session_get('tri'.'_liste_menus'):interdire_scripts(entites_html(sinon(table_valeur(@$Pile[0], (string)'par', null), 'multi titre'),true))))?tri_protege_champ($t):'')=='identifiant'),'ajax') .
		'</th>
			<th class="action" scope="col"></th>
			<th class="id" scope="col">' .
		lien_ou_expose(parametre_url(parametre_url(self(),(($s=in_array('id_menu',array('>','<')))?'sens':'tri').'_liste_menus',$s?(strpos('< >','id_menu')-1):'id_menu'),'var_memotri',strncmp('_liste_menus','session',7)==0?(($s=in_array('id_menu',array('>','<')))?'sens':'tri').'_liste_menus':''),'#',$s?(((intval($t=(isset($Pile[0]['sens'.'_liste_menus']))?$Pile[0]['sens'.'_liste_menus']:((strncmp('_liste_menus','session',7)==0 AND session_get('sens'.'_liste_menus'))?session_get('sens'.'_liste_menus'):(is_array($s=table_valeur($Pile["vars"], (string)'defaut_tri', null))?(isset($s[$st=(($t=(isset($Pile[0]['tri'.'_liste_menus']))?$Pile[0]['tri'.'_liste_menus']:((strncmp('_liste_menus','session',7)==0 AND session_get('tri'.'_liste_menus'))?session_get('tri'.'_liste_menus'):interdire_scripts(entites_html(sinon(table_valeur(@$Pile[0], (string)'par', null), 'multi titre'),true))))?tri_protege_champ($t):'')])?$s[$st]:reset($s)):$s)))==-1 OR $t=='inverse')?-1:1)==(strpos('< >','id_menu')-1)):((($t=(isset($Pile[0]['tri'.'_liste_menus']))?$Pile[0]['tri'.'_liste_menus']:((strncmp('_liste_menus','session',7)==0 AND session_get('tri'.'_liste_menus'))?session_get('tri'.'_liste_menus'):interdire_scripts(entites_html(sinon(table_valeur(@$Pile[0], (string)'par', null), 'multi titre'),true))))?tri_protege_champ($t):'')=='id_menu'),'ajax') .
		'</th>
		</tr>
	</thead>
	<tbody>
	') . $t1 . (	'
	</tbody>
</table>
' .
		(($t3 = strval(filtre_pagination_dist($Numrows["_liste_menus"]["grand_total"],
 		'menl',
		isset($Pile[0]['debutmenl'])?$Pile[0]['debutmenl']:intval(_request('debutmenl')),
		(($a = intval(interdire_scripts(entites_html(sinon(table_valeur(@$Pile[0], (string)'nb', null), '10'),true)))) ? $a : 10), true, 'prive', '', array())))!=='' ?
				('<p class="pagination">' . $t3 . '</p>') :
				'') .
		'
</div>
')) :
		((($t2 = strval(interdire_scripts(sinon(table_valeur(@$Pile[0], (string)'sinon', null), ''))))!=='' ?
			('
<div class="liste-objets menus caption-wrap"><strong class="caption">' . $t2 . '</strong></div>
') :
			''))) .
'
');

	return analyse_resultat_skel('html_b314c9a58ed1e35cc73d803cbee96908', $Cache, $page, '../plugins/auto/menus/v1.7.26/prive/objets/liste/menus.html');
}
?>