<?php

/*
 * Squelette : prive/formulaires/inc-logo_auteur.html
 * Date :      Tue, 16 Jun 2020 14:01:34 GMT
 * Compile :   Wed, 17 Jun 2020 09:06:10 GMT
 * Boucles :   
 */ 
//
// Fonction principale du squelette prive/formulaires/inc-logo_auteur.html
// Temps de compilation total: 1.382 ms
//

function html_1cef9c8fc07d954583485e7b9e9b3d0e($Cache, $Pile, $doublons = array(), $Numrows = array(), $SP = 0) {

	if (isset($Pile[0]["doublons"]) AND is_array($Pile[0]["doublons"]))
		$doublons = nettoyer_env_doublons($Pile[0]["doublons"]);

	$connect = '';
	$page = (
'<'.'?php header(' . _q((	'Content-type:text/html;charset=' .
	interdire_scripts(entites_html(table_valeur(@$Pile[0], (string)'charset', null),true)))) . '); ?'.'>' .
filtrer('image_graver',filtrer('image_recadre',filtrer('image_passe_partout',
((!is_array($l = quete_logo('id_auteur', 'ON', @$Pile[0]['id_auteur'],'', 0))) ? '':
 ("<img class=\"spip_logo spip_logos\" alt=\"\" src=\"$l[0]\"" . $l[2] .  ($l[1] ? " onmouseover=\"this.src='$l[1]'\" onmouseout=\"this.src='$l[0]'\"" : "") . ' />')),'48','48'),'48','48')) .
'
');

	return analyse_resultat_skel('html_1cef9c8fc07d954583485e7b9e9b3d0e', $Cache, $page, 'prive/formulaires/inc-logo_auteur.html');
}
?>