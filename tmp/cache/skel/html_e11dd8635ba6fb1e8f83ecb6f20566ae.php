<?php

/*
 * Squelette : ../plugins/auto/pages/v1.5.2/prive/objets/editer/identifiant_page.html
 * Date :      Tue, 02 Jun 2020 20:28:52 GMT
 * Compile :   Wed, 17 Jun 2020 09:52:29 GMT
 * Boucles :   
 */ 
//
// Fonction principale du squelette ../plugins/auto/pages/v1.5.2/prive/objets/editer/identifiant_page.html
// Temps de compilation total: 0.918 ms
//

function html_e11dd8635ba6fb1e8f83ecb6f20566ae($Cache, $Pile, $doublons = array(), $Numrows = array(), $SP = 0) {

	if (isset($Pile[0]["doublons"]) AND is_array($Pile[0]["doublons"]))
		$doublons = nettoyer_env_doublons($Pile[0]["doublons"]);

	$connect = '';
	$page = (
'<div class="ajax">
	' .
executer_balise_dynamique('FORMULAIRE_EDITER_IDENTIFIANT_PAGE',
	array(interdire_scripts(entites_html(table_valeur(@$Pile[0], (string)'id_article', null),true))),
	array('../plugins/auto/pages/v1.5.2/prive/objets/editer/identifiant_page.html','html_e11dd8635ba6fb1e8f83ecb6f20566ae','',2,$GLOBALS['spip_lang'])) .
'</div>
');

	return analyse_resultat_skel('html_e11dd8635ba6fb1e8f83ecb6f20566ae', $Cache, $page, '../plugins/auto/pages/v1.5.2/prive/objets/editer/identifiant_page.html');
}
?>