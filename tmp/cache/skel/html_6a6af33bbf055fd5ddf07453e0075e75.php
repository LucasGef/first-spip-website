<?php

/*
 * Squelette : plugins/auto/spipr_dist/v2.2.6/content/sommaire.html
 * Date :      Mon, 30 Mar 2020 09:25:24 GMT
 * Compile :   Wed, 17 Jun 2020 07:16:57 GMT
 * Boucles :   _art
 */ 

function BOUCLE_arthtml_6a6af33bbf055fd5ddf07453e0075e75(&$Cache, &$Pile, &$doublons, &$Numrows, $SP) {

	static $command = array();
	static $connect;
	$command['connect'] = $connect = '';
	if (!isset($command['table'])) {
		$command['table'] = 'articles';
		$command['id'] = '_art';
		$command['from'] = array('articles' => 'spip_articles');
		$command['type'] = array();
		$command['groupby'] = array();
		$command['select'] = array("rand() AS hasard",
		"articles.id_article",
		"articles.lang",
		"articles.titre");
		$command['orderby'] = array('hasard');
		$command['where'] = 
			array(
quete_condition_statut('articles.statut','publie,prop,prepa/auteur','publie',''), 
quete_condition_postdates('articles.date',''));
		$command['join'] = array();
		$command['limit'] = '0,1';
		$command['having'] = 
			array();
	}
	if (defined("_BOUCLE_PROFILER")) $timer = time()+(float)microtime();
	$t0 = "";
	// REQUETE
	$iter = IterFactory::create(
		"SQL",
		$command,
		array('plugins/auto/spipr_dist/v2.2.6/content/sommaire.html','html_6a6af33bbf055fd5ddf07453e0075e75','_art',7,$GLOBALS['spip_lang'])
	);
	if (!$iter->err()) {
	lang_select($GLOBALS['spip_lang']);
	$SP++;
	// RESULTATS
	while ($Pile[$SP]=$iter->fetch()) {

		lang_select_public($Pile[$SP]['lang'], '', $Pile[$SP]['titre']);
		$t0 .= (
'
		' .

'<'.'?php echo recuperer_fond( ' . argumenter_squelette('inclure/article-hero') . ', array_merge('.var_export($Pile[0],1).',array(\'id_article\' => ' . argumenter_squelette($Pile[$SP]['id_article']) . ',
	\'lang\' => ' . argumenter_squelette($GLOBALS["spip_lang"]) . ')), array("compil"=>array(\'plugins/auto/spipr_dist/v2.2.6/content/sommaire.html\',\'html_6a6af33bbf055fd5ddf07453e0075e75\',\'\',8,$GLOBALS[\'spip_lang\']),\'ajax\' => ($v=( ' . argumenter_squelette(@$Pile[0]['ajax']) . '))?$v:true), _request("connect"));
?'.'>
		' .
vide($Pile['vars'][$_zzz=(string)'exclus'] = array($Pile[$SP]['id_article'])));
		lang_select();
	}
	lang_select();
	$iter->free();
	}
	if (defined("_BOUCLE_PROFILER")
	AND 1000*($timer = (time()+(float)microtime())-$timer) > _BOUCLE_PROFILER)
		spip_log(intval(1000*$timer)."ms BOUCLE_art @ plugins/auto/spipr_dist/v2.2.6/content/sommaire.html","profiler"._LOG_AVERTISSEMENT);
	return $t0;
}

//
// Fonction principale du squelette plugins/auto/spipr_dist/v2.2.6/content/sommaire.html
// Temps de compilation total: 0.674 ms
//

function html_6a6af33bbf055fd5ddf07453e0075e75($Cache, $Pile, $doublons = array(), $Numrows = array(), $SP = 0) {

	if (isset($Pile[0]["doublons"]) AND is_array($Pile[0]["doublons"]))
		$doublons = nettoyer_env_doublons($Pile[0]["doublons"]);

	$connect = '';
	$page = (
'<section>
	' .
(($t1 = strval(interdire_scripts(((entites_html(sinon(table_valeur(@$Pile[0], (string)'cartouche', null), '1'),true)) ?' ' :''))))!=='' ?
		($t1 . 
'<'.'?php echo recuperer_fond( ' . argumenter_squelette((	'cartouche/' .
		interdire_scripts(entites_html(table_valeur(@$Pile[0], (string)'type-page', null),true)))) . ', array_merge('.var_export($Pile[0],1).',array(\'lang\' => ' . argumenter_squelette($GLOBALS["spip_lang"]) . ')), array("compil"=>array(\'plugins/auto/spipr_dist/v2.2.6/content/sommaire.html\',\'html_6a6af33bbf055fd5ddf07453e0075e75\',\'\',2,$GLOBALS[\'spip_lang\'])), _request("connect"));
?'.'>') :
		'') .
'

	<div class="main">
		' .
(($t1 = strval(interdire_scripts(propre($GLOBALS['meta']['descriptif_site'], $connect, $Pile[0]))))!=='' ?
		('<div id="descriptif_site_spip">' . $t1 . '</div>') :
		'') .
'
		' .
vide($Pile['vars'][$_zzz=(string)'exclus'] = array()) .
BOUCLE_arthtml_6a6af33bbf055fd5ddf07453e0075e75($Cache, $Pile, $doublons, $Numrows, $SP) .
'

		<div class="liste long articles">
		' .

'<'.'?php echo recuperer_fond( ' . argumenter_squelette('liste/articles-resume') . ', array_merge('.var_export($Pile[0],1).',array(\'articles_exclus\' => ' . argumenter_squelette(table_valeur($Pile["vars"], (string)'exclus', null)) . ',
	\'lang\' => ' . argumenter_squelette($GLOBALS["spip_lang"]) . ')), array("compil"=>array(\'plugins/auto/spipr_dist/v2.2.6/content/sommaire.html\',\'html_6a6af33bbf055fd5ddf07453e0075e75\',\'\',13,$GLOBALS[\'spip_lang\']),\'ajax\' => ($v=( ' . argumenter_squelette(@$Pile[0]['ajax']) . '))?$v:true), _request("connect"));
?'.'>
		</div>
	</div>

</section>
');

	return analyse_resultat_skel('html_6a6af33bbf055fd5ddf07453e0075e75', $Cache, $page, 'plugins/auto/spipr_dist/v2.2.6/content/sommaire.html');
}
?>