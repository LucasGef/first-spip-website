<?php

/*
 * Squelette : plugins/auto/spipr_dist/v2.2.6/head/sommaire.html
 * Date :      Mon, 30 Mar 2020 09:25:24 GMT
 * Compile :   Wed, 17 Jun 2020 07:16:54 GMT
 * Boucles :   
 */ 
//
// Fonction principale du squelette plugins/auto/spipr_dist/v2.2.6/head/sommaire.html
// Temps de compilation total: 1.232 ms
//

function html_d5ad028d72e396c8110ff31eaad6d885($Cache, $Pile, $doublons = array(), $Numrows = array(), $SP = 0) {

	if (isset($Pile[0]["doublons"]) AND is_array($Pile[0]["doublons"]))
		$doublons = nettoyer_env_doublons($Pile[0]["doublons"]);

	$connect = '';
	$page = (
'
<title>' .
interdire_scripts(textebrut(typo(typo($GLOBALS['meta']['nom_site'], "TYPO", $connect, $Pile[0])))) .
(($t1 = strval(interdire_scripts(textebrut(typo(typo($GLOBALS['meta']['slogan_site'], "TYPO", $connect, $Pile[0]))))))!=='' ?
		(' - ' . $t1) :
		'') .
'</title>
' .
(($t1 = strval(interdire_scripts(attribut_html(textebrut(couper(propre($GLOBALS['meta']['descriptif_site'], $connect, $Pile[0]),'150'))))))!=='' ?
		('<meta name="description" content="' . $t1 . '" />') :
		'') .
'
' .
(($t1 = strval(url_absolue_si(find_in_path('favicon.ico'))))!=='' ?
		('<link rel="icon" type="image/x-icon" href="' . $t1 . (	'" />
' .
	(($t2 = strval(url_absolue_si(find_in_path('favicon.ico'))))!=='' ?
			('<link rel="shortcut icon" type="image/x-icon" href="' . $t2 . '" />') :
			''))) :
		'') .
'
');

	return analyse_resultat_skel('html_d5ad028d72e396c8110ff31eaad6d885', $Cache, $page, 'plugins/auto/spipr_dist/v2.2.6/head/sommaire.html');
}
?>