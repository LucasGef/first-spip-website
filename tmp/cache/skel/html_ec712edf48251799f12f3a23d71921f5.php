<?php

/*
 * Squelette : plugins/auto/spipr_dist/v2.2.6/content/recherche.html
 * Date :      Mon, 30 Mar 2020 09:25:24 GMT
 * Compile :   Wed, 17 Jun 2020 07:36:22 GMT
 * Boucles :   
 */ 
//
// Fonction principale du squelette plugins/auto/spipr_dist/v2.2.6/content/recherche.html
// Temps de compilation total: 0.140 ms
//

function html_ec712edf48251799f12f3a23d71921f5($Cache, $Pile, $doublons = array(), $Numrows = array(), $SP = 0) {

	if (isset($Pile[0]["doublons"]) AND is_array($Pile[0]["doublons"]))
		$doublons = nettoyer_env_doublons($Pile[0]["doublons"]);

	$connect = '';
	$page = (
'<section>
	' .
(($t1 = strval(interdire_scripts(((entites_html(sinon(table_valeur(@$Pile[0], (string)'cartouche', null), '1'),true)) ?' ' :''))))!=='' ?
		($t1 . 
'<'.'?php echo recuperer_fond( ' . argumenter_squelette((	'cartouche/' .
		interdire_scripts(entites_html(table_valeur(@$Pile[0], (string)'type-page', null),true)))) . ', array_merge('.var_export($Pile[0],1).',array(\'lang\' => ' . argumenter_squelette($GLOBALS["spip_lang"]) . ')), array("compil"=>array(\'plugins/auto/spipr_dist/v2.2.6/content/recherche.html\',\'html_ec712edf48251799f12f3a23d71921f5\',\'\',2,$GLOBALS[\'spip_lang\'])), _request("connect"));
?'.'>') :
		'') .
'

	<div class="main">
		' .

'<'.'?php echo recuperer_fond( ' . argumenter_squelette('liste/articles-recherche') . ', array_merge('.var_export($Pile[0],1).',array(\'lang\' => ' . argumenter_squelette($GLOBALS["spip_lang"]) . ')), array("compil"=>array(\'plugins/auto/spipr_dist/v2.2.6/content/recherche.html\',\'html_ec712edf48251799f12f3a23d71921f5\',\'\',5,$GLOBALS[\'spip_lang\']),\'ajax\' => ($v=( ' . argumenter_squelette(@$Pile[0]['ajax']) . '))?$v:true), _request("connect"));
?'.'>
		' .

'<'.'?php echo recuperer_fond( ' . argumenter_squelette('liste/rubriques-recherche') . ', array_merge('.var_export($Pile[0],1).',array(\'lang\' => ' . argumenter_squelette($GLOBALS["spip_lang"]) . ')), array("compil"=>array(\'plugins/auto/spipr_dist/v2.2.6/content/recherche.html\',\'html_ec712edf48251799f12f3a23d71921f5\',\'\',6,$GLOBALS[\'spip_lang\']),\'ajax\' => ($v=( ' . argumenter_squelette(@$Pile[0]['ajax']) . '))?$v:true), _request("connect"));
?'.'>
	</div>
</section>
');

	return analyse_resultat_skel('html_ec712edf48251799f12f3a23d71921f5', $Cache, $page, 'plugins/auto/spipr_dist/v2.2.6/content/recherche.html');
}
?>