<?php

/*
 * Squelette : plugins/auto/spipr_dist/v2.2.6/inclure/head.html
 * Date :      Mon, 30 Mar 2020 09:25:24 GMT
 * Compile :   Wed, 17 Jun 2020 09:07:09 GMT
 * Boucles :   
 */ 
//
// Fonction principale du squelette plugins/auto/spipr_dist/v2.2.6/inclure/head.html
// Temps de compilation total: 0.490 ms
//

function html_4eb9bf759d0eeb3a7adb63c316cd8f65($Cache, $Pile, $doublons = array(), $Numrows = array(), $SP = 0) {

	if (isset($Pile[0]["doublons"]) AND is_array($Pile[0]["doublons"]))
		$doublons = nettoyer_env_doublons($Pile[0]["doublons"]);

	$connect = '';
	$page = (
'

<meta http-equiv="Content-Type" content="text/html; charset=' .
interdire_scripts($GLOBALS['meta']['charset']) .
'" />


<meta name="generator" content="SPIP' .
(($t1 = strval(spip_version()))!=='' ?
		(' ' . $t1) :
		'') .
'" />

<script type="text/javascript">
var error_on_ajaxform=\'' .
unicode_to_javascript(addslashes(html2unicode(_T('public|spip|ecrire:erreur_technique_ajaxform')))) .
'\';
</script>

' .
(($t1 = strval(interdire_scripts(generer_url_public('backend', ''))))!=='' ?
		((	'<link rel="alternate" type="application/rss+xml" title="' .
	_T('public|spip|ecrire:syndiquer_site') .
	'" href="') . $t1 . '" />') :
		'') .
'
<meta name="viewport" content="width=device-width, initial-scale=1.0">

' .
(($t1 = strval(filtre_prefixer_css_dist(timestamp(direction_css(scss_select_css('css/font.css'))))))!=='' ?
		('<link rel="stylesheet" href="' . $t1 . '" type="text/css" />') :
		'') .
'
' .
(($t1 = strval(filtre_prefixer_css_dist(timestamp(direction_css(scss_select_css('css/bootstrap.css'))))))!=='' ?
		('<link rel="stylesheet" href="' . $t1 . '" type="text/css" />') :
		'') .
'
' .
(($t1 = strval(timestamp(direction_css(scss_select_css('css/icons.css')))))!=='' ?
		('<link rel="stylesheet" href="' . $t1 . '" type="text/css" />') :
		'') .
'
' .
(($t1 = strval(timestamp(direction_css(scss_select_css('css/box.css')))))!=='' ?
		('<link rel="stylesheet" href="' . $t1 . '" type="text/css" />') :
		'') .
'
' .
(($t1 = strval(timestamp(direction_css(scss_select_css('css/box_skins.css')))))!=='' ?
		('<link rel="stylesheet" href="' . $t1 . '" type="text/css" />') :
		'') .
'

' .
(($t1 = strval(timestamp(direction_css(scss_select_css('css/spip.css')))))!=='' ?
		('<link rel="stylesheet" href="' . $t1 . '" type="text/css" />') :
		'') .
'
' .
(($t1 = strval(timestamp(direction_css(scss_select_css('css/spip.comment.css')))))!=='' ?
		('<link rel="stylesheet" href="' . $t1 . '" type="text/css" />') :
		'') .
'
' .
(($t1 = strval(timestamp(direction_css(scss_select_css('css/spip.list.css')))))!=='' ?
		('<link rel="stylesheet" href="' . $t1 . '" type="text/css" />') :
		'') .
'
' .
(($t1 = strval(timestamp(direction_css(scss_select_css('css/spip.petition.css')))))!=='' ?
		('<link rel="stylesheet" href="' . $t1 . '" type="text/css" />') :
		'') .
'
' .
(($t1 = strval(timestamp(direction_css(scss_select_css('css/spip.pagination.css')))))!=='' ?
		('<link rel="stylesheet" href="' . $t1 . '" type="text/css" />') :
		'') .
'
' .
(($t1 = strval(timestamp(direction_css(scss_select_css('css/spip.admin.css')))))!=='' ?
		('<link rel="stylesheet" href="' . $t1 . '" type="text/css" />') :
		'') .
'


' .
pipeline('insert_head_css','<!-- insert_head_css -->') .
'

' .
(($t1 = strval(filtre_prefixer_css_dist(timestamp(direction_css(scss_select_css('css/spipr_dist.css'))))))!=='' ?
		('<link rel="stylesheet" href="' . $t1 . '" type="text/css" />') :
		'') .
'



' .
(($t1 = strval(filtre_prefixer_css_dist(timestamp(direction_css(scss_select_css('css/theme.css'))))))!=='' ?
		('<link rel="stylesheet" href="' . $t1 . '" type="text/css" />') :
		'') .
'


' .
(($t1 = strval(timestamp(direction_css(scss_select_css('css/print.css')))))!=='' ?
		('<link rel="stylesheet" href="' . $t1 . '" type="text/css" media="print" />') :
		'') .
'

' .
((find_in_path('inc-theme-head.html'))  ?
		(' ' . (	'
' .
	recuperer_fond( 'inc-theme-head' , array_merge($Pile[0],array()), array('compil'=>array('plugins/auto/spipr_dist/v2.2.6/inclure/head.html','html_4eb9bf759d0eeb3a7adb63c316cd8f65','',40,$GLOBALS['spip_lang'])), _request('connect')))) :
		'') .
'


' .
(($t1 = strval(filtre_prefixer_css_dist(timestamp(direction_css(scss_select_css('css/perso.css'))))))!=='' ?
		('<link rel="stylesheet" href="' . $t1 . '" type="text/css" />') :
		'') .
'


' .
'<'.'?php header("X-Spip-Filtre: insert_head_css_conditionnel"); ?'.'>'. pipeline('insert_head','<!-- insert_head -->') .
'
' .
(($t1 = strval(timestamp(find_in_path('js/bootstrap-util.js'))))!=='' ?
		('<script type="text/javascript" src="' . $t1 . '"></script>') :
		'') .
'
' .
(($t1 = strval(timestamp(find_in_path('js/bootstrap-collapse.js'))))!=='' ?
		('<script type="text/javascript" src="' . $t1 . '"></script>') :
		'') .
'

');

	return analyse_resultat_skel('html_4eb9bf759d0eeb3a7adb63c316cd8f65', $Cache, $page, 'plugins/auto/spipr_dist/v2.2.6/inclure/head.html');
}
?>