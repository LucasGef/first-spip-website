<?php

/*
 * Squelette : plugins/auto/spipr_dist/v2.2.6/head/404.html
 * Date :      Mon, 30 Mar 2020 09:25:24 GMT
 * Compile :   Wed, 17 Jun 2020 08:56:36 GMT
 * Boucles :   
 */ 
//
// Fonction principale du squelette plugins/auto/spipr_dist/v2.2.6/head/404.html
// Temps de compilation total: 0.117 ms
//

function html_271b41df408bcd0e90e9c818664da186($Cache, $Pile, $doublons = array(), $Numrows = array(), $SP = 0) {

	if (isset($Pile[0]["doublons"]) AND is_array($Pile[0]["doublons"]))
		$doublons = nettoyer_env_doublons($Pile[0]["doublons"]);

	$connect = '';
	$page = (
'<title>' .
_T('public|spip|ecrire:pass_erreur') .
' 404 - ' .
interdire_scripts(textebrut(typo($GLOBALS['meta']['nom_site'], "TYPO", $connect, $Pile[0]))) .
'</title>
<meta name="robots" content="none" />

' .
(($t1 = strval(url_absolue_si(find_in_path('favicon.ico'))))!=='' ?
		('<link rel="icon" type="image/x-icon" href="' . $t1 . (	'" />
' .
	(($t2 = strval(url_absolue_si(find_in_path('favicon.ico'))))!=='' ?
			('<link rel="shortcut icon" type="image/x-icon" href="' . $t2 . '" />') :
			''))) :
		'') .
'
');

	return analyse_resultat_skel('html_271b41df408bcd0e90e9c818664da186', $Cache, $page, 'plugins/auto/spipr_dist/v2.2.6/head/404.html');
}
?>